'use strict';

var fs = require('fs');

function toArrayBuffer(buffer) {

  // 创建一个缓存对象，长度等于buffer.length
  var ab = new ArrayBuffer(buffer.length); 

  // 创建一个Uint8类型的数组对象。
  var view = new Uint8Array(ab);

  for (var i = 0; i < buffer.length; ++i) {
      view[i] = buffer[i];  // 把buffer的数据拷贝到ab缓存内。
  }
  return ab;  // 返回新的 ArrayBuffer对象。
 }

let getResData = function(name){
  let uuid = Editor.assetdb.urlToUuid("db://assets/resources/"+name)
  // Editor.log("123"+uuid)

  let path = Editor.assetdb.uuidToFspath(uuid)
  // Editor.log(path)

  let data = fs.readFileSync(path)
  
  return toArrayBuffer(data.buffer)
}

module.exports = {
  load () {
    // execute when package loaded
  },

  unload () {
    // execute when package unloaded
  },

  // register your ipc messages here
  messages: {
    'open' () {

      
      var keys = [
        'test.fui',
        'test@atlas0.png'
      ]

      var urls = [
          'fui_test/test.fui',
          'fui_test/test@atlas0.png'
      ]
      

      // let uuid = Editor.assetdb.urlToUuid("db://assets/resources/fui_test/test.fui")
      // // Editor.log("123"+uuid)

      // let path = Editor.assetdb.uuidToFspath(uuid)
      // // Editor.log(path)

      // let data = fs.readFileSync(path,'binary')

      let a = Editor.assetdb.urlToUuid("db://assets/resources/fui_test/test.fui")
      Editor.log(a)
      

      let d = {}
      for(let i = 0;i<urls.length;i++){
        let url = urls[i]
        let data = getResData(url)
        d[keys[i]]=data
      }

      // Editor.log(fairygui)

      // open entry panel registered in package.json
      // Editor.Panel.open('fairygui-ccc');
      Editor.Scene.callSceneScript('fairygui-ccc', 'add-node',d, function (err, code) {
        console.log(`add-node callback :  code - ${code}`);
      });


    },
    'say-hello' () {
      Editor.log('Hello World!');
      // send ipc message to panel
      Editor.Ipc.sendToPanel('fairygui-ccc', 'fairygui-ccc:hello');
    },
    'clicked' () {
      Editor.log('Button clicked!');
    }
  },
};