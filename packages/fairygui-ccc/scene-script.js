'use strict';

var path = require('path');

function getFileExtension2(filename) {
    return filename.split('.').pop();
  }

let message = {};

message['add-node'] = function (event, data) {

    

    //Editor.log(window);

    let fariygui = window.fariygui

    var arrayBufferHandler = function (item, callback) {
        var url = item.url;
        var xhr = cc.loader.getXMLHttpRequest();
        xhr.open("GET", url, true);
        xhr.responseType = "arraybuffer";
        xhr.onload = function (oEvent) {
            var arrayBuffer = xhr.response;
            if (arrayBuffer) {
                var result = new Uint8Array(arrayBuffer);
                // 任何需要的处理
                callback(null, result);
            }
            else {
                callback("errorMessage"); // 第一个参数需要传递错误信息
            }
        }
        // 错误处理
        xhr.onerror = function (oEvent) { // 同样需要调用 callback 返回错误信息
            callback("errorMessage"); // 第一个参数需要传递错误信息
        }

        xhr.send(null);
    };

    cc.loader.addDownloadHandlers({
        'fui': arrayBufferHandler
    });

    // var keys = [
    //     'Basics',
    //     'Basics@atlas_fjqr7k',
    //     'Basics@atlas_nz0z20',
    //     'Basics@atlas0',
    //     'Basics@atlas1',
    //     'Basics@gojg7u',
    //     'Basics@o4lt7w',
    // ]

    // var urls = [
    //     'fui_test/Basics',
    //     'fui_test/Basics@atlas_fjqr7k',
    //     'fui_test/Basics@atlas_nz0z20',
    //     'fui_test/Basics@atlas0',
    //     'fui_test/Basics@atlas1',
    //     'fui_test/Basics@gojg7u',
    //     'fui_test/Basics@o4lt7w',
    // ];

    var keys = [
        'test',
        'test@atlas0'
    ]

    var urls = [
        'fui_test/test',
        'fui_test/test@atlas0'
    ]

    cc.loader.loadResArray(urls, function (err, assets) {
        Editor.log(err);
        if (err) {
            console.error(err);
            return;
        }

        for (let i = 0; i < assets.length; i++) {
            let a = assets[i]
            if (a._native == ".fui") {
                cc.loader.load(a.nativeUrl, function (err, asset) {
                    if (!err) {
                        fairygui.UIPackage.addPackage("test", asset)
                        let goot = fairygui.GRoot.inst
                    
                        let canvas = cc.find('Canvas');
                        
                        goot.displayObject.parent = canvas.node
                        goot.displayObject.x = -canvas.designResolution.width/2
                        goot.displayObject.y = -canvas.designResolution.height/2

                        // let n = fairygui.UIPackage.createObject("Basics", "Demo_Image")
                        let n = fairygui.UIPackage.createObject("test", "test")
                        goot.addChild(n)
                    }
                })
            } else {
                fairygui.UIPackage.addLoadedRes(keys[i], a)
            }
        }

    });


    // for(let key in data){
    //     // 后缀
    //     let ext = path.extname(key)
    //     let base = path.basename(key,ext)

    //     Editor.log(base+"   "+ext);

    //     if(ext==".png"){
            
    //         let tex = new cc.Texture2D().initWithData(data[key])
    //         fairygui.UIPackage.addLoadedRes(base, tex)
    //     }
    // }

    // Editor.log(123);

    // fairygui.UIPackage.addPackage("test", data["test.fui"])
    
    // let goot = fairygui.GRoot.inst
    

    // var canvas = 
    // goot.displayObject.parent = canvas.node

    // let n = fairygui.UIPackage.createObject("test", "test")
    // goot.addChild(n)

    if (event.reply) {
        event.reply("hello world!");
    }

    // var canvas = cc.find('Canvas');
    // Editor.log('children length : ' + canvas.children.length);


    // let scene = cc.director.getScene()
    // var node = new cc.Node('Sprite');
    // node.parent = scene;

    // if (event.reply) {
    //     event.reply(canvas.children.length);
    // }

    // let a = Editor.assetdb.urlToUuid("db://assets/resources/fui_test/Basics.bytes")
    // Editor.log(a)

    // cc.loader.loadRes("test_assets/atlas", cc.SpriteAtlas, (err, atlas) => {
    //     Editor.log("333"+err);
    //     var spriteFrames = atlas.getSpriteFrames();
        
    //     var clip = cc.AnimationClip.createWithSpriteFrames(spriteFrames, 10);
    //     clip.name = 'run';
    //     clip.wrapMode = cc.WrapMode.Loop;

    //     animation.addClip(clip);
    //     animation.play('run');
    // });

    // cc.loader.loadRes('audio/ding',cc.AudioClip,function(err,asset){
    //     Editor.log("22"+err);
    // })

    // Editor.log("1");
    // cc.loader.loadRes('fui_test/Basics.bytes',function(err,asset){
    //     Editor.log("2"+err);
    //     if(!err){
    //         cc.loader.load(asset.nativeUrl,function(err,result){
    //             Editor.log("3"+err);
    //         })
    //     }
    // })

    // let i18n = cc.require('LanguageData');
    // i18n.init(language);
    // i18n.updateSceneRenderers();
    // if (!event.reply) {
    //     return;
    // }

    // if (language) {
    //     event.reply(null, 'successful');
    // } else {
    //     event.reply(new Error('language not specified!'));
    // }
};

module.exports = message;