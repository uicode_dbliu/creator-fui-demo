export default class FUILoader{

    static _inst:FUILoader;

    static get inst(){
        if(FUILoader._inst==null){
            FUILoader._inst = new FUILoader()
        }
        return FUILoader._inst;
    }

    constructor(){
        cc.loader.addDownloadHandlers({
            'fui': this.__arrayBufferHandler
        });
    }

    loadRes(keys,urls,cb){
        cc.loader.loadResArray(urls, function (err, assets) {
            if (err) {
                console.error(err);
                return;
            }

            for (let i = 0; i < assets.length; i++) {
                let a = assets[i]
                if (a._native == ".fui") {
                    cc.loader.load(a.nativeUrl, function (err, asset) {
                        if (!err) {

                            if(cb){
                                cb(asset)
                            }
                        }
                    })
                } else {
                    fairygui.UIPackage.addLoadedRes(keys[i], a)
                }
            }

        });
    }

    private __arrayBufferHandler(item, callback) {
        var url = item.url;
        var xhr = cc.loader.getXMLHttpRequest();
        xhr.open("GET", url, true);
        xhr.responseType = "arraybuffer";
        xhr.onload = function (oEvent) {
            var arrayBuffer = xhr.response;
            if (arrayBuffer) {
                var result = new Uint8Array(arrayBuffer);
                // 任何需要的处理
                callback(null, result);
            }
            else {
                callback("errorMessage"); // 第一个参数需要传递错误信息
            }
        }
        // 错误处理
        xhr.onerror = function (oEvent) { // 同样需要调用 callback 返回错误信息
            callback("errorMessage"); // 第一个参数需要传递错误信息
        }

        xhr.send(null);
    };
}