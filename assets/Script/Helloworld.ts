const {ccclass, property} = cc._decorator;

@ccclass
export default class Helloworld extends cc.Component {

    @property(cc.Label)
    label: cc.Label = null;

    @property
    text: string = 'hello';

    start () {
        // init logic
        this.label.string = this.text;

        var arrayBufferHandler = function (item, callback) {
            var url = item.url;
            var xhr = cc.loader.getXMLHttpRequest();
            xhr.open("GET", url, true);
            xhr.responseType = "arraybuffer";
            xhr.onload = function (oEvent) {
                var arrayBuffer = xhr.response;
                if (arrayBuffer) {
                    var result = new Uint8Array(arrayBuffer);
                    // 任何需要的处理
                    callback(null, result);
                }
                else {
                    callback("errorMessage"); // 第一个参数需要传递错误信息
                }
            }
            // 错误处理
            xhr.onerror = function (oEvent) { // 同样需要调用 callback 返回错误信息
                callback("errorMessage"); // 第一个参数需要传递错误信息
            }

            xhr.send(null);
        };

        cc.loader.addDownloadHandlers({
            'fui': arrayBufferHandler
        });

        var keys = [
            'Basics',
            'Basics@atlas_fjqr7k',
            'Basics@atlas_nz0z20',
            'Basics@atlas0',
            'Basics@atlas1',
            'Basics@gojg7u',
            'Basics@o4lt7w',
        ]

        var urls = [
            'fui_test/Basics',
            'fui_test/Basics@atlas_fjqr7k',
            'fui_test/Basics@atlas_nz0z20',
            'fui_test/Basics@atlas0',
            'fui_test/Basics@atlas1',
            'fui_test/Basics@gojg7u',
            'fui_test/Basics@o4lt7w',
        ];

        // var keys = [
        //     'test',
        //     'test@atlas0'
        // ]

        // var urls = [
        //     'fui_test/test',
        //     'fui_test/test@atlas0'
        // ]

        // var keys = [
        //     'MainMenu',
        //     'MainMenu@atlas0',
        // ]

        // var urls = [
        //     'fui_test/MainMenu',
        //     'fui_test/MainMenu@atlas0',
        // ];

        let self = this

        cc.loader.loadResArray(urls, function (err, assets) {
            if (err) {
                console.error(err);
                return;
            }

            for (let i = 0; i < assets.length; i++) {
                let a = assets[i]
                if (a._native == ".fui") {
                    cc.loader.load(a.nativeUrl, function (err, asset) {
                        if (!err) {
                            fairygui.UIPackage.addPackage("Basics", asset)
                            // fairygui.UIPackage.addPackage("test", asset)
                            // fairygui.UIPackage.addPackage("MainMenu", asset)
                            let goot = fairygui.GRoot.inst
                        
                            let canvas = cc.Canvas.instance
                            
                            goot.displayObject.parent = canvas.node
                            
                            // let n = fairygui.UIPackage.createObject("test", "test")

                            let n = fairygui.UIPackage.createObject("Basics", "Main") // TODO
                            // let n = fairygui.UIPackage.createObject("Basics", "Demo_Graph") // 10%
                            // let n = fairygui.UIPackage.createObject("Basics", "Demo_Image") // 10%
                            // let n = fairygui.UIPackage.createObject("Basics", "Demo_MovieClip") // 10%
                            // let n = fairygui.UIPackage.createObject("Basics", "Demo_Text")
                            // let n = fairygui.UIPackage.createObject("Basics", "Demo_List") // 10%
                            //  let n = fairygui.UIPackage.createObject("Basics", "Demo_Grid") // TODO
                            // let n = fairygui.UIPackage.createObject("Basics", "Demo_Loader") // 10%
                            // let n = fairygui.UIPackage.createObject("Basics", "Demo_Controller") // TODO
                            // let n = fairygui.UIPackage.createObject("Basics", "Demo_Relation") // TODO
                            // let n = fairygui.UIPackage.createObject("Basics", "Demo_Clip&Scroll") // 10%
                            // let n = fairygui.UIPackage.createObject("Basics", "Demo_Clip&Scroll") // 10%
                            // let n = fairygui.UIPackage.createObject("Basics", "Demo_Component") // 10%
                            // let n = fairygui.UIPackage.createObject("Basics", "Demo_Label") // 10%
                            // let n = fairygui.UIPackage.createObject("Basics", "Demo_Button") // 10%
                            // let n = fairygui.UIPackage.createObject("Basics", "Demo_ComboBox") // 10%
                            // let n = fairygui.UIPackage.createObject("Basics", "Demo_Slider") // 10%
                            // let n = fairygui.UIPackage.createObject("Basics", "Demo_ProgressBar") // 10%
                            // let n = fairygui.UIPackage.createObject("Basics", "Demo_Popup") // 10%
                            // let n = fairygui.UIPackage.createObject("Basics", "Demo_Window") // 10%
                            // let n = fairygui.UIPackage.createObject("Basics", "Demo_Depth") // 10%
                            // let n = fairygui.UIPackage.createObject("Basics", "Demo_Drag&Drop") // 10%

                            
                            // let n = fairygui.UIPackage.createObject("MainMenu", "Main") // 10%
                            
                            
                            
                            // let n = fairygui.UIPackage.createObject("Basics", "Demo_Button") // TODO
                            // let n = fairygui.UIPackage.createObject("Basics", "Demo_Label") // TODO
                            
                            goot.addChild(n)
                            // goot.displayObject.y = goot.displayObject.y + 200
                        }
                    })
                } else {
                    fairygui.UIPackage.addLoadedRes(keys[i], a)
                }
            }

        });
    }
}
