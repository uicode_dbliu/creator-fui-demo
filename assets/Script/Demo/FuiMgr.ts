export default class FuiMgr {

    // scene_list;

    static _inst;

    last_layer_name:string;

    static get inst():FuiMgr{
        if(FuiMgr._inst==null){
            FuiMgr._inst = new FuiMgr();
        }
        return FuiMgr._inst;
    }

    constructor() {
        // this.scene_list = []
    }

    // pushScene(scene){

    //     let canvas = cc.Canvas.instance
    //     scene.displayObject.parent = canvas.node

    //     for (const s of this.scene_list) {
    //         s.displayObject.parent = null
    //     }

    //     this.scene_list.push(scene)
    // }

    // replaceScene(scene){
    //     if(this.scene_list.length>0){

    //         let canvas = cc.Canvas.instance
    //         scene.displayObject.parent = canvas.node

    //         let x = this.scene_list.splice(-1,1,scene)
    //         for (const s of x) {
    //             s.displayObject.parent = null
    //         }
    //     }else{
    //         this.pushScene(scene)
    //     }
        
    // }

    replaceUI(name){
        let canvas = cc.Canvas.instance
        if(this.last_layer_name){
            canvas.node.removeComponent(this.last_layer_name)
        }

        let c = canvas.node.addComponent(name)
        c.name = name
        this.last_layer_name = name
    }
}