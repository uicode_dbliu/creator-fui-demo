import DemoScene from "./DemoScene";
import FuiMgr from "./FuiMgr";

const { ccclass, property } = cc._decorator;

@ccclass
export default class MenuScene extends DemoScene {
    continueInit() {
        let v = fairygui.UIPackage.createObject("MainMenu", "Main")
        this._groot.addChild(v)

        v.getChild('n1').addClickListener(()=>{
            FuiMgr.inst.replaceUI("BasicsScene")
        })
    }
}