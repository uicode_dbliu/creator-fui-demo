import FUILoader from "../FUILoader";
import FuiMgr from "./FuiMgr";

const { ccclass, property } = cc._decorator;

@ccclass
export default class DemoScene extends cc.Component {

    _groot: any

    start() {
        console.log("start")
    }

    update() {
        // console.log("update")
    }
    lateUpdate() {
        // console.log("lateUpdate")
    }

    onEnable() {
        console.log("onEnable")
    }
    onDisable() {
        console.log("onDisable")
    }

    onDestroy() {
        console.log("onDestroy")
        this._groot.displayObject.parent = null
    }

    onLoad() {
        console.log("onLoad")
        let goot = new fairygui.GRoot()
        this._groot = goot

        var keys = [
            'MainMenu',
            'MainMenu@atlas0',
        ]

        var urls = [
            'fui_test/MainMenu',
            'fui_test/MainMenu@atlas0'
        ]

        let loader = FUILoader.inst
        loader.loadRes(keys, urls, (asset) => {

            fairygui.UIPackage.addPackage("MainMenu", asset)

            this.continueInit();

            this.initCloseButton()

            let canvas = cc.Canvas.instance
            goot.displayObject.parent = canvas.node
        })


    }

    continueInit() {

    }

    initCloseButton() {
        var closeButton = fairygui.UIPackage.createObject("MainMenu", "CloseButton");
        closeButton.setXY(this._groot.width - closeButton.width - 10, this._groot.height - closeButton.height - 10);
        closeButton.addRelation(this._groot, fairygui.RelationType.Right_Right);
        closeButton.addRelation(this._groot, fairygui.RelationType.Bottom_Bottom);
        closeButton.sortingOrder = 10000
        closeButton.addClickListener(fairygui.handler(this, this.onClose));
        this._groot.addChild(closeButton)
    }

    onClose() {
        console.log(this.name)
        if(this.name!="MenuScene"){
            FuiMgr.inst.replaceUI("MenuScene")
        }else{
            console.log("点击了关闭按钮")
        }
    }
}