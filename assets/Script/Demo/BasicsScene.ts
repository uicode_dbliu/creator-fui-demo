import FUILoader from "../FUILoader";
import DemoScene from "./DemoScene";

const { ccclass, property } = cc._decorator;

@ccclass
export default class BasicsScene extends DemoScene {

    _backBtn;
    _cc;
    _demoContainer;
    _demoObjects={};

    continueInit(){
        var keys = [
            'Basics',
            'Basics@atlas_fjqr7k',
            'Basics@atlas_nz0z20',
            'Basics@atlas0',
            'Basics@atlas1',
            'Basics@gojg7u',
            'Basics@o4lt7w',
        ]

        var urls = [
            'fui_test/Basics',
            'fui_test/Basics@atlas_fjqr7k',
            'fui_test/Basics@atlas_nz0z20',
            'fui_test/Basics@atlas0',
            'fui_test/Basics@atlas1',
            'fui_test/Basics@gojg7u',
            'fui_test/Basics@o4lt7w',
        ];

        let self:BasicsScene = this

        let loader = FUILoader.inst
        loader.loadRes(keys, urls, (asset) => {
            fairygui.UIConfig.buttonSound = "ui://Basics/click"
            fairygui.UIConfig.verticalScrollBar = "ui://Basics/ScrollBar_VT"
            fairygui.UIConfig.horizontalScrollBar = "ui://Basics/ScrollBar_HZ"
            fairygui.UIConfig.tooltipsWin = "ui://Basics/WindowFrame"
            fairygui.UIConfig.popupMenu = "ui://Basics/PopupMenu"

            fairygui.UIPackage.addPackage("Basics", asset)

            let _view = fairygui.UIPackage.createObject("Basics", "Main")
            self._groot.addChild(_view)

            self._backBtn = _view.getChild("btn_Back")
            self._backBtn.visible = false
            self._backBtn.addClickListener(fairygui.handler(self, self.onClickBack))

            self._demoContainer = _view.getChild("container");
            self._cc = _view.getController("c1");

            let cnt = _view.numChildren
            for(let i =0;i<cnt;i++){
                let obj = _view.getChildAt(i)
                if (obj.group != null && obj.group.name == "btns") {
                    obj.addClickListener(fairygui.handler(self, self.runDemo))
                }
            
            }

        })

    }

    runDemo(context){
        let name = context.getSender().name
        name = name.substring(4)

        console.log(name)

        let v = this._demoObjects[name]
        if(v==null){
            v = fairygui.UIPackage.createObject("Basics", "Demo_"+name)
            this._demoObjects[name]=v
        }


        this._demoContainer.removeChildren()
        this._demoContainer.addChild(v)
        this._cc.setSelectedIndex(1)
        this._backBtn.visible = true
    }

    onClickBack(){
        this._cc.setSelectedIndex(0)
        this._backBtn.visible = false
    }

    onDestroy() {

    }
}