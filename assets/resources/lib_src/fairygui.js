var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var fairygui;
(function (fairygui) {
    var EventCallbackItem = /** @class */ (function () {
        function EventCallbackItem() {
        }
        return EventCallbackItem;
    }());
    fairygui.EventCallbackItem = EventCallbackItem;
})(fairygui || (fairygui = {}));
var fairygui;
(function (fairygui) {
    var EventContext = /** @class */ (function () {
        function EventContext() {
            this._sender = null;
            this._data = null;
            this._inputEvent = null;
            this._isStopped = null;
            this._defaultPrevented = false;
            this._touchCapture = 0;
            this._type = 0;
            this._dataValue = null;
        }
        EventContext.prototype.getType = function () {
            return this._type;
        };
        EventContext.prototype.getSender = function () {
            return this._sender;
        };
        EventContext.prototype.getInput = function () {
            return this._inputEvent;
        };
        EventContext.prototype.stopPropagation = function () {
            return this._isStopped;
        };
        EventContext.prototype.preventDefault = function () {
            this._defaultPrevented = true;
        };
        EventContext.prototype.isDefaultPrevented = function () {
            return this._defaultPrevented;
        };
        EventContext.prototype.captureTouch = function () { this._touchCapture = 1; };
        EventContext.prototype.uncaptureTouch = function () { this._touchCapture = 2; };
        EventContext.prototype.getDataValue = function () { return this._dataValue; };
        EventContext.prototype.getData = function () { return this._data; };
        return EventContext;
    }());
    fairygui.EventContext = EventContext;
})(fairygui || (fairygui = {}));
var fairygui;
(function (fairygui) {
    var InputEvent = /** @class */ (function () {
        function InputEvent() {
            this._target = null;
            this._touch = null;
            this._inputProcessor = null;
            this._touchId = -1;
            this._clickCount = 0;
            this._mouseWheelDelta = 0;
            this._keyCode = 0;
            this._keyModifiers = 0;
            this._pos = new cc.Vec2(0, 0);
        }
        InputEvent.prototype.getTarget = function () { return this._target; };
        InputEvent.prototype.getX = function () { return this._pos.x; };
        InputEvent.prototype.getY = function () { return this._pos.y; };
        InputEvent.prototype.getPosition = function () { return this._pos; };
        InputEvent.prototype.getTouch = function () { return this._touch; };
        InputEvent.prototype.getTouchId = function () { return this._touchId; };
        InputEvent.prototype.isDoubleClick = function () { return this._clickCount == 2; };
        InputEvent.prototype.getProcessor = function () { return this._inputProcessor; };
        return InputEvent;
    }());
    fairygui.InputEvent = InputEvent;
})(fairygui || (fairygui = {}));
var fairygui;
(function (fairygui) {
    var InputProcessor = /** @class */ (function () {
        function InputProcessor() {
            this._touches = [];
            this._owner.displayObject.on(cc.Node.EventType.TOUCH_START, function (event) {
                console.log('TOUCH_START');
            }, this);
            this._owner.displayObject.on(cc.Node.EventType.TOUCH_MOVE, function (event) {
                console.log('TOUCH_MOVE');
            }, this);
            this._owner.displayObject.on(cc.Node.EventType.TOUCH_END, function (event) {
                console.log('TOUCH_END');
            }, this);
            this._owner.displayObject.on(cc.Node.EventType.TOUCH_CANCEL, function (event) {
                console.log('TOUCH_CANCEL');
            }, this);
        }
        InputProcessor.prototype.getRecentInput = function () {
            return this._recentInput;
        };
        return InputProcessor;
    }());
    fairygui.InputProcessor = InputProcessor;
})(fairygui || (fairygui = {}));
var fairygui;
(function (fairygui) {
    var UIEventDispatcher = /** @class */ (function () {
        function UIEventDispatcher() {
            this._callbacks = [];
            this._dispatching = 0;
        }
        UIEventDispatcher.prototype.addEventListener = function (eventType, callback, tag) {
            if (tag) {
                for (var _i = 0, _a = this._callbacks; _i < _a.length; _i++) {
                    var v = _a[_i];
                    if (v.eventType == eventType && v.tag == tag) {
                        v.callback = callback;
                        return;
                    }
                }
            }
            var item = new fairygui.EventCallbackItem();
            item.callback = callback;
            item.eventType = eventType;
            item.tag = tag;
            item.dispatching = 0;
            this._callbacks.push(item);
        };
        UIEventDispatcher.prototype.removeEventListener = function (eventType, tag) {
            if (this._callbacks.length == 0) {
                return;
            }
            for (var i = this._callbacks.length - 1; i >= 0; i--) {
                var v = this._callbacks[i];
                if (v.eventType == eventType && tag == null || v.tag == tag) {
                    if (this._dispatching > 0) {
                        v.callback = null;
                    }
                    else {
                        this._callbacks.splice(i, 1);
                    }
                }
            }
        };
        UIEventDispatcher.prototype.removeEventListeners = function () {
            if (this._callbacks.length == 0) {
                return;
            }
            if (this._dispatching > 0) {
                for (var _i = 0, _a = this._callbacks; _i < _a.length; _i++) {
                    var v = _a[_i];
                    v.callback = null;
                }
            }
            else {
                this._callbacks = [];
            }
        };
        UIEventDispatcher.prototype.hasEventListener = function (eventType, tag) {
            if (this._callbacks.length == 0) {
                return false;
            }
            for (var _i = 0, _a = this._callbacks; _i < _a.length; _i++) {
                var v = _a[_i];
                if (v.eventType == eventType && (v.tag == tag || v.tag == null) && v.callback != null) {
                    return true;
                }
            }
            return false;
        };
        UIEventDispatcher.prototype.dispatchEvent = function (eventType, data, dataValue) {
            if (data === void 0) { data = null; }
            if (dataValue === void 0) { dataValue = null; }
            if (this._callbacks.length == 0) {
                return false;
            }
            var context = new fairygui.EventContext();
            context._sender = this;
            context._type = eventType;
            context._dataValue = dataValue;
            context._data = data;
            if (fairygui.InputProcessor._activeProcessor) {
                context._inputEvent = fairygui.InputProcessor._activeProcessor.getRecentInput();
            }
            this._doDispatch(eventType, context);
            return context._defaultPrevented;
        };
        UIEventDispatcher.prototype.bubbleEvent = function (eventType, data, dataValue) {
            var context = new fairygui.EventContext();
            if (fairygui.InputProcessor._activeProcessor) {
                context._inputEvent = fairygui.InputProcessor._activeProcessor.getRecentInput();
            }
            context._type = eventType;
            context._dataValue = dataValue;
            context._data = data;
            this._doBubble(eventType, context);
            return context._defaultPrevented;
        };
        UIEventDispatcher.prototype.isDispatchingEvent = function (eventType) {
            for (var _i = 0, _a = this._callbacks; _i < _a.length; _i++) {
                var v = _a[_i];
                if (v.eventType == eventType) {
                    return v.dispatching > 0;
                }
            }
            return false;
        };
        UIEventDispatcher.prototype._doDispatch = function (eventType, context) {
            this._dispatching++;
            context._sender = this;
            var hasDeletedItems = false;
            var cnt = this._callbacks.length;
            for (var i = 0; i < cnt; i++) {
                var ci = this._callbacks[i];
                if (ci.callback == null) {
                    hasDeletedItems = true;
                    continue;
                }
                if (ci.eventType == eventType) {
                    ci.dispatchEvent++;
                    context._touchCapture = 0;
                    ci.callback(context);
                    ci.dispatchEvent--;
                    if (context._touchCapture != 0 && (this instanceof fairygui.GObject)) {
                        if (context._touchCapture == 1 && eventType == fairygui.UIEventType.TouchBegin) {
                            context.getInput().getProcessor().addTouchMonitor(context.getInput().getTouchId(), this);
                        }
                        else {
                            context.getInput().getProcessor().removeTouchMonitor(this);
                        }
                    }
                }
            }
            this._dispatching--;
            if (hasDeletedItems && this._dispatching == 0) {
                for (var i = this._callbacks.length - 1; i >= 0; i--) {
                    var v = this._callbacks[i];
                    if (v.callback == null) {
                        this._callbacks.splice(i, 1);
                    }
                }
            }
        };
        UIEventDispatcher.prototype._doBubble = function (eventType, context) {
            if (this._callbacks.length > 0) {
                context._isStopped = false;
                this._doDispatch(eventType, context);
                if (context._isStopped == true) {
                    return;
                }
            }
            var p = this.parent;
            if (p) {
                p._doBubble(eventType, context);
            }
        };
        return UIEventDispatcher;
    }());
    fairygui.UIEventDispatcher = UIEventDispatcher;
})(fairygui || (fairygui = {}));
var fairygui;
(function (fairygui) {
    var Controller = /** @class */ (function (_super) {
        __extends(Controller, _super);
        function Controller() {
            var _this = _super.call(this) || this;
            _this._selectedIndex = 0;
            _this._previousIndex = 0;
            _this.changing = false;
            _this._pageIds = [];
            _this._pageNames = [];
            _this._selectedIndex = -1;
            _this._previousIndex = -1;
            return _this;
        }
        Controller.prototype.dispose = function () {
        };
        Object.defineProperty(Controller.prototype, "name", {
            get: function () {
                return this._name;
            },
            set: function (value) {
                this._name = value;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Controller.prototype, "parent", {
            get: function () {
                return this._parent;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Controller.prototype, "selectedIndex", {
            get: function () {
                return this._selectedIndex;
            },
            set: function (value) {
                if (this._selectedIndex != value) {
                    if (value > this._pageIds.length - 1)
                        throw "index out of bounds: " + value;
                    this.changing = true;
                    this._previousIndex = this._selectedIndex;
                    this._selectedIndex = value;
                    this._parent.applyController(this);
                    this.dispatchEvent(new StateChangeEvent(StateChangeEvent.CHANGED));
                    this.changing = false;
                }
            },
            enumerable: true,
            configurable: true
        });
        //功能和设置selectedIndex一样，但不会触发事件
        Controller.prototype.setSelectedIndex = function (value) {
            if (value === void 0) { value = 0; }
            if (this._selectedIndex != value) {
                if (value > this._pageIds.length - 1)
                    throw "index out of bounds: " + value;
                this.changing = true;
                this._previousIndex = this._selectedIndex;
                this._selectedIndex = value;
                this._parent.applyController(this);
                this.changing = false;
            }
        };
        Object.defineProperty(Controller.prototype, "previsousIndex", {
            get: function () {
                return this._previousIndex;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Controller.prototype, "selectedPage", {
            get: function () {
                if (this._selectedIndex == -1)
                    return null;
                else
                    return this._pageNames[this._selectedIndex];
            },
            set: function (val) {
                var i = this._pageNames.indexOf(val);
                if (i == -1)
                    i = 0;
                this.selectedIndex = i;
            },
            enumerable: true,
            configurable: true
        });
        //功能和设置selectedPage一样，但不会触发事件
        Controller.prototype.setSelectedPage = function (value) {
            var i = this._pageNames.indexOf(value);
            if (i == -1)
                i = 0;
            this.setSelectedIndex(i);
        };
        Object.defineProperty(Controller.prototype, "previousPage", {
            get: function () {
                if (this._previousIndex == -1)
                    return null;
                else
                    return this._pageNames[this._previousIndex];
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Controller.prototype, "pageCount", {
            get: function () {
                return this._pageIds.length;
            },
            enumerable: true,
            configurable: true
        });
        Controller.prototype.getPageName = function (index) {
            if (index === void 0) { index = 0; }
            return this._pageNames[index];
        };
        Controller.prototype.addPage = function (name) {
            if (name === void 0) { name = ""; }
            this.addPageAt(name, this._pageIds.length);
        };
        Controller.prototype.addPageAt = function (name, index) {
            if (index === void 0) { index = 0; }
            var nid = "" + (Controller._nextPageId++);
            if (index == this._pageIds.length) {
                this._pageIds.push(nid);
                this._pageNames.push(name);
            }
            else {
                this._pageIds.splice(index, 0, nid);
                this._pageNames.splice(index, 0, name);
            }
        };
        Controller.prototype.removePage = function (name) {
            var i = this._pageNames.indexOf(name);
            if (i != -1) {
                this._pageIds.splice(i, 1);
                this._pageNames.splice(i, 1);
                if (this._selectedIndex >= this._pageIds.length)
                    this.selectedIndex = this._selectedIndex - 1;
                else
                    this._parent.applyController(this);
            }
        };
        Controller.prototype.removePageAt = function (index) {
            if (index === void 0) { index = 0; }
            this._pageIds.splice(index, 1);
            this._pageNames.splice(index, 1);
            if (this._selectedIndex >= this._pageIds.length)
                this.selectedIndex = this._selectedIndex - 1;
            else
                this._parent.applyController(this);
        };
        Controller.prototype.clearPages = function () {
            this._pageIds.length = 0;
            this._pageNames.length = 0;
            if (this._selectedIndex != -1)
                this.selectedIndex = -1;
            else
                this._parent.applyController(this);
        };
        Controller.prototype.hasPage = function (aName) {
            return this._pageNames.indexOf(aName) != -1;
        };
        Controller.prototype.getPageIndexById = function (aId) {
            return this._pageIds.indexOf(aId);
        };
        Controller.prototype.getPageIdByName = function (aName) {
            var i = this._pageNames.indexOf(aName);
            if (i != -1)
                return this._pageIds[i];
            else
                return null;
        };
        Controller.prototype.getPageNameById = function (aId) {
            var i = this._pageIds.indexOf(aId);
            if (i != -1)
                return this._pageNames[i];
            else
                return null;
        };
        Controller.prototype.getPageId = function (index) {
            if (index === void 0) { index = 0; }
            return this._pageIds[index];
        };
        Object.defineProperty(Controller.prototype, "selectedPageId", {
            get: function () {
                if (this._selectedIndex == -1)
                    return null;
                else
                    return this._pageIds[this._selectedIndex];
            },
            set: function (val) {
                var i = this._pageIds.indexOf(val);
                this.selectedIndex = i;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Controller.prototype, "oppositePageId", {
            set: function (val) {
                var i = this._pageIds.indexOf(val);
                if (i > 0)
                    this.selectedIndex = 0;
                else if (this._pageIds.length > 1)
                    this.selectedIndex = 1;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Controller.prototype, "previousPageId", {
            get: function () {
                if (this._previousIndex == -1)
                    return null;
                else
                    return this._pageIds[this._previousIndex];
            },
            enumerable: true,
            configurable: true
        });
        Controller.prototype.runActions = function () {
            if (this._actions) {
                var cnt = this._actions.length;
                for (var i = 0; i < cnt; i++)
                    this._actions[i].run(this, this.previousPageId, this.selectedPageId);
            }
        };
        Controller.prototype.setup = function (xml) {
            this._name = xml.attributes.name;
            this._autoRadioGroupDepth = xml.attributes.autoRadioGroupDepth == "true";
            var i = 0;
            var k = 0;
            var str = xml.attributes.pages;
            if (str) {
                var arr = str.split(",");
                var cnt = arr.length;
                for (i = 0; i < cnt; i += 2) {
                    this._pageIds.push(arr[i]);
                    this._pageNames.push(arr[i + 1]);
                }
            }
            var col = xml.children;
            var length1 = col.length;
            if (length1 > 0) {
                if (!this._actions)
                    this._actions = new Array();
                for (var i1 = 0; i1 < length1; i1++) {
                    var cxml = col[i1];
                    var action = ControllerAction.createAction(cxml.attributes.type);
                    action.setup(cxml);
                    this._actions.push(action);
                }
            }
            str = xml.attributes.transitions;
            if (str) {
                if (!this._actions)
                    this._actions = new Array();
                arr = str.split(",");
                cnt = arr.length;
                var ii;
                for (i = 0; i < cnt; i++) {
                    str = arr[i];
                    if (!str)
                        continue;
                    var taction = new PlayTransitionAction();
                    k = str.indexOf("=");
                    taction.transitionName = str.substr(k + 1);
                    str = str.substring(0, k);
                    k = str.indexOf("-");
                    ii = parseInt(str.substring(k + 1));
                    if (ii < this._pageIds.length)
                        taction.toPage = [this._pageIds[ii]];
                    str = str.substring(0, k);
                    if (str != "*") {
                        ii = parseInt(str);
                        if (ii < this._pageIds.length)
                            taction.fromPage = [this._pageIds[ii]];
                    }
                    taction.stopOnExit = true;
                    this._actions.push(taction);
                }
            }
            if (this._parent && this._pageIds.length > 0)
                this._selectedIndex = 0;
            else
                this._selectedIndex = -1;
        };
        Controller._nextPageId = 0;
        return Controller;
    }(fairygui.UIEventDispatcher));
    fairygui.Controller = Controller;
})(fairygui || (fairygui = {}));
var fairygui;
(function (fairygui) {
    var UIContainer = /** @class */ (function (_super) {
        __extends(UIContainer, _super);
        function UIContainer() {
            return _super.call(this) || this;
            // this.touchEnabled = true;
            // this.touchChildren = true;
        }
        return UIContainer;
    }(cc.Node));
    fairygui.UIContainer = UIContainer;
})(fairygui || (fairygui = {}));
var fairygui;
(function (fairygui) {
    var UIInnerContainer = /** @class */ (function (_super) {
        __extends(UIInnerContainer, _super);
        function UIInnerContainer() {
            return _super.call(this) || this;
            // this.touchEnabled = true;
            // this.touchChildren = true;
        }
        UIInnerContainer.prototype.setPosition2 = function (posOrX, y) {
            var x = posOrX;
            if (posOrX instanceof cc.Vec2) {
                x = posOrX.x;
                y = posOrX.y;
            }
            y = this.parent.getContentSize().height - y;
            this.setPosition(x, y);
        };
        UIInnerContainer.prototype.getPosition2 = function () {
            var pt = this.getPosition();
            pt.y = this.parent.getContentSize().height - pt.y;
            return pt;
        };
        UIInnerContainer.prototype.setPositionY2 = function (y) {
            y = this.parent.getContentSize().height - y;
            this.setPosition(this.x, y);
        };
        UIInnerContainer.prototype.getPositionY2 = function () {
            var y = this.y;
            y = this.parent.getContentSize().height - y;
            return y;
        };
        return UIInnerContainer;
    }(cc.Node));
    fairygui.UIInnerContainer = UIInnerContainer;
})(fairygui || (fairygui = {}));
var fairygui;
(function (fairygui) {
    var BitmapFont = /** @class */ (function () {
        function BitmapFont() {
            this.size = 0;
            this.glyphs = {};
        }
        return BitmapFont;
    }());
    fairygui.BitmapFont = BitmapFont;
})(fairygui || (fairygui = {}));
var fairygui;
(function (fairygui) {
    var BMGlyph = /** @class */ (function () {
        function BMGlyph() {
            this.x = 0;
            this.y = 0;
            this.offsetX = 0;
            this.offsetY = 0;
            this.width = 0;
            this.height = 0;
            this.advance = 0;
            this.lineHeight = 0;
            this.channel = 0;
        }
        return BMGlyph;
    }());
    fairygui.BMGlyph = BMGlyph;
})(fairygui || (fairygui = {}));
var fairygui;
(function (fairygui) {
    var UBBParser = /** @class */ (function () {
        function UBBParser() {
            this._readPos = 0;
            this.smallFontSize = 12;
            this.normalFontSize = 14;
            this.largeFontSize = 16;
            this.defaultImgWidth = 0;
            this.defaultImgHeight = 0;
            this._handlers = {};
            this._handlers["url"] = this.onTag_URL;
            this._handlers["img"] = this.onTag_IMG;
            this._handlers["b"] = this.onTag_Simple;
            this._handlers["i"] = this.onTag_Simple;
            this._handlers["u"] = this.onTag_Simple;
            this._handlers["sup"] = this.onTag_Simple;
            this._handlers["sub"] = this.onTag_Simple;
            this._handlers["color"] = this.onTag_COLOR;
            this._handlers["font"] = this.onTag_FONT;
            this._handlers["size"] = this.onTag_SIZE;
        }
        UBBParser.prototype.onTag_URL = function (tagName, end, attr) {
            if (!end) {
                if (attr != null)
                    return "<a href=\"" + attr + "\" target=\"_blank\">";
                else {
                    var href = this.getTagText();
                    return "<a href=\"" + href + "\" target=\"_blank\">";
                }
            }
            else
                return "</a>";
        };
        UBBParser.prototype.onTag_IMG = function (tagName, end, attr) {
            if (!end) {
                var src = this.getTagText(true);
                if (!src)
                    return null;
                if (this.defaultImgWidth)
                    return "<img src=\"" + src + "\" width=\"" + this.defaultImgWidth + "\" height=\"" + this.defaultImgHeight + "\"/>";
                else
                    return "<img src=\"" + src + "\"/>";
            }
            else
                return null;
        };
        UBBParser.prototype.onTag_Simple = function (tagName, end, attr) {
            return end ? ("</" + tagName + ">") : ("<" + tagName + ">");
        };
        UBBParser.prototype.onTag_COLOR = function (tagName, end, attr) {
            if (!end)
                return "<font color=\"" + attr + "\">";
            else
                return "</font>";
        };
        UBBParser.prototype.onTag_FONT = function (tagName, end, attr) {
            if (!end)
                return "<font face=\"" + attr + "\">";
            else
                return "</font>";
        };
        UBBParser.prototype.onTag_SIZE = function (tagName, end, attr) {
            if (!end) {
                if (attr == "normal")
                    attr = "" + this.normalFontSize;
                else if (attr == "small")
                    attr = "" + this.smallFontSize;
                else if (attr == "large")
                    attr = "" + this.largeFontSize;
                else if (attr.length && attr.charAt(0) == "+")
                    attr = "" + (this.smallFontSize + parseInt(attr.substr(1)));
                else if (attr.length && attr.charAt(0) == "-")
                    attr = "" + (this.smallFontSize - parseInt(attr.substr(1)));
                return "<font size=\"" + attr + "\">";
            }
            else
                return "</font>";
        };
        UBBParser.prototype.getTagText = function (remove) {
            if (remove === void 0) { remove = false; }
            var pos1 = this._readPos;
            var pos2;
            var result = "";
            while ((pos2 = this._text.indexOf("[", pos1)) != -1) {
                if (this._text.charCodeAt(pos2 - 1) == 92) //\
                 {
                    result += this._text.substring(pos1, pos2 - 1);
                    result += "[";
                    pos1 = pos2 + 1;
                }
                else {
                    result += this._text.substring(pos1, pos2);
                    break;
                }
            }
            if (pos2 == -1)
                return null;
            if (remove)
                this._readPos = pos2;
            return result;
        };
        UBBParser.prototype.parse = function (text, remove) {
            if (remove === void 0) { remove = false; }
            this._text = text;
            var pos1 = 0, pos2, pos3;
            var end;
            var tag, attr;
            var repl;
            var func;
            var result = "";
            while ((pos2 = this._text.indexOf("[", pos1)) != -1) {
                if (pos2 > 0 && this._text.charCodeAt(pos2 - 1) == 92) //\
                 {
                    result += this._text.substring(pos1, pos2 - 1);
                    result += "[";
                    pos1 = pos2 + 1;
                    continue;
                }
                result += this._text.substring(pos1, pos2);
                pos1 = pos2;
                pos2 = this._text.indexOf("]", pos1);
                if (pos2 == -1)
                    break;
                end = this._text.charAt(pos1 + 1) == '/';
                tag = this._text.substring(end ? pos1 + 2 : pos1 + 1, pos2);
                this._readPos = pos2 + 1;
                attr = null;
                repl = null;
                pos3 = tag.indexOf("=");
                if (pos3 != -1) {
                    attr = tag.substring(pos3 + 1);
                    tag = tag.substring(0, pos3);
                }
                tag = tag.toLowerCase();
                func = this._handlers[tag];
                if (func != null) {
                    if (!remove) {
                        repl = func.call(this, tag, end, attr);
                        if (repl != null)
                            result += repl;
                    }
                }
                else
                    result += this._text.substring(pos1, this._readPos);
                pos1 = this._readPos;
            }
            if (pos1 < this._text.length)
                result += this._text.substr(pos1);
            this._text = null;
            return result;
        };
        UBBParser.inst = new UBBParser();
        return UBBParser;
    }());
    fairygui.UBBParser = UBBParser;
})(fairygui || (fairygui = {}));
var fairygui;
(function (fairygui) {
    var ToolSet = /** @class */ (function () {
        function ToolSet() {
        }
        ToolSet.getFileName = function (source) {
            var i = source.lastIndexOf("/");
            if (i != -1)
                source = source.substr(i + 1);
            i = source.lastIndexOf("\\");
            if (i != -1)
                source = source.substr(i + 1);
            i = source.lastIndexOf(".");
            if (i != -1)
                return source.substring(0, i);
            else
                return source;
        };
        ToolSet.startsWith = function (source, str, ignoreCase) {
            if (ignoreCase === void 0) { ignoreCase = false; }
            if (!source)
                return false;
            else if (source.length < str.length)
                return false;
            else {
                source = source.substring(0, str.length);
                if (!ignoreCase)
                    return source == str;
                else
                    return source.toLowerCase() == str.toLowerCase();
            }
        };
        ToolSet.endsWith = function (source, str, ignoreCase) {
            if (ignoreCase === void 0) { ignoreCase = false; }
            if (!source)
                return false;
            else if (source.length < str.length)
                return false;
            else {
                source = source.substring(source.length - str.length);
                if (!ignoreCase)
                    return source == str;
                else
                    return source.toLowerCase() == str.toLowerCase();
            }
        };
        ToolSet.trim = function (targetString) {
            return ToolSet.trimLeft(ToolSet.trimRight(targetString));
        };
        ToolSet.trimLeft = function (targetString) {
            var tempChar = "";
            for (var i = 0; i < targetString.length; i++) {
                tempChar = targetString.charAt(i);
                if (tempChar != " " && tempChar != "\n" && tempChar != "\r") {
                    break;
                }
            }
            return targetString.substr(i);
        };
        ToolSet.trimRight = function (targetString) {
            var tempChar = "";
            for (var i = targetString.length - 1; i >= 0; i--) {
                tempChar = targetString.charAt(i);
                if (tempChar != " " && tempChar != "\n" && tempChar != "\r") {
                    break;
                }
            }
            return targetString.substring(0, i + 1);
        };
        ToolSet.convertToHtmlColor = function (argb, hasAlpha) {
            if (hasAlpha === void 0) { hasAlpha = false; }
            var alpha;
            if (hasAlpha)
                alpha = (argb >> 24 & 0xFF).toString(16);
            else
                alpha = "";
            var red = (argb >> 16 & 0xFF).toString(16);
            var green = (argb >> 8 & 0xFF).toString(16);
            var blue = (argb & 0xFF).toString(16);
            if (alpha.length == 1)
                alpha = "0" + alpha;
            if (red.length == 1)
                red = "0" + red;
            if (green.length == 1)
                green = "0" + green;
            if (blue.length == 1)
                blue = "0" + blue;
            return "#" + alpha + red + green + blue;
        };
        ToolSet.convertFromHtmlColor = function (str, hasAlpha) {
            if (hasAlpha === void 0) { hasAlpha = false; }
            if (str.length < 1)
                return 0;
            if (str.charAt(0) == "#")
                str = str.substr(1);
            if (str.length == 8)
                return (parseInt(str.substr(0, 2), 16) << 24) + parseInt(str.substr(2), 16);
            else if (hasAlpha)
                return 0xFF000000 + parseInt(str, 16);
            else
                return parseInt(str, 16);
        };
        ToolSet.displayObjectToGObject = function (obj) {
            while (obj != null && !(obj instanceof egret.Stage)) {
                if (obj["$owner"])
                    return obj["$owner"];
                obj = obj.parent;
            }
            return null;
        };
        ToolSet.findChildNode = function (xml, name) {
            var col = xml.children;
            if (col) {
                var length1 = col.length;
                for (var i1 = 0; i1 < length1; i1++) {
                    var cxml = col[i1];
                    if (cxml.name == name) {
                        return cxml;
                    }
                }
            }
            return null;
        };
        ToolSet.encodeHTML = function (str) {
            if (!str)
                return "";
            else
                return str.replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;").replace("'", "&apos;");
        };
        ToolSet.parseUBB = function (text) {
            return ToolSet.defaultUBBParser.parse(text);
        };
        ToolSet.clamp = function (value, min, max) {
            if (value < min)
                value = min;
            else if (value > max)
                value = max;
            return value;
        };
        ToolSet.clamp01 = function (value) {
            if (value > 1)
                value = 1;
            else if (value < 0)
                value = 0;
            return value;
        };
        ToolSet.lerp = function (start, end, percent) {
            return (start + percent * (end - start));
        };
        ToolSet.defaultUBBParser = new fairygui.UBBParser();
        return ToolSet;
    }());
    fairygui.ToolSet = ToolSet;
})(fairygui || (fairygui = {}));
// Author: Daniele Giardini - http://www.demigiant.com
// Created: 2014/07/19 14:11
// 
// License Copyright (c) Daniele Giardini.
// This work is subject to the terms at http://dotween.demigiant.com/license.php
// 
// =============================================================
// Contains Daniele Giardini's C# port of the easing equations created by Robert Penner
// (all easing equations except for Flash, InFlash, OutFlash, InOutFlash,
// which use some parts of Robert Penner's equations but were created by Daniele Giardini)
// http://robertpenner.com/easing, see license below:
// =============================================================
//
// TERMS OF USE - EASING EQUATIONS
//
// Open source under the BSD License.
//
// Copyright ? 2001 Robert Penner
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
//
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
// - Neither the name of the author nor the names of contributors may be used to endorse
// or promote products derived from this software without specific prior written permission.
// - THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
var fairygui;
(function (fairygui) {
    var EaseManager = /** @class */ (function () {
        function EaseManager() {
        }
        EaseManager.evaluate = function (easeType, time, duration, overshootOrAmplitude, period) {
            switch (easeType) {
                case fairygui.EaseType.Linear:
                    return time / duration;
                case fairygui.EaseType.SineIn:
                    return -Math.cos(time / duration * EaseManager._PiOver2) + 1;
                case fairygui.EaseType.SineOut:
                    return Math.sin(time / duration * EaseManager._PiOver2);
                case fairygui.EaseType.SineInOut:
                    return -0.5 * (Math.cos(Math.PI * time / duration) - 1);
                case fairygui.EaseType.QuadIn:
                    return (time /= duration) * time;
                case fairygui.EaseType.QuadOut:
                    return -(time /= duration) * (time - 2);
                case fairygui.EaseType.QuadInOut:
                    if ((time /= duration * 0.5) < 1)
                        return 0.5 * time * time;
                    return -0.5 * ((--time) * (time - 2) - 1);
                case fairygui.EaseType.CubicIn:
                    return (time /= duration) * time * time;
                case fairygui.EaseType.CubicOut:
                    return ((time = time / duration - 1) * time * time + 1);
                case fairygui.EaseType.CubicInOut:
                    if ((time /= duration * 0.5) < 1)
                        return 0.5 * time * time * time;
                    return 0.5 * ((time -= 2) * time * time + 2);
                case fairygui.EaseType.QuartIn:
                    return (time /= duration) * time * time * time;
                case fairygui.EaseType.QuartOut:
                    return -((time = time / duration - 1) * time * time * time - 1);
                case fairygui.EaseType.QuartInOut:
                    if ((time /= duration * 0.5) < 1)
                        return 0.5 * time * time * time * time;
                    return -0.5 * ((time -= 2) * time * time * time - 2);
                case fairygui.EaseType.QuintIn:
                    return (time /= duration) * time * time * time * time;
                case fairygui.EaseType.QuintOut:
                    return ((time = time / duration - 1) * time * time * time * time + 1);
                case fairygui.EaseType.QuintInOut:
                    if ((time /= duration * 0.5) < 1)
                        return 0.5 * time * time * time * time * time;
                    return 0.5 * ((time -= 2) * time * time * time * time + 2);
                case fairygui.EaseType.ExpoIn:
                    return (time == 0) ? 0 : Math.pow(2, 10 * (time / duration - 1));
                case fairygui.EaseType.ExpoOut:
                    if (time == duration)
                        return 1;
                    return (-Math.pow(2, -10 * time / duration) + 1);
                case fairygui.EaseType.ExpoInOut:
                    if (time == 0)
                        return 0;
                    if (time == duration)
                        return 1;
                    if ((time /= duration * 0.5) < 1)
                        return 0.5 * Math.pow(2, 10 * (time - 1));
                    return 0.5 * (-Math.pow(2, -10 * --time) + 2);
                case fairygui.EaseType.CircIn:
                    return -(Math.sqrt(1 - (time /= duration) * time) - 1);
                case fairygui.EaseType.CircOut:
                    return Math.sqrt(1 - (time = time / duration - 1) * time);
                case fairygui.EaseType.CircInOut:
                    if ((time /= duration * 0.5) < 1)
                        return -0.5 * (Math.sqrt(1 - time * time) - 1);
                    return 0.5 * (Math.sqrt(1 - (time -= 2) * time) + 1);
                case fairygui.EaseType.ElasticIn:
                    var s0;
                    if (time == 0)
                        return 0;
                    if ((time /= duration) == 1)
                        return 1;
                    if (period == 0)
                        period = duration * 0.3;
                    if (overshootOrAmplitude < 1) {
                        overshootOrAmplitude = 1;
                        s0 = period / 4;
                    }
                    else
                        s0 = period / EaseManager._TwoPi * Math.asin(1 / overshootOrAmplitude);
                    return -(overshootOrAmplitude * Math.pow(2, 10 * (time -= 1)) * Math.sin((time * duration - s0) * EaseManager._TwoPi / period));
                case fairygui.EaseType.ElasticOut:
                    var s1;
                    if (time == 0)
                        return 0;
                    if ((time /= duration) == 1)
                        return 1;
                    if (period == 0)
                        period = duration * 0.3;
                    if (overshootOrAmplitude < 1) {
                        overshootOrAmplitude = 1;
                        s1 = period / 4;
                    }
                    else
                        s1 = period / EaseManager._TwoPi * Math.asin(1 / overshootOrAmplitude);
                    return (overshootOrAmplitude * Math.pow(2, -10 * time) * Math.sin((time * duration - s1) * EaseManager._TwoPi / period) + 1);
                case fairygui.EaseType.ElasticInOut:
                    var s;
                    if (time == 0)
                        return 0;
                    if ((time /= duration * 0.5) == 2)
                        return 1;
                    if (period == 0)
                        period = duration * (0.3 * 1.5);
                    if (overshootOrAmplitude < 1) {
                        overshootOrAmplitude = 1;
                        s = period / 4;
                    }
                    else
                        s = period / EaseManager._TwoPi * Math.asin(1 / overshootOrAmplitude);
                    if (time < 1)
                        return -0.5 * (overshootOrAmplitude * Math.pow(2, 10 * (time -= 1)) * Math.sin((time * duration - s) * EaseManager._TwoPi / period));
                    return overshootOrAmplitude * Math.pow(2, -10 * (time -= 1)) * Math.sin((time * duration - s) * EaseManager._TwoPi / period) * 0.5 + 1;
                case fairygui.EaseType.BackIn:
                    return (time /= duration) * time * ((overshootOrAmplitude + 1) * time - overshootOrAmplitude);
                case fairygui.EaseType.BackOut:
                    return ((time = time / duration - 1) * time * ((overshootOrAmplitude + 1) * time + overshootOrAmplitude) + 1);
                case fairygui.EaseType.BackInOut:
                    if ((time /= duration * 0.5) < 1)
                        return 0.5 * (time * time * (((overshootOrAmplitude *= (1.525)) + 1) * time - overshootOrAmplitude));
                    return 0.5 * ((time -= 2) * time * (((overshootOrAmplitude *= (1.525)) + 1) * time + overshootOrAmplitude) + 2);
                case fairygui.EaseType.BounceIn:
                    return Bounce.easeIn(time, duration);
                case fairygui.EaseType.BounceOut:
                    return Bounce.easeOut(time, duration);
                case fairygui.EaseType.BounceInOut:
                    return Bounce.easeInOut(time, duration);
                default:
                    return -(time /= duration) * (time - 2);
            }
        };
        EaseManager._PiOver2 = Math.PI * 0.5;
        EaseManager._TwoPi = Math.PI * 2;
        return EaseManager;
    }());
    fairygui.EaseManager = EaseManager;
    var Bounce = /** @class */ (function () {
        function Bounce() {
        }
        Bounce.easeIn = function (time, duration) {
            return 1 - Bounce.easeOut(duration - time, duration);
        };
        Bounce.easeOut = function (time, duration) {
            if ((time /= duration) < (1 / 2.75)) {
                return (7.5625 * time * time);
            }
            if (time < (2 / 2.75)) {
                return (7.5625 * (time -= (1.5 / 2.75)) * time + 0.75);
            }
            if (time < (2.5 / 2.75)) {
                return (7.5625 * (time -= (2.25 / 2.75)) * time + 0.9375);
            }
            return (7.5625 * (time -= (2.625 / 2.75)) * time + 0.984375);
        };
        Bounce.easeInOut = function (time, duration) {
            if (time < duration * 0.5) {
                return Bounce.easeIn(time * 2, duration) * 0.5;
            }
            return Bounce.easeOut(time * 2 - duration, duration) * 0.5 + 0.5;
        };
        return Bounce;
    }());
})(fairygui || (fairygui = {}));
var fairygui;
(function (fairygui) {
    var EaseType = /** @class */ (function () {
        function EaseType() {
        }
        EaseType.parseEaseType = function (value) {
            var type = EaseType.easeTypeMap[value];
            if (type == undefined)
                return EaseType.ExpoOut;
            else
                return type;
        };
        EaseType.Linear = 0;
        EaseType.SineIn = 1;
        EaseType.SineOut = 2;
        EaseType.SineInOut = 3;
        EaseType.QuadIn = 4;
        EaseType.QuadOut = 5;
        EaseType.QuadInOut = 6;
        EaseType.CubicIn = 7;
        EaseType.CubicOut = 8;
        EaseType.CubicInOut = 9;
        EaseType.QuartIn = 10;
        EaseType.QuartOut = 11;
        EaseType.QuartInOut = 12;
        EaseType.QuintIn = 13;
        EaseType.QuintOut = 14;
        EaseType.QuintInOut = 15;
        EaseType.ExpoIn = 16;
        EaseType.ExpoOut = 17;
        EaseType.ExpoInOut = 18;
        EaseType.CircIn = 19;
        EaseType.CircOut = 20;
        EaseType.CircInOut = 21;
        EaseType.ElasticIn = 22;
        EaseType.ElasticOut = 23;
        EaseType.ElasticInOut = 24;
        EaseType.BackIn = 25;
        EaseType.BackOut = 26;
        EaseType.BackInOut = 27;
        EaseType.BounceIn = 28;
        EaseType.BounceOut = 29;
        EaseType.BounceInOut = 30;
        EaseType.Custom = 31;
        EaseType.easeTypeMap = {
            "Linear": EaseType.Linear,
            "Elastic.In": EaseType.ElasticIn,
            "Elastic.Out": EaseType.ElasticInOut,
            "Elastic.InOut": EaseType.ElasticInOut,
            "Quad.In": EaseType.QuadIn,
            "Quad.Out": EaseType.QuadOut,
            "Quad.InOut": EaseType.QuadInOut,
            "Cube.In": EaseType.CubicIn,
            "Cube.Out": EaseType.CubicOut,
            "Cube.InOut": EaseType.CubicInOut,
            "Quart.In": EaseType.QuartIn,
            "Quart.Out": EaseType.QuartOut,
            "Quart.InOut": EaseType.QuartInOut,
            "Quint.In": EaseType.QuintIn,
            "Quint.Out": EaseType.QuintOut,
            "Quint.InOut": EaseType.QuintInOut,
            "Sine.In": EaseType.SineIn,
            "Sine.Out": EaseType.SineOut,
            "Sine.InOut": EaseType.SineInOut,
            "Bounce.In": EaseType.BounceIn,
            "Bounce.Out": EaseType.BounceOut,
            "Bounce.InOut": EaseType.BounceInOut,
            "Circ.In": EaseType.CircIn,
            "Circ.Out": EaseType.CircOut,
            "Circ.InOut": EaseType.CircInOut,
            "Expo.In": EaseType.ExpoIn,
            "Expo.Out": EaseType.ExpoOut,
            "Expo.InOut": EaseType.ExpoInOut,
            "Back.In": EaseType.BackIn,
            "Back.Out": EaseType.BackOut,
            "Back.InOut": EaseType.BackInOut
        };
        return EaseType;
    }());
    fairygui.EaseType = EaseType;
})(fairygui || (fairygui = {}));
var fairygui;
(function (fairygui) {
    var GTween = /** @class */ (function () {
        function GTween() {
        }
        GTween.to = function (start, end, duration) {
            return fairygui.TweenManager.createTween()._to(start, end, duration);
        };
        GTween.to2 = function (start, start2, end, end2, duration) {
            return fairygui.TweenManager.createTween()._to2(start, start2, end, end2, duration);
        };
        GTween.to3 = function (start, start2, start3, end, end2, end3, duration) {
            return fairygui.TweenManager.createTween()._to3(start, start2, start3, end, end2, end3, duration);
        };
        GTween.to4 = function (start, start2, start3, start4, end, end2, end3, end4, duration) {
            return fairygui.TweenManager.createTween()._to4(start, start2, start3, start4, end, end2, end3, end4, duration);
        };
        GTween.toColor = function (start, end, duration) {
            return fairygui.TweenManager.createTween()._toColor(start, end, duration);
        };
        GTween.delayedCall = function (delay) {
            return fairygui.TweenManager.createTween().setDelay(delay);
        };
        GTween.shake = function (startX, startY, amplitude, duration) {
            return fairygui.TweenManager.createTween()._shake(startX, startY, amplitude, duration);
        };
        GTween.isTweening = function (target, propType) {
            return fairygui.TweenManager.isTweening(target, propType);
        };
        GTween.kill = function (target, complete, propType) {
            if (complete === void 0) { complete = false; }
            if (propType === void 0) { propType = null; }
            fairygui.TweenManager.killTweens(target, false, null);
        };
        GTween.getTween = function (target, propType) {
            if (propType === void 0) { propType = null; }
            return fairygui.TweenManager.getTween(target, propType);
        };
        GTween.safeMode = true;
        return GTween;
    }());
    fairygui.GTween = GTween;
})(fairygui || (fairygui = {}));
var fairygui;
(function (fairygui) {
    var GTweener = /** @class */ (function () {
        function GTweener() {
            this._startValue = new fairygui.TweenValue();
            this._endValue = new fairygui.TweenValue();
            this._value = new fairygui.TweenValue();
            this._deltaValue = new fairygui.TweenValue();
            this._reset();
        }
        GTweener.prototype.setDelay = function (value) {
            this._delay = value;
            return this;
        };
        Object.defineProperty(GTweener.prototype, "delay", {
            get: function () {
                return this._delay;
            },
            enumerable: true,
            configurable: true
        });
        GTweener.prototype.setDuration = function (value) {
            this._duration = value;
            return this;
        };
        Object.defineProperty(GTweener.prototype, "duration", {
            get: function () {
                return this._duration;
            },
            enumerable: true,
            configurable: true
        });
        GTweener.prototype.setBreakpoint = function (value) {
            this._breakpoint = value;
            return this;
        };
        GTweener.prototype.setEase = function (value) {
            this._easeType = value;
            return this;
        };
        GTweener.prototype.setEasePeriod = function (value) {
            this._easePeriod = value;
            return this;
        };
        GTweener.prototype.setEaseOvershootOrAmplitude = function (value) {
            this._easeOvershootOrAmplitude = value;
            return this;
        };
        GTweener.prototype.setRepeat = function (repeat, yoyo) {
            if (yoyo === void 0) { yoyo = false; }
            this._repeat = repeat;
            this._yoyo = yoyo;
            return this;
        };
        Object.defineProperty(GTweener.prototype, "repeat", {
            get: function () {
                return this._repeat;
            },
            enumerable: true,
            configurable: true
        });
        GTweener.prototype.setTimeScale = function (value) {
            this._timeScale = value;
            return this;
        };
        GTweener.prototype.setSnapping = function (value) {
            this._snapping = value;
            return this;
        };
        GTweener.prototype.setTarget = function (value, propType) {
            if (propType === void 0) { propType = null; }
            this._target = value;
            this._propType = propType;
            return this;
        };
        Object.defineProperty(GTweener.prototype, "target", {
            get: function () {
                return this._target;
            },
            enumerable: true,
            configurable: true
        });
        GTweener.prototype.setUserData = function (value) {
            this._userData = value;
            return this;
        };
        Object.defineProperty(GTweener.prototype, "userData", {
            get: function () {
                return this._userData;
            },
            enumerable: true,
            configurable: true
        });
        GTweener.prototype.onUpdate = function (callback, caller) {
            this._onUpdate = callback;
            this._onUpdateCaller = caller;
            return this;
        };
        GTweener.prototype.onStart = function (callback, caller) {
            this._onStart = callback;
            this._onStartCaller = caller;
            return this;
        };
        GTweener.prototype.onComplete = function (callback, caller) {
            this._onComplete = callback;
            this._onCompleteCaller = caller;
            return this;
        };
        Object.defineProperty(GTweener.prototype, "startValue", {
            get: function () {
                return this._startValue;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GTweener.prototype, "endValue", {
            get: function () {
                return this._endValue;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GTweener.prototype, "value", {
            get: function () {
                return this._value;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GTweener.prototype, "deltaValue", {
            get: function () {
                return this._deltaValue;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GTweener.prototype, "normalizedTime", {
            get: function () {
                return this._normalizedTime;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GTweener.prototype, "completed", {
            get: function () {
                return this._ended != 0;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GTweener.prototype, "allCompleted", {
            get: function () {
                return this._ended == 1;
            },
            enumerable: true,
            configurable: true
        });
        GTweener.prototype.setPaused = function (paused) {
            this._paused = paused;
            return this;
        };
        /**
         * seek position of the tween, in seconds.
         */
        GTweener.prototype.seek = function (time) {
            if (this._killed)
                return;
            this._elapsedTime = time;
            if (this._elapsedTime < this._delay) {
                if (this._started)
                    this._elapsedTime = this._delay;
                else
                    return;
            }
            this.update();
        };
        GTweener.prototype.kill = function (complete) {
            if (complete === void 0) { complete = false; }
            if (this._killed)
                return;
            if (complete) {
                if (this._ended == 0) {
                    if (this._breakpoint >= 0)
                        this._elapsedTime = this._delay + this._breakpoint;
                    else if (this._repeat >= 0)
                        this._elapsedTime = this._delay + this._duration * (this._repeat + 1);
                    else
                        this._elapsedTime = this._delay + this._duration * 2;
                    this.update();
                }
                this.callCompleteCallback();
            }
            this._killed = true;
        };
        GTweener.prototype._to = function (start, end, duration) {
            this._valueSize = 1;
            this._startValue.x = start;
            this._endValue.x = end;
            this._duration = duration;
            return this;
        };
        GTweener.prototype._to2 = function (start, start2, end, end2, duration) {
            this._valueSize = 2;
            this._startValue.x = start;
            this._endValue.x = end;
            this._startValue.y = start2;
            this._endValue.y = end2;
            this._duration = duration;
            return this;
        };
        GTweener.prototype._to3 = function (start, start2, start3, end, end2, end3, duration) {
            this._valueSize = 3;
            this._startValue.x = start;
            this._endValue.x = end;
            this._startValue.y = start2;
            this._endValue.y = end2;
            this._startValue.z = start3;
            this._endValue.z = end3;
            this._duration = duration;
            return this;
        };
        GTweener.prototype._to4 = function (start, start2, start3, start4, end, end2, end3, end4, duration) {
            this._valueSize = 4;
            this._startValue.x = start;
            this._endValue.x = end;
            this._startValue.y = start2;
            this._endValue.y = end2;
            this._startValue.z = start3;
            this._endValue.z = end3;
            this._startValue.w = start4;
            this._endValue.w = end4;
            this._duration = duration;
            return this;
        };
        GTweener.prototype._toColor = function (start, end, duration) {
            this._valueSize = 4;
            this._startValue.color = start;
            this._endValue.color = end;
            this._duration = duration;
            return this;
        };
        GTweener.prototype._shake = function (startX, startY, amplitude, duration) {
            this._valueSize = 5;
            this._startValue.x = startX;
            this._startValue.y = startY;
            this._startValue.w = amplitude;
            this._duration = duration;
            return this;
        };
        GTweener.prototype._init = function () {
            this._delay = 0;
            this._duration = 0;
            this._breakpoint = -1;
            this._easeType = fairygui.EaseType.QuadOut;
            this._timeScale = 1;
            this._easePeriod = 0;
            this._easeOvershootOrAmplitude = 1.70158;
            this._snapping = false;
            this._repeat = 0;
            this._yoyo = false;
            this._valueSize = 0;
            this._started = false;
            this._paused = false;
            this._killed = false;
            this._elapsedTime = 0;
            this._normalizedTime = 0;
            this._ended = 0;
        };
        GTweener.prototype._reset = function () {
            this._target = null;
            this._userData = null;
            this._onStart = this._onUpdate = this._onComplete = null;
            this._onStartCaller = this._onUpdateCaller = this._onCompleteCaller = null;
        };
        GTweener.prototype._update = function (dt) {
            if (this._timeScale != 1)
                dt *= this._timeScale;
            if (dt == 0)
                return;
            if (this._ended != 0) //Maybe completed by seek
             {
                this.callCompleteCallback();
                this._killed = true;
                return;
            }
            this._elapsedTime += dt;
            this.update();
            if (this._ended != 0) {
                if (!this._killed) {
                    this.callCompleteCallback();
                    this._killed = true;
                }
            }
        };
        GTweener.prototype.update = function () {
            this._ended = 0;
            if (this._valueSize == 0) //DelayedCall
             {
                if (this._elapsedTime >= this._delay + this._duration)
                    this._ended = 1;
                return;
            }
            if (!this._started) {
                if (this._elapsedTime < this._delay)
                    return;
                this._started = true;
                this.callStartCallback();
                if (this._killed)
                    return;
            }
            var reversed = false;
            var tt = this._elapsedTime - this._delay;
            if (this._breakpoint >= 0 && tt >= this._breakpoint) {
                tt = this._breakpoint;
                this._ended = 2;
            }
            if (this._repeat != 0) {
                var round = Math.floor(tt / this._duration);
                tt -= this._duration * round;
                if (this._yoyo)
                    reversed = round % 2 == 1;
                if (this._repeat > 0 && this._repeat - round < 0) {
                    if (this._yoyo)
                        reversed = this._repeat % 2 == 1;
                    tt = this._duration;
                    this._ended = 1;
                }
            }
            else if (tt >= this._duration) {
                tt = this._duration;
                this._ended = 1;
            }
            this._normalizedTime = fairygui.EaseManager.evaluate(this._easeType, reversed ? (this._duration - tt) : tt, this._duration, this._easeOvershootOrAmplitude, this._easePeriod);
            this._value.setZero();
            this._deltaValue.setZero();
            if (this._valueSize == 5) {
                if (this._ended == 0) {
                    var r = this._startValue.w * (1 - this._normalizedTime);
                    var rx = r * (Math.random() > 0.5 ? 1 : -1);
                    var ry = r * (Math.random() > 0.5 ? 1 : -1);
                    this._deltaValue.x = rx;
                    this._deltaValue.y = ry;
                    this._value.x = this._startValue.x + rx;
                    this._value.y = this._startValue.y + ry;
                }
                else {
                    this._value.x = this._startValue.x;
                    this._value.y = this._startValue.y;
                }
            }
            else {
                for (var i = 0; i < this._valueSize; i++) {
                    var n1 = this._startValue.getField(i);
                    var n2 = this._endValue.getField(i);
                    var f = n1 + (n2 - n1) * this._normalizedTime;
                    if (this._snapping)
                        f = Math.round(f);
                    this._deltaValue.setField(i, f - this._value.getField(i));
                    this._value.setField(i, f);
                }
            }
            if (this._target != null && this._propType != null) {
                if (this._propType instanceof Function) {
                    switch (this._valueSize) {
                        case 1:
                            this._propType.call(this._target, this._value.x);
                            break;
                        case 2:
                            this._propType.call(this._target, this._value.x, this._value.y);
                            break;
                        case 3:
                            this._propType.call(this._target, this._value.x, this._value.y, this._value.z);
                            break;
                        case 4:
                            this._propType.call(this._target, this._value.x, this._value.y, this._value.z, this._value.w);
                            break;
                        case 5:
                            this._propType.call(this._target, this._value.color);
                            break;
                        case 6:
                            this._propType.call(this._target, this._value.x, this._value.y);
                            break;
                    }
                }
                else {
                    if (this._valueSize == 5)
                        this._target[this._propType] = this._value.color;
                    else
                        this._target[this._propType] = this._value.x;
                }
            }
            this.callUpdateCallback();
        };
        GTweener.prototype.callStartCallback = function () {
            if (this._onStart != null) {
                try {
                    this._onStart.call(this._onStartCaller, this);
                }
                catch (err) {
                    console.log("FairyGUI: error in start callback > " + err);
                }
            }
        };
        GTweener.prototype.callUpdateCallback = function () {
            if (this._onUpdate != null) {
                try {
                    this._onUpdate.call(this._onUpdateCaller, this);
                }
                catch (err) {
                    console.log("FairyGUI: error in update callback > " + err);
                }
            }
        };
        GTweener.prototype.callCompleteCallback = function () {
            if (this._onComplete != null) {
                try {
                    this._onComplete.call(this._onCompleteCaller, this);
                }
                catch (err) {
                    console.log("FairyGUI: error in complete callback > " + err);
                }
            }
        };
        return GTweener;
    }());
    fairygui.GTweener = GTweener;
})(fairygui || (fairygui = {}));
var fairygui;
(function (fairygui) {
    var TweenManager = /** @class */ (function () {
        function TweenManager() {
        }
        TweenManager.createTween = function () {
            if (!TweenManager._inited) {
                egret.startTick(TweenManager.update, null);
                TweenManager._inited = true;
                TweenManager._lastTime = egret.getTimer();
            }
            var tweener;
            var cnt = TweenManager._tweenerPool.length;
            if (cnt > 0) {
                tweener = TweenManager._tweenerPool.pop();
            }
            else
                tweener = new fairygui.GTweener();
            tweener._init();
            TweenManager._activeTweens[TweenManager._totalActiveTweens++] = tweener;
            if (TweenManager._totalActiveTweens == TweenManager._activeTweens.length)
                TweenManager._activeTweens.length = TweenManager._activeTweens.length + Math.ceil(TweenManager._activeTweens.length * 0.5);
            return tweener;
        };
        TweenManager.isTweening = function (target, propType) {
            if (target == null)
                return false;
            var anyType = propType == null || propType == undefined;
            for (var i = 0; i < TweenManager._totalActiveTweens; i++) {
                var tweener = TweenManager._activeTweens[i];
                if (tweener != null && tweener.target == target && !tweener._killed
                    && (anyType || tweener._propType == propType))
                    return true;
            }
            return false;
        };
        TweenManager.killTweens = function (target, completed, propType) {
            if (target == null)
                return false;
            var flag = false;
            var cnt = TweenManager._totalActiveTweens;
            var anyType = propType == null || propType == undefined;
            for (var i = 0; i < cnt; i++) {
                var tweener = TweenManager._activeTweens[i];
                if (tweener != null && tweener.target == target && !tweener._killed
                    && (anyType || tweener._propType == propType)) {
                    tweener.kill(completed);
                    flag = true;
                }
            }
            return flag;
        };
        TweenManager.getTween = function (target, propType) {
            if (target == null)
                return null;
            var cnt = TweenManager._totalActiveTweens;
            var anyType = propType == null || propType == undefined;
            for (var i = 0; i < cnt; i++) {
                var tweener = TweenManager._activeTweens[i];
                if (tweener != null && tweener.target == target && !tweener._killed
                    && (anyType || tweener._propType == propType)) {
                    return tweener;
                }
            }
            return null;
        };
        TweenManager.update = function (timestamp) {
            var dt = timestamp - TweenManager._lastTime;
            TweenManager._lastTime = timestamp;
            if (dt > 100)
                dt = 100;
            dt /= 1000;
            var cnt = TweenManager._totalActiveTweens;
            var freePosStart = -1;
            var freePosCount = 0;
            for (var i = 0; i < cnt; i++) {
                var tweener = TweenManager._activeTweens[i];
                if (tweener == null) {
                    if (freePosStart == -1)
                        freePosStart = i;
                    freePosCount++;
                }
                else if (tweener._killed) {
                    tweener._reset();
                    TweenManager._tweenerPool.push(tweener);
                    TweenManager._activeTweens[i] = null;
                    if (freePosStart == -1)
                        freePosStart = i;
                    freePosCount++;
                }
                else {
                    if (!tweener._paused)
                        tweener._update(dt);
                    if (freePosStart != -1) {
                        TweenManager._activeTweens[freePosStart] = tweener;
                        TweenManager._activeTweens[i] = null;
                        freePosStart++;
                    }
                }
            }
            if (freePosStart >= 0) {
                if (TweenManager._totalActiveTweens != cnt) //new tweens added
                 {
                    var j = cnt;
                    cnt = TweenManager._totalActiveTweens - cnt;
                    for (i = 0; i < cnt; i++)
                        TweenManager._activeTweens[freePosStart++] = TweenManager._activeTweens[j++];
                }
                TweenManager._totalActiveTweens = freePosStart;
            }
            return false;
        };
        TweenManager._activeTweens = new Array(30);
        TweenManager._tweenerPool = new Array();
        TweenManager._totalActiveTweens = 0;
        TweenManager._lastTime = 0;
        TweenManager._inited = false;
        return TweenManager;
    }());
    fairygui.TweenManager = TweenManager;
})(fairygui || (fairygui = {}));
var fairygui;
(function (fairygui) {
    var TweenValue = /** @class */ (function () {
        function TweenValue() {
            this.x = this.y = this.z = this.w = 0;
        }
        Object.defineProperty(TweenValue.prototype, "color", {
            get: function () {
                return (this.w << 24) + (this.x << 16) + (this.y << 8) + this.z;
            },
            set: function (value) {
                this.x = (value & 0xFF0000) >> 16;
                this.y = (value & 0x00FF00) >> 8;
                this.z = (value & 0x0000FF);
                this.w = (value & 0xFF000000) >> 24;
            },
            enumerable: true,
            configurable: true
        });
        TweenValue.prototype.getField = function (index) {
            switch (index) {
                case 0:
                    return this.x;
                case 1:
                    return this.y;
                case 2:
                    return this.z;
                case 3:
                    return this.w;
                default:
                    throw new Error("Index out of bounds: " + index);
            }
        };
        TweenValue.prototype.setField = function (index, value) {
            switch (index) {
                case 0:
                    this.x = value;
                    break;
                case 1:
                    this.y = value;
                    break;
                case 2:
                    this.z = value;
                    break;
                case 3:
                    this.w = value;
                    break;
                default:
                    throw new Error("Index out of bounds: " + index);
            }
        };
        TweenValue.prototype.setZero = function () {
            this.x = this.y = this.z = this.w = 0;
        };
        return TweenValue;
    }());
    fairygui.TweenValue = TweenValue;
})(fairygui || (fairygui = {}));
var fairygui;
(function (fairygui) {
    var ButtonMode;
    (function (ButtonMode) {
        ButtonMode[ButtonMode["Common"] = 0] = "Common";
        ButtonMode[ButtonMode["Check"] = 1] = "Check";
        ButtonMode[ButtonMode["Radio"] = 2] = "Radio";
    })(ButtonMode = fairygui.ButtonMode || (fairygui.ButtonMode = {}));
    ;
    var AutoSizeType;
    (function (AutoSizeType) {
        AutoSizeType[AutoSizeType["None"] = 0] = "None";
        AutoSizeType[AutoSizeType["Both"] = 1] = "Both";
        AutoSizeType[AutoSizeType["Height"] = 2] = "Height";
    })(AutoSizeType = fairygui.AutoSizeType || (fairygui.AutoSizeType = {}));
    ;
    var AlignType;
    (function (AlignType) {
        AlignType[AlignType["Left"] = 0] = "Left";
        AlignType[AlignType["Center"] = 1] = "Center";
        AlignType[AlignType["Right"] = 2] = "Right";
    })(AlignType = fairygui.AlignType || (fairygui.AlignType = {}));
    ;
    var VertAlignType;
    (function (VertAlignType) {
        VertAlignType[VertAlignType["Top"] = 0] = "Top";
        VertAlignType[VertAlignType["Middle"] = 1] = "Middle";
        VertAlignType[VertAlignType["Bottom"] = 2] = "Bottom";
    })(VertAlignType = fairygui.VertAlignType || (fairygui.VertAlignType = {}));
    ;
    var LoaderFillType;
    (function (LoaderFillType) {
        LoaderFillType[LoaderFillType["None"] = 0] = "None";
        LoaderFillType[LoaderFillType["Scale"] = 1] = "Scale";
        LoaderFillType[LoaderFillType["ScaleMatchHeight"] = 2] = "ScaleMatchHeight";
        LoaderFillType[LoaderFillType["ScaleMatchWidth"] = 3] = "ScaleMatchWidth";
        LoaderFillType[LoaderFillType["ScaleFree"] = 4] = "ScaleFree";
        LoaderFillType[LoaderFillType["ScaleNoBorder"] = 5] = "ScaleNoBorder";
    })(LoaderFillType = fairygui.LoaderFillType || (fairygui.LoaderFillType = {}));
    ;
    var ListLayoutType;
    (function (ListLayoutType) {
        ListLayoutType[ListLayoutType["SingleColumn"] = 0] = "SingleColumn";
        ListLayoutType[ListLayoutType["SingleRow"] = 1] = "SingleRow";
        ListLayoutType[ListLayoutType["FlowHorizontal"] = 2] = "FlowHorizontal";
        ListLayoutType[ListLayoutType["FlowVertical"] = 3] = "FlowVertical";
        ListLayoutType[ListLayoutType["Pagination"] = 4] = "Pagination";
    })(ListLayoutType = fairygui.ListLayoutType || (fairygui.ListLayoutType = {}));
    ;
    var ListSelectionMode;
    (function (ListSelectionMode) {
        ListSelectionMode[ListSelectionMode["Single"] = 0] = "Single";
        ListSelectionMode[ListSelectionMode["Multiple"] = 1] = "Multiple";
        ListSelectionMode[ListSelectionMode["Multiple_SingleClick"] = 2] = "Multiple_SingleClick";
        ListSelectionMode[ListSelectionMode["None"] = 3] = "None";
    })(ListSelectionMode = fairygui.ListSelectionMode || (fairygui.ListSelectionMode = {}));
    ;
    var OverflowType;
    (function (OverflowType) {
        OverflowType[OverflowType["Visible"] = 0] = "Visible";
        OverflowType[OverflowType["Hidden"] = 1] = "Hidden";
        OverflowType[OverflowType["Scroll"] = 2] = "Scroll";
        OverflowType[OverflowType["Scale"] = 3] = "Scale";
        OverflowType[OverflowType["ScaleFree"] = 4] = "ScaleFree";
    })(OverflowType = fairygui.OverflowType || (fairygui.OverflowType = {}));
    ;
    var PackageItemType;
    (function (PackageItemType) {
        PackageItemType[PackageItemType["Image"] = 0] = "Image";
        PackageItemType[PackageItemType["Swf"] = 1] = "Swf";
        PackageItemType[PackageItemType["MovieClip"] = 2] = "MovieClip";
        PackageItemType[PackageItemType["Sound"] = 3] = "Sound";
        PackageItemType[PackageItemType["Component"] = 4] = "Component";
        PackageItemType[PackageItemType["Misc"] = 5] = "Misc";
        PackageItemType[PackageItemType["Font"] = 6] = "Font";
        PackageItemType[PackageItemType["Atlas"] = 7] = "Atlas";
    })(PackageItemType = fairygui.PackageItemType || (fairygui.PackageItemType = {}));
    ;
    var ProgressTitleType;
    (function (ProgressTitleType) {
        ProgressTitleType[ProgressTitleType["Percent"] = 0] = "Percent";
        ProgressTitleType[ProgressTitleType["ValueAndMax"] = 1] = "ValueAndMax";
        ProgressTitleType[ProgressTitleType["Value"] = 2] = "Value";
        ProgressTitleType[ProgressTitleType["Max"] = 3] = "Max";
    })(ProgressTitleType = fairygui.ProgressTitleType || (fairygui.ProgressTitleType = {}));
    ;
    var ScrollBarDisplayType;
    (function (ScrollBarDisplayType) {
        ScrollBarDisplayType[ScrollBarDisplayType["Default"] = 0] = "Default";
        ScrollBarDisplayType[ScrollBarDisplayType["Visible"] = 1] = "Visible";
        ScrollBarDisplayType[ScrollBarDisplayType["Auto"] = 2] = "Auto";
        ScrollBarDisplayType[ScrollBarDisplayType["Hidden"] = 3] = "Hidden";
    })(ScrollBarDisplayType = fairygui.ScrollBarDisplayType || (fairygui.ScrollBarDisplayType = {}));
    ;
    var ScrollType;
    (function (ScrollType) {
        ScrollType[ScrollType["Horizontal"] = 0] = "Horizontal";
        ScrollType[ScrollType["Vertical"] = 1] = "Vertical";
        ScrollType[ScrollType["Both"] = 2] = "Both";
    })(ScrollType = fairygui.ScrollType || (fairygui.ScrollType = {}));
    ;
    var FlipType;
    (function (FlipType) {
        FlipType[FlipType["None"] = 0] = "None";
        FlipType[FlipType["Horizontal"] = 1] = "Horizontal";
        FlipType[FlipType["Vertical"] = 2] = "Vertical";
        FlipType[FlipType["Both"] = 3] = "Both";
    })(FlipType = fairygui.FlipType || (fairygui.FlipType = {}));
    ;
    var ChildrenRenderOrder;
    (function (ChildrenRenderOrder) {
        ChildrenRenderOrder[ChildrenRenderOrder["Ascent"] = 0] = "Ascent";
        ChildrenRenderOrder[ChildrenRenderOrder["Descent"] = 1] = "Descent";
        ChildrenRenderOrder[ChildrenRenderOrder["Arch"] = 2] = "Arch";
    })(ChildrenRenderOrder = fairygui.ChildrenRenderOrder || (fairygui.ChildrenRenderOrder = {}));
    ;
    var GroupLayoutType;
    (function (GroupLayoutType) {
        GroupLayoutType[GroupLayoutType["None"] = 0] = "None";
        GroupLayoutType[GroupLayoutType["Horizontal"] = 1] = "Horizontal";
        GroupLayoutType[GroupLayoutType["Vertical"] = 2] = "Vertical";
    })(GroupLayoutType = fairygui.GroupLayoutType || (fairygui.GroupLayoutType = {}));
    ;
    var PopupDirection;
    (function (PopupDirection) {
        PopupDirection[PopupDirection["AUTO"] = 0] = "AUTO";
        PopupDirection[PopupDirection["UP"] = 1] = "UP";
        PopupDirection[PopupDirection["DOWN"] = 2] = "DOWN";
    })(PopupDirection = fairygui.PopupDirection || (fairygui.PopupDirection = {}));
    ;
    var UIEventType;
    (function (UIEventType) {
        UIEventType[UIEventType["Enter"] = 0] = "Enter";
        UIEventType[UIEventType["Exit"] = 1] = "Exit";
        UIEventType[UIEventType["Changed"] = 2] = "Changed";
        UIEventType[UIEventType["Submit"] = 3] = "Submit";
        UIEventType[UIEventType["TouchBegin"] = 10] = "TouchBegin";
        UIEventType[UIEventType["TouchMove"] = 11] = "TouchMove";
        UIEventType[UIEventType["TouchEnd"] = 12] = "TouchEnd";
        UIEventType[UIEventType["Click"] = 13] = "Click";
        UIEventType[UIEventType["RollOver"] = 14] = "RollOver";
        UIEventType[UIEventType["RollOut"] = 15] = "RollOut";
        UIEventType[UIEventType["MouseWheel"] = 16] = "MouseWheel";
        UIEventType[UIEventType["RightClick"] = 17] = "RightClick";
        UIEventType[UIEventType["MiddleClick"] = 18] = "MiddleClick";
        UIEventType[UIEventType["PositionChange"] = 20] = "PositionChange";
        UIEventType[UIEventType["SizeChange"] = 21] = "SizeChange";
        UIEventType[UIEventType["KeyDown"] = 30] = "KeyDown";
        UIEventType[UIEventType["KeyUp"] = 31] = "KeyUp";
        UIEventType[UIEventType["Scroll"] = 40] = "Scroll";
        UIEventType[UIEventType["ScrollEnd"] = 41] = "ScrollEnd";
        UIEventType[UIEventType["PullDownRelease"] = 42] = "PullDownRelease";
        UIEventType[UIEventType["PullUpRelease"] = 43] = "PullUpRelease";
        UIEventType[UIEventType["ClickItem"] = 50] = "ClickItem";
        UIEventType[UIEventType["ClickLink"] = 51] = "ClickLink";
        UIEventType[UIEventType["ClickMenu"] = 52] = "ClickMenu";
        UIEventType[UIEventType["RightClickItem"] = 53] = "RightClickItem";
        UIEventType[UIEventType["DragStart"] = 60] = "DragStart";
        UIEventType[UIEventType["DragMove"] = 61] = "DragMove";
        UIEventType[UIEventType["DragEnd"] = 62] = "DragEnd";
        UIEventType[UIEventType["Drop"] = 63] = "Drop";
        UIEventType[UIEventType["GearStop"] = 70] = "GearStop";
    })(UIEventType = fairygui.UIEventType || (fairygui.UIEventType = {}));
    ;
    var RelationType;
    (function (RelationType) {
        RelationType[RelationType["Left_Left"] = 0] = "Left_Left";
        RelationType[RelationType["Left_Center"] = 1] = "Left_Center";
        RelationType[RelationType["Left_Right"] = 2] = "Left_Right";
        RelationType[RelationType["Center_Center"] = 3] = "Center_Center";
        RelationType[RelationType["Right_Left"] = 4] = "Right_Left";
        RelationType[RelationType["Right_Center"] = 5] = "Right_Center";
        RelationType[RelationType["Right_Right"] = 6] = "Right_Right";
        RelationType[RelationType["Top_Top"] = 7] = "Top_Top";
        RelationType[RelationType["Top_Middle"] = 8] = "Top_Middle";
        RelationType[RelationType["Top_Bottom"] = 9] = "Top_Bottom";
        RelationType[RelationType["Middle_Middle"] = 10] = "Middle_Middle";
        RelationType[RelationType["Bottom_Top"] = 11] = "Bottom_Top";
        RelationType[RelationType["Bottom_Middle"] = 12] = "Bottom_Middle";
        RelationType[RelationType["Bottom_Bottom"] = 13] = "Bottom_Bottom";
        RelationType[RelationType["Width"] = 14] = "Width";
        RelationType[RelationType["Height"] = 15] = "Height";
        RelationType[RelationType["LeftExt_Left"] = 16] = "LeftExt_Left";
        RelationType[RelationType["LeftExt_Right"] = 17] = "LeftExt_Right";
        RelationType[RelationType["RightExt_Left"] = 18] = "RightExt_Left";
        RelationType[RelationType["RightExt_Right"] = 19] = "RightExt_Right";
        RelationType[RelationType["TopExt_Top"] = 20] = "TopExt_Top";
        RelationType[RelationType["TopExt_Bottom"] = 21] = "TopExt_Bottom";
        RelationType[RelationType["BottomExt_Top"] = 22] = "BottomExt_Top";
        RelationType[RelationType["BottomExt_Bottom"] = 23] = "BottomExt_Bottom";
        RelationType[RelationType["Size"] = 24] = "Size";
    })(RelationType = fairygui.RelationType || (fairygui.RelationType = {}));
    ;
    function parseButtonMode(value) {
        switch (value) {
            case "Common":
                return ButtonMode.Common;
            case "Check":
                return ButtonMode.Check;
            case "Radio":
                return ButtonMode.Radio;
            default:
                return ButtonMode.Common;
        }
    }
    fairygui.parseButtonMode = parseButtonMode;
    function parseAutoSizeType(value) {
        switch (value) {
            case "none":
                return AutoSizeType.None;
            case "both":
                return AutoSizeType.Both;
            case "height":
                return AutoSizeType.Height;
            default:
                return AutoSizeType.None;
        }
    }
    fairygui.parseAutoSizeType = parseAutoSizeType;
    function parseAlignType(value) {
        switch (value) {
            case "left":
                return AlignType.Left;
            case "center":
                return AlignType.Center;
            case "right":
                return AlignType.Right;
            default:
                return AlignType.Left;
        }
    }
    fairygui.parseAlignType = parseAlignType;
    // export function getAlignTypeString(type: AlignType): string {
    //     return type == AlignType.Left ? egret.HorizontalAlign.LEFT :
    //         (type == AlignType.Center ? egret.HorizontalAlign.CENTER : egret.HorizontalAlign.RIGHT);
    // }
    // export function getVertAlignTypeString(type: VertAlignType): string {
    //     return type == VertAlignType.Top ? egret.VerticalAlign.TOP :
    //         (type == VertAlignType.Middle ? egret.VerticalAlign.MIDDLE : egret.VerticalAlign.BOTTOM);
    // }
    function parseVertAlignType(value) {
        switch (value) {
            case "top":
                return VertAlignType.Top;
            case "middle":
                return VertAlignType.Middle;
            case "bottom":
                return VertAlignType.Bottom;
            default:
                return VertAlignType.Top;
        }
    }
    fairygui.parseVertAlignType = parseVertAlignType;
    function parseLoaderFillType(value) {
        switch (value) {
            case "none":
                return LoaderFillType.None;
            case "scale":
                return LoaderFillType.Scale;
            case "scaleMatchHeight":
                return LoaderFillType.ScaleMatchHeight;
            case "scaleMatchWidth":
                return LoaderFillType.ScaleMatchWidth;
            case "scaleFree":
                return LoaderFillType.ScaleFree;
            case "scaleNoBorder":
                return LoaderFillType.ScaleNoBorder;
            default:
                return LoaderFillType.None;
        }
    }
    fairygui.parseLoaderFillType = parseLoaderFillType;
    function parseListLayoutType(value) {
        switch (value) {
            case "column":
                return ListLayoutType.SingleColumn;
            case "row":
                return ListLayoutType.SingleRow;
            case "flow_hz":
                return ListLayoutType.FlowHorizontal;
            case "flow_vt":
                return ListLayoutType.FlowVertical;
            case "pagination":
                return ListLayoutType.Pagination;
            default:
                return ListLayoutType.SingleColumn;
        }
    }
    fairygui.parseListLayoutType = parseListLayoutType;
    function parseListSelectionMode(value) {
        switch (value) {
            case "single":
                return ListSelectionMode.Single;
            case "multiple":
                return ListSelectionMode.Multiple;
            case "multipleSingleClick":
                return ListSelectionMode.Multiple_SingleClick;
            case "none":
                return ListSelectionMode.None;
            default:
                return ListSelectionMode.Single;
        }
    }
    fairygui.parseListSelectionMode = parseListSelectionMode;
    function parseOverflowType(value) {
        switch (value) {
            case "visible":
                return OverflowType.Visible;
            case "hidden":
                return OverflowType.Hidden;
            case "scroll":
                return OverflowType.Scroll;
            case "scale":
                return OverflowType.Scale;
            case "scaleFree":
                return OverflowType.ScaleFree;
            default:
                return OverflowType.Visible;
        }
    }
    fairygui.parseOverflowType = parseOverflowType;
    function parsePackageItemType(value) {
        switch (value) {
            case "image":
                return PackageItemType.Image;
            case "movieclip":
                return PackageItemType.MovieClip;
            case "sound":
                return PackageItemType.Sound;
            case "component":
                return PackageItemType.Component;
            case "swf":
                return PackageItemType.Swf;
            case "font":
                return PackageItemType.Font;
            case "atlas":
                return PackageItemType.Atlas;
            default:
                return PackageItemType.Misc;
        }
    }
    fairygui.parsePackageItemType = parsePackageItemType;
    function parseProgressTitleType(value) {
        switch (value) {
            case "percent":
                return ProgressTitleType.Percent;
            case "valueAndmax":
                return ProgressTitleType.ValueAndMax;
            case "value":
                return ProgressTitleType.Value;
            case "max":
                return ProgressTitleType.Max;
            default:
                return ProgressTitleType.Percent;
        }
    }
    fairygui.parseProgressTitleType = parseProgressTitleType;
    function parseScrollBarDisplayType(value) {
        switch (value) {
            case "default":
                return ScrollBarDisplayType.Default;
            case "visible":
                return ScrollBarDisplayType.Visible;
            case "auto":
                return ScrollBarDisplayType.Auto;
            case "hidden":
                return ScrollBarDisplayType.Hidden;
            default:
                return ScrollBarDisplayType.Default;
        }
    }
    fairygui.parseScrollBarDisplayType = parseScrollBarDisplayType;
    function parseScrollType(value) {
        switch (value) {
            case "horizontal":
                return ScrollType.Horizontal;
            case "vertical":
                return ScrollType.Vertical;
            case "both":
                return ScrollType.Both;
            default:
                return ScrollType.Vertical;
        }
    }
    fairygui.parseScrollType = parseScrollType;
    function parseFlipType(value) {
        switch (value) {
            case "hz":
                return FlipType.Horizontal;
            case "vt":
                return FlipType.Vertical;
            case "both":
                return FlipType.Both;
            default:
                return FlipType.None;
        }
    }
    fairygui.parseFlipType = parseFlipType;
    function parseChildrenRenderOrder(value) {
        switch (value) {
            case "ascent":
                return ChildrenRenderOrder.Ascent;
            case "descent":
                return ChildrenRenderOrder.Descent;
            case "arch":
                return ChildrenRenderOrder.Arch;
            default:
                return ChildrenRenderOrder.Ascent;
        }
    }
    fairygui.parseChildrenRenderOrder = parseChildrenRenderOrder;
    function parseGroupLayoutType(value) {
        switch (value) {
            case "hz":
                return GroupLayoutType.Horizontal;
            case "vt":
                return GroupLayoutType.Vertical;
            default:
                return GroupLayoutType.None;
        }
    }
    fairygui.parseGroupLayoutType = parseGroupLayoutType;
})(fairygui || (fairygui = {}));
var fairygui;
(function (fairygui) {
    var GearBase = /** @class */ (function () {
        function GearBase(owner) {
            this._owner = owner;
            this._easeType = fairygui.EaseType.QuadOut;
            this._tweenTime = 0.3;
            this._tweenDelay = 0;
            this._displayLockToken = 0;
        }
        Object.defineProperty(GearBase.prototype, "controller", {
            get: function () {
                return this._controller;
            },
            set: function (val) {
                if (val != this._controller) {
                    this._controller = val;
                    if (this._controller)
                        this.init();
                }
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GearBase.prototype, "tween", {
            get: function () {
                return this._tween;
            },
            set: function (val) {
                this._tween = val;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GearBase.prototype, "tweenDelay", {
            get: function () {
                return this._tweenDelay;
            },
            set: function (val) {
                this._tweenDelay = val;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GearBase.prototype, "tweenTime", {
            get: function () {
                return this._tweenTime;
            },
            set: function (value) {
                this._tweenTime = value;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GearBase.prototype, "easeType", {
            get: function () {
                return this._easeType;
            },
            set: function (value) {
                this._easeType = value;
            },
            enumerable: true,
            configurable: true
        });
        GearBase.prototype.setup = function (xml) {
            this._controller = this._owner.parent.getController(xml.attributes.controller);
            if (this._controller == null)
                return;
            this.init();
            var str;
            str = xml.attributes.tween;
            if (str)
                this._tween = true;
            str = xml.attributes.ease;
            if (str)
                this._easeType = fairygui.EaseType.parseEaseType(str);
            str = xml.attributes.duration;
            if (str)
                this._tweenTime = parseFloat(str);
            str = xml.attributes.delay;
            if (str)
                this._tweenDelay = parseFloat(str);
            if (this instanceof fairygui.GearDisplay) {
                str = xml.attributes.pages;
                if (str)
                    this.pages = str.split(",");
            }
            else {
                var pages;
                var values;
                str = xml.attributes.pages;
                if (str)
                    pages = str.split(",");
                if (pages) {
                    str = xml.attributes.values;
                    if (str)
                        values = str.split("|");
                    else
                        values = [];
                    for (var i = 0; i < pages.length; i++) {
                        str = values[i];
                        if (str == null)
                            str = "";
                        this.addStatus(pages[i], str);
                    }
                }
                str = xml.attributes.default;
                if (str)
                    this.addStatus(null, str);
            }
        };
        GearBase.prototype.updateFromRelations = function (dx, dy) {
        };
        GearBase.prototype.addStatus = function (pageId, value) {
        };
        GearBase.prototype.init = function () {
        };
        GearBase.prototype.apply = function () {
        };
        GearBase.prototype.updateState = function () {
        };
        GearBase.disableAllTweenEffect = false;
        return GearBase;
    }());
    fairygui.GearBase = GearBase;
})(fairygui || (fairygui = {}));
var fairygui;
(function (fairygui) {
    var GearSize = /** @class */ (function (_super) {
        __extends(GearSize, _super);
        function GearSize(owner) {
            return _super.call(this, owner) || this;
        }
        GearSize.prototype.init = function () {
            this._default = new GearSizeValue(this._owner.width, this._owner.height, this._owner.scaleX, this._owner.scaleY);
            this._storage = {};
        };
        GearSize.prototype.addStatus = function (pageId, value) {
            if (value == "-" || value.length == 0)
                return;
            var arr = value.split(",");
            var gv;
            if (pageId == null)
                gv = this._default;
            else {
                gv = new GearSizeValue();
                this._storage[pageId] = gv;
            }
            gv.width = parseInt(arr[0]);
            gv.height = parseInt(arr[1]);
            if (arr.length > 2) {
                gv.scaleX = parseFloat(arr[2]);
                gv.scaleY = parseFloat(arr[3]);
            }
        };
        GearSize.prototype.apply = function () {
            var gv = this._storage[this._controller.selectedPageId];
            if (!gv)
                gv = this._default;
            if (this._tween && !fairygui.UIPackage._constructing && !fairygui.GearBase.disableAllTweenEffect) {
                if (this._tweener != null) {
                    if (this._tweener.endValue.x != gv.width || this._tweener.endValue.y != gv.height
                        || this._tweener.endValue.z != gv.scaleX || this._tweener.endValue.w != gv.scaleY) {
                        this._tweener.kill(true);
                        this._tweener = null;
                    }
                    else
                        return;
                }
                var a = gv.width != this._owner.width || gv.height != this._owner.height;
                var b = gv.scaleX != this._owner.scaleX || gv.scaleY != this._owner.scaleY;
                if (a || b) {
                    if (this._owner.checkGearController(0, this._controller))
                        this._displayLockToken = this._owner.addDisplayLock();
                    this._tweener = fairygui.GTween.to4(this._owner.width, this._owner.height, this._owner.scaleX, this._owner.scaleY, gv.width, gv.height, gv.scaleX, gv.scaleY, this.tweenTime)
                        .setDelay(this._tweenDelay)
                        .setEase(this._easeType)
                        .setUserData((a ? 1 : 0) + (b ? 2 : 0))
                        .setTarget(this)
                        .onUpdate(this.__tweenUpdate, this)
                        .onComplete(this.__tweenComplete, this);
                }
            }
            else {
                this._owner._gearLocked = true;
                this._owner.setSize(gv.width, gv.height, this._owner.gearXY.controller == this._controller);
                this._owner.setScale(gv.scaleX, gv.scaleY);
                this._owner._gearLocked = false;
            }
        };
        GearSize.prototype.__tweenUpdate = function (tweener) {
            var flag = tweener.userData;
            this._owner._gearLocked = true;
            if ((flag & 1) != 0)
                this._owner.setSize(tweener.value.x, tweener.value.y, this._owner.checkGearController(1, this._controller));
            if ((flag & 2) != 0)
                this._owner.setScale(tweener.value.z, tweener.value.w);
            this._owner._gearLocked = false;
        };
        GearSize.prototype.__tweenComplete = function () {
            if (this._displayLockToken != 0) {
                this._owner.releaseDisplayLock(this._displayLockToken);
                this._displayLockToken = 0;
            }
            this._tweener = null;
        };
        GearSize.prototype.updateState = function () {
            var gv = this._storage[this._controller.selectedPageId];
            if (!gv) {
                gv = new GearSizeValue();
                this._storage[this._controller.selectedPageId] = gv;
            }
            gv.width = this._owner.width;
            gv.height = this._owner.height;
            gv.scaleX = this._owner.scaleX;
            gv.scaleY = this._owner.scaleY;
        };
        GearSize.prototype.updateFromRelations = function (dx, dy) {
            if (this._controller == null || this._storage == null)
                return;
            for (var key in this._storage) {
                var gv = this._storage[key];
                gv.width += dx;
                gv.height += dy;
            }
            this._default.width += dx;
            this._default.height += dy;
            this.updateState();
        };
        return GearSize;
    }(fairygui.GearBase));
    fairygui.GearSize = GearSize;
    var GearSizeValue = /** @class */ (function () {
        function GearSizeValue(width, height, scaleX, scaleY) {
            if (width === void 0) { width = 0; }
            if (height === void 0) { height = 0; }
            if (scaleX === void 0) { scaleX = 0; }
            if (scaleY === void 0) { scaleY = 0; }
            this.width = width;
            this.height = height;
            this.scaleX = scaleX;
            this.scaleY = scaleY;
        }
        return GearSizeValue;
    }());
})(fairygui || (fairygui = {}));
var fairygui;
(function (fairygui) {
    var GearXY = /** @class */ (function (_super) {
        __extends(GearXY, _super);
        function GearXY(owner) {
            return _super.call(this, owner) || this;
        }
        GearXY.prototype.init = function () {
            this._default = new egret.Point(this._owner.x, this._owner.y);
            this._storage = {};
        };
        GearXY.prototype.addStatus = function (pageId, value) {
            if (value == "-" || value.length == 0)
                return;
            var arr = value.split(",");
            var pt;
            if (pageId == null)
                pt = this._default;
            else {
                pt = new egret.Point();
                this._storage[pageId] = pt;
            }
            pt.x = parseInt(arr[0]);
            pt.y = parseInt(arr[1]);
        };
        GearXY.prototype.apply = function () {
            var pt = this._storage[this._controller.selectedPageId];
            if (!pt)
                pt = this._default;
            if (this._tween && !fairygui.UIPackage._constructing && !fairygui.GearBase.disableAllTweenEffect) {
                if (this._tweener != null) {
                    if (this._tweener.endValue.x != pt.x || this._tweener.endValue.y != pt.y) {
                        this._tweener.kill(true);
                        this._tweener = null;
                    }
                    else
                        return;
                }
                if (this._owner.x != pt.x || this._owner.y != pt.y) {
                    if (this._owner.checkGearController(0, this._controller))
                        this._displayLockToken = this._owner.addDisplayLock();
                    this._tweener = fairygui.GTween.to2(this._owner.x, this._owner.y, pt.x, pt.y, this._tweenTime)
                        .setDelay(this._tweenDelay)
                        .setEase(this._easeType)
                        .setTarget(this)
                        .onUpdate(this.__tweenUpdate, this)
                        .onComplete(this.__tweenComplete, this);
                }
            }
            else {
                this._owner._gearLocked = true;
                this._owner.setXY(pt.x, pt.y);
                this._owner._gearLocked = false;
            }
        };
        GearXY.prototype.__tweenUpdate = function (tweener) {
            this._owner._gearLocked = true;
            this._owner.setXY(tweener.value.x, tweener.value.y);
            this._owner._gearLocked = false;
        };
        GearXY.prototype.__tweenComplete = function () {
            if (this._displayLockToken != 0) {
                this._owner.releaseDisplayLock(this._displayLockToken);
                this._displayLockToken = 0;
            }
            this._tweener = null;
        };
        GearXY.prototype.updateState = function () {
            var pt = this._storage[this._controller.selectedPageId];
            if (!pt) {
                pt = new egret.Point();
                this._storage[this._controller.selectedPageId] = pt;
            }
            pt.x = this._owner.x;
            pt.y = this._owner.y;
        };
        GearXY.prototype.updateFromRelations = function (dx, dy) {
            if (this._controller == null || this._storage == null)
                return;
            for (var key in this._storage) {
                var pt = this._storage[key];
                pt.x += dx;
                pt.y += dy;
            }
            this._default.x += dx;
            this._default.y += dy;
            this.updateState();
        };
        return GearXY;
    }(fairygui.GearBase));
    fairygui.GearXY = GearXY;
})(fairygui || (fairygui = {}));
var fairygui;
(function (fairygui) {
    var GearText = /** @class */ (function (_super) {
        __extends(GearText, _super);
        function GearText(owner) {
            return _super.call(this, owner) || this;
        }
        GearText.prototype.init = function () {
            this._default = this._owner.text;
            this._storage = {};
        };
        GearText.prototype.addStatus = function (pageId, value) {
            if (pageId == null)
                this._default = value;
            else
                this._storage[pageId] = value;
        };
        GearText.prototype.apply = function () {
            this._owner._gearLocked = true;
            var data = this._storage[this._controller.selectedPageId];
            if (data != undefined)
                this._owner.text = data;
            else
                this._owner.text = this._default;
            this._owner._gearLocked = false;
        };
        GearText.prototype.updateState = function () {
            this._storage[this._controller.selectedPageId] = this._owner.text;
        };
        return GearText;
    }(fairygui.GearBase));
    fairygui.GearText = GearText;
})(fairygui || (fairygui = {}));
var fairygui;
(function (fairygui) {
    var GearIcon = /** @class */ (function (_super) {
        __extends(GearIcon, _super);
        function GearIcon(owner) {
            return _super.call(this, owner) || this;
        }
        GearIcon.prototype.init = function () {
            this._default = this._owner.icon;
            this._storage = {};
        };
        GearIcon.prototype.addStatus = function (pageId, value) {
            if (pageId == null)
                this._default = value;
            else
                this._storage[pageId] = value;
        };
        GearIcon.prototype.apply = function () {
            this._owner._gearLocked = true;
            var data = this._storage[this._controller.selectedPageId];
            if (data != undefined)
                this._owner.icon = data;
            else
                this._owner.icon = this._default;
            this._owner._gearLocked = false;
        };
        GearIcon.prototype.updateState = function () {
            this._storage[this._controller.selectedPageId] = this._owner.icon;
        };
        return GearIcon;
    }(fairygui.GearBase));
    fairygui.GearIcon = GearIcon;
})(fairygui || (fairygui = {}));
var fairygui;
(function (fairygui) {
    var Transition = /** @class */ (function () {
        function Transition(owner) {
            this._ownerBaseX = 0;
            this._ownerBaseY = 0;
            this._totalTimes = 0;
            this._totalTasks = 0;
            this._playing = false;
            this._paused = false;
            this._options = 0;
            this._reversed = false;
            this._totalDuration = 0;
            this._autoPlay = false;
            this._autoPlayTimes = 1;
            this._autoPlayDelay = 0;
            this._timeScale = 1;
            this._startTime = 0;
            this._endTime = 0;
            this._owner = owner;
            this._items = new Array();
        }
        Transition.prototype.play = function (onComplete, onCompleteObj, onCompleteParam, times, delay, startTime, endTime) {
            if (onComplete === void 0) { onComplete = null; }
            if (onCompleteObj === void 0) { onCompleteObj = null; }
            if (onCompleteParam === void 0) { onCompleteParam = null; }
            if (times === void 0) { times = 1; }
            if (delay === void 0) { delay = 0; }
            if (startTime === void 0) { startTime = 0; }
            if (endTime === void 0) { endTime = -1; }
            this._play(onComplete, onCompleteObj, onCompleteParam, times, delay, startTime, endTime, false);
        };
        Transition.prototype.playReverse = function (onComplete, onCompleteObj, onCompleteParam, times, delay) {
            if (onComplete === void 0) { onComplete = null; }
            if (onCompleteObj === void 0) { onCompleteObj = null; }
            if (onCompleteParam === void 0) { onCompleteParam = null; }
            if (times === void 0) { times = 1; }
            if (delay === void 0) { delay = 0; }
            this._play(onComplete, onCompleteObj, onCompleteParam, times, delay, 0, -1, true);
        };
        Transition.prototype.changePlayTimes = function (value) {
            this._totalTimes = value;
        };
        Transition.prototype.setAutoPlay = function (value, times, delay) {
            if (times === void 0) { times = -1; }
            if (delay === void 0) { delay = 0; }
            if (this._autoPlay != value) {
                this._autoPlay = value;
                this._autoPlayTimes = times;
                this._autoPlayDelay = delay;
                if (this._autoPlay) {
                    if (this._owner.onStage)
                        this.play(null, null, this._autoPlayTimes, this._autoPlayDelay);
                }
                else {
                    if (!this._owner.onStage)
                        this.stop(false, true);
                }
            }
        };
        Transition.prototype._play = function (onComplete, onCompleteCaller, onCompleteParam, times, delay, startTime, endTime, reversed) {
            if (onComplete === void 0) { onComplete = null; }
            if (onCompleteCaller === void 0) { onCompleteCaller = null; }
            if (onCompleteParam === void 0) { onCompleteParam = null; }
            if (times === void 0) { times = 1; }
            if (delay === void 0) { delay = 0; }
            if (startTime === void 0) { startTime = 0; }
            if (endTime === void 0) { endTime = -1; }
            if (reversed === void 0) { reversed = false; }
            this.stop(true, true);
            this._totalTimes = times;
            this._reversed = reversed;
            this._startTime = startTime;
            this._endTime = endTime;
            this._playing = true;
            this._paused = false;
            this._onComplete = onComplete;
            this._onCompleteParam = onCompleteParam;
            this._onCompleteCaller = onCompleteCaller;
            var cnt = this._items.length;
            for (var i = 0; i < cnt; i++) {
                var item = this._items[i];
                if (item.target == null) {
                    if (item.targetId)
                        item.target = this._owner.getChildById(item.targetId);
                    else
                        item.target = this._owner;
                }
                else if (item.target != this._owner && item.target.parent != this._owner)
                    item.target = null;
                if (item.target != null && item.type == TransitionActionType.Transition) {
                    var trans = item.target.getTransition(item.value.transName);
                    if (trans == this)
                        trans = null;
                    if (trans != null) {
                        if (item.value.playTimes == 0) //stop
                         {
                            var j;
                            for (j = i - 1; j >= 0; j--) {
                                var item2 = this._items[j];
                                if (item2.type == TransitionActionType.Transition) {
                                    if (item2.value.trans == trans) {
                                        item2.value.stopTime = item.time - item2.time;
                                        break;
                                    }
                                }
                            }
                            if (j < 0)
                                item.value.stopTime = 0;
                            else
                                trans = null; //no need to handle stop anymore
                        }
                        else
                            item.value.stopTime = -1;
                    }
                    item.value.trans = trans;
                }
            }
            if (delay == 0)
                this.onDelayedPlay();
            else
                fairygui.GTween.delayedCall(delay).onComplete(this.onDelayedPlay, this);
        };
        Transition.prototype.stop = function (setToComplete, processCallback) {
            if (setToComplete === void 0) { setToComplete = true; }
            if (processCallback === void 0) { processCallback = false; }
            if (!this._playing)
                return;
            this._playing = false;
            this._totalTasks = 0;
            this._totalTimes = 0;
            var func = this._onComplete;
            var param = this._onCompleteParam;
            var thisObj = this._onCompleteCaller;
            this._onComplete = null;
            this._onCompleteParam = null;
            this._onCompleteCaller = null;
            fairygui.GTween.kill(this); //delay start
            var cnt = this._items.length;
            if (this._reversed) {
                for (var i = cnt - 1; i >= 0; i--) {
                    var item = this._items[i];
                    if (item.target == null)
                        continue;
                    this.stopItem(item, setToComplete);
                }
            }
            else {
                for (i = 0; i < cnt; i++) {
                    item = this._items[i];
                    if (item.target == null)
                        continue;
                    this.stopItem(item, setToComplete);
                }
            }
            if (processCallback && func != null) {
                func.call(thisObj, param);
            }
        };
        Transition.prototype.stopItem = function (item, setToComplete) {
            if (item.displayLockToken != 0) {
                item.target.releaseDisplayLock(item.displayLockToken);
                item.displayLockToken = 0;
            }
            if (item.tweener != null) {
                item.tweener.kill(setToComplete);
                item.tweener = null;
                if (item.type == TransitionActionType.Shake && !setToComplete) //震动必须归位，否则下次就越震越远了。
                 {
                    item.target._gearLocked = true;
                    item.target.setXY(item.target.x - item.value.lastOffsetX, item.target.y - item.value.lastOffsetY);
                    item.target._gearLocked = false;
                }
            }
            if (item.type == TransitionActionType.Transition) {
                var trans = item.value.trans;
                if (trans != null)
                    trans.stop(setToComplete, false);
            }
        };
        Transition.prototype.setPaused = function (paused) {
            if (!this._playing || this._paused == paused)
                return;
            this._paused = paused;
            var tweener = fairygui.GTween.getTween(this);
            if (tweener != null)
                tweener.setPaused(paused);
            var cnt = this._items.length;
            for (var i = 0; i < cnt; i++) {
                var item = this._items[i];
                if (item.target == null)
                    continue;
                if (item.type == TransitionActionType.Transition) {
                    if (item.value.trans != null)
                        item.value.trans.setPaused(paused);
                }
                else if (item.type == TransitionActionType.Animation) {
                    if (paused) {
                        item.value.flag = (item.target).playing;
                        (item.target).playing = false;
                    }
                    else
                        (item.target).playing = item.value.flag;
                }
                if (item.tweener != null)
                    item.tweener.setPaused(paused);
            }
        };
        Transition.prototype.dispose = function () {
            if (this._playing)
                fairygui.GTween.kill(this); //delay start
            var cnt = this._items.length;
            for (var i = 0; i < cnt; i++) {
                var item = this._items[i];
                if (item.tweener != null) {
                    item.tweener.kill();
                    item.tweener = null;
                }
                item.target = null;
                item.hook = null;
                if (item.tweenConfig != null)
                    item.tweenConfig.endHook = null;
            }
            this._items.length = 0;
            this._playing = false;
            this._onComplete = null;
            this._onCompleteCaller = null;
            this._onCompleteParam = null;
        };
        Object.defineProperty(Transition.prototype, "playing", {
            get: function () {
                return this._playing;
            },
            enumerable: true,
            configurable: true
        });
        Transition.prototype.setValue = function (label) {
            var args = [];
            for (var _i = 1; _i < arguments.length; _i++) {
                args[_i - 1] = arguments[_i];
            }
            var cnt = this._items.length;
            var value;
            for (var i = 0; i < cnt; i++) {
                var item = this._items[i];
                if (item.label == label) {
                    if (item.tweenConfig != null)
                        value = item.tweenConfig.startValue;
                    else
                        value = item.value;
                }
                else if (item.tweenConfig != null && item.tweenConfig.endLabel == label) {
                    value = item.tweenConfig.endValue;
                }
                else
                    continue;
                switch (item.type) {
                    case TransitionActionType.XY:
                    case TransitionActionType.Size:
                    case TransitionActionType.Pivot:
                    case TransitionActionType.Scale:
                    case TransitionActionType.Skew:
                        value.b1 = true;
                        value.b2 = true;
                        value.f1 = parseFloat(args[0]);
                        value.f2 = parseFloat(args[1]);
                        break;
                    case TransitionActionType.Alpha:
                        value.f1 = parseFloat(args[0]);
                        break;
                    case TransitionActionType.Rotation:
                        value.f1 = parseFloat(args[0]);
                        break;
                    case TransitionActionType.Color:
                        value.f1 = parseFloat(args[0]);
                        break;
                    case TransitionActionType.Animation:
                        value.frame = parseInt(args[0]);
                        if (args.length > 1)
                            value.playing = args[1];
                        break;
                    case TransitionActionType.Visible:
                        value.visible = args[0];
                        break;
                    case TransitionActionType.Sound:
                        value.sound = args[0];
                        if (args.length > 1)
                            value.volume = parseFloat(args[1]);
                        break;
                    case TransitionActionType.Transition:
                        value.transName = args[0];
                        if (args.length > 1)
                            value.playTimes = parseInt(args[1]);
                        break;
                    case TransitionActionType.Shake:
                        value.amplitude = parseFloat(args[0]);
                        if (args.length > 1)
                            value.duration = parseFloat(args[1]);
                        break;
                    case TransitionActionType.ColorFilter:
                        value.f1 = parseFloat(args[0]);
                        value.f2 = parseFloat(args[1]);
                        value.f3 = parseFloat(args[2]);
                        value.f4 = parseFloat(args[3]);
                        break;
                }
            }
        };
        Transition.prototype.setHook = function (label, callback, caller) {
            var cnt = this._items.length;
            for (var i = 0; i < cnt; i++) {
                var item = this._items[i];
                if (item.label == label) {
                    item.hook = callback;
                    item.hookCaller = caller;
                    break;
                }
                else if (item.tweenConfig != null && item.tweenConfig.endLabel == label) {
                    item.tweenConfig.endHook = callback;
                    item.tweenConfig.endHookCaller = caller;
                    break;
                }
            }
        };
        Transition.prototype.clearHooks = function () {
            var cnt = this._items.length;
            for (var i = 0; i < cnt; i++) {
                var item = this._items[i];
                item.hook = null;
                item.hookCaller = null;
                if (item.tweenConfig != null) {
                    item.tweenConfig.endHook = null;
                    item.tweenConfig.endHookCaller = null;
                }
            }
        };
        Transition.prototype.setTarget = function (label, newTarget) {
            var cnt = this._items.length;
            for (var i = 0; i < cnt; i++) {
                var item = this._items[i];
                if (item.label == label)
                    item.targetId = newTarget.id;
            }
        };
        Transition.prototype.setDuration = function (label, value) {
            var cnt = this._items.length;
            for (var i = 0; i < cnt; i++) {
                var item = this._items[i];
                if (item.tweenConfig != null && item.label == label)
                    item.tweenConfig.duration = value;
            }
        };
        Transition.prototype.getLabelTime = function (label) {
            var cnt = this._items.length;
            for (var i = 0; i < cnt; i++) {
                var item = this._items[i];
                if (item.label == label)
                    return item.time;
                else if (item.tweenConfig != null && item.tweenConfig.endLabel == label)
                    return item.time + item.tweenConfig.duration;
            }
            return Number.NaN;
        };
        Object.defineProperty(Transition.prototype, "timeScale", {
            get: function () {
                return this._timeScale;
            },
            set: function (value) {
                this._timeScale = value;
                if (this._timeScale != value) {
                    if (this._playing) {
                        var cnt = this._items.length;
                        for (var i = 0; i < cnt; i++) {
                            var item = this._items[i];
                            if (item.tweener != null)
                                item.tweener.setTimeScale(value);
                            else if (item.type == TransitionActionType.Transition) {
                                if (item.value.trans != null)
                                    item.value.trans.timeScale = value;
                            }
                            else if (item.type == TransitionActionType.Animation) {
                                if (item.target != null)
                                    (item.target).timeScale = value;
                            }
                        }
                    }
                }
            },
            enumerable: true,
            configurable: true
        });
        Transition.prototype.updateFromRelations = function (targetId, dx, dy) {
            var cnt = this._items.length;
            if (cnt == 0)
                return;
            for (var i = 0; i < cnt; i++) {
                var item = this._items[i];
                if (item.type == TransitionActionType.XY && item.targetId == targetId) {
                    if (item.tweenConfig != null) {
                        item.tweenConfig.startValue.f1 += dx;
                        item.tweenConfig.startValue.f2 += dy;
                        item.tweenConfig.endValue.f1 += dx;
                        item.tweenConfig.endValue.f2 += dy;
                    }
                    else {
                        item.value.f1 += dx;
                        item.value.f2 += dy;
                    }
                }
            }
        };
        Transition.prototype.onOwnerAddedToStage = function () {
            if (this._autoPlay && !this._playing)
                this.play(null, null, null, this._autoPlayTimes, this._autoPlayDelay);
        };
        Transition.prototype.onOwnerRemovedFromStage = function () {
            if ((this._options & Transition.OPTION_AUTO_STOP_DISABLED) == 0)
                this.stop((this._options & Transition.OPTION_AUTO_STOP_AT_END) != 0 ? true : false, false);
        };
        Transition.prototype.onDelayedPlay = function () {
            this.internalPlay();
            this._playing = this._totalTasks > 0;
            if (this._playing) {
                if ((this._options & Transition.OPTION_IGNORE_DISPLAY_CONTROLLER) != 0) {
                    var cnt = this._items.length;
                    for (var i = 0; i < cnt; i++) {
                        var item = this._items[i];
                        if (item.target != null && item.target != this._owner)
                            item.displayLockToken = item.target.addDisplayLock();
                    }
                }
            }
            else if (this._onComplete != null) {
                var func = this._onComplete;
                var param = this._onCompleteParam;
                var thisObj = this._onCompleteCaller;
                this._onComplete = null;
                this._onCompleteParam = null;
                this._onCompleteCaller = null;
                func.call(thisObj, param);
            }
        };
        Transition.prototype.internalPlay = function () {
            this._ownerBaseX = this._owner.x;
            this._ownerBaseY = this._owner.y;
            this._totalTasks = 0;
            var cnt = this._items.length;
            var item;
            var needSkipAnimations = false;
            if (!this._reversed) {
                for (var i = 0; i < cnt; i++) {
                    item = this._items[i];
                    if (item.target == null)
                        continue;
                    if (item.type == TransitionActionType.Animation && this._startTime != 0 && item.time <= this._startTime) {
                        needSkipAnimations = true;
                        item.value.flag = false;
                    }
                    else
                        this.playItem(item);
                }
            }
            else {
                for (i = 0; i < cnt; i++) {
                    item = this._items[i];
                    if (item.target == null)
                        continue;
                    this.playItem(item);
                }
            }
            if (needSkipAnimations)
                this.skipAnimations();
        };
        Transition.prototype.playItem = function (item) {
            var time;
            if (item.tweenConfig != null) {
                if (this._reversed)
                    time = (this._totalDuration - item.time - item.tweenConfig.duration);
                else
                    time = item.time;
                if (this._endTime == -1 || time <= this._endTime) {
                    var startValue;
                    var endValue;
                    if (this._reversed) {
                        startValue = item.tweenConfig.endValue;
                        endValue = item.tweenConfig.startValue;
                    }
                    else {
                        startValue = item.tweenConfig.startValue;
                        endValue = item.tweenConfig.endValue;
                    }
                    item.value.b1 = startValue.b1 || endValue.b1;
                    item.value.b2 = startValue.b2 || endValue.b2;
                    switch (item.type) {
                        case TransitionActionType.XY:
                        case TransitionActionType.Size:
                        case TransitionActionType.Scale:
                        case TransitionActionType.Skew:
                            item.tweener = fairygui.GTween.to2(startValue.f1, startValue.f2, endValue.f1, endValue.f2, item.tweenConfig.duration);
                            break;
                        case TransitionActionType.Alpha:
                        case TransitionActionType.Rotation:
                            item.tweener = fairygui.GTween.to(startValue.f1, endValue.f1, item.tweenConfig.duration);
                            break;
                        case TransitionActionType.Color:
                            item.tweener = fairygui.GTween.toColor(startValue.f1, endValue.f1, item.tweenConfig.duration);
                            break;
                        case TransitionActionType.ColorFilter:
                            item.tweener = fairygui.GTween.to4(startValue.f1, startValue.f2, startValue.f3, startValue.f4, endValue.f1, endValue.f2, endValue.f3, endValue.f4, item.tweenConfig.duration);
                            break;
                    }
                    item.tweener.setDelay(time)
                        .setEase(item.tweenConfig.easeType)
                        .setRepeat(item.tweenConfig.repeat, item.tweenConfig.yoyo)
                        .setTimeScale(this._timeScale)
                        .setTarget(item)
                        .onStart(this.onTweenStart, this)
                        .onUpdate(this.onTweenUpdate, this)
                        .onComplete(this.onTweenComplete, this);
                    if (this._endTime >= 0)
                        item.tweener.setBreakpoint(this._endTime - time);
                    this._totalTasks++;
                }
            }
            else if (item.type == TransitionActionType.Shake) {
                if (this._reversed)
                    time = (this._totalDuration - item.time - item.value.duration);
                else
                    time = item.time;
                item.value.offsetX = item.value.offsetY = 0;
                item.value.lastOffsetX = item.value.lastOffsetY = 0;
                item.tweener = fairygui.GTween.shake(0, 0, item.value.amplitude, item.value.duration)
                    .setDelay(time)
                    .setTimeScale(this._timeScale)
                    .setTarget(item)
                    .onUpdate(this.onTweenUpdate, this)
                    .onComplete(this.onTweenComplete, this);
                if (this._endTime >= 0)
                    item.tweener.setBreakpoint(this._endTime - item.time);
                this._totalTasks++;
            }
            else {
                if (this._reversed)
                    time = (this._totalDuration - item.time);
                else
                    time = item.time;
                if (time <= this._startTime) {
                    this.applyValue(item);
                    this.callHook(item, false);
                }
                else if (this._endTime == -1 || time <= this._endTime) {
                    this._totalTasks++;
                    item.tweener = fairygui.GTween.delayedCall(time)
                        .setTimeScale(this._timeScale)
                        .setTarget(item)
                        .onComplete(this.onDelayedPlayItem, this);
                }
            }
            if (item.tweener != null)
                item.tweener.seek(this._startTime);
        };
        Transition.prototype.skipAnimations = function () {
            var frame;
            var playStartTime;
            var playTotalTime;
            var value;
            var target;
            var item;
            var cnt = this._items.length;
            for (var i = 0; i < cnt; i++) {
                item = this._items[i];
                if (item.type != TransitionActionType.Animation || item.time > this._startTime)
                    continue;
                value = item.value;
                if (value.flag)
                    continue;
                target = item.target;
                frame = target.frame;
                playStartTime = target.playing ? 0 : -1;
                playTotalTime = 0;
                for (var j = i; j < cnt; j++) {
                    item = this._items[j];
                    if (item.type != TransitionActionType.Animation || item.target != target || item.time > this._startTime)
                        continue;
                    value = item.value;
                    value.flag = true;
                    if (value.frame != -1) {
                        frame = value.frame;
                        if (value.playing)
                            playStartTime = item.time;
                        else
                            playStartTime = -1;
                        playTotalTime = 0;
                    }
                    else {
                        if (value.playing) {
                            if (playStartTime < 0)
                                playStartTime = item.time;
                        }
                        else {
                            if (playStartTime >= 0)
                                playTotalTime += (item.time - playStartTime);
                            playStartTime = -1;
                        }
                    }
                    this.callHook(item, false);
                }
                if (playStartTime >= 0)
                    playTotalTime += (this._startTime - playStartTime);
                target.playing = playStartTime >= 0;
                target.frame = frame;
                if (playTotalTime > 0)
                    target.advance(playTotalTime * 1000);
            }
        };
        Transition.prototype.onDelayedPlayItem = function (tweener) {
            var item = tweener.target;
            item.tweener = null;
            this._totalTasks--;
            this.applyValue(item);
            this.callHook(item, false);
            this.checkAllComplete();
        };
        Transition.prototype.onTweenStart = function (tweener) {
            var item = tweener.target;
            if (item.type == TransitionActionType.XY || item.type == TransitionActionType.Size) //位置和大小要到start才最终确认起始值
             {
                var startValue;
                var endValue;
                if (this._reversed) {
                    startValue = item.tweenConfig.endValue;
                    endValue = item.tweenConfig.startValue;
                }
                else {
                    startValue = item.tweenConfig.startValue;
                    endValue = item.tweenConfig.endValue;
                }
                if (item.type == TransitionActionType.XY) {
                    if (item.target != this._owner) {
                        if (!startValue.b1)
                            startValue.f1 = item.target.x;
                        if (!startValue.b2)
                            startValue.f2 = item.target.y;
                    }
                }
                else {
                    if (!startValue.b1)
                        startValue.f1 = item.target.width;
                    if (!startValue.b2)
                        startValue.f2 = item.target.height;
                }
                if (!endValue.b1)
                    endValue.f1 = startValue.f1;
                if (!endValue.b2)
                    endValue.f2 = startValue.f2;
                tweener.startValue.x = startValue.f1;
                tweener.startValue.y = startValue.f2;
                tweener.endValue.x = endValue.f1;
                tweener.endValue.y = endValue.f2;
            }
            this.callHook(item, false);
        };
        Transition.prototype.onTweenUpdate = function (tweener) {
            var item = tweener.target;
            switch (item.type) {
                case TransitionActionType.XY:
                case TransitionActionType.Size:
                case TransitionActionType.Scale:
                case TransitionActionType.Skew:
                    item.value.f1 = tweener.value.x;
                    item.value.f2 = tweener.value.y;
                    break;
                case TransitionActionType.Alpha:
                case TransitionActionType.Rotation:
                    item.value.f1 = tweener.value.x;
                    break;
                case TransitionActionType.Color:
                    item.value.f1 = tweener.value.color;
                    break;
                case TransitionActionType.ColorFilter:
                    item.value.f1 = tweener.value.x;
                    item.value.f2 = tweener.value.y;
                    item.value.f3 = tweener.value.z;
                    item.value.f4 = tweener.value.w;
                    break;
                case TransitionActionType.Shake:
                    item.value.offsetX = tweener.deltaValue.x;
                    item.value.offsetY = tweener.deltaValue.y;
                    break;
            }
            this.applyValue(item);
        };
        Transition.prototype.onTweenComplete = function (tweener) {
            var item = tweener.target;
            item.tweener = null;
            this._totalTasks--;
            if (tweener.allCompleted) //当整体播放结束时间在这个tween的中间时不应该调用结尾钩子
                this.callHook(item, true);
            this.checkAllComplete();
        };
        Transition.prototype.onPlayTransCompleted = function (item) {
            this._totalTasks--;
            this.checkAllComplete();
        };
        Transition.prototype.callHook = function (item, tweenEnd) {
            if (tweenEnd) {
                if (item.tweenConfig != null && item.tweenConfig.endHook != null)
                    item.tweenConfig.endHook.call(item.tweenConfig.endHookCaller);
            }
            else {
                if (item.time >= this._startTime && item.hook != null)
                    item.hook.call(item.hookCaller);
            }
        };
        Transition.prototype.checkAllComplete = function () {
            if (this._playing && this._totalTasks == 0) {
                if (this._totalTimes < 0) {
                    this.internalPlay();
                }
                else {
                    this._totalTimes--;
                    if (this._totalTimes > 0)
                        this.internalPlay();
                    else {
                        this._playing = false;
                        var cnt = this._items.length;
                        for (var i = 0; i < cnt; i++) {
                            var item = this._items[i];
                            if (item.target != null && item.displayLockToken != 0) {
                                item.target.releaseDisplayLock(item.displayLockToken);
                                item.displayLockToken = 0;
                            }
                        }
                        if (this._onComplete != null) {
                            var func = this._onComplete;
                            var param = this._onCompleteParam;
                            var thisObj = this._onCompleteCaller;
                            this._onComplete = null;
                            this._onCompleteParam = null;
                            this._onCompleteCaller = null;
                            func.call(thisObj, param);
                        }
                    }
                }
            }
        };
        Transition.prototype.applyValue = function (item) {
            item.target._gearLocked = true;
            switch (item.type) {
                case TransitionActionType.XY:
                    if (item.target == this._owner) {
                        var f1, f2;
                        if (!item.value.b1)
                            f1 = item.target.x;
                        else
                            f1 = item.value.f1 + this._ownerBaseX;
                        if (!item.value.b2)
                            f2 = item.target.y;
                        else
                            f2 = item.value.f2 + this._ownerBaseY;
                        item.target.setXY(f1, f2);
                    }
                    else {
                        if (!item.value.b1)
                            item.value.f1 = item.target.x;
                        if (!item.value.b2)
                            item.value.f2 = item.target.y;
                        item.target.setXY(item.value.f1, item.value.f2);
                    }
                    break;
                case TransitionActionType.Size:
                    if (!item.value.b1)
                        item.value.f1 = item.target.width;
                    if (!item.value.b2)
                        item.value.f2 = item.target.height;
                    item.target.setSize(item.value.f1, item.value.f2);
                    break;
                case TransitionActionType.Pivot:
                    item.target.setPivot(item.value.f1, item.value.f2, item.target.pivotAsAnchor);
                    break;
                case TransitionActionType.Alpha:
                    item.target.alpha = item.value.f1;
                    break;
                case TransitionActionType.Rotation:
                    item.target.rotation = item.value.f1;
                    break;
                case TransitionActionType.Scale:
                    item.target.setScale(item.value.f1, item.value.f2);
                    break;
                case TransitionActionType.Skew:
                    item.target.setSkew(item.value.f1, item.value.f2);
                    break;
                case TransitionActionType.Color:
                    (item.target).color = item.value.f1;
                    break;
                case TransitionActionType.Animation:
                    if (item.value.frame >= 0)
                        (item.target).frame = item.value.frame;
                    (item.target).playing = item.value.playing;
                    (item.target).timeScale = this._timeScale;
                    break;
                case TransitionActionType.Visible:
                    item.target.visible = item.value.visible;
                    break;
                case TransitionActionType.Transition:
                    if (this._playing) {
                        var trans = item.value.trans;
                        if (trans != null) {
                            this._totalTasks++;
                            var startTime = this._startTime > item.time ? (this._startTime - item.time) : 0;
                            var endTime = this._endTime >= 0 ? (this._endTime - item.time) : -1;
                            if (item.value.stopTime >= 0 && (endTime < 0 || endTime > item.value.stopTime))
                                endTime = item.value.stopTime;
                            trans.timeScale = this._timeScale;
                            trans._play(this.onPlayTransCompleted, this, item, item.value.playTimes, 0, startTime, endTime, this._reversed);
                        }
                    }
                    break;
                case TransitionActionType.Sound:
                    if (this._playing && item.time >= this._startTime) {
                        if (item.value.audioClip == null) {
                            var pi = fairygui.UIPackage.getItemByURL(item.value.sound);
                            if (pi)
                                item.value.audioClip = pi.owner.getItemAsset(pi);
                        }
                        if (item.value.audioClip)
                            fairygui.GRoot.inst.playOneShotSound(item.value.audioClip, item.value.volume);
                    }
                    break;
                case TransitionActionType.Shake:
                    item.target.setXY(item.target.x - item.value.lastOffsetX + item.value.offsetX, item.target.y - item.value.lastOffsetY + item.value.offsetY);
                    item.value.lastOffsetX = item.value.offsetX;
                    item.value.lastOffsetY = item.value.offsetY;
                    break;
                case TransitionActionType.ColorFilter:
                    {
                        var arr = item.target.filters;
                        var cf;
                        if (!arr || !(arr[0] instanceof egret.ColorMatrixFilter)) {
                            cf = new egret.ColorMatrixFilter();
                            arr = [cf];
                        }
                        else
                            cf = arr[0];
                        var cm = new ColorMatrix();
                        cm.adjustBrightness(item.value.f1);
                        cm.adjustContrast(item.value.f2);
                        cm.adjustSaturation(item.value.f3);
                        cm.adjustHue(item.value.f4);
                        cf.matrix = cm.matrix;
                        item.target.filters = arr;
                        break;
                    }
            }
            item.target._gearLocked = false;
        };
        Transition.prototype.setup = function (xml) {
            this.name = xml.attributes.name;
            var str = xml.attributes.options;
            if (str)
                this._options = parseInt(str);
            this._autoPlay = xml.attributes.autoPlay == "true";
            if (this._autoPlay) {
                str = xml.attributes.autoPlayRepeat;
                if (str)
                    this._autoPlayTimes = parseInt(str);
                str = xml.attributes.autoPlayDelay;
                if (str)
                    this._autoPlayDelay = parseFloat(str);
            }
            str = xml.attributes.fps;
            var frameInterval;
            if (str)
                frameInterval = 1 / parseInt(str);
            else
                frameInterval = 1 / 24;
            var col = xml.children;
            var length1 = col.length;
            for (var i1 = 0; i1 < length1; i1++) {
                var cxml = col[i1];
                if (cxml.name != "item")
                    continue;
                str = cxml.attributes.type;
                var item = new TransitionItem(this.parseItemType(str));
                this._items.push(item);
                item.time = parseInt(cxml.attributes.time) * frameInterval;
                item.targetId = cxml.attributes.target;
                if (cxml.attributes.tween == "true")
                    item.tweenConfig = new TweenConfig();
                item.label = cxml.attributes.label;
                if (item.tweenConfig) {
                    item.tweenConfig.duration = parseInt(cxml.attributes.duration) * frameInterval;
                    if (item.time + item.tweenConfig.duration > this._totalDuration)
                        this._totalDuration = item.time + item.tweenConfig.duration;
                    str = cxml.attributes.ease;
                    if (str)
                        item.tweenConfig.easeType = fairygui.EaseType.parseEaseType(str);
                    str = cxml.attributes.repeat;
                    if (str)
                        item.tweenConfig.repeat = parseInt(str);
                    item.tweenConfig.yoyo = cxml.attributes.yoyo == "true";
                    item.tweenConfig.endLabel = cxml.attributes.label2;
                    var v = cxml.attributes.endValue;
                    if (v) {
                        this.decodeValue(item, cxml.attributes.startValue, item.tweenConfig.startValue);
                        this.decodeValue(item, v, item.tweenConfig.endValue);
                    }
                    else {
                        this.decodeValue(item, cxml.attributes.startValue, item.value);
                        item.tweenConfig = null;
                    }
                }
                else {
                    if (item.time > this._totalDuration)
                        this._totalDuration = item.time;
                    this.decodeValue(item, cxml.attributes.value, item.value);
                }
            }
        };
        Transition.prototype.parseItemType = function (str) {
            var type;
            switch (str) {
                case "XY":
                    type = TransitionActionType.XY;
                    break;
                case "Size":
                    type = TransitionActionType.Size;
                    break;
                case "Scale":
                    type = TransitionActionType.Scale;
                    break;
                case "Pivot":
                    type = TransitionActionType.Pivot;
                    break;
                case "Alpha":
                    type = TransitionActionType.Alpha;
                    break;
                case "Rotation":
                    type = TransitionActionType.Rotation;
                    break;
                case "Color":
                    type = TransitionActionType.Color;
                    break;
                case "Animation":
                    type = TransitionActionType.Animation;
                    break;
                case "Visible":
                    type = TransitionActionType.Visible;
                    break;
                case "Sound":
                    type = TransitionActionType.Sound;
                    break;
                case "Transition":
                    type = TransitionActionType.Transition;
                    break;
                case "Shake":
                    type = TransitionActionType.Shake;
                    break;
                case "ColorFilter":
                    type = TransitionActionType.ColorFilter;
                    break;
                case "Skew":
                    type = TransitionActionType.Skew;
                    break;
                default:
                    type = TransitionActionType.Unknown;
                    break;
            }
            return type;
        };
        Transition.prototype.decodeValue = function (item, str, value) {
            var arr;
            switch (item.type) {
                case TransitionActionType.XY:
                case TransitionActionType.Size:
                case TransitionActionType.Pivot:
                case TransitionActionType.Skew:
                    arr = str.split(",");
                    if (arr[0] == "-") {
                        value.b1 = false;
                    }
                    else {
                        value.f1 = parseFloat(arr[0]);
                        value.b1 = true;
                    }
                    if (arr[1] == "-") {
                        value.b2 = false;
                    }
                    else {
                        value.f2 = parseFloat(arr[1]);
                        value.b2 = true;
                    }
                    break;
                case TransitionActionType.Alpha:
                    value.f1 = parseFloat(str);
                    break;
                case TransitionActionType.Rotation:
                    value.f1 = parseFloat(str);
                    break;
                case TransitionActionType.Scale:
                    arr = str.split(",");
                    value.f1 = parseFloat(arr[0]);
                    value.f2 = parseFloat(arr[1]);
                    break;
                case TransitionActionType.Color:
                    value.f1 = fairygui.ToolSet.convertFromHtmlColor(str);
                    break;
                case TransitionActionType.Animation:
                    arr = str.split(",");
                    if (arr[0] == "-")
                        value.frame = -1;
                    else
                        value.frame = parseInt(arr[0]);
                    value.playing = arr[1] == "p";
                    break;
                case TransitionActionType.Visible:
                    value.visible = str == "true";
                    break;
                case TransitionActionType.Sound:
                    arr = str.split(",");
                    value.sound = arr[0];
                    if (arr.length > 1) {
                        var intv = parseInt(arr[1]);
                        if (intv == 0 || intv == 100)
                            value.volume = 1;
                        else
                            value.volume = intv / 100;
                    }
                    else
                        value.volume = 1;
                    break;
                case TransitionActionType.Transition:
                    arr = str.split(",");
                    value.transName = arr[0];
                    if (arr.length > 1)
                        value.playTimes = parseInt(arr[1]);
                    else
                        value.playTimes = 1;
                    break;
                case TransitionActionType.Shake:
                    arr = str.split(",");
                    value.amplitude = parseFloat(arr[0]);
                    value.duration = parseFloat(arr[1]);
                    break;
                case TransitionActionType.ColorFilter:
                    arr = str.split(",");
                    value.f1 = parseFloat(arr[0]);
                    value.f2 = parseFloat(arr[1]);
                    value.f3 = parseFloat(arr[2]);
                    value.f4 = parseFloat(arr[3]);
                    break;
            }
        };
        Transition.OPTION_IGNORE_DISPLAY_CONTROLLER = 1;
        Transition.OPTION_AUTO_STOP_DISABLED = 2;
        Transition.OPTION_AUTO_STOP_AT_END = 4;
        return Transition;
    }());
    fairygui.Transition = Transition;
    var TransitionActionType = /** @class */ (function () {
        function TransitionActionType() {
        }
        TransitionActionType.XY = 0;
        TransitionActionType.Size = 1;
        TransitionActionType.Scale = 2;
        TransitionActionType.Pivot = 3;
        TransitionActionType.Alpha = 4;
        TransitionActionType.Rotation = 5;
        TransitionActionType.Color = 6;
        TransitionActionType.Animation = 7;
        TransitionActionType.Visible = 8;
        TransitionActionType.Sound = 9;
        TransitionActionType.Transition = 10;
        TransitionActionType.Shake = 11;
        TransitionActionType.ColorFilter = 12;
        TransitionActionType.Skew = 13;
        TransitionActionType.Unknown = 14;
        return TransitionActionType;
    }());
    var TransitionItem = /** @class */ (function () {
        function TransitionItem(type) {
            this.type = type;
            switch (type) {
                case TransitionActionType.XY:
                case TransitionActionType.Size:
                case TransitionActionType.Scale:
                case TransitionActionType.Pivot:
                case TransitionActionType.Skew:
                case TransitionActionType.Alpha:
                case TransitionActionType.Rotation:
                case TransitionActionType.Color:
                case TransitionActionType.ColorFilter:
                    this.value = new TValue();
                    break;
                case TransitionActionType.Animation:
                    this.value = new TValue_Animation();
                    break;
                case TransitionActionType.Shake:
                    this.value = new TValue_Shake();
                    break;
                case TransitionActionType.Sound:
                    this.value = new TValue_Sound();
                    break;
                case TransitionActionType.Transition:
                    this.value = new TValue_Transition();
                    break;
                case TransitionActionType.Visible:
                    this.value = new TValue_Visible();
                    break;
            }
        }
        return TransitionItem;
    }());
    var TweenConfig = /** @class */ (function () {
        function TweenConfig() {
            this.duration = 0;
            this.repeat = 0;
            this.yoyo = false;
            this.easeType = fairygui.EaseType.QuadOut;
            this.startValue = new TValue();
            this.endValue = new TValue();
        }
        return TweenConfig;
    }());
    var TValue_Visible = /** @class */ (function () {
        function TValue_Visible() {
        }
        return TValue_Visible;
    }());
    var TValue_Animation = /** @class */ (function () {
        function TValue_Animation() {
        }
        return TValue_Animation;
    }());
    var TValue_Sound = /** @class */ (function () {
        function TValue_Sound() {
        }
        return TValue_Sound;
    }());
    var TValue_Transition = /** @class */ (function () {
        function TValue_Transition() {
        }
        return TValue_Transition;
    }());
    var TValue_Shake = /** @class */ (function () {
        function TValue_Shake() {
        }
        return TValue_Shake;
    }());
    var TValue = /** @class */ (function () {
        function TValue() {
            this.f1 = this.f2 = this.f3 = this.f4 = 0;
            this.b1 = this.b2 = true;
        }
        return TValue;
    }());
})(fairygui || (fairygui = {}));
var fairygui;
(function (fairygui) {
    // export class GObject extends egret.EventDispatcher {
    var GObject = /** @class */ (function (_super) {
        __extends(GObject, _super);
        //liu
        function GObject() {
            var _this = _super.call(this) || this;
            _this._x = 0;
            _this._y = 0;
            _this._alpha = 1;
            _this._rotation = 0;
            _this._visible = true;
            _this._touchable = true;
            _this._grayed = false;
            _this._draggable = false;
            _this._scaleX = 1;
            _this._scaleY = 1;
            _this._skewX = 0;
            _this._skewY = 0;
            _this._pivotX = 0;
            _this._pivotY = 0;
            _this._pivotAsAnchor = false;
            _this._pivotOffsetX = 0;
            _this._pivotOffsetY = 0;
            _this._sortingOrder = 0;
            _this._internalVisible = true;
            _this._handlingController = false;
            _this._focusable = false;
            _this._pixelSnapping = false;
            _this.sourceWidth = 0;
            _this.sourceHeight = 0;
            _this.initWidth = 0;
            _this.initHeight = 0;
            _this.minWidth = 0;
            _this.minHeight = 0;
            _this.maxWidth = 0;
            _this.maxHeight = 0;
            _this._width = 0;
            _this._height = 0;
            _this._rawWidth = 0;
            _this._rawHeight = 0;
            _this._yOffset = 0;
            _this._sizePercentInGroup = 0;
            //Size的实现方式，有两种，0-GObject的w/h等于DisplayObject的w/h。1-GObject的sourceWidth/sourceHeight等于DisplayObject的w/h，剩余部分由scale实现
            _this._sizeImplType = 0;
            _this._id = "" + GObject._gInstanceCounter++;
            _this._name = "";
            _this.init();
            _this._relations = new fairygui.Relations(_this);
            _this._gears = [];
            return _this;
        }
        /*
        析构函数
         */
        GObject.prototype.dispose = function () {
            this.removeFromParent();
            this._relations.dispose();
        };
        //liu
        GObject.prototype.init = function () {
            this.createDisplayObject();
            this.setupDisplayObject();
        };
        //liu 对应handleInit
        GObject.prototype.createDisplayObject = function () {
            this._displayObject = new cc.Node("GObject_displayObject");
        };
        //liu
        GObject.prototype.setupDisplayObject = function () {
            if (this._displayObject != null) {
                this._displayObject.setAnchorPoint(new cc.Vec2(0, 1));
                // this._displayObject.setCascadeOpacityEnabled(true);
                // this._displayObject.setOnEnterCallback(CC_CALLBACK_0(GObject::onEnter, this));
                // this._displayObject.setOnExitCallback(CC_CALLBACK_0(GObject::onExit, this));
            }
        };
        Object.defineProperty(GObject.prototype, "id", {
            get: function () {
                return this._id;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GObject.prototype, "name", {
            get: function () {
                return this._name;
            },
            set: function (value) {
                this._name = value;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GObject.prototype, "x", {
            get: function () {
                return this._x;
            },
            set: function (value) {
                this.setXY(value, this._y);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GObject.prototype, "y", {
            get: function () {
                return this._y;
            },
            set: function (value) {
                this.setXY(this._x, value);
            },
            enumerable: true,
            configurable: true
        });
        //liu
        GObject.prototype.setXY = function (xv, yv) {
            if (this._x != xv || this._y != yv) {
                var dx = xv - this._x;
                var dy = yv - this._y;
                this._x = xv;
                this._y = yv;
                this.handleXYChanged();
                if (this instanceof fairygui.GGroup)
                    this.moveChildren(dx, dy);
                this.updateGear(1);
                if (this._parent && !(this._parent instanceof GList)) {
                    this._parent.setBoundsChangedFlag();
                    if (this._group != null)
                        this._group.setBoundsChangedFlag();
                    this.dispatchEvent(fairygui.UIEventType.PositionChange);
                }
                if (GObject.draggingObject == this && !GObject.sUpdateInDragging)
                    this.localToGlobalRect(0, 0, this.width, this.height, GObject.sGlobalRect);
            }
        };
        Object.defineProperty(GObject.prototype, "xMin", {
            get: function () {
                return this._pivotAsAnchor ? (this._x - this._width * this._pivotX) : this._x;
            },
            set: function (value) {
                if (this._pivotAsAnchor)
                    this.setXY(value + this._width * this._pivotX, this._y);
                else
                    this.setXY(value, this._y);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GObject.prototype, "yMin", {
            //liu
            get: function () {
                return this._pivotAsAnchor ? (this._y - this._height * this._pivotY) : this._y;
            },
            //liu
            set: function (value) {
                if (this._pivotAsAnchor)
                    this.setXY(this._x, value + this._height * this._pivotY);
                else
                    this.setXY(this._x, value);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GObject.prototype, "pixelSnapping", {
            //liu
            get: function () {
                return this._pixelSnapping;
            },
            //liu
            set: function (value) {
                if (this._pixelSnapping != value) {
                    this._pixelSnapping = value;
                    this.handleXYChanged();
                }
            },
            enumerable: true,
            configurable: true
        });
        //liu
        GObject.prototype.center = function (restraint) {
            if (restraint === void 0) { restraint = false; }
            var r;
            if (this._parent != null)
                r = this.parent;
            else
                r = this.root;
            this.setXY((r.width - this.width) / 2, (r.height - this.height) / 2);
            if (restraint) {
                this.addRelation(r, fairygui.RelationType.Center_Center);
                this.addRelation(r, fairygui.RelationType.Middle_Middle);
            }
        };
        Object.defineProperty(GObject.prototype, "width", {
            //liu 不一致
            get: function () {
                this.ensureSizeCorrect();
                if (this._relations.sizeDirty)
                    this._relations.ensureRelationsSizeCorrect();
                return this._width;
            },
            //liu
            set: function (value) {
                this.setSize(value, this._rawHeight);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GObject.prototype, "height", {
            //liu 不一致
            get: function () {
                this.ensureSizeCorrect();
                if (this._relations.sizeDirty)
                    this._relations.ensureRelationsSizeCorrect();
                return this._height;
            },
            //liu
            set: function (value) {
                this.setSize(this._rawWidth, value);
            },
            enumerable: true,
            configurable: true
        });
        //liu
        GObject.prototype.setSize = function (wv, hv, ignorePivot) {
            if (ignorePivot === void 0) { ignorePivot = false; }
            if (this._rawWidth != wv || this._rawHeight != hv) {
                this._rawWidth = wv;
                this._rawHeight = hv;
                if (wv < this.minWidth)
                    wv = this.minWidth;
                if (hv < this.minHeight)
                    hv = this.minHeight;
                if (this.maxWidth > 0 && wv > this.maxWidth)
                    wv = this.maxWidth;
                if (this.maxHeight > 0 && hv > this.maxHeight)
                    hv = this.maxHeight;
                var dWidth = wv - this._width;
                var dHeight = hv - this._height;
                this._width = wv;
                this._height = hv;
                this.handleSizeChanged();
                if (this._pivotX != 0 || this._pivotY != 0) {
                    if (!this._pivotAsAnchor) {
                        if (!ignorePivot)
                            this.setXY(this.x - this._pivotX * dWidth, this.y - this._pivotY * dHeight);
                        // this.updatePivotOffset();
                        this.handleXYChanged();
                    }
                    else {
                        // this.applyPivot();
                        this.handleXYChanged();
                    }
                }
                else {
                    this.handleXYChanged();
                }
                if (this instanceof fairygui.GGroup)
                    this.resizeChildren(dWidth, dHeight);
                this.updateGear(2);
                if (this._parent) {
                    this._relations.onOwnerSizeChanged(dWidth, dHeight, this._pivotAsAnchor || !ignorePivot);
                    this._parent.setBoundsChangedFlag();
                    if (this._group != null)
                        this._group.setBoundsChangedFlag(true);
                }
                this.dispatchEvent(fairygui.UIEventType.SizeChange);
            }
        };
        //liu no find
        GObject.prototype.ensureSizeCorrect = function () {
        };
        Object.defineProperty(GObject.prototype, "actualWidth", {
            //liu no find
            get: function () {
                return this.width * Math.abs(this._scaleX);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GObject.prototype, "actualHeight", {
            //liu no find
            get: function () {
                return this.height * Math.abs(this._scaleY);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GObject.prototype, "scaleX", {
            //liu
            get: function () {
                return this._scaleX;
            },
            //liu
            set: function (value) {
                this.setScale(value, this._scaleY);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GObject.prototype, "scaleY", {
            //liu
            get: function () {
                return this._scaleY;
            },
            //liu
            set: function (value) {
                this.setScale(this._scaleX, value);
            },
            enumerable: true,
            configurable: true
        });
        //liu 不一致
        GObject.prototype.setScale = function (sx, sy) {
            if (this._scaleX != sx || this._scaleY != sy) {
                this._scaleX = sx;
                this._scaleY = sy;
                this.handleScaleChanged();
                // this.applyPivot();
                this.updateGear(2);
            }
        };
        Object.defineProperty(GObject.prototype, "skewX", {
            get: function () {
                return this._skewX;
            },
            set: function (value) {
                this.setSkew(value, this._skewY);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GObject.prototype, "skewY", {
            get: function () {
                return this._skewY;
            },
            set: function (value) {
                this.setSkew(this._skewX, value);
            },
            enumerable: true,
            configurable: true
        });
        // liu 不一致
        GObject.prototype.setSkew = function (xv, yv) {
            if (this._skewX != xv || this._skewY != yv) {
                this._skewX = xv;
                this._skewY = yv;
                if (this._displayObject != null) {
                    this._displayObject.skewX = xv;
                    this._displayObject.skewY = yv;
                }
                // this.applyPivot();
            }
        };
        Object.defineProperty(GObject.prototype, "pivotX", {
            get: function () {
                return this._pivotX;
            },
            set: function (value) {
                this.setPivot(value, this._pivotY);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GObject.prototype, "pivotY", {
            get: function () {
                return this._pivotY;
            },
            set: function (value) {
                this.setPivot(this._pivotX, value);
            },
            enumerable: true,
            configurable: true
        });
        //liu 不一致
        GObject.prototype.setPivot = function (xv, yv, asAnchor) {
            if (yv === void 0) { yv = 0; }
            if (asAnchor === void 0) { asAnchor = false; }
            if (this._pivotX != xv || this._pivotY != yv || this._pivotAsAnchor != asAnchor) {
                this._pivotX = xv;
                this._pivotY = yv;
                this._pivotAsAnchor = asAnchor;
                // this.updatePivotOffset();
                if (this._displayObject) {
                    this._displayObject.setAnchorPoint(this._pivotX, this._pivotY);
                }
                this.handleXYChanged();
            }
        };
        Object.defineProperty(GObject.prototype, "pivotAsAnchor", {
            get: function () {
                return this._pivotAsAnchor;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GObject.prototype, "touchable", {
            // protected internalSetPivot(xv: number, yv: number = 0, asAnchor: boolean): void {
            //     this._pivotX = xv;
            //     this._pivotY = yv;
            //     this._pivotAsAnchor = asAnchor;
            //     if (asAnchor)
            //         this.handleXYChanged();
            // }
            // private updatePivotOffset(): void {
            //     if (this._displayObject != null) {
            //         if (this._pivotX != 0 || this._pivotY != 0) {
            //             var px: number;
            //             var py: number;
            //             if (this._sizeImplType == 0) {
            //                 px = this._pivotX * this._width;
            //                 py = this._pivotY * this._height;
            //             }
            //             else {
            //                 px = this._pivotX * this.sourceWidth;
            //                 py = this._pivotY * this.sourceHeight;
            //             }
            //             // var pt: cc.Vec2 = this._displayObject.matrix.transformPoint(px, py, GObject.sHelperPoint);
            //             // this._pivotOffsetX = this._pivotX * this._width - (pt.x - this._displayObject.x);
            //             // this._pivotOffsetY = this._pivotY * this._height - (pt.y - this._displayObject.y);
            //         }
            //         else {
            //             this._pivotOffsetX = 0;
            //             this._pivotOffsetY = 0;
            //         }
            //     }
            // }
            // private applyPivot(): void {
            //     if (this._pivotX != 0 || this._pivotY != 0) {
            //         this.updatePivotOffset();
            //         this.handleXYChanged();
            //     }
            // }
            get: function () {
                return this._touchable;
            },
            /*
            public set touchable(value: boolean) {
                if (this._touchable != value) {
                    this._touchable = value;
                    this.updateGear(3);
    
                    if ((this instanceof GImage) || (this instanceof GMovieClip)
                        || (this instanceof GTextField) && !(this instanceof GTextInput) && !(this instanceof GRichTextField))
                        //Touch is not supported by GImage/GMovieClip/GTextField
                        return;
    
                    if (this._displayObject != null) {
                        this._displayObject.touchEnabled = this._touchable;
                        if (this._displayObject instanceof egret.DisplayObjectContainer)
                            (<egret.DisplayObjectContainer>this._displayObject).touchChildren = this._touchable;
                    }
                }
            }
            */
            set: function (value) {
                this._touchable = value;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GObject.prototype, "grayed", {
            get: function () {
                return this._grayed;
            },
            set: function (value) {
                if (this._grayed != value) {
                    this._grayed = value;
                    this.handleGrayedChanged();
                    this.updateGear(3);
                }
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GObject.prototype, "enabled", {
            get: function () {
                return !this._grayed && this._touchable;
            },
            set: function (value) {
                this.grayed = !value;
                this.touchable = value;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GObject.prototype, "rotation", {
            get: function () {
                return this._rotation;
            },
            //liu
            set: function (value) {
                if (this._rotation != value) {
                    this._rotation = value;
                    if (this._displayObject)
                        this._displayObject.rotation = this.normalizeRotation;
                    // this.applyPivot();
                    this.updateGear(3);
                }
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GObject.prototype, "normalizeRotation", {
            //liu
            get: function () {
                var rot = this._rotation % 360;
                if (rot > 180)
                    rot -= 360;
                else if (rot < -180)
                    rot += 360;
                return rot;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GObject.prototype, "alpha", {
            //liu
            get: function () {
                return this._alpha;
            },
            //liu
            set: function (value) {
                if (this._alpha != value) {
                    this._alpha = value;
                    this.handleAlphaChanged();
                    this.updateGear(3);
                }
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GObject.prototype, "visible", {
            //liu
            get: function () {
                return this._visible;
            },
            //liu
            set: function (value) {
                if (this._visible != value) {
                    this._visible = value;
                    this.handleVisibleChanged();
                    if (this._parent)
                        this._parent.setBoundsChangedFlag();
                }
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GObject.prototype, "internalVisible", {
            //liu
            get: function () {
                return this._internalVisible && (!this._group || this._group.internalVisible);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GObject.prototype, "internalVisible2", {
            //liu
            get: function () {
                return this._visible && (!this._group || this._group.internalVisible2);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GObject.prototype, "sortingOrder", {
            //liu
            get: function () {
                return this._sortingOrder;
            },
            //liu
            set: function (value) {
                if (value < 0)
                    value = 0;
                if (this._sortingOrder != value) {
                    var old = this._sortingOrder;
                    this._sortingOrder = value;
                    if (this._parent != null)
                        this._parent.childSortingOrderChanged(this, old, this._sortingOrder);
                }
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GObject.prototype, "focusable", {
            get: function () {
                return this._focusable;
            },
            set: function (value) {
                this._focusable = value;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GObject.prototype, "focused", {
            get: function () {
                return this.root.focus == this;
            },
            enumerable: true,
            configurable: true
        });
        GObject.prototype.requestFocus = function () {
            var p = this;
            while (p && !p._focusable)
                p = p.parent;
            if (p != null)
                this.root.focus = p;
        };
        Object.defineProperty(GObject.prototype, "tooltips", {
            get: function () {
                return this._tooltips;
            },
            set: function (value) {
                this._tooltips = value;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GObject.prototype, "inContainer", {
            // public get blendMode(): string {
            //     return this._displayObject.blendMode;
            // }
            //
            // public set blendMode(value: string) {
            //     this._displayObject.blendMode = value;
            // }
            // public get filters(): egret.Filter[] {
            //     return this._displayObject.filters;
            // }
            //
            // public set filters(value: egret.Filter[]) {
            //     this._displayObject.filters = value;
            // }
            get: function () {
                return this._displayObject != null && this._displayObject.parent != null;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GObject.prototype, "onStage", {
            //liu 不一致
            get: function () {
                return this._displayObject != null && this._displayObject.activeInHierarchy != false;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GObject.prototype, "resourceURL", {
            //liu
            get: function () {
                if (this.packageItem != null)
                    return "ui://" + this.packageItem.owner.id + this.packageItem.id;
                else
                    return null;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GObject.prototype, "group", {
            get: function () {
                return this._group;
            },
            //liu 不一致
            set: function (value) {
                if (this._group != value) {
                    if (this._group != null)
                        this._group.setBoundsChangedFlag(true);
                    this._group = value;
                    if (this._group != null)
                        this._group.setBoundsChangedFlag(true);
                    this.handleVisibleChanged();
                    if (this._parent) {
                        this._parent.childStateChanged(this);
                    }
                }
            },
            enumerable: true,
            configurable: true
        });
        //liu
        GObject.prototype.getGear = function (index) {
            var gear = this._gears[index];
            if (gear == null) {
                switch (index) {
                    case 0:
                        gear = new fairygui.GearDisplay(this);
                        break;
                    case 1:
                        gear = new fairygui.GearXY(this);
                        break;
                    case 2:
                        gear = new fairygui.GearSize(this);
                        break;
                    case 3:
                        gear = new fairygui.GearLook(this);
                        break;
                    case 4:
                        gear = new fairygui.GearColor(this);
                        break;
                    case 5:
                        gear = new fairygui.GearAnimation(this);
                        break;
                    case 6:
                        gear = new fairygui.GearText(this);
                        break;
                    case 7:
                        gear = new fairygui.GearIcon(this);
                        break;
                    default:
                        throw "FairyGUI: invalid gear index!";
                }
                this._gears[index] = gear;
            }
            return gear;
        };
        //liu
        GObject.prototype.updateGear = function (index) {
            if (this._underConstruct || this._gearLocked)
                return;
            var gear = this._gears[index];
            if (gear != null && gear.controller != null)
                gear.updateState();
        };
        //liu
        GObject.prototype.checkGearController = function (index, c) {
            return this._gears[index] != null && this._gears[index].controller == c;
        };
        //liu
        GObject.prototype.updateGearFromRelations = function (index, dx, dy) {
            if (this._gears[index] != null)
                this._gears[index].updateFromRelations(dx, dy);
        };
        //liu
        GObject.prototype.addDisplayLock = function () {
            var gearDisplay = this._gears[0];
            if (gearDisplay && gearDisplay.controller) {
                var ret = gearDisplay.addLock();
                this.checkGearDisplay();
                return ret;
            }
            else
                return 0;
        };
        //liu
        GObject.prototype.releaseDisplayLock = function (token) {
            var gearDisplay = this._gears[0];
            if (gearDisplay && gearDisplay.controller) {
                gearDisplay.releaseLock(token);
                this.checkGearDisplay();
            }
        };
        //liu
        GObject.prototype.checkGearDisplay = function () {
            if (this._handlingController)
                return;
            var connected = this._gears[0] == null || this._gears[0].connected;
            if (connected != this._internalVisible) {
                this._internalVisible = connected;
                if (this._parent)
                    this._parent.childStateChanged(this);
            }
        };
        Object.defineProperty(GObject.prototype, "gearXY", {
            get: function () {
                return this.getGear(1);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GObject.prototype, "gearSize", {
            get: function () {
                return this.getGear(2);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GObject.prototype, "gearLook", {
            get: function () {
                return this.getGear(3);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GObject.prototype, "relations", {
            get: function () {
                return this._relations;
            },
            enumerable: true,
            configurable: true
        });
        //liu
        GObject.prototype.addRelation = function (target, relationType, usePercent) {
            if (usePercent === void 0) { usePercent = false; }
            this._relations.add(target, relationType, usePercent);
        };
        //liu
        GObject.prototype.removeRelation = function (target, relationType) {
            if (relationType === void 0) { relationType = 0; }
            this._relations.remove(target, relationType);
        };
        Object.defineProperty(GObject.prototype, "displayObject", {
            get: function () {
                return this._displayObject;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GObject.prototype, "parent", {
            // protected setDisplayObject(value: cc.Node): void {
            //     this._displayObject = value;
            // }
            get: function () {
                return this._parent;
            },
            set: function (val) {
                this._parent = val;
            },
            enumerable: true,
            configurable: true
        });
        GObject.prototype.removeFromParent = function () {
            if (this._parent)
                this._parent.removeChild(this);
        };
        Object.defineProperty(GObject.prototype, "root", {
            //liu 感觉效率好低啊
            get: function () {
                if (this instanceof fairygui.GRoot)
                    return this;
                var p = this._parent;
                while (p) {
                    if (p instanceof fairygui.GRoot)
                        return p;
                    p = p.parent;
                }
                return fairygui.GRoot.inst;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GObject.prototype, "asCom", {
            get: function () {
                return (this instanceof fairygui.GComponent) ? this : null;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GObject.prototype, "asButton", {
            get: function () {
                return (this instanceof fairygui.GButton) ? this : null;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GObject.prototype, "asLabel", {
            get: function () {
                return (this instanceof GLabel) ? this : null;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GObject.prototype, "asProgress", {
            get: function () {
                return (this instanceof GProgressBar) ? this : null;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GObject.prototype, "asTextField", {
            get: function () {
                return (this instanceof GTextField) ? this : null;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GObject.prototype, "asRichTextField", {
            get: function () {
                return (this instanceof GRichTextField) ? this : null;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GObject.prototype, "asTextInput", {
            get: function () {
                return (this instanceof GTextInput) ? this : null;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GObject.prototype, "asLoader", {
            get: function () {
                return (this instanceof fairygui.GLoader) ? this : null;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GObject.prototype, "asList", {
            get: function () {
                return (this instanceof GList) ? this : null;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GObject.prototype, "asGraph", {
            get: function () {
                return (this instanceof fairygui.GGraph) ? this : null;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GObject.prototype, "asGroup", {
            get: function () {
                return (this instanceof fairygui.GGroup) ? this : null;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GObject.prototype, "asSlider", {
            get: function () {
                return (this instanceof GSlider) ? this : null;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GObject.prototype, "asComboBox", {
            get: function () {
                return (this instanceof GComboBox) ? this : null;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GObject.prototype, "asImage", {
            get: function () {
                return (this instanceof fairygui.GImage) ? this : null;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GObject.prototype, "asMovieClip", {
            get: function () {
                return (this instanceof GMovieClip) ? this : null;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GObject.prototype, "text", {
            // public static cast(obj: cc.Node): GObject {
            //     return <GObject><any>obj["$owner"];
            // }
            get: function () {
                return null;
            },
            set: function (value) {
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GObject.prototype, "icon", {
            get: function () {
                return null;
            },
            set: function (value) {
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GObject.prototype, "draggable", {
            // public addClickListener(listener: Function, thisObj: any): void {
            //     this.addEventListener(egret.TouchEvent.TOUCH_TAP, listener, thisObj);
            // }
            //
            // public removeClickListener(listener: Function, thisObj: any): void {
            //     this.removeEventListener(egret.TouchEvent.TOUCH_TAP, listener, thisObj);
            // }
            //
            // public hasClickListener(): boolean {
            //     return this.hasEventListener(egret.TouchEvent.TOUCH_TAP);
            // }
            // public addEventListener(type: string, listener: Function, thisObject: any): void {
            //     super.addEventListener(type, listener, thisObject);
            //
            //     if (this._displayObject != null) {
            //         this._displayObject.addEventListener(type, this._reDispatch, this);
            //     }
            // }
            // public removeEventListener(type: string, listener: Function, thisObject: any): void {
            //     super.removeEventListener(type, listener, thisObject);
            //
            //     if (this._displayObject != null && !this.hasEventListener(type)) {
            //         this._displayObject.removeEventListener(type, this._reDispatch, this);
            //     }
            // }
            // private _reDispatch(evt: egret.Event): void {
            //     this.dispatchEvent(evt);
            // }
            get: function () {
                return this._draggable;
            },
            set: function (value) {
                if (this._draggable != value) {
                    this._draggable = value;
                    this.initDrag();
                }
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GObject.prototype, "dragBounds", {
            get: function () {
                return this._dragBounds;
            },
            set: function (value) {
                this._dragBounds = value;
            },
            enumerable: true,
            configurable: true
        });
        // liu 不一致
        GObject.prototype.startDrag = function (touchPointID) {
            if (touchPointID === void 0) { touchPointID = -1; }
            if (this._displayObject.activeInHierarchy == false)
                return;
            this.dragBegin(null);
        };
        GObject.prototype.stopDrag = function () {
            this.dragEnd();
        };
        Object.defineProperty(GObject.prototype, "dragging", {
            get: function () {
                return GObject.draggingObject == this;
            },
            enumerable: true,
            configurable: true
        });
        // public localToRoot(ax: number = 0, ay: number = 0, resultPoint?: cc.Vec2): cc.Vec2 {
        //     var pt: cc.Vec2 = this._displayObject.localToGlobal(ax, ay, resultPoint);
        //     pt.x /= GRoot.contentScaleFactor;
        //     pt.y /= GRoot.contentScaleFactor;
        //     return pt;
        // }
        //
        // public rootToLocal(ax: number = 0, ay: number = 0, resultPoint?: cc.Vec2): cc.Vec2 {
        //     ax *= GRoot.contentScaleFactor;
        //     ay *= GRoot.contentScaleFactor;
        //     return this._displayObject.globalToLocal(ax, ay, resultPoint);
        // }
        //liu
        GObject.prototype.localToGlobal = function (ax, ay, resultPoint) {
            if (ax === void 0) { ax = 0; }
            if (ay === void 0) { ay = 0; }
            if (resultPoint == null) {
                resultPoint = new cc.Vec2();
            }
            if (this._pivotAsAnchor) {
                ax += this._pivotX * this._width;
                ay += this._pivotY * this._height;
            }
            ay = this._height - ay;
            var p = this._displayObject.convertToWorldSpace(new cc.Vec2(ax, ay));
            p.y = this.root.height - p.y;
            resultPoint.x = p.x;
            resultPoint.y = p.y;
            return resultPoint;
        };
        //liu
        GObject.prototype.localToGlobalRect = function (ax, ay, aWidth, aHeight, resultRect) {
            if (ax === void 0) { ax = 0; }
            if (ay === void 0) { ay = 0; }
            if (aWidth === void 0) { aWidth = 0; }
            if (aHeight === void 0) { aHeight = 0; }
            if (resultRect == null)
                resultRect = new cc.Rect();
            var pt = this.localToGlobal(ax, ay);
            resultRect.x = pt.x;
            resultRect.y = pt.y;
            pt = this.localToGlobal(ax + aWidth, ay + aHeight);
            resultRect.width = pt.x - resultRect.x;
            resultRect.height = pt.y - resultRect.y;
            return resultRect;
        };
        //liu
        GObject.prototype.globalToLocal = function (ax, ay, resultPoint) {
            if (ax === void 0) { ax = 0; }
            if (ay === void 0) { ay = 0; }
            if (resultPoint == null) {
                resultPoint = new cc.Vec2();
            }
            ay = this.root.height - ay;
            var p = this.displayObject.convertToNodeSpace(new cc.Vec2(ax, ay));
            p.y = this._height - p.y;
            if (this._pivotAsAnchor) {
                p.x -= this._pivotX * this._width;
                p.y -= this._pivotY * this._height;
            }
            resultPoint.x = p.x;
            resultPoint.y = p.y;
            return resultPoint;
        };
        //liu
        GObject.prototype.globalToLocalRect = function (ax, ay, aWidth, aHeight, resultRect) {
            if (ax === void 0) { ax = 0; }
            if (ay === void 0) { ay = 0; }
            if (aWidth === void 0) { aWidth = 0; }
            if (aHeight === void 0) { aHeight = 0; }
            if (resultRect == null)
                resultRect = new cc.Rect();
            var pt = this.globalToLocal(ax, ay);
            resultRect.x = pt.x;
            resultRect.y = pt.y;
            pt = this.globalToLocal(ax + aWidth, ay + aHeight);
            resultRect.width = pt.x - resultRect.x;
            resultRect.height = pt.y - resultRect.y;
            return resultRect;
        };
        // liu
        GObject.prototype.handleControllerChanged = function (c) {
            this._handlingController = true;
            for (var i = 0; i < 8; i++) {
                var gear = this._gears[i];
                if (gear != null && gear.controller == c)
                    gear.apply();
            }
            this._handlingController = false;
            this.checkGearDisplay();
        };
        // protected switchDisplayObject(newObj: cc.Node): void {
        //     if (newObj == this._displayObject)
        //         return;
        //
        //     var old: cc.Node = this._displayObject;
        //     if (this._displayObject.parent != null) {
        //         var i: number = this._displayObject.parent.getChildIndex(this._displayObject);
        //         this._displayObject.parent.addChildAt(newObj, i);
        //         this._displayObject.parent.removeChild(this._displayObject);
        //     }
        //     this._displayObject = newObj;
        //     this._displayObject.x = old.x;
        //     this._displayObject.y = old.y;
        //     this._displayObject.rotation = old.rotation;
        //     this._displayObject.alpha = old.alpha;
        //     this._displayObject.visible = old.visible;
        //     this._displayObject.touchEnabled = old.touchEnabled;
        //     this._displayObject.scaleX = old.scaleX;
        //     this._displayObject.scaleY = old.scaleY;
        //
        //     if (this._displayObject instanceof egret.DisplayObjectContainer)
        //         (<egret.DisplayObjectContainer>this._displayObject).touchChildren = this._touchable;
        // }
        //liu TODO
        GObject.prototype.handleXYChanged = function () {
            if (this._displayObject) {
                var xv = this._x;
                var yv = -this._y;
                if (this._pivotAsAnchor) {
                    xv += this._pivotX * this._width;
                    yv -= this._pivotY * this._height;
                }
                /*
                if (_isAdoptiveChild)
                {
                    if (_displayObject->getParent())
                        pt.y += _displayObject->getParent()->getContentSize().height;
                    else if (_parent)
                        pt.y += _parent->_size.height;
                }
                 */
                if (this._pixelSnapping) {
                    xv = Math.round(xv);
                    yv = Math.round(yv);
                }
                this._displayObject.x = xv;
                this._displayObject.y = yv;
            }
        };
        //liu
        GObject.prototype.handleSizeChanged = function () {
            if (this._sizeImplType == 0 || this.sourceWidth == 0 || this.sourceHeight == 0) {
                this._displayObject.setContentSize(this._width, this._height);
            }
            else {
                this._displayObject.scaleX = this._width / this.sourceWidth * this._scaleX;
                this._displayObject.scaleY = this._height / this.sourceHeight * this._scaleY;
            }
        };
        //liu
        GObject.prototype.handleScaleChanged = function () {
            if (this._displayObject != null) {
                if (this._sizeImplType == 0 || this.sourceWidth == 0 || this.sourceHeight == 0) {
                    this._displayObject.scaleX = this._scaleX;
                    this._displayObject.scaleY = this._scaleY;
                }
                else {
                    this._displayObject.scaleX = this._width / this.sourceWidth * this._scaleX;
                    this._displayObject.scaleY = this._height / this.sourceHeight * this._scaleY;
                }
            }
        };
        GObject.prototype.handleGrayedChanged = function () {
            // if (this._displayObject) {
            //     if (this._grayed) {
            //         var colorFlilter = new egret.ColorMatrixFilter(GObject.colorMatrix);
            //         this._displayObject.filters = [colorFlilter];
            //     }
            //     else
            //         this._displayObject.filters = null;
            // }
        };
        // liu 不一致
        GObject.prototype.handleAlphaChanged = function () {
            if (this._displayObject)
                this._displayObject.opacity = this._alpha * 255;
        };
        // liu 不一致 可能有bug
        //16-02-3
        // 请使用 label.enabled = false // 隐藏单个组件
        // 或者 label.node.active = false // 整个 node 的所有组件一起禁用
        GObject.prototype.handleVisibleChanged = function () {
            if (this._displayObject)
                this._displayObject.active = this.internalVisible2;
        };
        GObject.prototype.constructFromResource = function () {
        };
        // liu 不一致
        GObject.prototype.setup_beforeAdd = function (xml) {
            var str;
            var arr;
            this._id = xml.attributes.id;
            this._name = xml.attributes.name;
            str = xml.attributes.xy;
            arr = str.split(",");
            this.setXY(parseInt(arr[0]), parseInt(arr[1]));
            str = xml.attributes.size;
            if (str) {
                arr = str.split(",");
                this.initWidth = parseInt(arr[0]);
                this.initHeight = parseInt(arr[1]);
                this.setSize(this.initWidth, this.initHeight, true);
            }
            str = xml.attributes.restrictSize;
            if (str) {
                arr = str.split(",");
                this.minWidth = parseInt(arr[0]);
                this.maxWidth = parseInt(arr[1]);
                this.minHeight = parseInt(arr[2]);
                this.maxHeight = parseInt(arr[3]);
            }
            str = xml.attributes.scale;
            if (str) {
                arr = str.split(",");
                this.setScale(parseFloat(arr[0]), parseFloat(arr[1]));
            }
            str = xml.attributes.rotation;
            if (str)
                this.rotation = parseFloat(str);
            str = xml.attributes.skew;
            if (str) {
                arr = str.split(",");
                this.setSkew(parseFloat(arr[0]), parseFloat(arr[1]));
            }
            str = xml.attributes.pivot;
            if (str) {
                arr = str.split(",");
                str = xml.attributes.anchor;
                this.setPivot(parseFloat(arr[0]), parseFloat(arr[1]), str == "true");
            }
            str = xml.attributes.alpha;
            if (str)
                this.alpha = parseFloat(str);
            if (xml.attributes.touchable == "false")
                this.touchable = false;
            if (xml.attributes.visible == "false")
                this.visible = false;
            if (xml.attributes.grayed == "true")
                this.grayed = true;
            this.tooltips = xml.attributes.tooltips;
            // str = xml.attributes.blend;
            // if (str)
            //     this.blendMode = str;
            // str = xml.attributes.filter;
            // if (str) {
            //     switch (str) {
            //         case "color":
            //             str = xml.attributes.filterData;
            //             arr = str.split(",");
            //             var cm: ColorMatrix = new ColorMatrix();
            //             cm.adjustBrightness(parseFloat(arr[0]));
            //             cm.adjustContrast(parseFloat(arr[1]));
            //             cm.adjustSaturation(parseFloat(arr[2]));
            //             cm.adjustHue(parseFloat(arr[3]));
            //             var cf: egret.ColorMatrixFilter = new egret.ColorMatrixFilter(cm.matrix);
            //             this.filters = [cf];
            //             break;
            //     }
            // }
            str = xml.attributes.customData;
            if (str) {
                this.data = str;
            }
        };
        //liu
        GObject.prototype.setup_afterAdd = function (xml) {
            var cxml;
            var str = xml.attributes.group;
            if (str)
                this._group = (this._parent.getChildById(str));
            var col = xml.children;
            if (col) {
                var length1 = col.length;
                for (var i1 = 0; i1 < length1; i1++) {
                    var cxml = col[i1];
                    var index = GObject.GearXMLKeys[cxml.name];
                    if (index != undefined)
                        this.getGear(index).setup(cxml);
                }
            }
        };
        GObject.prototype.initDrag = function () {
            if (this._draggable)
                this.addEventListener(egret.TouchEvent.TOUCH_BEGIN, this.__begin, this);
            else
                this.removeEventListener(egret.TouchEvent.TOUCH_BEGIN, this.__begin, this);
        };
        GObject.prototype.dragBegin = function (evt) {
            if (GObject.draggingObject != null)
                GObject.draggingObject.stopDrag();
            if (evt != null) {
                GObject.sGlobalDragStart.x = evt.stageX;
                GObject.sGlobalDragStart.y = evt.stageY;
            }
            else {
                GObject.sGlobalDragStart.x = fairygui.GRoot.mouseX;
                GObject.sGlobalDragStart.y = fairygui.GRoot.mouseY;
            }
            this.localToGlobalRect(0, 0, this.width, this.height, GObject.sGlobalRect);
            GObject.draggingObject = this;
            fairygui.GRoot.inst.nativeStage.addEventListener(egret.TouchEvent.TOUCH_MOVE, this.__moving2, this);
            fairygui.GRoot.inst.nativeStage.addEventListener(egret.TouchEvent.TOUCH_END, this.__end2, this);
        };
        GObject.prototype.dragEnd = function () {
            if (GObject.draggingObject == this) {
                fairygui.GRoot.inst.nativeStage.removeEventListener(egret.TouchEvent.TOUCH_MOVE, this.__moving2, this);
                fairygui.GRoot.inst.nativeStage.removeEventListener(egret.TouchEvent.TOUCH_END, this.__end2, this);
                GObject.draggingObject = null;
            }
        };
        GObject.prototype.reset = function () {
            fairygui.GRoot.inst.nativeStage.removeEventListener(egret.TouchEvent.TOUCH_MOVE, this.__moving, this);
            fairygui.GRoot.inst.nativeStage.removeEventListener(egret.TouchEvent.TOUCH_END, this.__end, this);
        };
        GObject.prototype.__begin = function (evt) {
            if (this._touchDownPoint == null)
                this._touchDownPoint = new cc.Vec2();
            this._touchDownPoint.x = evt.stageX;
            this._touchDownPoint.y = evt.stageY;
            fairygui.GRoot.inst.nativeStage.addEventListener(egret.TouchEvent.TOUCH_MOVE, this.__moving, this);
            fairygui.GRoot.inst.nativeStage.addEventListener(egret.TouchEvent.TOUCH_END, this.__end, this);
        };
        GObject.prototype.__end = function (evt) {
            this.reset();
        };
        GObject.prototype.__moving = function (evt) {
            var sensitivity = fairygui.UIConfig.touchDragSensitivity;
            if (this._touchDownPoint != null
                && Math.abs(this._touchDownPoint.x - evt.stageX) < sensitivity
                && Math.abs(this._touchDownPoint.y - evt.stageY) < sensitivity)
                return;
            this.reset();
            var dragEvent = new DragEvent(DragEvent.DRAG_START);
            dragEvent.stageX = evt.stageX;
            dragEvent.stageY = evt.stageY;
            dragEvent.touchPointID = evt.touchPointID;
            this.dispatchEvent(dragEvent);
            if (!dragEvent.isDefaultPrevented())
                this.dragBegin(evt);
        };
        GObject.prototype.__moving2 = function (evt) {
            var xx = evt.stageX - GObject.sGlobalDragStart.x + GObject.sGlobalRect.x;
            var yy = evt.stageY - GObject.sGlobalDragStart.y + GObject.sGlobalRect.y;
            if (this._dragBounds != null) {
                var rect = fairygui.GRoot.inst.localToGlobalRect(this._dragBounds.x, this._dragBounds.y, this._dragBounds.width, this._dragBounds.height, GObject.sDragHelperRect);
                if (xx < rect.x)
                    xx = rect.x;
                else if (xx + GObject.sGlobalRect.width > rect.right) {
                    xx = rect.right - GObject.sGlobalRect.width;
                    if (xx < rect.x)
                        xx = rect.x;
                }
                if (yy < rect.y)
                    yy = rect.y;
                else if (yy + GObject.sGlobalRect.height > rect.bottom) {
                    yy = rect.bottom - GObject.sGlobalRect.height;
                    if (yy < rect.y)
                        yy = rect.y;
                }
            }
            GObject.sUpdateInDragging = true;
            var pt = this.parent.globalToLocal(xx, yy, GObject.sHelperPoint);
            this.setXY(Math.round(pt.x), Math.round(pt.y));
            GObject.sUpdateInDragging = false;
            var dragEvent = new DragEvent(DragEvent.DRAG_MOVING);
            dragEvent.stageX = evt.stageX;
            dragEvent.stageY = evt.stageY;
            dragEvent.touchPointID = evt.touchPointID;
            this.dispatchEvent(dragEvent);
        };
        GObject.prototype.__end2 = function (evt) {
            if (GObject.draggingObject == this) {
                this.stopDrag();
                var dragEvent = new DragEvent(DragEvent.DRAG_END);
                dragEvent.stageX = evt.stageX;
                dragEvent.stageY = evt.stageY;
                dragEvent.touchPointID = evt.touchPointID;
                this.dispatchEvent(dragEvent);
            }
        };
        GObject._gInstanceCounter = 0;
        GObject.XY_CHANGED = "__xyChanged";
        GObject.SIZE_CHANGED = "__sizeChanged";
        GObject.SIZE_DELAY_CHANGE = "__sizeDelayChange";
        GObject.GEAR_STOP = "gearStop";
        GObject.colorMatrix = [
            0.3, 0.6, 0, 0, 0,
            0.3, 0.6, 0, 0, 0,
            0.3, 0.6, 0, 0, 0,
            0, 0, 0, 1, 0
        ];
        GObject.GearXMLKeys = {
            "gearDisplay": 0,
            "gearXY": 1,
            "gearSize": 2,
            "gearLook": 3,
            "gearColor": 4,
            "gearAni": 5,
            "gearText": 6,
            "gearIcon": 7
        };
        //drag support
        //-------------------------------------------------------------------
        GObject.sGlobalDragStart = new cc.Vec2();
        GObject.sGlobalRect = new cc.Rect();
        GObject.sHelperPoint = new cc.Vec2();
        GObject.sDragHelperRect = new cc.Rect();
        return GObject;
    }(fairygui.UIEventDispatcher));
    fairygui.GObject = GObject;
})(fairygui || (fairygui = {}));
var fairygui;
(function (fairygui) {
    var DisplayListItem = /** @class */ (function () {
        function DisplayListItem(packageItem, type) {
            this.packageItem = packageItem;
            this.type = type;
        }
        return DisplayListItem;
    }());
    fairygui.DisplayListItem = DisplayListItem;
})(fairygui || (fairygui = {}));
var fairygui;
(function (fairygui) {
    var PackageItem = /** @class */ (function () {
        function PackageItem() {
            this.width = 0;
            this.height = 0;
            this.tileGridIndice = 0;
            //movieclip
            this.interval = 0;
            this.repeatDelay = 0;
        }
        PackageItem.prototype.load = function () {
            return this.owner.getItemAsset(this);
        };
        PackageItem.prototype.toString = function () {
            return this.name;
        };
        return PackageItem;
    }());
    fairygui.PackageItem = PackageItem;
})(fairygui || (fairygui = {}));
var fairygui;
(function (fairygui) {
    var GComponent = /** @class */ (function (_super) {
        __extends(GComponent, _super);
        function GComponent() {
            var _this = _super.call(this) || this;
            _this._sortingChildCount = 0;
            _this._childrenRenderOrder = fairygui.ChildrenRenderOrder.Ascent;
            _this._apexIndex = 0;
            _this._children = [];
            _this._controllers = [];
            _this._transitions = [];
            _this._margin = new fairygui.Margin();
            _this._alignOffset = new cc.Vec2();
            return _this;
        }
        //liu 不一致 对应handleInit
        GComponent.prototype.createDisplayObject = function () {
            // this._rootContainer = new UIContainer();
            // this._rootContainer["$owner"] = this;
            // this.setDisplayObject(this._rootContainer);
            // this._container = this._rootContainer;
            this._rootContainer = new fairygui.UIContainer();
            this._displayObject = this._rootContainer;
            this._container = new fairygui.UIInnerContainer();
            // this._container.setCascadeOpacityEnabled(true);
            this._displayObject.addChild(this._container);
        };
        GComponent.prototype.dispose = function () {
            var i;
            var cnt;
            cnt = this._transitions.length;
            for (i = 0; i < cnt; ++i) {
                var trans = this._transitions[i];
                trans.dispose();
            }
            cnt = this._controllers.length;
            for (i = 0; i < cnt; ++i) {
                var cc = this._controllers[i];
                cc.dispose();
            }
            if (this._scrollPane)
                this._scrollPane.dispose();
            cnt = this._children.length;
            for (i = cnt - 1; i >= 0; --i) {
                var obj = this._children[i];
                obj.parent = null; //avoid removeFromParent call
                obj.dispose();
            }
            this._boundsChanged = false;
            _super.prototype.dispose.call(this);
        };
        Object.defineProperty(GComponent.prototype, "displayListContainer", {
            get: function () {
                return this._container;
            },
            enumerable: true,
            configurable: true
        });
        // liu
        GComponent.prototype.addChild = function (child) {
            this.addChildAt(child, this._children.length);
            return child;
        };
        //liu
        GComponent.prototype.addChildAt = function (child, index) {
            if (index === void 0) { index = 0; }
            if (!child)
                throw "child is null";
            var numChildren = this._children.length;
            if (index >= 0 && index <= numChildren) {
                if (child.parent == this) {
                    this.setChildIndex(child, index);
                }
                else {
                    child.removeFromParent();
                    child.parent = this;
                    var cnt = this._children.length;
                    if (child.sortingOrder != 0) {
                        this._sortingChildCount++;
                        index = this.getInsertPosForSortingChild(child);
                    }
                    else if (this._sortingChildCount > 0) {
                        if (index > (cnt - this._sortingChildCount))
                            index = cnt - this._sortingChildCount;
                    }
                    if (index == cnt)
                        this._children.push(child);
                    else
                        this._children.splice(index, 0, child);
                    this.childStateChanged(child);
                    this.setBoundsChangedFlag();
                    if (child.group) {
                        child.group.setBoundsChangedFlag(true);
                    }
                }
                return child;
            }
            else {
                throw "Invalid child index";
            }
        };
        //liu
        GComponent.prototype.getInsertPosForSortingChild = function (target) {
            var cnt = this._children.length;
            var i = 0;
            for (i = 0; i < cnt; i++) {
                var child = this._children[i];
                if (child == target)
                    continue;
                if (target.sortingOrder < child.sortingOrder)
                    break;
            }
            return i;
        };
        //liu
        GComponent.prototype.removeChild = function (child, dispose) {
            if (dispose === void 0) { dispose = false; }
            var childIndex = this._children.indexOf(child);
            if (childIndex != -1) {
                this.removeChildAt(childIndex, dispose);
            }
            return child;
        };
        // liu
        GComponent.prototype.removeChildAt = function (index, dispose) {
            if (dispose === void 0) { dispose = false; }
            if (index >= 0 && index < this.numChildren) {
                var child = this._children[index];
                child.parent = null;
                if (child.sortingOrder != 0)
                    this._sortingChildCount--;
                this._children.splice(index, 1);
                child.group = null;
                if (child.inContainer) {
                    this._container.removeChild(child.displayObject, false);
                    if (this._childrenRenderOrder == fairygui.ChildrenRenderOrder.Arch)
                        fairygui.GTimers.inst.callLater(this.buildNativeDisplayList, this);
                }
                if (dispose)
                    child.dispose();
                this.setBoundsChangedFlag();
                return child;
            }
            else {
                throw "Invalid child index";
            }
        };
        //liu
        GComponent.prototype.removeChildren = function (beginIndex, endIndex, dispose) {
            if (beginIndex === void 0) { beginIndex = 0; }
            if (endIndex === void 0) { endIndex = -1; }
            if (dispose === void 0) { dispose = false; }
            if (endIndex < 0 || endIndex >= this.numChildren)
                endIndex = this.numChildren - 1;
            for (var i = beginIndex; i <= endIndex; ++i)
                this.removeChildAt(beginIndex, dispose);
        };
        //liu
        GComponent.prototype.getChildAt = function (index) {
            if (index === void 0) { index = 0; }
            if (index >= 0 && index < this.numChildren)
                return this._children[index];
            else
                throw "Invalid child index";
        };
        //liu
        GComponent.prototype.getChild = function (name) {
            var cnt = this._children.length;
            for (var i = 0; i < cnt; ++i) {
                if (this._children[i].name == name)
                    return this._children[i];
            }
            return null;
        };
        GComponent.prototype.getVisibleChild = function (name) {
            var cnt = this._children.length;
            for (var i = 0; i < cnt; ++i) {
                var child = this._children[i];
                if (child.internalVisible && child.internalVisible && child.name == name)
                    return child;
            }
            return null;
        };
        //liu
        GComponent.prototype.getChildInGroup = function (group, name) {
            var cnt = this._children.length;
            for (var i = 0; i < cnt; ++i) {
                var child = this._children[i];
                if (child.group == group && child.name == name)
                    return child;
            }
            return null;
        };
        //liu
        GComponent.prototype.getChildById = function (id) {
            var cnt = this._children.length;
            for (var i = 0; i < cnt; ++i) {
                if (this._children[i]._id == id)
                    return this._children[i];
            }
            return null;
        };
        //liu
        GComponent.prototype.getChildIndex = function (child) {
            return this._children.indexOf(child);
        };
        //liu
        GComponent.prototype.setChildIndex = function (child, index) {
            if (index === void 0) { index = 0; }
            var oldIndex = this._children.indexOf(child);
            if (oldIndex == -1)
                throw "Not a child of this container";
            if (child.sortingOrder != 0) //no effect
                return;
            var cnt = this._children.length;
            if (this._sortingChildCount > 0) {
                if (index > (cnt - this._sortingChildCount - 1))
                    index = cnt - this._sortingChildCount - 1;
            }
            this._setChildIndex(child, oldIndex, index);
        };
        GComponent.prototype.setChildIndexBefore = function (child, index) {
            var oldIndex = this._children.indexOf(child);
            if (oldIndex == -1)
                throw "Not a child of this container";
            if (child.sortingOrder != 0) //no effect
                return oldIndex;
            var cnt = this._children.length;
            if (this._sortingChildCount > 0) {
                if (index > (cnt - this._sortingChildCount - 1))
                    index = cnt - this._sortingChildCount - 1;
            }
            if (oldIndex < index)
                return this._setChildIndex(child, oldIndex, index - 1);
            else
                return this._setChildIndex(child, oldIndex, index);
        };
        //liu 对应 moveChild
        GComponent.prototype._setChildIndex = function (child, oldIndex, index) {
            if (index === void 0) { index = 0; }
            var cnt = this._children.length;
            if (index > cnt)
                index = cnt;
            if (oldIndex == index)
                return oldIndex;
            this._children.splice(oldIndex, 1);
            this._children.splice(index, 0, child);
            if (child.inContainer) {
                var displayIndex = 0;
                var g;
                var i;
                if (this._childrenRenderOrder == fairygui.ChildrenRenderOrder.Ascent) {
                    // for (i = 0; i < index; i++) {
                    //     g = this._children[i];
                    //     if (g.inContainer)
                    //         displayIndex++;
                    // }
                    // if (displayIndex == this._container.numChildren)
                    //     displayIndex--;
                    // this._container.setChildIndex(child.displayObject, displayIndex);
                    var fromIndex = Math.min(index, oldIndex);
                    var toIndex = Math.min(Math.max(index, oldIndex), cnt - 1);
                    for (var i_1 = fromIndex; i_1 <= toIndex; i_1++) {
                        g = this._children[i_1];
                        if (g.inContainer) {
                            g.displayObject.zIndex = i_1;
                        }
                    }
                }
                else if (this._childrenRenderOrder == fairygui.ChildrenRenderOrder.Descent) {
                    // for (i = cnt - 1; i > index; i--) {
                    //     g = this._children[i];
                    //     if (g.inContainer)
                    //         displayIndex++;
                    // }
                    // if (displayIndex == this._container.numChildren)
                    //     displayIndex--;
                    // this._container.setChildIndex(child.displayObject, displayIndex);
                    var fromIndex = Math.min(index, oldIndex);
                    var toIndex = Math.min(Math.max(index, oldIndex), cnt - 1);
                    for (var i_2 = fromIndex; i_2 <= toIndex; i_2++) {
                        g = this._children[i_2];
                        if (g.inContainer) {
                            g.displayObject.zIndex = cnt - 1 - i_2;
                        }
                    }
                }
                else {
                    fairygui.GTimers.inst.callLater(this.buildNativeDisplayList, this);
                }
                this.setBoundsChangedFlag();
            }
            return index;
        };
        //liu
        GComponent.prototype.swapChildren = function (child1, child2) {
            var index1 = this._children.indexOf(child1);
            var index2 = this._children.indexOf(child2);
            if (index1 == -1 || index2 == -1)
                throw "Not a child of this container";
            this.swapChildrenAt(index1, index2);
        };
        //liu
        GComponent.prototype.swapChildrenAt = function (index1, index2) {
            if (index2 === void 0) { index2 = 0; }
            var child1 = this._children[index1];
            var child2 = this._children[index2];
            this.setChildIndex(child1, index2);
            this.setChildIndex(child2, index1);
        };
        Object.defineProperty(GComponent.prototype, "numChildren", {
            get: function () {
                return this._children.length;
            },
            enumerable: true,
            configurable: true
        });
        //liu
        GComponent.prototype.isAncestorOf = function (child) {
            if (child == null)
                return false;
            var p = child.parent;
            while (p) {
                if (p == this)
                    return true;
                p = p.parent;
            }
            return false;
        };
        //TODO 不一致
        GComponent.prototype.addController = function (controller) {
            this._controllers.push(controller);
            controller._parent = this;
            this.applyController(controller);
        };
        //liu
        GComponent.prototype.getControllerAt = function (index) {
            return this._controllers[index];
        };
        //liu
        GComponent.prototype.getController = function (name) {
            var cnt = this._controllers.length;
            for (var i = 0; i < cnt; ++i) {
                var c = this._controllers[i];
                if (c.name == name)
                    return c;
            }
            return null;
        };
        //TODO 不一致
        GComponent.prototype.removeController = function (c) {
            var index = this._controllers.indexOf(c);
            if (index == -1)
                throw "controller not exists";
            c._parent = null;
            this._controllers.splice(index, 1);
            var length = this._children.length;
            for (var i = 0; i < length; i++) {
                var child = this._children[i];
                child.handleControllerChanged(c);
            }
        };
        Object.defineProperty(GComponent.prototype, "controllers", {
            get: function () {
                return this._controllers;
            },
            enumerable: true,
            configurable: true
        });
        //liu  不一致
        GComponent.prototype.childStateChanged = function (child) {
            if (this._buildingDisplayList)
                return;
            var cnt = this._children.length;
            var g;
            var i;
            if (child instanceof fairygui.GGroup) {
                for (i = 0; i < cnt; i++) {
                    g = this._children[i];
                    if (g.group == child)
                        this.childStateChanged(g);
                }
                return;
            }
            if (!child.displayObject)
                return;
            if (child.internalVisible) {
                if (!child.displayObject.parent) {
                    var index = 0;
                    if (this._childrenRenderOrder == fairygui.ChildrenRenderOrder.Ascent) {
                        // for (i = 0; i < cnt; i++) {
                        //     g = this._children[i];
                        //     if (g == child)
                        //         break;
                        //
                        //     if (g.displayObject != null && g.displayObject.parent != null)
                        //         index++;
                        // }
                        // this._container.addChildAt(child.displayObject, index);
                        // for(i=index+1;i<cnt;i++){
                        //     let c = this._children[i];
                        //     if(c.displayObject.parent){
                        //         c.displayObject.zIndex = i
                        //     }
                        // }
                        var index_1 = this._children.indexOf(child);
                        this._container.addChild(child.displayObject, index_1);
                        for (i = index_1 + 1; i < cnt; i++) {
                            var c = this._children[i];
                            if (c.displayObject.parent) {
                                c.displayObject.zIndex = i;
                            }
                        }
                    }
                    else if (this._childrenRenderOrder == fairygui.ChildrenRenderOrder.Descent) {
                        // for (i = cnt - 1; i >= 0; i--) {
                        //     g = this._children[i];
                        //     if (g == child)
                        //         break;
                        //
                        //     if (g.displayObject != null && g.displayObject.parent != null)
                        //         index++;
                        // }
                        // this._container.addChildAt(child.displayObject, index);
                        var index_2 = cnt - 1 - this._children.indexOf(child);
                        this._container.addChild(child.displayObject, index_2);
                        for (var i_3 = index_2 - 1; i_3 >= 0; i_3--) {
                            var c = this._children[i_3];
                            if (c.displayObject.parent) {
                                c.displayObject.zIndex = cnt - i_3 - 1;
                            }
                        }
                    }
                    else {
                        // this._container.addChild(child.displayObject);
                        fairygui.GTimers.inst.callLater(this.buildNativeDisplayList, this);
                    }
                }
            }
            else {
                if (child.displayObject.parent) {
                    this._container.removeChild(child.displayObject);
                    if (this._childrenRenderOrder == fairygui.ChildrenRenderOrder.Ascent) {
                        fairygui.GTimers.inst.callLater(this.buildNativeDisplayList, this);
                    }
                }
            }
        };
        //liu 少了_maskOwner相关
        GComponent.prototype.buildNativeDisplayList = function () {
            var cnt = this._children.length;
            if (cnt == 0)
                return;
            var i;
            var child;
            switch (this._childrenRenderOrder) {
                case fairygui.ChildrenRenderOrder.Ascent:
                    {
                        for (i = 0; i < cnt; i++) {
                            child = this._children[i];
                            if (child.displayObject != null && child.internalVisible)
                                this._container.addChild(child.displayObject, i);
                        }
                    }
                    break;
                case fairygui.ChildrenRenderOrder.Descent:
                    {
                        for (i = cnt - 1; i >= 0; i--) {
                            child = this._children[i];
                            if (child.displayObject != null && child.internalVisible)
                                this._container.addChild(child.displayObject, cnt - 1 - i);
                        }
                    }
                    break;
                case fairygui.ChildrenRenderOrder.Arch:
                    {
                        for (i = 0; i < this._apexIndex; i++) {
                            child = this._children[i];
                            if (child.displayObject != null && child.internalVisible)
                                this._container.addChild(child.displayObject, i);
                        }
                        for (i = cnt - 1; i >= this._apexIndex; i--) {
                            child = this._children[i];
                            if (child.displayObject != null && child.internalVisible)
                                this._container.addChild(child.displayObject, this._apexIndex + cnt - 1 - i);
                        }
                    }
                    break;
            }
        };
        //liu
        GComponent.prototype.applyController = function (c) {
            this._applyingController = c;
            var child;
            var length = this._children.length;
            for (var i = 0; i < length; i++) {
                child = this._children[i];
                child.handleControllerChanged(c);
            }
            this._applyingController = null;
            c.runActions();
        };
        //liu
        GComponent.prototype.applyAllControllers = function () {
            var cnt = this._controllers.length;
            for (var i = 0; i < cnt; ++i) {
                this.applyController(this._controllers[i]);
            }
        };
        //liu
        GComponent.prototype.adjustRadioGroupDepth = function (obj, c) {
            var cnt = this._children.length;
            var i;
            var child;
            var myIndex = -1, maxIndex = -1;
            for (i = 0; i < cnt; i++) {
                child = this._children[i];
                if (child == obj) {
                    myIndex = i;
                }
                else if ((child instanceof fairygui.GButton)
                    && child.relatedController == c) {
                    if (i > maxIndex)
                        maxIndex = i;
                }
            }
            if (myIndex < maxIndex) {
                if (this._applyingController != null)
                    this._children[maxIndex].handleControllerChanged(this._applyingController);
                this.swapChildrenAt(myIndex, maxIndex);
            }
        };
        //liu
        GComponent.prototype.getTransitionAt = function (index) {
            return this._transitions[index];
        };
        //liu
        GComponent.prototype.getTransition = function (transName) {
            var cnt = this._transitions.length;
            for (var i = 0; i < cnt; ++i) {
                var trans = this._transitions[i];
                if (trans.name == transName)
                    return trans;
            }
            return null;
        };
        //TODO
        GComponent.prototype.isChildInView = function (child) {
            if (this._rootContainer.scrollRect != null) {
                return child.x + child.width >= 0 && child.x <= this.width
                    && child.y + child.height >= 0 && child.y <= this.height;
            }
            else if (this._scrollPane != null) {
                return this._scrollPane.isChildInView(child);
            }
            else
                return true;
        };
        //liu
        GComponent.prototype.getFirstChildInView = function () {
            var cnt = this._children.length;
            for (var i = 0; i < cnt; ++i) {
                var child = this._children[i];
                if (this.isChildInView(child))
                    return i;
            }
            return -1;
        };
        Object.defineProperty(GComponent.prototype, "scrollPane", {
            get: function () {
                return this._scrollPane;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GComponent.prototype, "opaque", {
            get: function () {
                return this._opaque;
            },
            //liu 不一致
            set: function (value) {
                // if (this._opaque != value) {
                //     this._opaque = value;
                //     if (this._opaque)
                //         this.updateOpaque();
                //     else
                //         this._rootContainer.hitArea = null;
                // }
                this._opaque = value;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GComponent.prototype, "margin", {
            get: function () {
                return this._margin;
            },
            //liu 不一致
            set: function (value) {
                this._margin.copy(value);
                // if (this._rootContainer.scrollRect != null) {
                //     this._container.x = this._margin.left + this._alignOffset.x;
                //     this._container.y = this._margin.top + this._alignOffset.y;
                // }
                // this.handleSizeChanged();
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GComponent.prototype, "childrenRenderOrder", {
            //liu
            get: function () {
                return this._childrenRenderOrder;
            },
            //liu 不一致
            set: function (value) {
                if (this._childrenRenderOrder != value) {
                    this._childrenRenderOrder = value;
                    fairygui.GTimers.inst.callLater(this.buildNativeDisplayList, this);
                    // this.buildNativeDisplayList();
                }
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GComponent.prototype, "apexIndex", {
            //liu
            get: function () {
                return this._apexIndex;
            },
            //liu
            set: function (value) {
                if (this._apexIndex != value) {
                    this._apexIndex = value;
                    if (this._childrenRenderOrder == fairygui.ChildrenRenderOrder.Arch) {
                        // this.buildNativeDisplayList();
                        fairygui.GTimers.inst.callLater(this.buildNativeDisplayList, this);
                    }
                }
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GComponent.prototype, "mask", {
            // TODO
            get: function () {
                return this._rootContainer.mask;
            },
            // TODO
            set: function (value) {
                this._rootContainer.mask = value;
            },
            enumerable: true,
            configurable: true
        });
        // TODO
        GComponent.prototype.updateOpaque = function () {
            if (!this._rootContainer.hitArea)
                this._rootContainer.hitArea = new cc.Rect();
            this._rootContainer.hitArea.set(cc.rect(0, 0, this.width, this.height));
        };
        // TODO
        GComponent.prototype.updateScrollRect = function () {
            var rect = this._rootContainer.scrollRect;
            if (rect == null)
                rect = new cc.Rect();
            var w = this.width - this._margin.right;
            var h = this.height - this._margin.bottom;
            rect.set(cc.rect(0, 0, w, h));
            this._rootContainer.scrollRect = rect;
        };
        // liu
        GComponent.prototype.setupScroll = function (scrollBarMargin, scroll, scrollBarDisplay, flags, vtScrollBarRes, hzScrollBarRes, headerRes, footerRes) {
            if (this._rootContainer == this._container) {
                this._container = new cc.Node();
                this._rootContainer.addChild(this._container);
            }
            this._scrollPane = new fairygui.ScrollPane(this, scroll, scrollBarMargin, scrollBarDisplay, flags, vtScrollBarRes, hzScrollBarRes, headerRes, footerRes);
            this.setBoundsChangedFlag();
        };
        // TODO
        GComponent.prototype.setupOverflow = function (overflow) {
            if (overflow == fairygui.OverflowType.Hidden) {
                if (this._rootContainer == this._container) {
                    this._container = new cc.Node();
                    this._rootContainer.addChild(this._container);
                }
                this.updateScrollRect();
                this._container.x = this._margin.left;
                this._container.y = this._margin.top;
            }
            else if (this._margin.left != 0 || this._margin.top != 0) {
                if (this._rootContainer == this._container) {
                    this._container = new cc.Node();
                    this._rootContainer.addChild(this._container);
                }
                this._container.x = this._margin.left;
                this._container.y = this._margin.top;
            }
            this.setBoundsChangedFlag();
        };
        // TODO
        GComponent.prototype.handleSizeChanged = function () {
            if (this._scrollPane)
                this._scrollPane.onOwnerSizeChanged();
            else if (this._rootContainer.scrollRect != null)
                this.updateScrollRect();
            if (this._opaque)
                this.updateOpaque();
        };
        // liu 不一致
        GComponent.prototype.handleGrayedChanged = function () {
            var c = this.getController("grayed");
            if (c != null) {
                c.selectedIndex = this.grayed ? 1 : 0;
                return;
            }
            var v = this.grayed;
            var cnt = this._children.length;
            for (var i = 0; i < cnt; ++i) {
                this._children[i].grayed = v;
            }
        };
        // liu
        GComponent.prototype.handleControllerChanged = function (c) {
            _super.prototype.handleControllerChanged.call(this, c);
            if (this._scrollPane != null)
                this._scrollPane.handleControllerChanged(c);
        };
        // liu 不一致
        GComponent.prototype.setBoundsChangedFlag = function () {
            if (!this._scrollPane && !this._trackBounds)
                return;
            if (!this._boundsChanged) {
                this._boundsChanged = true;
                fairygui.GTimers.inst.callLater(this.__render, this);
            }
        };
        // liu 对应doUpdateBounds 不一致
        GComponent.prototype.__render = function () {
            if (this._boundsChanged) {
                var len = this._children.length;
                if (len > 0) {
                    for (var i = 0; i < len; i++) {
                        var child = this._children[i];
                        child.ensureSizeCorrect();
                    }
                }
                this.updateBounds();
            }
        };
        // liu 不一致
        GComponent.prototype.ensureBoundsCorrect = function () {
            var len = this._children.length;
            if (len > 0) {
                for (var i = 0; i < len; i++) {
                    var child = this._children[i];
                    child.ensureSizeCorrect();
                }
            }
            if (this._boundsChanged)
                this.updateBounds();
        };
        //liu
        GComponent.prototype.updateBounds = function () {
            var ax = 0, ay = 0, aw = 0, ah = 0;
            var len = this._children.length;
            if (len > 0) {
                ax = Number.POSITIVE_INFINITY, ay = Number.POSITIVE_INFINITY;
                var ar = Number.NEGATIVE_INFINITY, ab = Number.NEGATIVE_INFINITY;
                var tmp = 0;
                var i = 0;
                for (var i = 0; i < len; i++) {
                    var child = this._children[i];
                    tmp = child.x;
                    if (tmp < ax)
                        ax = tmp;
                    tmp = child.y;
                    if (tmp < ay)
                        ay = tmp;
                    tmp = child.x + child.actualWidth;
                    if (tmp > ar)
                        ar = tmp;
                    tmp = child.y + child.actualHeight;
                    if (tmp > ab)
                        ab = tmp;
                }
                aw = ar - ax;
                ah = ab - ay;
            }
            this.setBounds(ax, ay, aw, ah);
        };
        // liu
        GComponent.prototype.setBounds = function (ax, ay, aw, ah) {
            if (ah === void 0) { ah = 0; }
            this._boundsChanged = false;
            if (this._scrollPane)
                this._scrollPane.setContentSize(Math.round(ax + aw), Math.round(ay + ah));
        };
        Object.defineProperty(GComponent.prototype, "viewWidth", {
            // liu
            get: function () {
                if (this._scrollPane != null)
                    return this._scrollPane.viewWidth;
                else
                    return this.width - this._margin.left - this._margin.right;
            },
            // liu
            set: function (value) {
                if (this._scrollPane != null)
                    this._scrollPane.viewWidth = value;
                else
                    this.width = value + this._margin.left + this._margin.right;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GComponent.prototype, "viewHeight", {
            // liu
            get: function () {
                if (this._scrollPane != null)
                    return this._scrollPane.viewHeight;
                else
                    return this.height - this._margin.top - this._margin.bottom;
            },
            // liu
            set: function (value) {
                if (this._scrollPane != null)
                    this._scrollPane.viewHeight = value;
                else
                    this.height = value + this._margin.top + this._margin.bottom;
            },
            enumerable: true,
            configurable: true
        });
        // liu
        GComponent.prototype.getSnappingPosition = function (xValue, yValue, resultPoint) {
            if (!resultPoint)
                resultPoint = new cc.Vec2();
            var cnt = this._children.length;
            if (cnt == 0) {
                resultPoint.x = 0;
                resultPoint.y = 0;
                return resultPoint;
            }
            this.ensureBoundsCorrect();
            var obj = null;
            var prev = null;
            var i = 0;
            if (yValue != 0) {
                for (; i < cnt; i++) {
                    obj = this._children[i];
                    if (yValue < obj.y) {
                        if (i == 0) {
                            yValue = 0;
                            break;
                        }
                        else {
                            prev = this._children[i - 1];
                            if (yValue < prev.y + prev.actualHeight / 2) //top half part
                                yValue = prev.y;
                            else //bottom half part
                                yValue = obj.y;
                            break;
                        }
                    }
                }
                if (i == cnt)
                    yValue = obj.y;
            }
            if (xValue != 0) {
                if (i > 0)
                    i--;
                for (; i < cnt; i++) {
                    obj = this._children[i];
                    if (xValue < obj.x) {
                        if (i == 0) {
                            xValue = 0;
                            break;
                        }
                        else {
                            prev = this._children[i - 1];
                            if (xValue < prev.x + prev.actualWidth / 2) //top half part
                                xValue = prev.x;
                            else //bottom half part
                                xValue = obj.x;
                            break;
                        }
                    }
                }
                if (i == cnt)
                    xValue = obj.x;
            }
            resultPoint.x = xValue;
            resultPoint.y = yValue;
            return resultPoint;
        };
        //liu
        GComponent.prototype.childSortingOrderChanged = function (child, oldValue, newValue) {
            if (newValue === void 0) { newValue = 0; }
            if (newValue == 0) {
                this._sortingChildCount--;
                this.setChildIndex(child, this._children.length);
            }
            else {
                if (oldValue == 0)
                    this._sortingChildCount++;
                var oldIndex = this._children.indexOf(child);
                var index = this.getInsertPosForSortingChild(child);
                if (oldIndex < index)
                    this._setChildIndex(child, oldIndex, index - 1);
                else
                    this._setChildIndex(child, oldIndex, index);
            }
        };
        //liu
        GComponent.prototype.constructFromResource = function () {
            this.constructFromResource2(null, 0);
        };
        //
        GComponent.prototype.constructFromResource2 = function (objectPool, poolIndex) {
            var xml = this.packageItem.owner.getItemAsset(this.packageItem);
            this._underConstruct = true;
            var str;
            var arr;
            str = xml.attributes.size;
            arr = str.split(",");
            this.sourceWidth = parseInt(arr[0]);
            this.sourceHeight = parseInt(arr[1]);
            this.initWidth = this.sourceWidth;
            this.initHeight = this.sourceHeight;
            this.setSize(this.sourceWidth, this.sourceHeight);
            str = xml.attributes.restrictSize;
            if (str) {
                arr = str.split(",");
                this.minWidth = parseInt(arr[0]);
                this.maxWidth = parseInt(arr[1]);
                this.minHeight = parseInt(arr[2]);
                this.maxHeight = parseInt(arr[3]);
            }
            str = xml.attributes.pivot;
            if (str) {
                arr = str.split(",");
                str = xml.attributes.anchor;
                // this.internalSetPivot(parseFloat(arr[0]), parseFloat(arr[1]), str == "true");
                this.setPivot(parseFloat(arr[0]), parseFloat(arr[1]), str == "true");
            }
            str = xml.attributes.opaque;
            this.opaque = str != "false";
            /*
            p = xml->Attribute("hitTest");
            if (p)
            {
                std::vector<string> arr;
                ToolSet::splitString(p, ',', arr);
                PixelHitTestData* hitTestData = _packageItem->owner->getPixelHitTestData(arr[0]);
                if (hitTestData != nullptr)
                    setHitArea(new PixelHitTest(hitTestData, atoi(arr[1].c_str()), atoi(arr[2].c_str())));
            }
             */
            var overflow;
            str = xml.attributes.overflow;
            if (str)
                overflow = fairygui.parseOverflowType(str);
            else
                overflow = fairygui.OverflowType.Visible;
            str = xml.attributes.margin;
            if (str)
                this._margin.parse(str);
            if (overflow == fairygui.OverflowType.Scroll) {
                var scroll;
                str = xml.attributes.scroll;
                if (str)
                    scroll = fairygui.parseScrollType(str);
                else
                    scroll = fairygui.ScrollType.Vertical;
                var scrollBarDisplay;
                str = xml.attributes.scrollBar;
                if (str)
                    scrollBarDisplay = fairygui.parseScrollBarDisplayType(str);
                else
                    scrollBarDisplay = fairygui.ScrollBarDisplayType.Default;
                var scrollBarFlags;
                str = xml.attributes.scrollBarFlags;
                if (str)
                    scrollBarFlags = parseInt(str);
                else
                    scrollBarFlags = 0;
                var scrollBarMargin = new fairygui.Margin();
                str = xml.attributes.scrollBarMargin;
                if (str)
                    scrollBarMargin.parse(str);
                var vtScrollBarRes;
                var hzScrollBarRes;
                str = xml.attributes.scrollBarRes;
                if (str) {
                    arr = str.split(",");
                    vtScrollBarRes = arr[0];
                    hzScrollBarRes = arr[1];
                }
                var headerRes;
                var footerRes;
                str = xml.attributes.ptrRes;
                if (str) {
                    arr = str.split(",");
                    headerRes = arr[0];
                    footerRes = arr[1];
                }
                this.setupScroll(scrollBarMargin, scroll, scrollBarDisplay, scrollBarFlags, vtScrollBarRes, hzScrollBarRes, headerRes, footerRes);
            }
            else
                this.setupOverflow(overflow);
            this._buildingDisplayList = true;
            var col = xml.children;
            var length1 = 0;
            if (col)
                length1 = col.length;
            var i;
            var controller;
            for (i = 0; i < length1; i++) {
                var cxml = col[i];
                if (cxml.name == "controller") {
                    controller = new fairygui.Controller();
                    this._controllers.push(controller);
                    controller._parent = this;
                    controller.setup(cxml);
                }
            }
            var child;
            var displayList = this.packageItem.displayList;
            var childCount = displayList.length;
            for (i = 0; i < childCount; i++) {
                var di = displayList[i];
                if (objectPool != null) {
                    child = objectPool[poolIndex + i];
                }
                else if (di.packageItem) {
                    //TODO 不一致
                    child = fairygui.UIObjectFactory.newObject(di.packageItem);
                    child.packageItem = di.packageItem;
                    child.constructFromResource();
                }
                else
                    child = fairygui.UIObjectFactory.newObject2(di.type);
                child._underConstruct = true;
                child.setup_beforeAdd(di.desc);
                child.parent = this;
                this._children.push(child);
            }
            this.relations.setup(xml);
            for (i = 0; i < childCount; i++)
                this._children[i].relations.setup(displayList[i].desc);
            for (i = 0; i < childCount; i++) {
                child = this._children[i];
                child.setup_afterAdd(displayList[i].desc);
                child._underConstruct = false;
            }
            str = xml.attributes.mask;
            if (str) {
                //TODO 不一致
                this.mask = this.getChildById(str).displayObject;
            }
            var trans;
            for (i = 0; i < length1; i++) {
                var cxml = col[i];
                if (cxml.name == "transition") {
                    trans = new fairygui.Transition(this);
                    this._transitions.push(trans);
                    trans.setup(cxml);
                }
            }
            // TODO 不一致
            // if (this._transitions.length > 0) {
            //     this.displayObject.addEventListener(egret.Event.ADDED_TO_STAGE, this.___added, this);
            //     this.displayObject.addEventListener(egret.Event.REMOVED_FROM_STAGE, this.___removed, this);
            // }
            this.applyAllControllers();
            this._buildingDisplayList = false;
            this._underConstruct = false;
            this.buildNativeDisplayList();
            this.setBoundsChangedFlag();
            this.constructFromXML(xml);
        };
        // liu
        GComponent.prototype.constructFromXML = function (xml) {
        };
        GComponent.prototype.setup_afterAdd = function (xml) {
            _super.prototype.setup_afterAdd.call(this, xml);
            var str;
            //liu 不一致
            if (this.scrollPane) {
                str = xml.attributes.pageController;
                if (str)
                    this.scrollPane.pageController = this.parent.getController(str);
            }
            str = xml.attributes.controller;
            if (str) {
                var arr = str.split(",");
                for (var i = 0; i < arr.length; i += 2) {
                    var cc = this.getController(arr[i]);
                    if (cc)
                        cc.selectedPageId = arr[i + 1];
                }
            }
        };
        return GComponent;
    }(fairygui.GObject));
    fairygui.GComponent = GComponent;
})(fairygui || (fairygui = {}));
var fairygui;
(function (fairygui) {
    var GButton = /** @class */ (function (_super) {
        __extends(GButton, _super);
        function GButton() {
            var _this = _super.call(this) || this;
            _this._mode = fairygui.ButtonMode.Common;
            _this._title = "";
            _this._icon = "";
            _this._sound = fairygui.UIConfig.buttonSound;
            _this._soundVolumeScale = fairygui.UIConfig.buttonSoundVolumeScale;
            _this._pageOption = new fairygui.PageOption();
            _this._changeStateOnClick = true;
            _this._downEffect = 0;
            _this._downEffectValue = 0.8;
            return _this;
        }
        Object.defineProperty(GButton.prototype, "icon", {
            get: function () {
                return this._icon;
            },
            set: function (value) {
                this._icon = value;
                value = (this._selected && this._selectedIcon) ? this._selectedIcon : this._icon;
                if (this._iconObject != null)
                    this._iconObject.icon = value;
                this.updateGear(7);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GButton.prototype, "selectedIcon", {
            get: function () {
                return this._selectedIcon;
            },
            set: function (value) {
                this._selectedIcon = value;
                value = (this._selected && this._selectedIcon) ? this._selectedIcon : this._icon;
                if (this._iconObject != null)
                    this._iconObject.icon = value;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GButton.prototype, "title", {
            get: function () {
                return this._title;
            },
            set: function (value) {
                this._title = value;
                if (this._titleObject)
                    this._titleObject.text = (this._selected && this._selectedTitle) ? this._selectedTitle : this._title;
                this.updateGear(6);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GButton.prototype, "text", {
            get: function () {
                return this.title;
            },
            set: function (value) {
                this.title = value;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GButton.prototype, "selectedTitle", {
            get: function () {
                return this._selectedTitle;
            },
            set: function (value) {
                this._selectedTitle = value;
                if (this._titleObject)
                    this._titleObject.text = (this._selected && this._selectedTitle) ? this._selectedTitle : this._title;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GButton.prototype, "titleColor", {
            get: function () {
                if (this._titleObject instanceof GTextField)
                    return this._titleObject.color;
                else if (this._titleObject instanceof GLabel)
                    return this._titleObject.titleColor;
                else if (this._titleObject instanceof GButton)
                    return this._titleObject.titleColor;
                else
                    return 0;
            },
            set: function (value) {
                if (this._titleObject instanceof GTextField)
                    this._titleObject.color = value;
                else if (this._titleObject instanceof GLabel)
                    this._titleObject.titleColor = value;
                else if (this._titleObject instanceof GButton)
                    this._titleObject.titleColor = value;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GButton.prototype, "titleFontSize", {
            get: function () {
                if (this._titleObject instanceof GTextField)
                    return this._titleObject.fontSize;
                else if (this._titleObject instanceof GLabel)
                    return this._titleObject.titleFontSize;
                else if (this._titleObject instanceof GButton)
                    return this._titleObject.titleFontSize;
                else
                    return 0;
            },
            set: function (value) {
                if (this._titleObject instanceof GTextField)
                    this._titleObject.fontSize = value;
                else if (this._titleObject instanceof GLabel)
                    this._titleObject.titleFontSize = value;
                else if (this._titleObject instanceof GButton)
                    this._titleObject.titleFontSize = value;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GButton.prototype, "sound", {
            get: function () {
                return this._sound;
            },
            set: function (val) {
                this._sound = val;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GButton.prototype, "soundVolumeScale", {
            get: function () {
                return this._soundVolumeScale;
            },
            set: function (value) {
                this._soundVolumeScale = value;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GButton.prototype, "selected", {
            get: function () {
                return this._selected;
            },
            set: function (val) {
                if (this._mode == fairygui.ButtonMode.Common)
                    return;
                if (this._selected != val) {
                    this._selected = val;
                    if (this.grayed && this._buttonController && this._buttonController.hasPage(GButton.DISABLED)) {
                        if (this._selected)
                            this.setState(GButton.SELECTED_DISABLED);
                        else
                            this.setState(GButton.DISABLED);
                    }
                    else {
                        if (this._selected)
                            this.setState(this._over ? GButton.SELECTED_OVER : GButton.DOWN);
                        else
                            this.setState(this._over ? GButton.OVER : GButton.UP);
                    }
                    if (this._selectedTitle && this._titleObject)
                        this._titleObject.text = this._selected ? this._selectedTitle : this._title;
                    if (this._selectedIcon) {
                        var str = this._selected ? this._selectedIcon : this._icon;
                        if (this._iconObject != null)
                            this._iconObject.icon = str;
                    }
                    if (this._relatedController
                        && this._parent
                        && !this._parent._buildingDisplayList) {
                        if (this._selected) {
                            this._relatedController.selectedPageId = this._pageOption.id;
                            if (this._relatedController._autoRadioGroupDepth)
                                this._parent.adjustRadioGroupDepth(this, this._relatedController);
                        }
                        else if (this._mode == fairygui.ButtonMode.Check && this._relatedController.selectedPageId == this._pageOption.id)
                            this._relatedController.oppositePageId = this._pageOption.id;
                    }
                }
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GButton.prototype, "mode", {
            get: function () {
                return this._mode;
            },
            set: function (value) {
                if (this._mode != value) {
                    if (value == fairygui.ButtonMode.Common)
                        this.selected = false;
                    this._mode = value;
                }
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GButton.prototype, "relatedController", {
            get: function () {
                return this._relatedController;
            },
            set: function (val) {
                if (val != this._relatedController) {
                    this._relatedController = val;
                    this._pageOption.controller = val;
                    this._pageOption.clear();
                }
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GButton.prototype, "pageOption", {
            get: function () {
                return this._pageOption;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GButton.prototype, "changeStateOnClick", {
            get: function () {
                return this._changeStateOnClick;
            },
            set: function (value) {
                this._changeStateOnClick = value;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GButton.prototype, "linkedPopup", {
            get: function () {
                return this._linkedPopup;
            },
            set: function (value) {
                this._linkedPopup = value;
            },
            enumerable: true,
            configurable: true
        });
        GButton.prototype.addStateListener = function (listener, thisObj) {
            this.addEventListener(StateChangeEvent.CHANGED, listener, thisObj);
        };
        GButton.prototype.removeStateListener = function (listener, thisObj) {
            this.removeEventListener(StateChangeEvent.CHANGED, listener, thisObj);
        };
        GButton.prototype.fireClick = function (downEffect) {
            if (downEffect === void 0) { downEffect = true; }
            if (downEffect && this._mode == fairygui.ButtonMode.Common) {
                this.setState(GButton.OVER);
                fairygui.GTimers.inst.add(100, 1, function () { this.setState(GButton.DOWN); }, this);
                fairygui.GTimers.inst.add(200, 1, function () { this.setState(GButton.UP); }, this);
            }
            this.__click(null);
        };
        GButton.prototype.setState = function (val) {
            if (this._buttonController)
                this._buttonController.selectedPage = val;
            if (this._downEffect == 1) {
                var cnt = this.numChildren;
                if (val == GButton.DOWN || val == GButton.SELECTED_OVER || val == GButton.SELECTED_DISABLED) {
                    var r = this._downEffectValue * 255;
                    var color = (r << 16) + (r << 8) + r;
                    for (var i = 0; i < cnt; i++) {
                        var obj = this.getChildAt(i);
                        if (obj["color"] != undefined && !(obj instanceof GTextField))
                            obj.color = color;
                    }
                }
                else {
                    for (var i = 0; i < cnt; i++) {
                        var obj = this.getChildAt(i);
                        if (obj["color"] != undefined && !(obj instanceof GTextField))
                            obj.color = 0xFFFFFF;
                    }
                }
            }
            else if (this._downEffect == 2) {
                if (val == GButton.DOWN || val == GButton.SELECTED_OVER || val == GButton.SELECTED_DISABLED) {
                    if (!this._downScaled) {
                        this._downScaled = true;
                        this.setScale(this.scaleX * this._downEffectValue, this.scaleY * this._downEffectValue);
                    }
                }
                else {
                    if (this._downScaled) {
                        this._downScaled = false;
                        this.setScale(this.scaleX / this._downEffectValue, this.scaleY / this._downEffectValue);
                    }
                }
            }
        };
        GButton.prototype.handleControllerChanged = function (c) {
            _super.prototype.handleControllerChanged.call(this, c);
            if (this._relatedController == c)
                this.selected = this._pageOption.id == c.selectedPageId;
        };
        GButton.prototype.handleGrayedChanged = function () {
            if (this._buttonController && this._buttonController.hasPage(GButton.DISABLED)) {
                if (this.grayed) {
                    if (this._selected && this._buttonController.hasPage(GButton.SELECTED_DISABLED))
                        this.setState(GButton.SELECTED_DISABLED);
                    else
                        this.setState(GButton.DISABLED);
                }
                else if (this._selected)
                    this.setState(GButton.DOWN);
                else
                    this.setState(GButton.UP);
            }
            else
                _super.prototype.handleGrayedChanged.call(this);
        };
        GButton.prototype.constructFromXML = function (xml) {
            _super.prototype.constructFromXML.call(this, xml);
            xml = fairygui.ToolSet.findChildNode(xml, "Button");
            var str;
            str = xml.attributes.mode;
            if (str)
                this._mode = fairygui.parseButtonMode(str);
            str = xml.attributes.sound;
            if (str != null)
                this._sound = str;
            str = xml.attributes.volume;
            if (str)
                this._soundVolumeScale = parseInt(str) / 100;
            str = xml.attributes.downEffect;
            if (str) {
                this._downEffect = str == "dark" ? 1 : (str == "scale" ? 2 : 0);
                str = xml.attributes.downEffectValue;
                this._downEffectValue = parseFloat(str);
                if (this._downEffect == 2)
                    this.setPivot(0.5, 0.5);
            }
            this._buttonController = this.getController("button");
            this._titleObject = this.getChild("title");
            this._iconObject = this.getChild("icon");
            if (this._titleObject != null)
                this._title = this._titleObject.text;
            if (this._iconObject != null)
                this._icon = this._iconObject.icon;
            if (this._mode == fairygui.ButtonMode.Common)
                this.setState(GButton.UP);
            // this.addEventListener(egret.TouchEvent.TOUCH_BEGIN, this.__mousedown, this);
            // this.addEventListener(egret.TouchEvent.TOUCH_TAP, this.__click, this);
        };
        GButton.prototype.setup_afterAdd = function (xml) {
            _super.prototype.setup_afterAdd.call(this, xml);
            xml = fairygui.ToolSet.findChildNode(xml, "Button");
            if (xml) {
                var str;
                str = xml.attributes.title;
                if (str)
                    this.title = str;
                str = xml.attributes.icon;
                if (str)
                    this.icon = str;
                str = xml.attributes.selectedTitle;
                if (str)
                    this.selectedTitle = str;
                str = xml.attributes.selectedIcon;
                if (str)
                    this.selectedIcon = str;
                str = xml.attributes.titleColor;
                if (str)
                    this.titleColor = fairygui.ToolSet.convertFromHtmlColor(str);
                str = xml.attributes.titleFontSize;
                if (str)
                    this.titleFontSize = parseInt(str);
                str = xml.attributes.sound;
                if (str != null)
                    this._sound = str;
                str = xml.attributes.volume;
                if (str)
                    this._soundVolumeScale = parseInt(str) / 100;
                str = xml.attributes.controller;
                if (str)
                    this._relatedController = this._parent.getController(str);
                else
                    this._relatedController = null;
                this._pageOption.id = xml.attributes.page;
                this.selected = xml.attributes.checked == "true";
            }
        };
        GButton.prototype.__rollover = function (evt) {
            if (!this._buttonController || !this._buttonController.hasPage(GButton.OVER))
                return;
            this._over = true;
            if (this._down)
                return;
            this.setState(this._selected ? GButton.SELECTED_OVER : GButton.OVER);
        };
        GButton.prototype.__rollout = function (evt) {
            if (!this._buttonController || !this._buttonController.hasPage(GButton.OVER))
                return;
            this._over = false;
            if (this._down)
                return;
            this.setState(this._selected ? GButton.DOWN : GButton.UP);
        };
        GButton.prototype.__mousedown = function (evt) {
            this._down = true;
            fairygui.GRoot.inst.nativeStage.addEventListener(egret.TouchEvent.TOUCH_END, this.__mouseup, this);
            if (this._mode == fairygui.ButtonMode.Common) {
                if (this.grayed && this._buttonController && this._buttonController.hasPage(GButton.DISABLED))
                    this.setState(GButton.SELECTED_DISABLED);
                else
                    this.setState(GButton.DOWN);
            }
            if (this._linkedPopup != null) {
                if (this._linkedPopup instanceof fairygui.Window)
                    (this._linkedPopup).toggleStatus();
                else
                    this.root.togglePopup(this._linkedPopup, this);
            }
        };
        GButton.prototype.__mouseup = function (evt) {
            if (this._down) {
                fairygui.GRoot.inst.nativeStage.removeEventListener(egret.TouchEvent.TOUCH_END, this.__mouseup, this);
                this._down = false;
                if (this._mode == fairygui.ButtonMode.Common) {
                    if (this.grayed && this._buttonController && this._buttonController.hasPage(GButton.DISABLED))
                        this.setState(GButton.DISABLED);
                    else if (this._over)
                        this.setState(GButton.OVER);
                    else
                        this.setState(GButton.UP);
                }
            }
        };
        GButton.prototype.__click = function (evt) {
            if (this._sound) {
                var pi = fairygui.UIPackage.getItemByURL(this._sound);
                if (pi) {
                    var sound = pi.owner.getItemAsset(pi);
                    if (sound)
                        fairygui.GRoot.inst.playOneShotSound(sound, this._soundVolumeScale);
                }
            }
            if (this._mode == fairygui.ButtonMode.Check) {
                if (this._changeStateOnClick) {
                    this.selected = !this._selected;
                    this.dispatchEvent(new StateChangeEvent(StateChangeEvent.CHANGED));
                }
            }
            else if (this._mode == fairygui.ButtonMode.Radio) {
                if (this._changeStateOnClick && !this._selected) {
                    this.selected = true;
                    this.dispatchEvent(new StateChangeEvent(StateChangeEvent.CHANGED));
                }
            }
            else {
                if (this._relatedController)
                    this._relatedController.selectedPageId = this._pageOption.id;
            }
        };
        GButton.UP = "up";
        GButton.DOWN = "down";
        GButton.OVER = "over";
        GButton.SELECTED_OVER = "selectedOver";
        GButton.DISABLED = "disabled";
        GButton.SELECTED_DISABLED = "selectedDisabled";
        return GButton;
    }(fairygui.GComponent));
    fairygui.GButton = GButton;
})(fairygui || (fairygui = {}));
var fairygui;
(function (fairygui) {
    var GearAnimation = /** @class */ (function (_super) {
        __extends(GearAnimation, _super);
        function GearAnimation(owner) {
            return _super.call(this, owner) || this;
        }
        GearAnimation.prototype.init = function () {
            this._default = new GearAnimationValue(this._owner.playing, this._owner.frame);
            this._storage = {};
        };
        GearAnimation.prototype.addStatus = function (pageId, value) {
            if (value == "-" || value.length == 0)
                return;
            var gv;
            if (pageId == null)
                gv = this._default;
            else {
                gv = new GearAnimationValue();
                this._storage[pageId] = gv;
            }
            var arr = value.split(",");
            gv.frame = parseInt(arr[0]);
            gv.playing = arr[1] == "p";
        };
        GearAnimation.prototype.apply = function () {
            this._owner._gearLocked = true;
            var gv = this._storage[this._controller.selectedPageId];
            if (!gv)
                gv = this._default;
            this._owner.frame = gv.frame;
            this._owner.playing = gv.playing;
            this._owner._gearLocked = false;
        };
        GearAnimation.prototype.updateState = function () {
            var gv = this._storage[this._controller.selectedPageId];
            if (!gv) {
                gv = new GearAnimationValue();
                this._storage[this._controller.selectedPageId] = gv;
            }
            gv.frame = this._owner.frame;
            gv.playing = this._owner.playing;
        };
        return GearAnimation;
    }(fairygui.GearBase));
    fairygui.GearAnimation = GearAnimation;
    var GearAnimationValue = /** @class */ (function () {
        function GearAnimationValue(playing, frame) {
            if (playing === void 0) { playing = true; }
            if (frame === void 0) { frame = 0; }
            this.playing = playing;
            this.frame = frame;
        }
        return GearAnimationValue;
    }());
})(fairygui || (fairygui = {}));
var fairygui;
(function (fairygui) {
    var GearColor = /** @class */ (function (_super) {
        __extends(GearColor, _super);
        function GearColor(owner) {
            return _super.call(this, owner) || this;
        }
        GearColor.prototype.init = function () {
            if (this._owner["strokeColor"] != undefined)
                this._default = new GearColorValue(this._owner.color, this._owner.strokeColor);
            else
                this._default = new GearColorValue(this._owner.color);
            this._storage = {};
        };
        GearColor.prototype.addStatus = function (pageId, value) {
            if (value == "-" || value.length == 0)
                return;
            var pos = value.indexOf(",");
            var col1;
            var col2;
            if (pos == -1) {
                col1 = fairygui.ToolSet.convertFromHtmlColor(value);
                col2 = NaN;
            }
            else {
                col1 = fairygui.ToolSet.convertFromHtmlColor(value.substr(0, pos));
                col2 = fairygui.ToolSet.convertFromHtmlColor(value.substr(pos + 1));
            }
            if (pageId == null) {
                this._default.color = col1;
                this._default.strokeColor = col2;
            }
            else
                this._storage[pageId] = new GearColorValue(col1, col2);
        };
        GearColor.prototype.apply = function () {
            this._owner._gearLocked = true;
            var gv = this._storage[this._controller.selectedPageId];
            if (!gv)
                gv = this._default;
            this._owner.color = gv.color;
            if (this._owner["strokeColor"] != undefined && !isNaN(gv.strokeColor))
                this._owner.strokeColor = gv.strokeColor;
            this._owner._gearLocked = false;
        };
        GearColor.prototype.updateState = function () {
            var gv = this._storage[this._controller.selectedPageId];
            if (!gv) {
                gv = new GearColorValue(null, null);
                this._storage[this._controller.selectedPageId] = gv;
            }
            gv.color = this._owner.color;
            if (this._owner["strokeColor"] != undefined)
                gv.strokeColor = this._owner.strokeColor;
        };
        return GearColor;
    }(fairygui.GearBase));
    fairygui.GearColor = GearColor;
    var GearColorValue = /** @class */ (function () {
        function GearColorValue(color, strokeColor) {
            if (color === void 0) { color = 0; }
            if (strokeColor === void 0) { strokeColor = 0; }
            this.color = color;
            this.strokeColor = strokeColor;
        }
        return GearColorValue;
    }());
})(fairygui || (fairygui = {}));
var fairygui;
(function (fairygui) {
    var GearDisplay = /** @class */ (function (_super) {
        __extends(GearDisplay, _super);
        function GearDisplay(owner) {
            var _this = _super.call(this, owner) || this;
            _this._displayLockToken = 1;
            _this._visible = 0;
            return _this;
        }
        GearDisplay.prototype.init = function () {
            this.pages = null;
        };
        GearDisplay.prototype.apply = function () {
            this._displayLockToken++;
            if (this._displayLockToken == 0)
                this._displayLockToken = 1;
            if (this.pages == null || this.pages.length == 0
                || this.pages.indexOf(this._controller.selectedPageId) != -1)
                this._visible = 1;
            else
                this._visible = 0;
        };
        GearDisplay.prototype.addLock = function () {
            this._visible++;
            return this._displayLockToken;
        };
        GearDisplay.prototype.releaseLock = function (token) {
            if (token == this._displayLockToken)
                this._visible--;
        };
        Object.defineProperty(GearDisplay.prototype, "connected", {
            get: function () {
                return this._controller == null || this._visible > 0;
            },
            enumerable: true,
            configurable: true
        });
        return GearDisplay;
    }(fairygui.GearBase));
    fairygui.GearDisplay = GearDisplay;
})(fairygui || (fairygui = {}));
var fairygui;
(function (fairygui) {
    var GearLook = /** @class */ (function (_super) {
        __extends(GearLook, _super);
        function GearLook(owner) {
            return _super.call(this, owner) || this;
        }
        GearLook.prototype.init = function () {
            this._default = new GearLookValue(this._owner.alpha, this._owner.rotation, this._owner.grayed, this._owner.touchable);
            this._storage = {};
        };
        GearLook.prototype.addStatus = function (pageId, value) {
            if (value == "-" || value.length == 0)
                return;
            var arr = value.split(",");
            var gv;
            if (pageId == null)
                gv = this._default;
            else {
                gv = new GearLookValue();
                this._storage[pageId] = gv;
            }
            gv.alpha = parseFloat(arr[0]);
            gv.rotation = parseInt(arr[1]);
            gv.grayed = arr[2] == "1" ? true : false;
            if (arr.length < 4)
                gv.touchable = this._owner.touchable;
            else
                gv.touchable = arr[3] == "1" ? true : false;
        };
        GearLook.prototype.apply = function () {
            var gv = this._storage[this._controller.selectedPageId];
            if (!gv)
                gv = this._default;
            if (this._tween && !fairygui.UIPackage._constructing && !fairygui.GearBase.disableAllTweenEffect) {
                this._owner._gearLocked = true;
                this._owner.grayed = gv.grayed;
                this._owner.touchable = gv.touchable;
                this._owner._gearLocked = false;
                if (this._tweener != null) {
                    if (this._tweener.endValue.x != gv.alpha || this._tweener.endValue.y != gv.rotation) {
                        this._tweener.kill(true);
                        this._tweener = null;
                    }
                    else
                        return;
                }
                var a = gv.alpha != this._owner.alpha;
                var b = gv.rotation != this._owner.rotation;
                if (a || b) {
                    if (this._owner.checkGearController(0, this._controller))
                        this._displayLockToken = this._owner.addDisplayLock();
                    this._tweener = fairygui.GTween.to2(this._owner.alpha, this._owner.rotation, gv.alpha, gv.rotation, this._tweenTime)
                        .setDelay(this._tweenDelay)
                        .setEase(this._easeType)
                        .setUserData((a ? 1 : 0) + (b ? 2 : 0))
                        .setTarget(this)
                        .onUpdate(this.__tweenUpdate, this)
                        .onComplete(this.__tweenComplete, this);
                }
            }
            else {
                this._owner._gearLocked = true;
                this._owner.grayed = gv.grayed;
                this._owner.touchable = gv.touchable;
                this._owner.alpha = gv.alpha;
                this._owner.rotation = gv.rotation;
                this._owner._gearLocked = false;
            }
        };
        GearLook.prototype.__tweenUpdate = function (tweener) {
            var flag = tweener.userData;
            this._owner._gearLocked = true;
            if ((flag & 1) != 0)
                this._owner.alpha = tweener.value.x;
            if ((flag & 2) != 0)
                this._owner.rotation = tweener.value.y;
            this._owner._gearLocked = false;
        };
        GearLook.prototype.__tweenComplete = function () {
            if (this._displayLockToken != 0) {
                this._owner.releaseDisplayLock(this._displayLockToken);
                this._displayLockToken = 0;
            }
            this._tweener = null;
        };
        GearLook.prototype.updateState = function () {
            var gv = this._storage[this._controller.selectedPageId];
            if (!gv) {
                gv = new GearLookValue();
                this._storage[this._controller.selectedPageId] = gv;
            }
            gv.alpha = this._owner.alpha;
            gv.rotation = this._owner.rotation;
            gv.grayed = this._owner.grayed;
            gv.touchable = this._owner.touchable;
        };
        return GearLook;
    }(fairygui.GearBase));
    fairygui.GearLook = GearLook;
    var GearLookValue = /** @class */ (function () {
        function GearLookValue(alpha, rotation, grayed, touchable) {
            if (alpha === void 0) { alpha = 0; }
            if (rotation === void 0) { rotation = 0; }
            if (grayed === void 0) { grayed = false; }
            if (touchable === void 0) { touchable = true; }
            this.alpha = alpha;
            this.rotation = rotation;
            this.grayed = grayed;
            this.touchable = touchable;
        }
        return GearLookValue;
    }());
})(fairygui || (fairygui = {}));
var fairygui;
(function (fairygui) {
    var GGraph = /** @class */ (function (_super) {
        __extends(GGraph, _super);
        function GGraph() {
            var _this = _super.call(this) || this;
            _this._type = 0;
            _this._lineSize = 0;
            _this._lineSize = 1;
            _this._lineAlpha = 1;
            _this._fillAlpha = 1;
            _this._fillColor = cc.Color.BLACK;
            _this._lineColor = cc.Color.BLACK;
            return _this;
        }
        GGraph.prototype.createDisplayObject = function () {
            this._node = new cc.Node("GGraph_DisplayObject");
            this._node["$owner"] = this;
            this._node.addComponent(cc.Graphics);
            this.setDisplayObject(this._node);
        };
        Object.defineProperty(GGraph.prototype, "graphics", {
            get: function () {
                return this._node.getComponent(cc.Graphics);
            },
            enumerable: true,
            configurable: true
        });
        //liu 不一致 TODO
        GGraph.prototype.drawRect = function (aWidth, aHeight, lineSize, lineColor, fillColor) {
            this._type = 0;
            this.setSize(aWidth, aHeight);
            this._type = 1;
            this._lineSize = lineSize;
            this._lineColor = lineColor;
            // this._lineAlpha = lineAlpha;
            this._fillColor = fillColor;
            // this._fillAlpha = fillAlpha;
            // this._corner = corner;
            this.drawCommon();
        };
        GGraph.prototype.drawEllipse = function (lineSize, lineColor, lineAlpha, fillColor, fillAlpha) {
            this._type = 2;
            this._lineSize = lineSize;
            this._lineColor = lineColor;
            this._lineAlpha = lineAlpha;
            this._fillColor = fillColor;
            this._fillAlpha = fillAlpha;
            this._corner = null;
            this.drawCommon();
        };
        GGraph.prototype.clearGraphics = function () {
            if (this.graphics) {
                this._type = 0;
                this.graphics.clear();
            }
        };
        Object.defineProperty(GGraph.prototype, "color", {
            get: function () {
                return this._fillColor;
            },
            set: function (value) {
                this._fillColor = value;
                if (this._type != 0)
                    this.drawCommon();
            },
            enumerable: true,
            configurable: true
        });
        GGraph.prototype.drawCommon = function () {
            this.graphics.clear();
            var w = this.width;
            var h = this.height;
            if (w == 0 || h == 0)
                return;
            if (this._lineSize == 0)
                // this.graphics.lineStyle(0, 0, 0);
                this.graphics.lineWidth = 0;
            else
                // this.graphics.lineStyle(this._lineSize, this._lineColor, this._lineAlpha);
                this.graphics.lineWidth = this._lineSize;
            this.graphics.strokeColor = this._lineColor;
            // this.graphics.beginFill(this._fillColor, this._fillAlpha);
            if (this._type == 1) {
                if (this._corner) {
                    // if (this._corner.length == 1)
                    //     this.graphics.drawRoundRect(0, 0, w, h, this._corner[0]*2, this._corner[0]*2);
                    // else
                    //     this.graphics.drawRoundRect(0, 0, w, h, this._corner[0]*2, this._corner[1]*2);
                    this.graphics.roundRect(0, 0, w, h, this._corner[0] * 2);
                }
                else
                    this.graphics.rect(0, 0, w, h);
            }
            else
                this.graphics.ellipse(0, 0, w, h);
            // this._graphics.endFill();
        };
        GGraph.prototype.replaceMe = function (target) {
            if (!this._parent)
                throw "parent not set";
            target.name = this.name;
            target.alpha = this.alpha;
            target.rotation = this.rotation;
            target.visible = this.visible;
            target.touchable = this.touchable;
            target.grayed = this.grayed;
            target.setXY(this.x, this.y);
            target.setSize(this.width, this.height);
            var index = this._parent.getChildIndex(this);
            this._parent.addChildAt(target, index);
            target.relations.copyFrom(this.relations);
            this._parent.removeChild(this, true);
        };
        GGraph.prototype.addBeforeMe = function (target) {
            if (this._parent == null)
                throw "parent not set";
            var index = this._parent.getChildIndex(this);
            this._parent.addChildAt(target, index);
        };
        GGraph.prototype.addAfterMe = function (target) {
            if (this._parent == null)
                throw "parent not set";
            var index = this._parent.getChildIndex(this);
            index++;
            this._parent.addChildAt(target, index);
        };
        // public setNativeObject(obj: cc.Node): void {
        //     this.delayCreateDisplayObject();
        //     this.displayObject.addChild(obj);
        // }
        // private delayCreateDisplayObject(): void {
        //     if (!this.displayObject) {
        //         var sprite: UISprite = new UISprite();
        //         sprite["$owner"] = this;
        //         this.setDisplayObject(sprite);
        //         if (this._parent)
        //             this._parent.childStateChanged(this);
        //         this.handleXYChanged();
        //         sprite.alpha = this.alpha;
        //         sprite.rotation = this.rotation;
        //         sprite.visible = this.visible;
        //         sprite.touchEnabled = this.touchable;
        //         sprite.touchChildren = this.touchable;
        //         sprite.hitArea = new egret.Rectangle(0, 0, this.width, this.height);
        //     }
        //     else {
        //         (<egret.Sprite>(this.displayObject)).graphics.clear();
        //         (<egret.Sprite>(this.displayObject)).removeChildren();
        //         this._graphics = null;
        //     }
        // }
        GGraph.prototype.handleSizeChanged = function () {
            if (this.graphics) {
                if (this._type != 0)
                    this.drawCommon();
            }
            // if (this.displayObject instanceof UISprite) {
            //     if ((<UISprite>(this.displayObject)).hitArea == null)
            //         (<UISprite>(this.displayObject)).hitArea = new egret.Rectangle(0, 0, this.width, this.height);
            //     else {
            //         (<UISprite>(this.displayObject)).hitArea.width = this.width;
            //         (<UISprite>(this.displayObject)).hitArea.height = this.height;
            //     }
            // }
        };
        GGraph.prototype.setup_beforeAdd = function (xml) {
            var str;
            var type = xml.attributes.type;
            if (type && type != "empty") {
                // var sprite: UISprite = new UISprite();
                // sprite["$owner"] = this;
                // this.setDisplayObject(sprite);
            }
            _super.prototype.setup_beforeAdd.call(this, xml);
            if (this.displayObject != null) {
                // this._graphics = (<egret.Sprite>(this.displayObject)).graphics;
                str = xml.attributes.lineSize;
                if (str)
                    this._lineSize = parseInt(str);
                str = xml.attributes.lineColor;
                if (str) {
                    // var c: number = ToolSet.convertFromHtmlColor(str, true);
                    // this._lineColor = c & 0xFFFFFF;
                    // this._lineAlpha = ((c >> 24) & 0xFF) / 0xFF;
                    this._lineColor = new cc.Color().fromHEX(str);
                    this._lineAlpha = this._lineColor.getA() / 0xFF;
                }
                str = xml.attributes.fillColor;
                if (str) {
                    // c = ToolSet.convertFromHtmlColor(str, true);
                    // this._fillColor = c & 0xFFFFFF;
                    // this._fillAlpha = ((c >> 24) & 0xFF) / 0xFF;
                    this._fillColor = new cc.Color().fromHEX(str);
                    this._fillAlpha = this._fillColor.getA() / 0xFF;
                }
                var arr;
                str = xml.attributes.corner;
                if (str) {
                    arr = str.split(",");
                    if (arr.length > 1)
                        this._corner = [parseInt(arr[0]), parseInt(arr[1])];
                    else
                        this._corner = [parseInt(arr[0])];
                }
                if (type == "rect")
                    this._type = 1;
                else
                    this._type = 2;
                this.drawCommon();
            }
        };
        return GGraph;
    }(fairygui.GObject));
    fairygui.GGraph = GGraph;
})(fairygui || (fairygui = {}));
var fairygui;
(function (fairygui) {
    var GGroup = /** @class */ (function (_super) {
        __extends(GGroup, _super);
        function GGroup() {
            var _this = _super.call(this) || this;
            _this._layout = 0;
            _this._lineGap = 0;
            _this._columnGap = 0;
            _this._percentReady = false;
            _this._boundsChanged = false;
            _this._updating = 0;
            return _this;
        }
        Object.defineProperty(GGroup.prototype, "layout", {
            get: function () {
                return this._layout;
            },
            set: function (value) {
                if (this._layout != value) {
                    this._layout = value;
                    this.setBoundsChangedFlag(true);
                }
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GGroup.prototype, "lineGap", {
            get: function () {
                return this._lineGap;
            },
            set: function (value) {
                if (this._lineGap != value) {
                    this._lineGap = value;
                    this.setBoundsChangedFlag();
                }
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GGroup.prototype, "columnGap", {
            get: function () {
                return this._columnGap;
            },
            set: function (value) {
                if (this._columnGap != value) {
                    this._columnGap = value;
                    this.setBoundsChangedFlag();
                }
            },
            enumerable: true,
            configurable: true
        });
        GGroup.prototype.setBoundsChangedFlag = function (childSizeChanged) {
            if (childSizeChanged === void 0) { childSizeChanged = false; }
            if (this._updating == 0 && this._parent != null) {
                if (childSizeChanged)
                    this._percentReady = false;
                if (!this._boundsChanged) {
                    this._boundsChanged = true;
                    if (this._layout != fairygui.GroupLayoutType.None)
                        fairygui.GTimers.inst.callLater(this.ensureBoundsCorrect, this);
                }
            }
        };
        GGroup.prototype.ensureBoundsCorrect = function () {
            if (this._boundsChanged)
                this.updateBounds();
        };
        GGroup.prototype.updateBounds = function () {
            fairygui.GTimers.inst.remove(this.ensureBoundsCorrect, this);
            this._boundsChanged = false;
            if (this._parent == null)
                return;
            this.handleLayout();
            var cnt = this._parent.numChildren;
            var i;
            var child;
            var ax = Number.POSITIVE_INFINITY, ay = Number.POSITIVE_INFINITY;
            var ar = Number.NEGATIVE_INFINITY, ab = Number.NEGATIVE_INFINITY;
            var tmp;
            var empty = true;
            for (i = 0; i < cnt; i++) {
                child = this._parent.getChildAt(i);
                if (child.group == this) {
                    tmp = child.x;
                    if (tmp < ax)
                        ax = tmp;
                    tmp = child.y;
                    if (tmp < ay)
                        ay = tmp;
                    tmp = child.x + child.width;
                    if (tmp > ar)
                        ar = tmp;
                    tmp = child.y + child.height;
                    if (tmp > ab)
                        ab = tmp;
                    empty = false;
                }
            }
            if (!empty) {
                this._updating = 1;
                this.setXY(ax, ay);
                this._updating = 2;
                this.setSize(ar - ax, ab - ay);
            }
            else {
                this._updating = 2;
                this.setSize(0, 0);
            }
            this._updating = 0;
        };
        GGroup.prototype.handleLayout = function () {
            this._updating |= 1;
            var child;
            var i;
            var cnt;
            if (this._layout == fairygui.GroupLayoutType.Horizontal) {
                var curX = NaN;
                cnt = this._parent.numChildren;
                for (i = 0; i < cnt; i++) {
                    child = this._parent.getChildAt(i);
                    if (child.group != this)
                        continue;
                    if (isNaN(curX))
                        curX = Math.floor(child.x);
                    else
                        child.x = curX;
                    if (child.width != 0)
                        curX += Math.floor(child.width + this._columnGap);
                }
                if (!this._percentReady)
                    this.updatePercent();
            }
            else if (this._layout == fairygui.GroupLayoutType.Vertical) {
                var curY = NaN;
                cnt = this._parent.numChildren;
                for (i = 0; i < cnt; i++) {
                    child = this._parent.getChildAt(i);
                    if (child.group != this)
                        continue;
                    if (isNaN(curY))
                        curY = Math.floor(child.y);
                    else
                        child.y = curY;
                    if (child.height != 0)
                        curY += Math.floor(child.height + this._lineGap);
                }
                if (!this._percentReady)
                    this.updatePercent();
            }
            this._updating &= 2;
        };
        GGroup.prototype.updatePercent = function () {
            this._percentReady = true;
            var cnt = this._parent.numChildren;
            var i;
            var child;
            var size = 0;
            if (this._layout == fairygui.GroupLayoutType.Horizontal) {
                for (i = 0; i < cnt; i++) {
                    child = this._parent.getChildAt(i);
                    if (child.group != this)
                        continue;
                    size += child.width;
                }
                for (i = 0; i < cnt; i++) {
                    child = this._parent.getChildAt(i);
                    if (child.group != this)
                        continue;
                    if (size > 0)
                        child._sizePercentInGroup = child.width / size;
                    else
                        child._sizePercentInGroup = 0;
                }
            }
            else {
                for (i = 0; i < cnt; i++) {
                    child = this._parent.getChildAt(i);
                    if (child.group != this)
                        continue;
                    size += child.height;
                }
                for (i = 0; i < cnt; i++) {
                    child = this._parent.getChildAt(i);
                    if (child.group != this)
                        continue;
                    if (size > 0)
                        child._sizePercentInGroup = child.height / size;
                    else
                        child._sizePercentInGroup = 0;
                }
            }
        };
        GGroup.prototype.moveChildren = function (dx, dy) {
            if ((this._updating & 1) != 0 || this._parent == null)
                return;
            this._updating |= 1;
            var cnt = this._parent.numChildren;
            var i;
            var child;
            for (i = 0; i < cnt; i++) {
                child = this._parent.getChildAt(i);
                if (child.group == this) {
                    child.setXY(child.x + dx, child.y + dy);
                }
            }
            this._updating &= 2;
        };
        GGroup.prototype.resizeChildren = function (dw, dh) {
            if (this._layout == fairygui.GroupLayoutType.None || (this._updating & 2) != 0 || this._parent == null)
                return;
            this._updating |= 2;
            if (!this._percentReady)
                this.updatePercent();
            var cnt = this._parent.numChildren;
            var i;
            var j;
            var child;
            var last = -1;
            var numChildren = 0;
            var lineSize = 0;
            var remainSize = 0;
            var found = false;
            for (i = 0; i < cnt; i++) {
                child = this._parent.getChildAt(i);
                if (child.group != this)
                    continue;
                last = i;
                numChildren++;
            }
            if (this._layout == fairygui.GroupLayoutType.Horizontal) {
                remainSize = lineSize = this.width - (numChildren - 1) * this._columnGap;
                var curX = NaN;
                var nw;
                for (i = 0; i < cnt; i++) {
                    child = this._parent.getChildAt(i);
                    if (child.group != this)
                        continue;
                    if (isNaN(curX))
                        curX = Math.floor(child.x);
                    else
                        child.x = curX;
                    if (last == i)
                        nw = remainSize;
                    else
                        nw = Math.round(child._sizePercentInGroup * lineSize);
                    child.setSize(nw, child._rawHeight + dh, true);
                    remainSize -= child.width;
                    if (last == i) {
                        if (remainSize >= 1) //可能由于有些元件有宽度限制，导致无法铺满
                         {
                            for (j = 0; j <= i; j++) {
                                child = this._parent.getChildAt(j);
                                if (child.group != this)
                                    continue;
                                if (!found) {
                                    nw = child.width + remainSize;
                                    if ((child.maxWidth == 0 || nw < child.maxWidth)
                                        && (child.minWidth == 0 || nw > child.minWidth)) {
                                        child.setSize(nw, child.height, true);
                                        found = true;
                                    }
                                }
                                else
                                    child.x += remainSize;
                            }
                        }
                    }
                    else
                        curX += (child.width + this._columnGap);
                }
            }
            else if (this._layout == fairygui.GroupLayoutType.Vertical) {
                remainSize = lineSize = this.height - (numChildren - 1) * this._lineGap;
                var curY = NaN;
                var nh;
                for (i = 0; i < cnt; i++) {
                    child = this._parent.getChildAt(i);
                    if (child.group != this)
                        continue;
                    if (isNaN(curY))
                        curY = Math.floor(child.y);
                    else
                        child.y = curY;
                    if (last == i)
                        nh = remainSize;
                    else
                        nh = Math.round(child._sizePercentInGroup * lineSize);
                    child.setSize(child._rawWidth + dw, nh, true);
                    remainSize -= child.height;
                    if (last == i) {
                        if (remainSize >= 1) //可能由于有些元件有宽度限制，导致无法铺满
                         {
                            for (j = 0; j <= i; j++) {
                                child = this._parent.getChildAt(j);
                                if (child.group != this)
                                    continue;
                                if (!found) {
                                    nh = child.height + remainSize;
                                    if ((child.maxHeight == 0 || nh < child.maxHeight)
                                        && (child.minHeight == 0 || nh > child.minHeight)) {
                                        child.setSize(child.width, nh, true);
                                        found = true;
                                    }
                                }
                                else
                                    child.y += remainSize;
                            }
                        }
                    }
                    else
                        curY += (child.height + this._lineGap);
                }
            }
            this._updating &= 1;
        };
        GGroup.prototype.handleAlphaChanged = function () {
            if (this._underConstruct)
                return;
            var cnt = this._parent.numChildren;
            for (var i = 0; i < cnt; i++) {
                var child = this._parent.getChildAt(i);
                if (child.group == this)
                    child.alpha = this.alpha;
            }
        };
        GGroup.prototype.handleVisibleChanged = function () {
            if (!this._parent)
                return;
            var cnt = this._parent.numChildren;
            for (var i = 0; i < cnt; i++) {
                var child = this._parent.getChildAt(i);
                if (child.group == this)
                    child.handleVisibleChanged();
            }
        };
        GGroup.prototype.setup_beforeAdd = function (xml) {
            _super.prototype.setup_beforeAdd.call(this, xml);
            var str;
            str = xml.attributes.layout;
            if (str != null) {
                this._layout = fairygui.parseGroupLayoutType(str);
                str = xml.attributes.lineGap;
                if (str)
                    this._lineGap = parseInt(str);
                str = xml.attributes.colGap;
                if (str)
                    this._columnGap = parseInt(str);
            }
        };
        GGroup.prototype.setup_afterAdd = function (xml) {
            _super.prototype.setup_afterAdd.call(this, xml);
            if (!this.visible)
                this.handleVisibleChanged();
        };
        return GGroup;
    }(fairygui.GObject));
    fairygui.GGroup = GGroup;
})(fairygui || (fairygui = {}));
var fairygui;
(function (fairygui) {
    var GImage = /** @class */ (function (_super) {
        __extends(GImage, _super);
        //private _c_sprite : cc.Sprite;
        function GImage() {
            var _this = _super.call(this) || this;
            _this._touchDisabled = true;
            _this._flip = fairygui.FlipType.None;
            return _this;
        }
        GImage.prototype.createDisplayObject = function () {
            this._content = new cc.Node("GImage_DisplayObject");
            // this._content["$owner"] = this;
            this._sprite = this._content.addComponent(cc.Sprite);
            // this._content.touchEnabled = false;
            // this.setDisplayObject(this._content);
            this._displayObject = this._content;
        };
        Object.defineProperty(GImage.prototype, "color", {
            // private getColorMatrix(): egret.ColorMatrixFilter {
            //     if (this._matrix)
            //         return this._matrix;
            //     var filters: egret.Filter[] = this.filters;
            //     if (filters) {
            //         for (var i: number = 0; i < filters.length; i++) {
            //             if (egret.is(filters[i], "egret.ColorMatrixFilter")) {
            //                 this._matrix = <egret.ColorMatrixFilter>filters[i];
            //                 return this._matrix;
            //             }
            //         }
            //     }
            //     var cmf: egret.ColorMatrixFilter = new egret.ColorMatrixFilter();
            //     this._matrix = cmf;
            //     filters = filters || [];
            //     filters.push(cmf);
            //     this.filters = filters;
            //     return cmf;
            // }
            get: function () {
                return this._content.color;
            },
            set: function (value) {
                this._content.color = value;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GImage.prototype, "flip", {
            // private applyColor(): void {
            //     var cfm: egret.ColorMatrixFilter = this.getColorMatrix();
            //     var matrix: number[] = cfm.matrix;
            //     matrix[0] = ((this._color >> 16) & 0xFF) / 255;
            //     matrix[6] = ((this._color >> 8) & 0xFF) / 255;
            //     matrix[12] = (this._color & 0xFF) / 255;
            //     cfm.matrix = matrix;
            // }
            get: function () {
                return this._flip;
            },
            //liu 不一致
            set: function (value) {
                if (this._flip != value) {
                    this._flip = value;
                    this._content.scaleX = this._content.scaleY = 1;
                    if (this._flip == fairygui.FlipType.Horizontal || this._flip == fairygui.FlipType.Both)
                        this._content.scaleX = -1;
                    if (this._flip == fairygui.FlipType.Vertical || this._flip == fairygui.FlipType.Both)
                        this._content.scaleY = -1;
                    this.handleXYChanged();
                }
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GImage.prototype, "texture", {
            get: function () {
                return this._sprite.spriteFrame;
            },
            set: function (value) {
                var rect = value.getRect();
                if (value != null) {
                    this.sourceWidth = rect.width;
                    this.sourceHeight = rect.height;
                }
                else {
                    this.sourceWidth = 0;
                    this.sourceHeight = 0;
                }
                this.initWidth = this.sourceWidth;
                this.initHeight = this.sourceHeight;
                // this._content.scale9Grid = null;
                // this._content.fillMode = egret.BitmapFillMode.SCALE;
                this._sprite.spriteFrame = value;
            },
            enumerable: true,
            configurable: true
        });
        GImage.prototype.dispose = function () {
            _super.prototype.dispose.call(this);
        };
        GImage.prototype.constructFromResource = function () {
            this.sourceWidth = this.packageItem.width;
            this.sourceHeight = this.packageItem.height;
            this.initWidth = this.sourceWidth;
            this.initHeight = this.sourceHeight;
            // this._content.scale9Grid = this.packageItem.scale9Grid;
            // this._content.smoothing = this.packageItem.smoothing;
            // if (this.packageItem.scaleByTile)
            //     this._content.fillMode = egret.BitmapFillMode.REPEAT;
            this.setSize(this.sourceWidth, this.sourceHeight);
            this.packageItem.load();
            this._sprite.spriteFrame = this.packageItem.texture;
        };
        GImage.prototype.handleXYChanged = function () {
            _super.prototype.handleXYChanged.call(this);
            if (this._flip != fairygui.FlipType.None) {
                if (this._content.scaleX == -1)
                    this._content.x += this.width;
                if (this._content.scaleY == -1)
                    this._content.y += this.height;
            }
        };
        GImage.prototype.handleSizeChanged = function () {
            this._content.width = this.width;
            this._content.height = this.height;
        };
        GImage.prototype.setup_beforeAdd = function (xml) {
            _super.prototype.setup_beforeAdd.call(this, xml);
            var str;
            str = xml.attributes.color;
            if (str)
                // this.color = ToolSet.convertFromHtmlColor(str);
                this.color = new cc.Color().fromHEX(str);
            // str = xml.attributes.flip;
            // if (str)
            //     this.flip = parseFlipType(str);
        };
        return GImage;
    }(fairygui.GObject));
    fairygui.GImage = GImage;
})(fairygui || (fairygui = {}));
var fairygui;
(function (fairygui) {
    var GObjectPool = /** @class */ (function () {
        function GObjectPool() {
            this._count = 0;
            this._pool = {};
        }
        GObjectPool.prototype.clear = function () {
            for (var i1 in this._pool) {
                var arr = this._pool[i1];
                var cnt = arr.length;
                for (var i = 0; i < cnt; i++)
                    arr[i].dispose();
            }
            this._pool = {};
            this._count = 0;
        };
        Object.defineProperty(GObjectPool.prototype, "count", {
            get: function () {
                return this._count;
            },
            enumerable: true,
            configurable: true
        });
        GObjectPool.prototype.getObject = function (url) {
            url = fairygui.UIPackage.normalizeURL(url);
            if (url == null)
                return null;
            var arr = this._pool[url];
            if (arr != null && arr.length) {
                this._count--;
                return arr.shift();
            }
            var child = fairygui.UIPackage.createObjectFromURL(url);
            return child;
        };
        GObjectPool.prototype.returnObject = function (obj) {
            var url = obj.resourceURL;
            if (!url)
                return;
            var arr = this._pool[url];
            if (arr == null) {
                arr = new Array();
                this._pool[url] = arr;
            }
            this._count++;
            arr.push(obj);
        };
        return GObjectPool;
    }());
    fairygui.GObjectPool = GObjectPool;
})(fairygui || (fairygui = {}));
var fairygui;
(function (fairygui) {
    var GLoader = /** @class */ (function (_super) {
        __extends(GLoader, _super);
        function GLoader() {
            var _this = _super.call(this) || this;
            _this._frame = 0;
            _this._color = 0;
            _this._contentSourceWidth = 0;
            _this._contentSourceHeight = 0;
            _this._contentWidth = 0;
            _this._contentHeight = 0;
            _this._playing = true;
            _this._url = "";
            _this._fill = fairygui.LoaderFillType.None;
            _this._align = fairygui.AlignType.Left;
            _this._verticalAlign = fairygui.VertAlignType.Top;
            _this._showErrorSign = true;
            _this._color = 0xFFFFFF;
            _this._gearAnimation = new fairygui.GearAnimation(_this);
            _this._gearColor = new fairygui.GearColor(_this);
            return _this;
        }
        GLoader.prototype.createDisplayObject = function () {
            this._container = new fairygui.UIContainer();
            this._container["$owner"] = this;
            this._container.hitArea = new cc.Rect();
            this.setDisplayObject(this._container);
        };
        GLoader.prototype.dispose = function () {
            if (this._contentItem == null && (this._content instanceof egret.Bitmap)) {
                var texture = this._content.texture;
                if (texture != null)
                    this.freeExternal(texture);
            }
            if (this._content2 != null)
                this._content2.dispose();
            _super.prototype.dispose.call(this);
        };
        Object.defineProperty(GLoader.prototype, "url", {
            get: function () {
                return this._url;
            },
            set: function (value) {
                if (this._url == value)
                    return;
                this._url = value;
                this.loadContent();
                this.updateGear(7);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GLoader.prototype, "icon", {
            get: function () {
                return this._url;
            },
            set: function (value) {
                this.url = value;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GLoader.prototype, "align", {
            get: function () {
                return this._align;
            },
            set: function (value) {
                if (this._align != value) {
                    this._align = value;
                    this.updateLayout();
                }
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GLoader.prototype, "verticalAlign", {
            get: function () {
                return this._verticalAlign;
            },
            set: function (value) {
                if (this._verticalAlign != value) {
                    this._verticalAlign = value;
                    this.updateLayout();
                }
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GLoader.prototype, "fill", {
            get: function () {
                return this._fill;
            },
            set: function (value) {
                if (this._fill != value) {
                    this._fill = value;
                    this.updateLayout();
                }
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GLoader.prototype, "shrinkOnly", {
            get: function () {
                return this._shrinkOnly;
            },
            set: function (value) {
                if (this._shrinkOnly != value) {
                    this._shrinkOnly = value;
                    this.updateLayout();
                }
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GLoader.prototype, "autoSize", {
            get: function () {
                return this._autoSize;
            },
            set: function (value) {
                if (this._autoSize != value) {
                    this._autoSize = value;
                    this.updateLayout();
                }
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GLoader.prototype, "playing", {
            get: function () {
                return this._playing;
            },
            set: function (value) {
                if (this._playing != value) {
                    this._playing = value;
                    if (this._content instanceof MovieClip)
                        this._content.playing = value;
                    this.updateGear(5);
                }
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GLoader.prototype, "frame", {
            get: function () {
                return this._frame;
            },
            set: function (value) {
                if (this._frame != value) {
                    this._frame = value;
                    if (this._content instanceof MovieClip)
                        this._content.frame = value;
                    this.updateGear(5);
                }
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GLoader.prototype, "timeScale", {
            get: function () {
                if (this._content instanceof MovieClip)
                    return this._content.timeScale;
                else
                    return 1;
            },
            set: function (value) {
                if (this._content instanceof MovieClip)
                    this._content.timeScale = value;
            },
            enumerable: true,
            configurable: true
        });
        GLoader.prototype.advance = function (timeInMiniseconds) {
            if (this._content instanceof MovieClip)
                this._content.advance(timeInMiniseconds);
        };
        Object.defineProperty(GLoader.prototype, "color", {
            get: function () {
                return this._color;
            },
            set: function (value) {
                if (this._color != value) {
                    this._color = value;
                    this.updateGear(4);
                    this.applyColor();
                }
            },
            enumerable: true,
            configurable: true
        });
        GLoader.prototype.applyColor = function () {
            //todo:
        };
        Object.defineProperty(GLoader.prototype, "showErrorSign", {
            get: function () {
                return this._showErrorSign;
            },
            set: function (value) {
                this._showErrorSign = value;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GLoader.prototype, "content", {
            get: function () {
                return this._content;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GLoader.prototype, "component", {
            get: function () {
                return this._content2;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GLoader.prototype, "texture", {
            get: function () {
                if (this._content instanceof egret.Bitmap)
                    return this._content.texture;
                else
                    return null;
            },
            set: function (value) {
                this.url = null;
                this.switchToMovieMode(false);
                this._content.texture = value;
                if (value != null) {
                    this._contentSourceWidth = value.textureWidth;
                    this._contentSourceHeight = value.textureHeight;
                }
                else {
                    this._contentSourceWidth = this._contentHeight = 0;
                }
                this.updateLayout();
            },
            enumerable: true,
            configurable: true
        });
        GLoader.prototype.loadContent = function () {
            this.clearContent();
            if (!this._url)
                return;
            if (fairygui.ToolSet.startsWith(this._url, "ui://"))
                this.loadFromPackage(this._url);
            else
                this.loadExternal();
        };
        GLoader.prototype.loadFromPackage = function (itemURL) {
            this._contentItem = fairygui.UIPackage.getItemByURL(itemURL);
            if (this._contentItem != null) {
                this._contentItem.load();
                if (this._autoSize)
                    this.setSize(this._contentItem.width, this._contentItem.height);
                if (this._contentItem.type == fairygui.PackageItemType.Image) {
                    if (this._contentItem.texture == null) {
                        this.setErrorState();
                    }
                    else {
                        this.switchToMovieMode(false);
                        var bm = this._content;
                        bm.texture = this._contentItem.texture;
                        bm.scale9Grid = this._contentItem.scale9Grid;
                        if (this._contentItem.scaleByTile)
                            bm.fillMode = egret.BitmapFillMode.REPEAT;
                        else
                            bm.fillMode = egret.BitmapFillMode.SCALE;
                        this._contentSourceWidth = this._contentItem.width;
                        this._contentSourceHeight = this._contentItem.height;
                        this.updateLayout();
                    }
                }
                else if (this._contentItem.type == fairygui.PackageItemType.MovieClip) {
                    this.switchToMovieMode(true);
                    this._contentSourceWidth = this._contentItem.width;
                    this._contentSourceHeight = this._contentItem.height;
                    var mc = this._content;
                    mc.interval = this._contentItem.interval;
                    mc.swing = this._contentItem.swing;
                    mc.repeatDelay = this._contentItem.repeatDelay;
                    mc.frames = this._contentItem.frames;
                    this.updateLayout();
                }
                else if (this._contentItem.type == fairygui.PackageItemType.Component) {
                    var obj = fairygui.UIPackage.createObjectFromURL(itemURL);
                    if (!obj)
                        this.setErrorState();
                    else if (!(obj instanceof fairygui.GComponent)) {
                        obj.dispose();
                        this.setErrorState();
                    }
                    else {
                        this._content2 = obj.asCom;
                        this._container.addChild(this._content2.displayObject);
                        this._contentSourceWidth = this._contentItem.width;
                        this._contentSourceHeight = this._contentItem.height;
                        this.updateLayout();
                    }
                }
                else
                    this.setErrorState();
            }
            else
                this.setErrorState();
        };
        GLoader.prototype.switchToMovieMode = function (value) {
            if (value) {
                if (!(this._content instanceof MovieClip))
                    this._content = new MovieClip();
            }
            else {
                if (!(this._content instanceof egret.Bitmap))
                    this._content = new egret.Bitmap();
            }
            this._container.addChild(this._content);
        };
        GLoader.prototype.loadExternal = function () {
            RES.getResAsync(this._url, this.__getResCompleted, this);
        };
        GLoader.prototype.freeExternal = function (texture) {
        };
        GLoader.prototype.onExternalLoadSuccess = function (texture) {
            if (!(this._content instanceof egret.Bitmap)) {
                this._content = new egret.Bitmap();
                this._container.addChild(this._content);
            }
            else
                this._container.addChild(this._content);
            this._content.texture = texture;
            this._content.scale9Grid = null;
            this._content.fillMode = egret.BitmapFillMode.SCALE;
            this._contentSourceWidth = texture.textureWidth;
            this._contentSourceHeight = texture.textureHeight;
            this.updateLayout();
        };
        GLoader.prototype.onExternalLoadFailed = function () {
            this.setErrorState();
        };
        GLoader.prototype.__getResCompleted = function (res, key) {
            if (res instanceof egret.Texture)
                this.onExternalLoadSuccess(res);
            else
                this.onExternalLoadFailed();
        };
        GLoader.prototype.setErrorState = function () {
            if (!this._showErrorSign)
                return;
            if (this._errorSign == null) {
                if (fairygui.UIConfig.loaderErrorSign != null) {
                    this._errorSign = GLoader._errorSignPool.getObject(fairygui.UIConfig.loaderErrorSign);
                }
            }
            if (this._errorSign != null) {
                this._errorSign.setSize(this.width, this.height);
                this._container.addChild(this._errorSign.displayObject);
            }
        };
        GLoader.prototype.clearErrorState = function () {
            if (this._errorSign != null) {
                this._container.removeChild(this._errorSign.displayObject);
                GLoader._errorSignPool.returnObject(this._errorSign);
                this._errorSign = null;
            }
        };
        GLoader.prototype.updateLayout = function () {
            if (this._content2 == null && this._content == null) {
                if (this._autoSize) {
                    this._updatingLayout = true;
                    this.setSize(50, 30);
                    this._updatingLayout = false;
                }
                return;
            }
            this._contentWidth = this._contentSourceWidth;
            this._contentHeight = this._contentSourceHeight;
            if (this._autoSize) {
                this._updatingLayout = true;
                if (this._contentWidth == 0)
                    this._contentWidth = 50;
                if (this._contentHeight == 0)
                    this._contentHeight = 30;
                this.setSize(this._contentWidth, this._contentHeight);
                this._updatingLayout = false;
                if (this._contentWidth == this._width && this._contentHeight == this._height) {
                    if (this._content2 != null) {
                        this._content2.setXY(0, 0);
                        this._content2.setScale(1, 1);
                    }
                    else {
                        this._content.x = 0;
                        this._content.y = 0;
                        this._content.scaleX = 1;
                        this._content.scaleY = 1;
                    }
                    return;
                }
            }
            var sx = 1, sy = 1;
            if (this._fill != fairygui.LoaderFillType.None) {
                sx = this.width / this._contentSourceWidth;
                sy = this.height / this._contentSourceHeight;
                if (sx != 1 || sy != 1) {
                    if (this._fill == fairygui.LoaderFillType.ScaleMatchHeight)
                        sx = sy;
                    else if (this._fill == fairygui.LoaderFillType.ScaleMatchWidth)
                        sy = sx;
                    else if (this._fill == fairygui.LoaderFillType.Scale) {
                        if (sx > sy)
                            sx = sy;
                        else
                            sy = sx;
                    }
                    else if (this._fill == fairygui.LoaderFillType.ScaleNoBorder) {
                        if (sx > sy)
                            sy = sx;
                        else
                            sx = sy;
                    }
                    if (this._shrinkOnly) {
                        if (sx > 1)
                            sx = 1;
                        if (sy > 1)
                            sy = 1;
                    }
                    this._contentWidth = this._contentSourceWidth * sx;
                    this._contentHeight = this._contentSourceHeight * sy;
                }
            }
            if (this._content2 != null) {
                this._content2.setScale(sx, sy);
            }
            else if (this._content instanceof egret.Bitmap) {
                this._content.width = this._contentWidth;
                this._content.height = this._contentHeight;
            }
            else {
                this._content.scaleX = sx;
                this._content.scaleY = sy;
            }
            var nx, ny;
            if (this._align == fairygui.AlignType.Center)
                nx = Math.floor((this.width - this._contentWidth) / 2);
            else if (this._align == fairygui.AlignType.Right)
                nx = this.width - this._contentWidth;
            else
                nx = 0;
            if (this._verticalAlign == fairygui.VertAlignType.Middle)
                ny = Math.floor((this.height - this._contentHeight) / 2);
            else if (this._verticalAlign == fairygui.VertAlignType.Bottom)
                ny = this.height - this._contentHeight;
            else
                ny = 0;
            if (this._content2 != null)
                this._content2.setXY(nx, ny);
            else {
                this._content.x = nx;
                this._content.y = ny;
            }
        };
        GLoader.prototype.clearContent = function () {
            this.clearErrorState();
            if (this._content != null && this._content.parent != null)
                this._container.removeChild(this._content);
            if (this._contentItem == null && (this._content instanceof egret.Bitmap)) {
                var texture = this._content.texture;
                if (texture != null)
                    this.freeExternal(texture);
            }
            if (this._content2 != null) {
                this._container.removeChild(this._content2.displayObject);
                this._content2.dispose();
                this._content2 = null;
            }
            this._contentItem = null;
        };
        GLoader.prototype.handleSizeChanged = function () {
            if (!this._updatingLayout)
                this.updateLayout();
            this._container.hitArea.set(cc.rect(0, 0, this.width, this.height));
        };
        GLoader.prototype.setup_beforeAdd = function (xml) {
            _super.prototype.setup_beforeAdd.call(this, xml);
            var str;
            str = xml.attributes.url;
            if (str)
                this._url = str;
            str = xml.attributes.align;
            if (str)
                this._align = fairygui.parseAlignType(str);
            str = xml.attributes.vAlign;
            if (str)
                this._verticalAlign = fairygui.parseVertAlignType(str);
            str = xml.attributes.fill;
            if (str)
                this._fill = fairygui.parseLoaderFillType(str);
            this._shrinkOnly = xml.attributes.shrinkOnly == "true";
            this._autoSize = xml.attributes.autoSize == "true";
            str = xml.attributes.errorSign;
            if (str)
                this._showErrorSign = str == "true";
            this._playing = xml.attributes.playing != "false";
            str = xml.attributes.color;
            if (str)
                this.color = fairygui.ToolSet.convertFromHtmlColor(str);
            if (this._url)
                this.loadContent();
        };
        GLoader._errorSignPool = new fairygui.GObjectPool();
        return GLoader;
    }(fairygui.GObject));
    fairygui.GLoader = GLoader;
})(fairygui || (fairygui = {}));
var fairygui;
(function (fairygui) {
    var GRoot = /** @class */ (function (_super) {
        __extends(GRoot, _super);
        function GRoot() {
            var _this = _super.call(this) || this;
            if (GRoot._inst == null)
                GRoot._inst = _this;
            _this.opaque = false;
            _this._volumeScale = 1;
            _this._popupStack = new Array();
            _this._justClosedPopups = new Array();
            return _this;
            // this.displayObject.addEventListener(egret.Event.ADDED_TO_STAGE, this.__addedToStage, this);
        }
        Object.defineProperty(GRoot, "inst", {
            get: function () {
                if (GRoot._inst == null)
                    new GRoot();
                return GRoot._inst;
            },
            enumerable: true,
            configurable: true
        });
        // public get nativeStage(): egret.Stage {
        //     return this._nativeStage;
        // }
        // liu 不一致
        GRoot.prototype.showWindow = function (win) {
            this.addChild(win);
            // win.requestFocus();
            //
            // if (win.x > this.width)
            //     win.x = this.width - win.width;
            // else if (win.x + win.width < 0)
            //     win.x = 0;
            //
            // if (win.y > this.height)
            //     win.y = this.height - win.height;
            // else if (win.y + win.height < 0)
            //     win.y = 0;
            this.adjustModalLayer();
        };
        // liu
        GRoot.prototype.hideWindow = function (win) {
            win.hide();
        };
        // liu
        GRoot.prototype.hideWindowImmediately = function (win) {
            if (win.parent == this)
                this.removeChild(win);
            this.adjustModalLayer();
        };
        // liu
        GRoot.prototype.bringToFront = function (win) {
            var cnt = this.numChildren;
            var i;
            if (this._modalLayer.parent != null && !win.modal)
                i = this.getChildIndex(this._modalLayer) - 1;
            else
                i = cnt - 1;
            for (; i >= 0; i--) {
                var g = this.getChildAt(i);
                if (g == win)
                    return;
                if (g instanceof fairygui.Window)
                    break;
            }
            if (i >= 0)
                this.setChildIndex(win, i);
        };
        // liu 不一致
        GRoot.prototype.showModalWait = function (msg) {
            if (msg === void 0) { msg = null; }
            this.getModalWaitingPane();
            if (this._modalWaitPane) {
                this.addChild(this._modalWaitPane);
                this._modalWaitPane.text = msg;
            }
        };
        GRoot.prototype.getModalWaitingPane = function () {
            if (fairygui.UIConfig.globalModalWaiting != null) {
                if (this._modalWaitPane == null) {
                    this._modalWaitPane = fairygui.UIPackage.createObjectFromURL(fairygui.UIConfig.globalModalWaiting);
                    this.sortingOrder = Number.POSITIVE_INFINITY;
                }
                this._modalWaitPane.setSize(this.width, this.height);
                this._modalWaitPane.addRelation(this, fairygui.RelationType.Size);
                return this._modalWaitPane;
            }
            return null;
        };
        // liu
        GRoot.prototype.closeModalWait = function () {
            if (this._modalWaitPane != null && this._modalWaitPane.parent != null)
                this.removeChild(this._modalWaitPane);
        };
        // liu 不一致
        GRoot.prototype.closeAllExceptModals = function () {
            var arr = this._children.slice();
            var cnt = arr.length;
            for (var i = 0; i < cnt; i++) {
                var g = arr[i];
                if ((g instanceof fairygui.Window) && !g.modal) {
                    // (<Window><any>g).hide();
                    this.hideWindowImmediately(g);
                }
            }
        };
        // liu 不一致
        GRoot.prototype.closeAllWindows = function () {
            var arr = this._children.slice();
            var cnt = arr.length;
            for (var i = 0; i < cnt; i++) {
                var g = arr[i];
                if (g instanceof fairygui.Window) {
                    // (<Window><any>g).hide();
                    this.hideWindowImmediately(g);
                }
            }
        };
        // liu
        GRoot.prototype.getTopWindow = function () {
            var cnt = this.numChildren;
            for (var i = cnt - 1; i >= 0; i--) {
                var g = this.getChildAt(i);
                if (g instanceof fairygui.Window) {
                    return g;
                }
            }
            return null;
        };
        Object.defineProperty(GRoot.prototype, "modalLayer", {
            // liu
            get: function () {
                if (!this._modalLayer) {
                    this.createModalLayer();
                }
                return this._modalLayer;
            },
            enumerable: true,
            configurable: true
        });
        // liu 不一致
        GRoot.prototype.createModalLayer = function () {
            this._modalLayer = new fairygui.GGraph();
            // this._modalLayer.setSize(this.width, this.height);
            //this._modalLayer.drawRect(0, 0, 0, UIConfig.modalLayerColor, UIConfig.modalLayerAlpha);
            this._modalLayer.drawRect(this.width, this.height, 0, cc.Color.WHITE, fairygui.UIConfig.modalLayerColor);
            this._modalLayer.addRelation(this, fairygui.RelationType.Size);
        };
        Object.defineProperty(GRoot.prototype, "hasModalWindow", {
            // liu
            get: function () {
                return this._modalLayer.parent != null;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GRoot.prototype, "modalWaiting", {
            // liu 对应isModalWaiting
            get: function () {
                return this._modalWaitPane && this._modalWaitPane.inContainer;
            },
            enumerable: true,
            configurable: true
        });
        // liu 不一致
        GRoot.prototype.showPopup = function (popup, target, downward) {
            if (target === void 0) { target = null; }
            if (downward === void 0) { downward = fairygui.PopupDirection.AUTO; }
            if (this._popupStack.length > 0) {
                // var k: number = this._popupStack.indexOf(popup);
                // if (k != -1) {
                //     for (var i: number = this._popupStack.length - 1; i >= k; i--)
                //         this.removeChild(this._popupStack.pop());
                // }
                this.hidePopup(popup);
            }
            this._popupStack.push(popup);
            if (target != null) {
                var p = target;
                while (p != null) {
                    if (p.parent == this) {
                        if (popup.sortingOrder < p.sortingOrder) {
                            popup.sortingOrder = p.sortingOrder;
                        }
                        break;
                    }
                    p = p.parent;
                }
            }
            this.addChild(popup);
            this.adjustModalLayer();
            if ((popup instanceof fairygui.Window) && target && downward == fairygui.PopupDirection.AUTO) {
                return;
            }
            // var pos: cc.Vec2;
            // var sizeW: number, sizeH: number = 0;
            // if (target) {
            //     pos = target.localToRoot();
            //     sizeW = target.width;
            //     sizeH = target.height;
            // }
            // else {
            //     pos = this.globalToLocal(GRoot.mouseX, GRoot.mouseY);
            // }
            // var xx: number, yy: number;
            // xx = pos.x;
            // if (xx + popup.width > this.width)
            //     xx = xx + sizeW - popup.width;
            // yy = pos.y + sizeH;
            // if ((downward == null && yy + popup.height > this.height)
            //     || downward == false) {
            //     yy = pos.y - popup.height - 1;
            //     if (yy < 0) {
            //         yy = 0;
            //         xx += sizeW / 2;
            //     }
            // }
            //
            // popup.x = xx;
            // popup.y = yy;
            var pos = this.getPopupPosition(popup, target, downward);
            popup.setXY(pos.x, pos.y);
        };
        // liu 不一致
        GRoot.prototype.getPopupPosition = function (popup, target, downward) {
            var pos;
            var sizeW, sizeH = 0;
            if (target) {
                pos = target.localToGlobal(0, 0);
                pos = this.globalToLocal(pos.x, pos.y);
                var p = target.localToGlobal(target.width, target.height);
                p = this.globalToLocal(p.x, p.y);
                sizeW = p.x - pos.x;
                sizeH = p.y - pos.y;
            }
            else {
                //TODO
                pos = this.globalToLocal(GRoot.mouseX, GRoot.mouseY);
            }
            var xx, yy;
            xx = pos.x;
            if (xx + popup.width > this.width)
                xx = xx + sizeW - popup.width;
            yy = pos.y + sizeH;
            if ((downward == fairygui.PopupDirection.AUTO && yy + popup.height > this.height)
                || downward == fairygui.PopupDirection.UP) {
                yy = pos.y - popup.height - 1;
                if (yy < 0) {
                    yy = 0;
                    xx += sizeW / 2;
                }
            }
            return new cc.Vec2(Math.round(xx), Math.round(yy));
        };
        //liu
        GRoot.prototype.togglePopup = function (popup, target, downward) {
            if (target === void 0) { target = null; }
            if (downward === void 0) { downward = fairygui.PopupDirection.AUTO; }
            if (this._justClosedPopups.indexOf(popup) != -1)
                return;
            this.showPopup(popup, target, downward);
        };
        //liu
        GRoot.prototype.hidePopup = function (popup) {
            if (popup === void 0) { popup = null; }
            if (popup != null) {
                var k = this._popupStack.indexOf(popup);
                if (k != -1) {
                    for (var i = this._popupStack.length - 1; i >= k; i--)
                        this.closePopup(this._popupStack.pop());
                }
            }
            else {
                var cnt = this._popupStack.length;
                for (i = cnt - 1; i >= 0; i--)
                    this.closePopup(this._popupStack[i]);
                this._popupStack.length = 0;
            }
        };
        Object.defineProperty(GRoot.prototype, "hasAnyPopup", {
            //liu
            get: function () {
                return this._popupStack.length != 0;
            },
            enumerable: true,
            configurable: true
        });
        //liu
        GRoot.prototype.closePopup = function (target) {
            if (target.parent != null) {
                if (target instanceof fairygui.Window)
                    target.hide();
                else
                    this.removeChild(target);
            }
        };
        //liu 不一致
        GRoot.prototype.showTooltips = function (msg) {
            if (this._defaultTooltipWin == null) {
                var resourceURL = fairygui.UIConfig.tooltipsWin;
                if (!resourceURL) {
                    console.error("UIConfig.tooltipsWin not defined");
                    return;
                }
                this._defaultTooltipWin = fairygui.UIPackage.createObjectFromURL(resourceURL);
                this._defaultTooltipWin.touchable = false; //TODO
            }
            this._defaultTooltipWin.text = msg;
            this.showTooltipsWin(this._defaultTooltipWin);
        };
        //liu 不一致
        GRoot.prototype.showTooltipsWin = function (tooltipWin, position) {
            if (position === void 0) { position = null; }
            this.hideTooltips();
            this._tooltipWin = tooltipWin;
            // GTimers.inst.callLater(this.doShowTooltipsWin, this);
            this.doShowTooltipsWin(position);
        };
        //liu
        GRoot.prototype.doShowTooltipsWin = function (position) {
            if (position === void 0) { position = null; }
            if (!this._tooltipWin) {
                return;
            }
            var xx = 0;
            var yy = 0;
            if (position == null) {
                //TODO
                xx = GRoot.mouseX + 10;
                yy = GRoot.mouseY + 20;
            }
            else {
                xx = position.x;
                yy = position.y;
            }
            var pt = this.globalToLocal(xx, yy);
            xx = pt.x;
            yy = pt.y;
            if (xx + this._tooltipWin.width > this.width) {
                xx = xx - this._tooltipWin.width - 1;
                if (xx < 0)
                    xx = 10;
            }
            if (yy + this._tooltipWin.height > this.height) {
                yy = yy - this._tooltipWin.height - 1;
                if (xx - this._tooltipWin.width - 1 > 0)
                    xx = xx - this._tooltipWin.width - 1;
                if (yy < 0)
                    yy = 10;
            }
            this._tooltipWin.x = xx;
            this._tooltipWin.y = yy;
            this.addChild(this._tooltipWin);
        };
        //liu
        GRoot.prototype.hideTooltips = function () {
            if (this._tooltipWin != null) {
                if (this._tooltipWin.parent)
                    this.removeChild(this._tooltipWin);
                this._tooltipWin = null;
            }
        };
        Object.defineProperty(GRoot.prototype, "volumeScale", {
            // public getObjectUnderPoint(globalX: number, globalY: number): GObject {
            //     var ret: egret.DisplayObject = this._nativeStage.$hitTest(globalX, globalY);
            //     if (ret)
            //         return ToolSet.displayObjectToGObject(ret);
            //     else
            //         return null;
            // }
            // public get focus(): GObject {
            //     if (this._focusedObject && !this._focusedObject.onStage)
            //         this._focusedObject = null;
            //
            //     return this._focusedObject;
            // }
            //
            // public set focus(value: GObject) {
            //     if (value && (!value.focusable || !value.onStage))
            //         throw "invalid focus target";
            //
            //     this.setFocus(value);
            // }
            //
            // private setFocus(value: GObject) {
            //     if (this._focusedObject != value) {
            //         this._focusedObject = value;
            //         this.dispatchEventWith(GRoot.FOCUS_CHANGED);
            //     }
            // }
            get: function () {
                return this._volumeScale;
            },
            set: function (value) {
                this._volumeScale = value;
            },
            enumerable: true,
            configurable: true
        });
        //liu 不一致
        GRoot.prototype.playOneShotSound = function (sound, volumeScale) {
            if (volumeScale === void 0) { volumeScale = 1; }
            var vs = this._volumeScale * volumeScale;
            var audioID = cc.audioEngine.play(sound, false, vs);
        };
        //liu
        GRoot.prototype.adjustModalLayer = function () {
            if (!this._modalLayer) {
                this.createModalLayer();
            }
            var cnt = this.numChildren;
            if (this._modalWaitPane != null && this._modalWaitPane.parent != null)
                this.setChildIndex(this._modalWaitPane, cnt - 1);
            for (var i = cnt - 1; i >= 0; i--) {
                var g = this.getChildAt(i);
                if ((g instanceof fairygui.Window) && g.modal) {
                    if (this._modalLayer.parent == null)
                        this.addChildAt(this._modalLayer, i);
                    else
                        this.setChildIndexBefore(this._modalLayer, i);
                    return;
                }
            }
            if (this._modalLayer.parent != null)
                this.removeChild(this._modalLayer);
        };
        GRoot.contentScaleFactor = 1;
        GRoot.FOCUS_CHANGED = "FocusChanged";
        return GRoot;
    }(fairygui.GComponent));
    fairygui.GRoot = GRoot;
})(fairygui || (fairygui = {}));
var fairygui;
(function (fairygui) {
    var Margin = /** @class */ (function () {
        function Margin() {
            this.left = 0;
            this.right = 0;
            this.top = 0;
            this.bottom = 0;
        }
        Margin.prototype.parse = function (str) {
            if (!str) {
                this.left = 0;
                this.right = 0;
                this.top = 0;
                this.bottom = 0;
                return;
            }
            var arr = str.split(",");
            if (arr.length == 1) {
                var k = parseInt(arr[0]);
                this.top = k;
                this.bottom = k;
                this.left = k;
                this.right = k;
            }
            else {
                this.top = parseInt(arr[0]);
                this.bottom = parseInt(arr[1]);
                this.left = parseInt(arr[2]);
                this.right = parseInt(arr[3]);
            }
        };
        Margin.prototype.copy = function (source) {
            this.top = source.top;
            this.bottom = source.bottom;
            this.left = source.left;
            this.right = source.right;
        };
        return Margin;
    }());
    fairygui.Margin = Margin;
})(fairygui || (fairygui = {}));
var fairygui;
(function (fairygui) {
    var GTimers = /** @class */ (function () {
        function GTimers() {
            this._enumI = 0;
            this._enumCount = 0;
            this._items = [];
            this._itemPool = [];
            // this._lastTime = egret.getTimer();
            // GTimers.time = this._lastTime;
            // egret.startTick(this.__timer, this);
        }
        Object.defineProperty(GTimers, "inst", {
            get: function () {
                if (GTimers._inst == null) {
                    GTimers._inst = new GTimers();
                }
                return GTimers._inst;
            },
            enumerable: true,
            configurable: true
        });
        GTimers.prototype.getItem = function () {
            if (this._itemPool.length)
                return this._itemPool.pop();
            else
                return new TimerItem();
        };
        GTimers.prototype.findItem = function (callback, thisObj) {
            var len = this._items.length;
            for (var i = 0; i < len; i++) {
                var item = this._items[i];
                if (item.callback == callback && item.thisObj == thisObj)
                    return item;
            }
            return null;
        };
        GTimers.prototype.add = function (delayInMiniseconds, repeat, callback, thisObj, callbackParam) {
            if (callbackParam === void 0) { callbackParam = null; }
            var item = this.findItem(callback, thisObj);
            if (!item) {
                item = this.getItem();
                item.callback = callback;
                item.hasParam = callback.length == 1;
                item.thisObj = thisObj;
                this._items.push(item);
            }
            item.delay = delayInMiniseconds;
            item.counter = 0;
            item.repeat = repeat;
            item.param = callbackParam;
            item.end = false;
        };
        GTimers.prototype.callLater = function (callback, thisObj, callbackParam) {
            if (callbackParam === void 0) { callbackParam = null; }
            this.add(1, 1, callback, thisObj, callbackParam);
        };
        GTimers.prototype.callDelay = function (delay, callback, thisObj, callbackParam) {
            if (callbackParam === void 0) { callbackParam = null; }
            this.add(delay, 1, callback, thisObj, callbackParam);
        };
        GTimers.prototype.callBy24Fps = function (callback, thisObj, callbackParam) {
            if (callbackParam === void 0) { callbackParam = null; }
            this.add(GTimers.FPS24, 0, callback, thisObj, callbackParam);
        };
        GTimers.prototype.exists = function (callback, thisObj) {
            var item = this.findItem(callback, thisObj);
            return item != null;
        };
        GTimers.prototype.remove = function (callback, thisObj) {
            var item = this.findItem(callback, thisObj);
            if (item) {
                var i = this._items.indexOf(item);
                this._items.splice(i, 1);
                if (i < this._enumI)
                    this._enumI--;
                this._enumCount--;
                item.reset();
                this._itemPool.push(item);
            }
        };
        GTimers.prototype.__timer = function (dt) {
            // GTimers.time = timeStamp;
            // GTimers.deltaTime = timeStamp - this._lastTime;
            // this._lastTime = timeStamp;
            GTimers.deltaTime = dt;
            if (GTimers.deltaTime > 100)
                GTimers.deltaTime = 100;
            this._enumI = 0;
            this._enumCount = this._items.length;
            while (this._enumI < this._enumCount) {
                var item = this._items[this._enumI];
                this._enumI++;
                if (item.advance(GTimers.deltaTime)) {
                    if (item.end) {
                        this._enumI--;
                        this._enumCount--;
                        this._items.splice(this._enumI, 1);
                    }
                    if (item.hasParam)
                        item.callback.call(item.thisObj, item.param);
                    else
                        item.callback.call(item.thisObj);
                    if (item.end) {
                        item.reset();
                        this._itemPool.push(item);
                    }
                }
            }
            return false;
        };
        // private _lastTime: number = 0;
        GTimers.deltaTime = 0;
        GTimers.FPS24 = 1000 / 24;
        return GTimers;
    }());
    fairygui.GTimers = GTimers;
    var TimerItem = /** @class */ (function () {
        function TimerItem() {
            this.delay = 0;
            this.counter = 0;
            this.repeat = 0;
        }
        TimerItem.prototype.advance = function (elapsed) {
            if (elapsed === void 0) { elapsed = 0; }
            this.counter += elapsed;
            if (this.counter >= this.delay) {
                this.counter -= this.delay;
                if (this.counter > this.delay)
                    this.counter = this.delay;
                if (this.repeat > 0) {
                    this.repeat--;
                    if (this.repeat == 0)
                        this.end = true;
                }
                return true;
            }
            else
                return false;
        };
        TimerItem.prototype.reset = function () {
            this.callback = null;
            this.thisObj = null;
            this.param = null;
        };
        return TimerItem;
    }());
})(fairygui || (fairygui = {}));
var fairygui;
(function (fairygui) {
    var GScrollBar = /** @class */ (function (_super) {
        __extends(GScrollBar, _super);
        function GScrollBar() {
            var _this = _super.call(this) || this;
            _this._dragOffset = new cc.Vec2();
            _this._scrollPerc = 0;
            return _this;
        }
        GScrollBar.prototype.setScrollPane = function (target, vertical) {
            this._target = target;
            this._vertical = vertical;
        };
        Object.defineProperty(GScrollBar.prototype, "displayPerc", {
            set: function (val) {
                if (this._vertical) {
                    if (!this._fixedGripSize)
                        this._grip.height = val * this._bar.height;
                    this._grip.y = this._bar.y + (this._bar.height - this._grip.height) * this._scrollPerc;
                }
                else {
                    if (!this._fixedGripSize)
                        this._grip.width = val * this._bar.width;
                    this._grip.x = this._bar.x + (this._bar.width - this._grip.width) * this._scrollPerc;
                }
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GScrollBar.prototype, "scrollPerc", {
            set: function (val) {
                this._scrollPerc = val;
                if (this._vertical)
                    this._grip.y = this._bar.y + (this._bar.height - this._grip.height) * this._scrollPerc;
                else
                    this._grip.x = this._bar.x + (this._bar.width - this._grip.width) * this._scrollPerc;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GScrollBar.prototype, "minSize", {
            get: function () {
                if (this._vertical)
                    return (this._arrowButton1 != null ? this._arrowButton1.height : 0) + (this._arrowButton2 != null ? this._arrowButton2.height : 0);
                else
                    return (this._arrowButton1 != null ? this._arrowButton1.width : 0) + (this._arrowButton2 != null ? this._arrowButton2.width : 0);
            },
            enumerable: true,
            configurable: true
        });
        GScrollBar.prototype.constructFromXML = function (xml) {
            _super.prototype.constructFromXML.call(this, xml);
            xml = fairygui.ToolSet.findChildNode(xml, "ScrollBar");
            if (xml != null)
                this._fixedGripSize = xml.attributes.fixedGripSize == "true";
            this._grip = this.getChild("grip");
            if (!this._grip) {
                console.error("需要定义grip");
                return;
            }
            this._bar = this.getChild("bar");
            if (!this._bar) {
                console.error("需要定义bar");
                return;
            }
            this._arrowButton1 = this.getChild("arrow1");
            this._arrowButton2 = this.getChild("arrow2");
            // this._grip.addEventListener(egret.TouchEvent.TOUCH_BEGIN, this.__gripMouseDown, this);
            // if (this._arrowButton1)
            //     this._arrowButton1.addEventListener(egret.TouchEvent.TOUCH_BEGIN, this.__arrowButton1Click, this);
            // if (this._arrowButton2)
            //     this._arrowButton2.addEventListener(egret.TouchEvent.TOUCH_BEGIN, this.__arrowButton2Click, this);
            // this.addEventListener(egret.TouchEvent.TOUCH_BEGIN, this.__barMouseDown, this);
        };
        GScrollBar.prototype.__gripMouseDown = function (evt) {
            if (!this._bar)
                return;
            evt.stopPropagation();
            this.globalToLocal(evt.stageX, evt.stageY, this._dragOffset);
            this._dragOffset.x -= this._grip.x;
            this._dragOffset.y -= this._grip.y;
            this._grip.displayObject.stage.addEventListener(egret.TouchEvent.TOUCH_MOVE, this.__gripMouseMove, this);
            this._grip.displayObject.stage.addEventListener(egret.TouchEvent.TOUCH_END, this.__gripMouseUp, this);
        };
        GScrollBar.prototype.__gripMouseMove = function (evt) {
            if (!this.onStage)
                return;
            var pt = this.globalToLocal(evt.stageX, evt.stageY, GScrollBar.sScrollbarHelperPoint);
            if (this._vertical) {
                var curY = pt.y - this._dragOffset.y;
                this._target.setPercY((curY - this._bar.y) / (this._bar.height - this._grip.height), false);
            }
            else {
                var curX = pt.x - this._dragOffset.x;
                this._target.setPercX((curX - this._bar.x) / (this._bar.width - this._grip.width), false);
            }
        };
        GScrollBar.prototype.__gripMouseUp = function (evt) {
            var st = evt.currentTarget;
            st.removeEventListener(egret.TouchEvent.TOUCH_MOVE, this.__gripMouseMove, this);
            st.removeEventListener(egret.TouchEvent.TOUCH_END, this.__gripMouseUp, this);
        };
        GScrollBar.prototype.__arrowButton1Click = function (evt) {
            evt.stopPropagation();
            if (this._vertical)
                this._target.scrollUp();
            else
                this._target.scrollLeft();
        };
        GScrollBar.prototype.__arrowButton2Click = function (evt) {
            evt.stopPropagation();
            if (this._vertical)
                this._target.scrollDown();
            else
                this._target.scrollRight();
        };
        GScrollBar.prototype.__barMouseDown = function (evt) {
            var pt = this._grip.globalToLocal(evt.stageX, evt.stageY, GScrollBar.sScrollbarHelperPoint);
            if (this._vertical) {
                if (pt.y < 0)
                    this._target.scrollUp(4);
                else
                    this._target.scrollDown(4);
            }
            else {
                if (pt.x < 0)
                    this._target.scrollLeft(4);
                else
                    this._target.scrollRight(4);
            }
        };
        GScrollBar.sScrollbarHelperPoint = new cc.Vec2();
        return GScrollBar;
    }(fairygui.GComponent));
    fairygui.GScrollBar = GScrollBar;
})(fairygui || (fairygui = {}));
var fairygui;
(function (fairygui) {
    var PageOption = /** @class */ (function () {
        function PageOption() {
        }
        Object.defineProperty(PageOption.prototype, "controller", {
            set: function (val) {
                this._controller = val;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(PageOption.prototype, "index", {
            get: function () {
                if (this._id)
                    return this._controller.getPageIndexById(this._id);
                else
                    return -1;
            },
            set: function (pageIndex) {
                this._id = this._controller.getPageId(pageIndex);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(PageOption.prototype, "name", {
            get: function () {
                if (this._id)
                    return this._controller.getPageNameById(this._id);
                else
                    return null;
            },
            set: function (pageName) {
                this._id = this._controller.getPageIdByName(pageName);
            },
            enumerable: true,
            configurable: true
        });
        PageOption.prototype.clear = function () {
            this._id = null;
        };
        Object.defineProperty(PageOption.prototype, "id", {
            get: function () {
                return this._id;
            },
            set: function (id) {
                this._id = id;
            },
            enumerable: true,
            configurable: true
        });
        return PageOption;
    }());
    fairygui.PageOption = PageOption;
})(fairygui || (fairygui = {}));
var fairygui;
(function (fairygui) {
    var RelationItem = /** @class */ (function () {
        function RelationItem(owner) {
            this._owner = owner;
            this._defs = new Array();
        }
        Object.defineProperty(RelationItem.prototype, "owner", {
            get: function () {
                return this._owner;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(RelationItem.prototype, "target", {
            get: function () {
                return this._target;
            },
            set: function (value) {
                if (this._target != value) {
                    if (this._target)
                        this.releaseRefTarget(this._target);
                    this._target = value;
                    if (this._target)
                        this.addRefTarget(this._target);
                }
            },
            enumerable: true,
            configurable: true
        });
        RelationItem.prototype.add = function (relationType, usePercent) {
            if (relationType == fairygui.RelationType.Size) {
                this.add(fairygui.RelationType.Width, usePercent);
                this.add(fairygui.RelationType.Height, usePercent);
                return;
            }
            var length = this._defs.length;
            for (var i = 0; i < length; i++) {
                var def = this._defs[i];
                if (def.type == relationType)
                    return;
            }
            this.internalAdd(relationType, usePercent);
        };
        RelationItem.prototype.internalAdd = function (relationType, usePercent) {
            if (relationType == fairygui.RelationType.Size) {
                this.internalAdd(fairygui.RelationType.Width, usePercent);
                this.internalAdd(fairygui.RelationType.Height, usePercent);
                return;
            }
            var info = new RelationDef();
            info.percent = usePercent;
            info.type = relationType;
            info.axis = (relationType <= fairygui.RelationType.Right_Right || relationType == fairygui.RelationType.Width || relationType >= fairygui.RelationType.LeftExt_Left && relationType <= fairygui.RelationType.RightExt_Right) ? 0 : 1;
            this._defs.push(info);
            //当使用中线关联时，因为需要除以2，很容易因为奇数宽度/高度造成小数点坐标；当使用百分比时，也会造成小数坐标；
            //所以设置了这类关联的对象，自动启用pixelSnapping
            if (usePercent || relationType == fairygui.RelationType.Left_Center || relationType == fairygui.RelationType.Center_Center || relationType == fairygui.RelationType.Right_Center
                || relationType == fairygui.RelationType.Top_Middle || relationType == fairygui.RelationType.Middle_Middle || relationType == fairygui.RelationType.Bottom_Middle)
                this._owner.pixelSnapping = true;
        };
        RelationItem.prototype.remove = function (relationType) {
            if (relationType === void 0) { relationType = 0; }
            if (relationType == fairygui.RelationType.Size) {
                this.remove(fairygui.RelationType.Width);
                this.remove(fairygui.RelationType.Height);
                return;
            }
            var dc = this._defs.length;
            for (var k = 0; k < dc; k++) {
                if (this._defs[k].type == relationType) {
                    this._defs.splice(k, 1);
                    break;
                }
            }
        };
        RelationItem.prototype.copyFrom = function (source) {
            this.target = source.target;
            this._defs.length = 0;
            var length = source._defs.length;
            for (var i = 0; i < length; i++) {
                var info = source._defs[i];
                var info2 = new RelationDef();
                info2.copyFrom(info);
                this._defs.push(info2);
            }
        };
        RelationItem.prototype.dispose = function () {
            if (this._target != null) {
                this.releaseRefTarget(this._target);
                this._target = null;
            }
        };
        Object.defineProperty(RelationItem.prototype, "isEmpty", {
            get: function () {
                return this._defs.length == 0;
            },
            enumerable: true,
            configurable: true
        });
        RelationItem.prototype.applyOnSelfResized = function (dWidth, dHeight, applyPivot) {
            var ox = this._owner.x;
            var oy = this._owner.y;
            var length = this._defs.length;
            for (var i = 0; i < length; i++) {
                var info = this._defs[i];
                switch (info.type) {
                    case fairygui.RelationType.Center_Center:
                        this._owner.x -= (0.5 - (applyPivot ? this._owner.pivotX : 0)) * dWidth;
                        break;
                    case fairygui.RelationType.Right_Center:
                    case fairygui.RelationType.Right_Left:
                    case fairygui.RelationType.Right_Right:
                        this._owner.x -= (1 - (applyPivot ? this._owner.pivotX : 0)) * dWidth;
                        break;
                    case fairygui.RelationType.Middle_Middle:
                        this._owner.y -= (0.5 - (applyPivot ? this._owner.pivotY : 0)) * dHeight;
                        break;
                    case fairygui.RelationType.Bottom_Middle:
                    case fairygui.RelationType.Bottom_Top:
                    case fairygui.RelationType.Bottom_Bottom:
                        this._owner.y -= (1 - (applyPivot ? this._owner.pivotY : 0)) * dHeight;
                        break;
                }
            }
            if (ox != this._owner.x || oy != this._owner.y) {
                ox = this._owner.x - ox;
                oy = this._owner.y - oy;
                this._owner.updateGearFromRelations(1, ox, oy);
                if (this._owner.parent != null) {
                    var len = this._owner.parent._transitions.length;
                    if (len > 0) {
                        for (var i = 0; i < len; ++i) {
                            this._owner.parent._transitions[i].updateFromRelations(this._owner.id, ox, oy);
                        }
                    }
                }
            }
        };
        RelationItem.prototype.applyOnXYChanged = function (info, dx, dy) {
            var tmp;
            switch (info.type) {
                case fairygui.RelationType.Left_Left:
                case fairygui.RelationType.Left_Center:
                case fairygui.RelationType.Left_Right:
                case fairygui.RelationType.Center_Center:
                case fairygui.RelationType.Right_Left:
                case fairygui.RelationType.Right_Center:
                case fairygui.RelationType.Right_Right:
                    this._owner.x += dx;
                    break;
                case fairygui.RelationType.Top_Top:
                case fairygui.RelationType.Top_Middle:
                case fairygui.RelationType.Top_Bottom:
                case fairygui.RelationType.Middle_Middle:
                case fairygui.RelationType.Bottom_Top:
                case fairygui.RelationType.Bottom_Middle:
                case fairygui.RelationType.Bottom_Bottom:
                    this._owner.y += dy;
                    break;
                case fairygui.RelationType.Width:
                case fairygui.RelationType.Height:
                    break;
                case fairygui.RelationType.LeftExt_Left:
                case fairygui.RelationType.LeftExt_Right:
                    tmp = this._owner.xMin;
                    this._owner.width = this._owner._rawWidth - dx;
                    this._owner.xMin = tmp + dx;
                    break;
                case fairygui.RelationType.RightExt_Left:
                case fairygui.RelationType.RightExt_Right:
                    tmp = this._owner.xMin;
                    this._owner.width = this._owner._rawWidth + dx;
                    this._owner.xMin = tmp;
                    break;
                case fairygui.RelationType.TopExt_Top:
                case fairygui.RelationType.TopExt_Bottom:
                    tmp = this._owner.yMin;
                    this._owner.height = this._owner._rawHeight - dy;
                    this._owner.yMin = tmp + dy;
                    break;
                case fairygui.RelationType.BottomExt_Top:
                case fairygui.RelationType.BottomExt_Bottom:
                    tmp = this._owner.yMin;
                    this._owner.height = this._owner._rawHeight + dy;
                    this._owner.yMin = tmp;
                    break;
            }
        };
        RelationItem.prototype.applyOnSizeChanged = function (info) {
            var pos = 0, pivot = 0, delta = 0;
            var v, tmp;
            if (info.axis == 0) {
                if (this._target != this._owner.parent) {
                    pos = this._target.x;
                    if (this._target.pivotAsAnchor)
                        pivot = this._target.pivotX;
                }
                if (info.percent) {
                    if (this._targetWidth != 0)
                        delta = this._target._width / this._targetWidth;
                }
                else
                    delta = this._target._width - this._targetWidth;
            }
            else {
                if (this._target != this._owner.parent) {
                    pos = this._target.y;
                    if (this._target.pivotAsAnchor)
                        pivot = this._target.pivotY;
                }
                if (info.percent) {
                    if (this._targetHeight != 0)
                        delta = this._target._height / this._targetHeight;
                }
                else
                    delta = this._target._height - this._targetHeight;
            }
            switch (info.type) {
                case fairygui.RelationType.Left_Left:
                    if (info.percent)
                        this._owner.xMin = pos + (this._owner.xMin - pos) * delta;
                    else if (pivot != 0)
                        this._owner.x += delta * (-pivot);
                    break;
                case fairygui.RelationType.Left_Center:
                    if (info.percent)
                        this._owner.xMin = pos + (this._owner.xMin - pos) * delta;
                    else
                        this._owner.x += delta * (0.5 - pivot);
                    break;
                case fairygui.RelationType.Left_Right:
                    if (info.percent)
                        this._owner.xMin = pos + (this._owner.xMin - pos) * delta;
                    else
                        this._owner.x += delta * (1 - pivot);
                    break;
                case fairygui.RelationType.Center_Center:
                    if (info.percent)
                        this._owner.xMin = pos + (this._owner.xMin + this._owner._rawWidth * 0.5 - pos) * delta - this._owner._rawWidth * 0.5;
                    else
                        this._owner.x += delta * (0.5 - pivot);
                    break;
                case fairygui.RelationType.Right_Left:
                    if (info.percent)
                        this._owner.xMin = pos + (this._owner.xMin + this._owner._rawWidth - pos) * delta - this._owner._rawWidth;
                    else if (pivot != 0)
                        this._owner.x += delta * (-pivot);
                    break;
                case fairygui.RelationType.Right_Center:
                    if (info.percent)
                        this._owner.xMin = pos + (this._owner.xMin + this._owner._rawWidth - pos) * delta - this._owner._rawWidth;
                    else
                        this._owner.x += delta * (0.5 - pivot);
                    break;
                case fairygui.RelationType.Right_Right:
                    if (info.percent)
                        this._owner.xMin = pos + (this._owner.xMin + this._owner._rawWidth - pos) * delta - this._owner._rawWidth;
                    else
                        this._owner.x += delta * (1 - pivot);
                    break;
                case fairygui.RelationType.Top_Top:
                    if (info.percent)
                        this._owner.yMin = pos + (this._owner.yMin - pos) * delta;
                    else if (pivot != 0)
                        this._owner.y += delta * (-pivot);
                    break;
                case fairygui.RelationType.Top_Middle:
                    if (info.percent)
                        this._owner.yMin = pos + (this._owner.yMin - pos) * delta;
                    else
                        this._owner.y += delta * (0.5 - pivot);
                    break;
                case fairygui.RelationType.Top_Bottom:
                    if (info.percent)
                        this._owner.yMin = pos + (this._owner.yMin - pos) * delta;
                    else
                        this._owner.y += delta * (1 - pivot);
                    break;
                case fairygui.RelationType.Middle_Middle:
                    if (info.percent)
                        this._owner.yMin = pos + (this._owner.yMin + this._owner._rawHeight * 0.5 - pos) * delta - this._owner._rawHeight * 0.5;
                    else
                        this._owner.y += delta * (0.5 - pivot);
                    break;
                case fairygui.RelationType.Bottom_Top:
                    if (info.percent)
                        this._owner.yMin = pos + (this._owner.yMin + this._owner._rawHeight - pos) * delta - this._owner._rawHeight;
                    else if (pivot != 0)
                        this._owner.y += delta * (-pivot);
                    break;
                case fairygui.RelationType.Bottom_Middle:
                    if (info.percent)
                        this._owner.yMin = pos + (this._owner.yMin + this._owner._rawHeight - pos) * delta - this._owner._rawHeight;
                    else
                        this._owner.y += delta * (0.5 - pivot);
                    break;
                case fairygui.RelationType.Bottom_Bottom:
                    if (info.percent)
                        this._owner.yMin = pos + (this._owner.yMin + this._owner._rawHeight - pos) * delta - this._owner._rawHeight;
                    else
                        this._owner.y += delta * (1 - pivot);
                    break;
                case fairygui.RelationType.Width:
                    if (this._owner._underConstruct && this._owner == this._target.parent)
                        v = this._owner.sourceWidth - this._target.initWidth;
                    else
                        v = this._owner._rawWidth - this._targetWidth;
                    if (info.percent)
                        v = v * delta;
                    if (this._target == this._owner.parent) {
                        if (this._owner.pivotAsAnchor) {
                            tmp = this._owner.xMin;
                            this._owner.setSize(this._target._width + v, this._owner._rawHeight, true);
                            this._owner.xMin = tmp;
                        }
                        else
                            this._owner.setSize(this._target._width + v, this._owner._rawHeight, true);
                    }
                    else
                        this._owner.width = this._target._width + v;
                    break;
                case fairygui.RelationType.Height:
                    if (this._owner._underConstruct && this._owner == this._target.parent)
                        v = this._owner.sourceHeight - this._target.initHeight;
                    else
                        v = this._owner._rawHeight - this._targetHeight;
                    if (info.percent)
                        v = v * delta;
                    if (this._target == this._owner.parent) {
                        if (this._owner.pivotAsAnchor) {
                            tmp = this._owner.yMin;
                            this._owner.setSize(this._owner._rawWidth, this._target._height + v, true);
                            this._owner.yMin = tmp;
                        }
                        else
                            this._owner.setSize(this._owner._rawWidth, this._target._height + v, true);
                    }
                    else
                        this._owner.height = this._target._height + v;
                    break;
                case fairygui.RelationType.LeftExt_Left:
                    tmp = this._owner.xMin;
                    if (info.percent)
                        v = pos + (tmp - pos) * delta - tmp;
                    else
                        v = delta * (-pivot);
                    this._owner.width = this._owner._rawWidth - v;
                    this._owner.xMin = tmp + v;
                    break;
                case fairygui.RelationType.LeftExt_Right:
                    tmp = this._owner.xMin;
                    if (info.percent)
                        v = pos + (tmp - pos) * delta - tmp;
                    else
                        v = delta * (1 - pivot);
                    this._owner.width = this._owner._rawWidth - v;
                    this._owner.xMin = tmp + v;
                    break;
                case fairygui.RelationType.RightExt_Left:
                    tmp = this._owner.xMin;
                    if (info.percent)
                        v = pos + (tmp + this._owner._rawWidth - pos) * delta - (tmp + this._owner._rawWidth);
                    else
                        v = delta * (-pivot);
                    this._owner.width = this._owner._rawWidth + v;
                    this._owner.xMin = tmp;
                    break;
                case fairygui.RelationType.RightExt_Right:
                    tmp = this._owner.xMin;
                    if (info.percent) {
                        if (this._owner == this._target.parent) {
                            if (this._owner._underConstruct)
                                this._owner.width = pos + this._target._width - this._target._width * pivot +
                                    (this._owner.sourceWidth - pos - this._target.initWidth + this._target.initWidth * pivot) * delta;
                            else
                                this._owner.width = pos + (this._owner._rawWidth - pos) * delta;
                        }
                        else {
                            v = pos + (tmp + this._owner._rawWidth - pos) * delta - (tmp + this._owner._rawWidth);
                            this._owner.width = this._owner._rawWidth + v;
                            this._owner.xMin = tmp;
                        }
                    }
                    else {
                        if (this._owner == this._target.parent) {
                            if (this._owner._underConstruct)
                                this._owner.width = this._owner.sourceWidth + (this._target._width - this._target.initWidth) * (1 - pivot);
                            else
                                this._owner.width = this._owner._rawWidth + delta * (1 - pivot);
                        }
                        else {
                            v = delta * (1 - pivot);
                            this._owner.width = this._owner._rawWidth + v;
                            this._owner.xMin = tmp;
                        }
                    }
                    break;
                case fairygui.RelationType.TopExt_Top:
                    tmp = this._owner.yMin;
                    if (info.percent)
                        v = pos + (tmp - pos) * delta - tmp;
                    else
                        v = delta * (-pivot);
                    this._owner.height = this._owner._rawHeight - v;
                    this._owner.yMin = tmp + v;
                    break;
                case fairygui.RelationType.TopExt_Bottom:
                    tmp = this._owner.yMin;
                    if (info.percent)
                        v = pos + (tmp - pos) * delta - tmp;
                    else
                        v = delta * (1 - pivot);
                    this._owner.height = this._owner._rawHeight - v;
                    this._owner.yMin = tmp + v;
                    break;
                case fairygui.RelationType.BottomExt_Top:
                    tmp = this._owner.yMin;
                    if (info.percent)
                        v = pos + (tmp + this._owner._rawHeight - pos) * delta - (tmp + this._owner._rawHeight);
                    else
                        v = delta * (-pivot);
                    this._owner.height = this._owner._rawHeight + v;
                    this._owner.yMin = tmp;
                    break;
                case fairygui.RelationType.BottomExt_Bottom:
                    tmp = this._owner.yMin;
                    if (info.percent) {
                        if (this._owner == this._target.parent) {
                            if (this._owner._underConstruct)
                                this._owner.height = pos + this._target._height - this._target._height * pivot +
                                    (this._owner.sourceHeight - pos - this._target.initHeight + this._target.initHeight * pivot) * delta;
                            else
                                this._owner.height = pos + (this._owner._rawHeight - pos) * delta;
                        }
                        else {
                            v = pos + (tmp + this._owner._rawHeight - pos) * delta - (tmp + this._owner._rawHeight);
                            this._owner.height = this._owner._rawHeight + v;
                            this._owner.yMin = tmp;
                        }
                    }
                    else {
                        if (this._owner == this._target.parent) {
                            if (this._owner._underConstruct)
                                this._owner.height = this._owner.sourceHeight + (this._target._height - this._target.initHeight) * (1 - pivot);
                            else
                                this._owner.height = this._owner._rawHeight + delta * (1 - pivot);
                        }
                        else {
                            v = delta * (1 - pivot);
                            this._owner.height = this._owner._rawHeight + v;
                            this._owner.yMin = tmp;
                        }
                    }
                    break;
            }
        };
        RelationItem.prototype.addRefTarget = function (target) {
            if (target != this._owner.parent)
                target.addEventListener(fairygui.GObject.XY_CHANGED, this.__targetXYChanged, this);
            target.addEventListener(fairygui.GObject.SIZE_CHANGED, this.__targetSizeChanged, this);
            target.addEventListener(fairygui.GObject.SIZE_DELAY_CHANGE, this.__targetSizeWillChange, this);
            this._targetX = this._target.x;
            this._targetY = this._target.y;
            this._targetWidth = this._target._width;
            this._targetHeight = this._target._height;
        };
        RelationItem.prototype.releaseRefTarget = function (target) {
            target.removeEventListener(fairygui.GObject.XY_CHANGED, this.__targetXYChanged, this);
            target.removeEventListener(fairygui.GObject.SIZE_CHANGED, this.__targetSizeChanged, this);
            target.removeEventListener(fairygui.GObject.SIZE_DELAY_CHANGE, this.__targetSizeWillChange, this);
        };
        RelationItem.prototype.__targetXYChanged = function (evt) {
            if (this._owner.relations.handling != null || this._owner.group != null && this._owner.group._updating) {
                this._targetX = this._target.x;
                this._targetY = this._target.y;
                return;
            }
            this._owner.relations.handling = this._target;
            var ox = this._owner.x;
            var oy = this._owner.y;
            var dx = this._target.x - this._targetX;
            var dy = this._target.y - this._targetY;
            var length = this._defs.length;
            for (var i = 0; i < length; i++) {
                var info = this._defs[i];
                this.applyOnXYChanged(info, dx, dy);
            }
            this._targetX = this._target.x;
            this._targetY = this._target.y;
            if (ox != this._owner.x || oy != this._owner.y) {
                ox = this._owner.x - ox;
                oy = this._owner.y - oy;
                this._owner.updateGearFromRelations(1, ox, oy);
                if (this._owner.parent != null) {
                    var len = this._owner.parent._transitions.length;
                    if (len > 0) {
                        for (var i = 0; i < len; ++i) {
                            this._owner.parent._transitions[i].updateFromRelations(this._owner.id, ox, oy);
                        }
                    }
                }
            }
            this._owner.relations.handling = null;
        };
        RelationItem.prototype.__targetSizeChanged = function (evt) {
            if (this._owner.relations.handling != null)
                return;
            this._owner.relations.handling = this._target;
            var ox = this._owner.x;
            var oy = this._owner.y;
            var ow = this._owner._rawWidth;
            var oh = this._owner._rawHeight;
            var length = this._defs.length;
            for (var i = 0; i < length; i++) {
                var info = this._defs[i];
                this.applyOnSizeChanged(info);
            }
            this._targetWidth = this._target._width;
            this._targetHeight = this._target._height;
            if (ox != this._owner.x || oy != this._owner.y) {
                ox = this._owner.x - ox;
                oy = this._owner.y - oy;
                this._owner.updateGearFromRelations(1, ox, oy);
                if (this._owner.parent != null) {
                    var len = this._owner.parent._transitions.length;
                    if (len > 0) {
                        for (var i = 0; i < len; ++i) {
                            this._owner.parent._transitions[i].updateFromRelations(this._owner.id, ox, oy);
                        }
                    }
                }
            }
            if (ow != this._owner._rawWidth || oh != this._owner._rawHeight) {
                ow = this._owner._rawWidth - ow;
                oh = this._owner._rawHeight - oh;
                this._owner.updateGearFromRelations(2, ow, oh);
            }
            this._owner.relations.handling = null;
        };
        RelationItem.prototype.__targetSizeWillChange = function (evt) {
            this._owner.relations.sizeDirty = true;
        };
        return RelationItem;
    }());
    fairygui.RelationItem = RelationItem;
    var RelationDef = /** @class */ (function () {
        function RelationDef() {
        }
        RelationDef.prototype.copyFrom = function (source) {
            this.percent = source.percent;
            this.type = source.type;
            this.axis = source.axis;
        };
        return RelationDef;
    }());
    fairygui.RelationDef = RelationDef;
})(fairygui || (fairygui = {}));
var fairygui;
(function (fairygui) {
    var Relations = /** @class */ (function () {
        function Relations(owner) {
            this._owner = owner;
            this._items = new Array();
        }
        Relations.prototype.add = function (target, relationType, usePercent) {
            if (usePercent === void 0) { usePercent = false; }
            var length = this._items.length;
            for (var i = 0; i < length; i++) {
                var item = this._items[i];
                if (item.target == target) {
                    item.add(relationType, usePercent);
                    return;
                }
            }
            var newItem = new fairygui.RelationItem(this._owner);
            newItem.target = target;
            newItem.add(relationType, usePercent);
            this._items.push(newItem);
        };
        Relations.prototype.addItems = function (target, sidePairs) {
            var arr = sidePairs.split(",");
            var s;
            var usePercent;
            var i;
            for (i = 0; i < 2; i++) {
                s = arr[i];
                if (!s)
                    continue;
                if (s.charAt(s.length - 1) == "%") {
                    s = s.substr(0, s.length - 1);
                    usePercent = true;
                }
                else
                    usePercent = false;
                var j = s.indexOf("-");
                if (j == -1)
                    s = s + "-" + s;
                var t = Relations.RELATION_NAMES.indexOf(s);
                if (t == -1)
                    throw "invalid relation type";
                this.add(target, t, usePercent);
            }
        };
        Relations.prototype.remove = function (target, relationType) {
            if (relationType === void 0) { relationType = 0; }
            var cnt = this._items.length;
            var i = 0;
            while (i < cnt) {
                var item = this._items[i];
                if (item.target == target) {
                    item.remove(relationType);
                    if (item.isEmpty) {
                        item.dispose();
                        this._items.splice(i, 1);
                        cnt--;
                    }
                    else
                        i++;
                }
                else
                    i++;
            }
        };
        Relations.prototype.contains = function (target) {
            var length = this._items.length;
            for (var i = 0; i < length; i++) {
                var item = this._items[i];
                if (item.target == target)
                    return true;
            }
            return false;
        };
        Relations.prototype.clearFor = function (target) {
            var cnt = this._items.length;
            var i = 0;
            while (i < cnt) {
                var item = this._items[i];
                if (item.target == target) {
                    item.dispose();
                    this._items.splice(i, 1);
                    cnt--;
                }
                else
                    i++;
            }
        };
        Relations.prototype.clearAll = function () {
            var length = this._items.length;
            for (var i = 0; i < length; i++) {
                var item = this._items[i];
                item.dispose();
            }
            this._items.length = 0;
        };
        Relations.prototype.copyFrom = function (source) {
            this.clearAll();
            var arr = source._items;
            var length = arr.length;
            for (var i = 0; i < length; i++) {
                var ri = arr[i];
                var item = new fairygui.RelationItem(this._owner);
                item.copyFrom(ri);
                this._items.push(item);
            }
        };
        Relations.prototype.dispose = function () {
            this.clearAll();
        };
        Relations.prototype.onOwnerSizeChanged = function (dWidth, dHeight, applyPivot) {
            if (this._items.length == 0)
                return;
            var length = this._items.length;
            for (var i = 0; i < length; i++) {
                var item = this._items[i];
                item.applyOnSelfResized(dWidth, dHeight, applyPivot);
            }
        };
        Relations.prototype.ensureRelationsSizeCorrect = function () {
            if (this._items.length == 0)
                return;
            this.sizeDirty = false;
            var length = this._items.length;
            for (var i = 0; i < length; i++) {
                var item = this._items[i];
                item.target.ensureSizeCorrect();
            }
        };
        Object.defineProperty(Relations.prototype, "empty", {
            get: function () {
                return this._items.length == 0;
            },
            enumerable: true,
            configurable: true
        });
        Relations.prototype.setup = function (xml) {
            var col = xml.children;
            if (col) {
                var targetId;
                var target;
                var length = col.length;
                for (var i = 0; i < length; i++) {
                    var cxml = col[i];
                    if (cxml.name != "relation")
                        continue;
                    targetId = cxml.attributes.target;
                    if (this._owner.parent) {
                        if (targetId)
                            target = this._owner.parent.getChildById(targetId);
                        else
                            target = this._owner.parent;
                    }
                    else {
                        //call from component construction
                        target = (this._owner).getChildById(targetId);
                    }
                    if (target)
                        this.addItems(target, cxml.attributes.sidePair);
                }
            }
        };
        Relations.RELATION_NAMES = [
            "left-left",
            "left-center",
            "left-right",
            "center-center",
            "right-left",
            "right-center",
            "right-right",
            "top-top",
            "top-middle",
            "top-bottom",
            "middle-middle",
            "bottom-top",
            "bottom-middle",
            "bottom-bottom",
            "width-width",
            "height-height",
            "leftext-left",
            "leftext-right",
            "rightext-left",
            "rightext-right",
            "topext-top",
            "topext-bottom",
            "bottomext-top",
            "bottomext-bottom" //23
        ];
        return Relations;
    }());
    fairygui.Relations = Relations;
})(fairygui || (fairygui = {}));
var fairygui;
(function (fairygui) {
    var ScrollPane = /** @class */ (function (_super) {
        __extends(ScrollPane, _super);
        function ScrollPane(owner, scrollType, scrollBarMargin, scrollBarDisplay, flags, vtScrollBarRes, hzScrollBarRes, headerRes, footerRes) {
            var _this = _super.call(this) || this;
            _this._owner = owner;
            _this._maskContainer = new cc.Node();
            // this._maskContainer.addComponent(cc.Mask)
            _this._owner._rootContainer.addChild(_this._maskContainer);
            _this._container = _this._owner._container;
            _this._container.x = 0;
            _this._container.y = 0;
            _this._container.removeFromParent();
            _this._maskContainer.addChild(_this._container);
            _this._scrollBarMargin = scrollBarMargin;
            _this._scrollType = scrollType;
            _this._scrollStep = fairygui.UIConfig.defaultScrollStep;
            _this._mouseWheelStep = _this._scrollStep * 2;
            _this._decelerationRate = fairygui.UIConfig.defaultScrollDecelerationRate;
            _this._displayOnLeft = (flags & 1) != 0;
            _this._snapToItem = (flags & 2) != 0;
            _this._displayInDemand = (flags & 4) != 0;
            _this._pageMode = (flags & 8) != 0;
            if (flags & 16)
                _this._touchEffect = true;
            else if (flags & 32)
                _this._touchEffect = false;
            else
                _this._touchEffect = fairygui.UIConfig.defaultScrollTouchEffect;
            if (flags & 64)
                _this._bouncebackEffect = true;
            else if (flags & 128)
                _this._bouncebackEffect = false;
            else
                _this._bouncebackEffect = fairygui.UIConfig.defaultScrollBounceEffect;
            _this._inertiaDisabled = (flags & 256) != 0;
            if ((flags & 512) == 0)
                _this._maskContainer.scrollRect = new cc.Rect();
            _this._scrollBarVisible = true;
            _this._mouseWheelEnabled = true;
            _this._xPos = 0;
            _this._yPos = 0;
            _this._aniFlag = 0;
            _this._footerLockedSize = 0;
            _this._headerLockedSize = 0;
            if (scrollBarDisplay == fairygui.ScrollBarDisplayType.Default)
                scrollBarDisplay = fairygui.UIConfig.defaultScrollBarDisplay;
            _this._viewSize = new cc.Vec2();
            _this._contentSize = new cc.Vec2();
            _this._pageSize = new cc.Vec2(1, 1);
            _this._overlapSize = new cc.Vec2();
            _this._tweenTime = new cc.Vec2();
            _this._tweenStart = new cc.Vec2();
            _this._tweenDuration = new cc.Vec2();
            _this._tweenChange = new cc.Vec2();
            _this._velocity = new cc.Vec2();
            _this._containerPos = new cc.Vec2();
            _this._beginTouchPos = new cc.Vec2();
            _this._lastTouchPos = new cc.Vec2();
            _this._lastTouchGlobalPos = new cc.Vec2();
            if (scrollBarDisplay == fairygui.ScrollBarDisplayType.Default)
                scrollBarDisplay = fairygui.UIConfig.defaultScrollBarDisplay;
            if (scrollBarDisplay != fairygui.ScrollBarDisplayType.Hidden) {
                if (_this._scrollType == fairygui.ScrollType.Both || _this._scrollType == fairygui.ScrollType.Vertical) {
                    var res = vtScrollBarRes ? vtScrollBarRes : fairygui.UIConfig.verticalScrollBar;
                    if (res) {
                        _this._vtScrollBar = (fairygui.UIPackage.createObjectFromURL(res));
                        if (!_this._vtScrollBar)
                            throw "cannot create scrollbar from " + res;
                        _this._vtScrollBar.setScrollPane(_this, true);
                        _this._owner._rootContainer.addChild(_this._vtScrollBar.displayObject);
                    }
                }
                if (_this._scrollType == fairygui.ScrollType.Both || _this._scrollType == fairygui.ScrollType.Horizontal) {
                    var res = hzScrollBarRes ? hzScrollBarRes : fairygui.UIConfig.horizontalScrollBar;
                    if (res) {
                        _this._hzScrollBar = (fairygui.UIPackage.createObjectFromURL(res));
                        if (!_this._hzScrollBar)
                            throw "cannot create scrollbar from " + res;
                        _this._hzScrollBar.setScrollPane(_this, false);
                        _this._owner._rootContainer.addChild(_this._hzScrollBar.displayObject);
                    }
                }
                _this._scrollBarDisplayAuto = scrollBarDisplay == fairygui.ScrollBarDisplayType.Auto;
                if (_this._scrollBarDisplayAuto) {
                    _this._scrollBarVisible = false;
                    if (_this._vtScrollBar)
                        _this._vtScrollBar.displayObject.visible = false;
                    if (_this._hzScrollBar)
                        _this._hzScrollBar.displayObject.visible = false;
                }
            }
            if (headerRes) {
                _this._header = (fairygui.UIPackage.createObjectFromURL(headerRes));
                if (_this._header == null)
                    throw "cannot create scrollPane header from " + headerRes;
            }
            if (footerRes) {
                _this._footer = (fairygui.UIPackage.createObjectFromURL(footerRes));
                if (_this._footer == null)
                    throw "cannot create scrollPane footer from " + footerRes;
            }
            if (_this._header != null || _this._footer != null)
                _this._refreshBarAxis = (_this._scrollType == fairygui.ScrollType.Both || _this._scrollType == fairygui.ScrollType.Vertical) ? "y" : "x";
            _this.setSize(owner.width, owner.height);
            return _this;
            // this._owner.addEventListener(egret.TouchEvent.TOUCH_BEGIN, this.__touchBegin, this);
        }
        ScrollPane.prototype.dispose = function () {
            if (this._tweening != 0)
                egret.stopTick(this.tweenUpdate, this);
            this._pageController = null;
            if (this._hzScrollBar != null)
                this._hzScrollBar.dispose();
            if (this._vtScrollBar != null)
                this._vtScrollBar.dispose();
            if (this._header != null)
                this._header.dispose();
            if (this._footer != null)
                this._footer.dispose();
        };
        Object.defineProperty(ScrollPane.prototype, "owner", {
            get: function () {
                return this._owner;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ScrollPane.prototype, "hzScrollBar", {
            get: function () {
                return this._hzScrollBar;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ScrollPane.prototype, "vtScrollBar", {
            get: function () {
                return this._vtScrollBar;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ScrollPane.prototype, "header", {
            get: function () {
                return this._header;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ScrollPane.prototype, "footer", {
            get: function () {
                return this._footer;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ScrollPane.prototype, "bouncebackEffect", {
            get: function () {
                return this._bouncebackEffect;
            },
            set: function (sc) {
                this._bouncebackEffect = sc;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ScrollPane.prototype, "touchEffect", {
            get: function () {
                return this._touchEffect;
            },
            set: function (sc) {
                this._touchEffect = sc;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ScrollPane.prototype, "scrollStep", {
            get: function () {
                return this._scrollStep;
            },
            set: function (val) {
                this._scrollStep = val;
                if (this._scrollStep == 0)
                    this._scrollStep = fairygui.UIConfig.defaultScrollStep;
                this._mouseWheelStep = this._scrollStep * 2;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ScrollPane.prototype, "decelerationRate", {
            get: function () {
                return this._decelerationRate;
            },
            set: function (val) {
                this._decelerationRate = val;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ScrollPane.prototype, "snapToItem", {
            get: function () {
                return this._snapToItem;
            },
            set: function (value) {
                this._snapToItem = value;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ScrollPane.prototype, "percX", {
            get: function () {
                return this._overlapSize.x == 0 ? 0 : this._xPos / this._overlapSize.x;
            },
            set: function (value) {
                this.setPercX(value, false);
            },
            enumerable: true,
            configurable: true
        });
        ScrollPane.prototype.setPercX = function (value, ani) {
            if (ani === void 0) { ani = false; }
            this._owner.ensureBoundsCorrect();
            this.setPosX(this._overlapSize.x * fairygui.ToolSet.clamp01(value), ani);
        };
        Object.defineProperty(ScrollPane.prototype, "percY", {
            get: function () {
                return this._overlapSize.y == 0 ? 0 : this._yPos / this._overlapSize.y;
            },
            set: function (value) {
                this.setPercY(value, false);
            },
            enumerable: true,
            configurable: true
        });
        ScrollPane.prototype.setPercY = function (value, ani) {
            if (ani === void 0) { ani = false; }
            this._owner.ensureBoundsCorrect();
            this.setPosY(this._overlapSize.y * fairygui.ToolSet.clamp01(value), ani);
        };
        Object.defineProperty(ScrollPane.prototype, "posX", {
            get: function () {
                return this._xPos;
            },
            set: function (value) {
                this.setPosX(value, false);
            },
            enumerable: true,
            configurable: true
        });
        ScrollPane.prototype.setPosX = function (value, ani) {
            if (ani === void 0) { ani = false; }
            this._owner.ensureBoundsCorrect();
            if (this._loop == 1)
                value = this.loopCheckingNewPos(value, "x");
            value = fairygui.ToolSet.clamp(value, 0, this._overlapSize.x);
            if (value != this._xPos) {
                this._xPos = value;
                this.posChanged(ani);
            }
        };
        Object.defineProperty(ScrollPane.prototype, "posY", {
            get: function () {
                return this._yPos;
            },
            set: function (value) {
                this.setPosY(value, false);
            },
            enumerable: true,
            configurable: true
        });
        ScrollPane.prototype.setPosY = function (value, ani) {
            if (ani === void 0) { ani = false; }
            this._owner.ensureBoundsCorrect();
            if (this._loop == 1)
                value = this.loopCheckingNewPos(value, "y");
            value = fairygui.ToolSet.clamp(value, 0, this._overlapSize.y);
            if (value != this._yPos) {
                this._yPos = value;
                this.posChanged(ani);
            }
        };
        Object.defineProperty(ScrollPane.prototype, "contentWidth", {
            get: function () {
                return this._contentSize.x;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ScrollPane.prototype, "contentHeight", {
            get: function () {
                return this._contentSize.y;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ScrollPane.prototype, "viewWidth", {
            get: function () {
                return this._viewSize.x;
            },
            set: function (value) {
                value = value + this._owner.margin.left + this._owner.margin.right;
                if (this._vtScrollBar != null)
                    value += this._vtScrollBar.width;
                this._owner.width = value;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ScrollPane.prototype, "viewHeight", {
            get: function () {
                return this._viewSize.y;
            },
            set: function (value) {
                value = value + this._owner.margin.top + this._owner.margin.bottom;
                if (this._hzScrollBar != null)
                    value += this._hzScrollBar.height;
                this._owner.height = value;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ScrollPane.prototype, "currentPageX", {
            get: function () {
                if (!this._pageMode)
                    return 0;
                var page = Math.floor(this._xPos / this._pageSize.x);
                if (this._xPos - page * this._pageSize.x > this._pageSize.x * 0.5)
                    page++;
                return page;
            },
            set: function (value) {
                this.setCurrentPageX(value, false);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ScrollPane.prototype, "currentPageY", {
            get: function () {
                if (!this._pageMode)
                    return 0;
                var page = Math.floor(this._yPos / this._pageSize.y);
                if (this._yPos - page * this._pageSize.y > this._pageSize.y * 0.5)
                    page++;
                return page;
            },
            set: function (value) {
                this.setCurrentPageY(value, false);
            },
            enumerable: true,
            configurable: true
        });
        ScrollPane.prototype.setCurrentPageX = function (value, ani) {
            if (this._pageMode && this._overlapSize.x > 0)
                this.setPosX(value * this._pageSize.x, ani);
        };
        ScrollPane.prototype.setCurrentPageY = function (value, ani) {
            if (this._pageMode && this._overlapSize.y > 0)
                this.setPosY(value * this._pageSize.y, ani);
        };
        Object.defineProperty(ScrollPane.prototype, "isBottomMost", {
            get: function () {
                return this._yPos == this._overlapSize.y || this._overlapSize.y == 0;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ScrollPane.prototype, "isRightMost", {
            get: function () {
                return this._xPos == this._overlapSize.x || this._overlapSize.x == 0;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ScrollPane.prototype, "pageController", {
            get: function () {
                return this._pageController;
            },
            set: function (value) {
                this._pageController = value;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ScrollPane.prototype, "scrollingPosX", {
            get: function () {
                return fairygui.ToolSet.clamp(-this._container.x, 0, this._overlapSize.x);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ScrollPane.prototype, "scrollingPosY", {
            get: function () {
                return fairygui.ToolSet.clamp(-this._container.y, 0, this._overlapSize.y);
            },
            enumerable: true,
            configurable: true
        });
        ScrollPane.prototype.scrollTop = function (ani) {
            if (ani === void 0) { ani = false; }
            this.setPercY(0, ani);
        };
        ScrollPane.prototype.scrollBottom = function (ani) {
            if (ani === void 0) { ani = false; }
            this.setPercY(1, ani);
        };
        ScrollPane.prototype.scrollUp = function (ratio, ani) {
            if (ratio === void 0) { ratio = 1; }
            if (ani === void 0) { ani = false; }
            if (this._pageMode)
                this.setPosY(this._yPos - this._pageSize.y * ratio, ani);
            else
                this.setPosY(this._yPos - this._scrollStep * ratio, ani);
            ;
        };
        ScrollPane.prototype.scrollDown = function (ratio, ani) {
            if (ratio === void 0) { ratio = 1; }
            if (ani === void 0) { ani = false; }
            if (this._pageMode)
                this.setPosY(this._yPos + this._pageSize.y * ratio, ani);
            else
                this.setPosY(this._yPos + this._scrollStep * ratio, ani);
        };
        ScrollPane.prototype.scrollLeft = function (ratio, ani) {
            if (ratio === void 0) { ratio = 1; }
            if (ani === void 0) { ani = false; }
            if (this._pageMode)
                this.setPosX(this._xPos - this._pageSize.x * ratio, ani);
            else
                this.setPosX(this._xPos - this._scrollStep * ratio, ani);
        };
        ScrollPane.prototype.scrollRight = function (ratio, ani) {
            if (ratio === void 0) { ratio = 1; }
            if (ani === void 0) { ani = false; }
            if (this._pageMode)
                this.setPosX(this._xPos + this._pageSize.x * ratio, ani);
            else
                this.setPosX(this._xPos + this._scrollStep * ratio, ani);
        };
        ScrollPane.prototype.scrollToView = function (target, ani, setFirst) {
            if (ani === void 0) { ani = false; }
            if (setFirst === void 0) { setFirst = false; }
            this._owner.ensureBoundsCorrect();
            if (this._needRefresh)
                this.refresh();
            var rect;
            if (target instanceof fairygui.GObject) {
                if (target.parent != this._owner) {
                    target.parent.localToGlobalRect(target.x, target.y, target.width, target.height, ScrollPane.sHelperRect);
                    rect = this._owner.globalToLocalRect(ScrollPane.sHelperRect.x, ScrollPane.sHelperRect.y, ScrollPane.sHelperRect.width, ScrollPane.sHelperRect.height, ScrollPane.sHelperRect);
                }
                else {
                    rect = ScrollPane.sHelperRect;
                    rect.setTo(target.x, target.y, target.width, target.height);
                }
            }
            else
                rect = target;
            if (this._overlapSize.y > 0) {
                var bottom = this._yPos + this._viewSize.y;
                if (setFirst || rect.y <= this._yPos || rect.height >= this._viewSize.y) {
                    if (this._pageMode)
                        this.setPosY(Math.floor(rect.y / this._pageSize.y) * this._pageSize.y, ani);
                    else
                        this.setPosY(rect.y, ani);
                }
                else if (rect.y + rect.height > bottom) {
                    if (this._pageMode)
                        this.setPosY(Math.floor(rect.y / this._pageSize.y) * this._pageSize.y, ani);
                    else if (rect.height <= this._viewSize.y / 2)
                        this.setPosY(rect.y + rect.height * 2 - this._viewSize.y, ani);
                    else
                        this.setPosY(rect.y + rect.height - this._viewSize.y, ani);
                }
            }
            if (this._overlapSize.x > 0) {
                var right = this._xPos + this._viewSize.x;
                if (setFirst || rect.x <= this._xPos || rect.width >= this._viewSize.x) {
                    if (this._pageMode)
                        this.setPosX(Math.floor(rect.x / this._pageSize.x) * this._pageSize.x, ani);
                    else
                        this.setPosX(rect.x, ani);
                }
                else if (rect.x + rect.width > right) {
                    if (this._pageMode)
                        this.setPosX(Math.floor(rect.x / this._pageSize.x) * this._pageSize.x, ani);
                    else if (rect.width <= this._viewSize.x / 2)
                        this.setPosX(rect.x + rect.width * 2 - this._viewSize.x, ani);
                    else
                        this.setPosX(rect.x + rect.width - this._viewSize.x, ani);
                }
            }
            if (!ani && this._needRefresh)
                this.refresh();
        };
        ScrollPane.prototype.isChildInView = function (obj) {
            if (this._overlapSize.y > 0) {
                var dist = obj.y + this._container.y;
                if (dist < -obj.height || dist > this._viewSize.y)
                    return false;
            }
            if (this._overlapSize.x > 0) {
                dist = obj.x + this._container.x;
                if (dist < -obj.width || dist > this._viewSize.x)
                    return false;
            }
            return true;
        };
        ScrollPane.prototype.cancelDragging = function () {
            this._owner.displayObject.removeEventListener(egret.TouchEvent.TOUCH_MOVE, this.__touchMove, this);
            this._owner.displayObject.removeEventListener(egret.TouchEvent.TOUCH_END, this.__touchEnd, this);
            this._owner.displayObject.removeEventListener(egret.TouchEvent.TOUCH_TAP, this.__touchTap, this);
            if (ScrollPane.draggingPane == this)
                ScrollPane.draggingPane = null;
            ScrollPane._gestureFlag = 0;
            this.isDragged = false;
            this._maskContainer.touchChildren = true;
        };
        ScrollPane.prototype.lockHeader = function (size) {
            if (this._headerLockedSize == size)
                return;
            this._headerLockedSize = size;
            if (!this._refreshEventDispatching && this._container[this._refreshBarAxis] >= 0) {
                this._tweenStart.setTo(this._container.x, this._container.y);
                this._tweenChange.setTo(0, 0);
                this._tweenChange[this._refreshBarAxis] = this._headerLockedSize - this._tweenStart[this._refreshBarAxis];
                this._tweenDuration.setTo(ScrollPane.TWEEN_TIME_DEFAULT, ScrollPane.TWEEN_TIME_DEFAULT);
                this._tweenTime.setTo(0, 0);
                this._tweening = 2;
                egret.startTick(this.tweenUpdate, this);
            }
        };
        ScrollPane.prototype.lockFooter = function (size) {
            if (this._footerLockedSize == size)
                return;
            this._footerLockedSize = size;
            if (!this._refreshEventDispatching && this._container[this._refreshBarAxis] <= -this._overlapSize[this._refreshBarAxis]) {
                this._tweenStart.setTo(this._container.x, this._container.y);
                this._tweenChange.setTo(0, 0);
                var max = this._overlapSize[this._refreshBarAxis];
                if (max == 0)
                    max = Math.max(this._contentSize[this._refreshBarAxis] + this._footerLockedSize - this._viewSize[this._refreshBarAxis], 0);
                else
                    max += this._footerLockedSize;
                this._tweenChange[this._refreshBarAxis] = -max - this._tweenStart[this._refreshBarAxis];
                this._tweenDuration.setTo(ScrollPane.TWEEN_TIME_DEFAULT, ScrollPane.TWEEN_TIME_DEFAULT);
                this._tweenTime.setTo(0, 0);
                this._tweening = 2;
                egret.startTick(this.tweenUpdate, this);
            }
        };
        ScrollPane.prototype.onOwnerSizeChanged = function () {
            this.setSize(this._owner.width, this._owner.height);
            this.posChanged(false);
        };
        ScrollPane.prototype.handleControllerChanged = function (c) {
            if (this._pageController == c) {
                if (this._scrollType == fairygui.ScrollType.Horizontal)
                    this.setCurrentPageX(c.selectedIndex, true);
                else
                    this.setCurrentPageY(c.selectedIndex, true);
            }
        };
        ScrollPane.prototype.updatePageController = function () {
            if (this._pageController != null && !this._pageController.changing) {
                var index;
                if (this._scrollType == fairygui.ScrollType.Horizontal)
                    index = this.currentPageX;
                else
                    index = this.currentPageY;
                if (index < this._pageController.pageCount) {
                    var c = this._pageController;
                    this._pageController = null; //防止HandleControllerChanged的调用
                    c.selectedIndex = index;
                    this._pageController = c;
                }
            }
        };
        ScrollPane.prototype.adjustMaskContainer = function () {
            var mx, my;
            if (this._displayOnLeft && this._vtScrollBar != null)
                mx = Math.floor(this._owner.margin.left + this._vtScrollBar.width);
            else
                mx = Math.floor(this._owner.margin.left);
            my = Math.floor(this._owner.margin.top);
            this._maskContainer.x = mx;
            this._maskContainer.y = my;
            if (this._owner._alignOffset.x != 0 || this._owner._alignOffset.y != 0) {
                if (this._alignContainer == null) {
                    this._alignContainer = new cc.Node();
                    this._maskContainer.addChild(this._alignContainer);
                    this._alignContainer.addChild(this._container);
                }
                this._alignContainer.x = this._owner._alignOffset.x;
                this._alignContainer.y = this._owner._alignOffset.y;
            }
            else if (this._alignContainer) {
                this._alignContainer.x = this._alignContainer.y = 0;
            }
        };
        ScrollPane.prototype.setSize = function (aWidth, aHeight) {
            this.adjustMaskContainer();
            if (this._hzScrollBar) {
                this._hzScrollBar.y = aHeight - this._hzScrollBar.height;
                if (this._vtScrollBar && !this._vScrollNone) {
                    this._hzScrollBar.width = aWidth - this._vtScrollBar.width - this._scrollBarMargin.left - this._scrollBarMargin.right;
                    if (this._displayOnLeft)
                        this._hzScrollBar.x = this._scrollBarMargin.left + this._vtScrollBar.width;
                    else
                        this._hzScrollBar.x = this._scrollBarMargin.left;
                }
                else {
                    this._hzScrollBar.width = aWidth - this._scrollBarMargin.left - this._scrollBarMargin.right;
                    this._hzScrollBar.x = this._scrollBarMargin.left;
                }
            }
            if (this._vtScrollBar) {
                if (!this._displayOnLeft)
                    this._vtScrollBar.x = aWidth - this._vtScrollBar.width;
                if (this._hzScrollBar)
                    this._vtScrollBar.height = aHeight - this._hzScrollBar.height - this._scrollBarMargin.top - this._scrollBarMargin.bottom;
                else
                    this._vtScrollBar.height = aHeight - this._scrollBarMargin.top - this._scrollBarMargin.bottom;
                this._vtScrollBar.y = this._scrollBarMargin.top;
            }
            this._viewSize.x = aWidth;
            this._viewSize.y = aHeight;
            if (this._hzScrollBar && !this._hScrollNone)
                this._viewSize.y -= this._hzScrollBar.height;
            if (this._vtScrollBar && !this._vScrollNone)
                this._viewSize.x -= this._vtScrollBar.width;
            this._viewSize.x -= (this._owner.margin.left + this._owner.margin.right);
            this._viewSize.y -= (this._owner.margin.top + this._owner.margin.bottom);
            this._viewSize.x = Math.max(1, this._viewSize.x);
            this._viewSize.y = Math.max(1, this._viewSize.y);
            this._pageSize.x = this._viewSize.x;
            this._pageSize.y = this._viewSize.y;
            this.handleSizeChanged();
        };
        ScrollPane.prototype.setContentSize = function (aWidth, aHeight) {
            if (this._contentSize.x == aWidth && this._contentSize.y == aHeight)
                return;
            this._contentSize.x = aWidth;
            this._contentSize.y = aHeight;
            this.handleSizeChanged();
        };
        ScrollPane.prototype.changeContentSizeOnScrolling = function (deltaWidth, deltaHeight, deltaPosX, deltaPosY) {
            var isRightmost = this._xPos == this._overlapSize.x;
            var isBottom = this._yPos == this._overlapSize.y;
            this._contentSize.x += deltaWidth;
            this._contentSize.y += deltaHeight;
            this.handleSizeChanged();
            if (this._tweening == 1) {
                //如果原来滚动位置是贴边，加入处理继续贴边。
                if (deltaWidth != 0 && isRightmost && this._tweenChange.x < 0) {
                    this._xPos = this._overlapSize.x;
                    this._tweenChange.x = -this._xPos - this._tweenStart.x;
                }
                if (deltaHeight != 0 && isBottom && this._tweenChange.y < 0) {
                    this._yPos = this._overlapSize.y;
                    this._tweenChange.y = -this._yPos - this._tweenStart.y;
                }
            }
            else if (this._tweening == 2) {
                //重新调整起始位置，确保能够顺滑滚下去
                if (deltaPosX != 0) {
                    this._container.x -= deltaPosX;
                    this._tweenStart.x -= deltaPosX;
                    this._xPos = -this._container.x;
                }
                if (deltaPosY != 0) {
                    this._container.y -= deltaPosY;
                    this._tweenStart.y -= deltaPosY;
                    this._yPos = -this._container.y;
                }
            }
            else if (this.isDragged) {
                if (deltaPosX != 0) {
                    this._container.x -= deltaPosX;
                    this._containerPos.x -= deltaPosX;
                    this._xPos = -this._container.x;
                }
                if (deltaPosY != 0) {
                    this._container.y -= deltaPosY;
                    this._containerPos.y -= deltaPosY;
                    this._yPos = -this._container.y;
                }
            }
            else {
                //如果原来滚动位置是贴边，加入处理继续贴边。
                if (deltaWidth != 0 && isRightmost) {
                    this._xPos = this._overlapSize.x;
                    this._container.x = -this._xPos;
                }
                if (deltaHeight != 0 && isBottom) {
                    this._yPos = this._overlapSize.y;
                    this._container.y = -this._yPos;
                }
            }
            if (this._pageMode)
                this.updatePageController();
        };
        ScrollPane.prototype.handleSizeChanged = function (onScrolling) {
            if (onScrolling === void 0) { onScrolling = false; }
            if (this._displayInDemand) {
                if (this._vtScrollBar) {
                    if (this._contentSize.y <= this._viewSize.y) {
                        if (!this._vScrollNone) {
                            this._vScrollNone = true;
                            this._viewSize.x += this._vtScrollBar.width;
                        }
                    }
                    else {
                        if (this._vScrollNone) {
                            this._vScrollNone = false;
                            this._viewSize.x -= this._vtScrollBar.width;
                        }
                    }
                }
                if (this._hzScrollBar) {
                    if (this._contentSize.x <= this._viewSize.x) {
                        if (!this._hScrollNone) {
                            this._hScrollNone = true;
                            this._viewSize.y += this._hzScrollBar.height;
                        }
                    }
                    else {
                        if (this._hScrollNone) {
                            this._hScrollNone = false;
                            this._viewSize.y -= this._hzScrollBar.height;
                        }
                    }
                }
            }
            if (this._vtScrollBar) {
                if (this._viewSize.y < this._vtScrollBar.minSize)
                    //没有使用this._vtScrollBar.visible是因为ScrollBar用了一个trick，它并不在owner的DisplayList里，因此this._vtScrollBar.visible是无效的
                    this._vtScrollBar.displayObject.visible = false;
                else {
                    this._vtScrollBar.displayObject.visible = this._scrollBarVisible && !this._vScrollNone;
                    if (this._contentSize.y == 0)
                        this._vtScrollBar.displayPerc = 0;
                    else
                        this._vtScrollBar.displayPerc = Math.min(1, this._viewSize.y / this._contentSize.y);
                }
            }
            if (this._hzScrollBar) {
                if (this._viewSize.x < this._hzScrollBar.minSize)
                    this._hzScrollBar.displayObject.visible = false;
                else {
                    this._hzScrollBar.displayObject.visible = this._scrollBarVisible && !this._hScrollNone;
                    if (this._contentSize.x == 0)
                        this._hzScrollBar.displayPerc = 0;
                    else
                        this._hzScrollBar.displayPerc = Math.min(1, this._viewSize.x / this._contentSize.x);
                }
            }
            var rect = this._maskContainer.scrollRect;
            if (rect) {
                rect.width = this._viewSize.x;
                rect.height = this._viewSize.y;
                this._maskContainer.scrollRect = rect;
            }
            if (this._scrollType == fairygui.ScrollType.Horizontal || this._scrollType == fairygui.ScrollType.Both)
                this._overlapSize.x = Math.ceil(Math.max(0, this._contentSize.x - this._viewSize.x));
            else
                this._overlapSize.x = 0;
            if (this._scrollType == fairygui.ScrollType.Vertical || this._scrollType == fairygui.ScrollType.Both)
                this._overlapSize.y = Math.ceil(Math.max(0, this._contentSize.y - this._viewSize.y));
            else
                this._overlapSize.y = 0;
            //边界检查
            this._xPos = fairygui.ToolSet.clamp(this._xPos, 0, this._overlapSize.x);
            this._yPos = fairygui.ToolSet.clamp(this._yPos, 0, this._overlapSize.y);
            if (this._refreshBarAxis != null) {
                var max = this._overlapSize[this._refreshBarAxis];
                if (max == 0)
                    max = Math.max(this._contentSize[this._refreshBarAxis] + this._footerLockedSize - this._viewSize[this._refreshBarAxis], 0);
                else
                    max += this._footerLockedSize;
                if (this._refreshBarAxis == "x") {
                    this._container.x = fairygui.ToolSet.clamp(this._container.x, -max, this._headerLockedSize);
                    this._container.y = fairygui.ToolSet.clamp(this._container.y, -this._overlapSize.y, 0);
                }
                else {
                    this._container.x = fairygui.ToolSet.clamp(this._container.x, -this._overlapSize.x, 0);
                    this._container.y = fairygui.ToolSet.clamp(this._container.y, -max, this._headerLockedSize);
                }
                if (this._header != null) {
                    if (this._refreshBarAxis == "x")
                        this._header.height = this._viewSize.y;
                    else
                        this._header.width = this._viewSize.x;
                }
                if (this._footer != null) {
                    if (this._refreshBarAxis == "y")
                        this._footer.height = this._viewSize.y;
                    else
                        this._footer.width = this._viewSize.x;
                }
            }
            else {
                this._container.x = fairygui.ToolSet.clamp(this._container.x, -this._overlapSize.x, 0);
                this._container.y = fairygui.ToolSet.clamp(this._container.y, -this._overlapSize.y, 0);
            }
            this.syncScrollBar(true);
            this.checkRefreshBar();
            if (this._pageMode)
                this.updatePageController();
        };
        ScrollPane.prototype.posChanged = function (ani) {
            if (this._aniFlag == 0)
                this._aniFlag = ani ? 1 : -1;
            else if (this._aniFlag == 1 && !ani)
                this._aniFlag = -1;
            this._needRefresh = true;
            fairygui.GTimers.inst.callLater(this.refresh, this);
        };
        ScrollPane.prototype.refresh = function () {
            this._needRefresh = false;
            fairygui.GTimers.inst.remove(this.refresh, this);
            if (this._pageMode || this._snapToItem) {
                ScrollPane.sEndPos.setTo(-this._xPos, -this._yPos);
                this.alignPosition(ScrollPane.sEndPos, false);
                this._xPos = -ScrollPane.sEndPos.x;
                this._yPos = -ScrollPane.sEndPos.y;
            }
            this.refresh2();
            this.dispatchEventWith(ScrollPane.SCROLL, false);
            if (this._needRefresh) //在onScroll事件里开发者可能修改位置，这里再刷新一次，避免闪烁
             {
                this._needRefresh = false;
                fairygui.GTimers.inst.remove(this.refresh, this);
                this.refresh2();
            }
            this.syncScrollBar();
            this._aniFlag = 0;
        };
        ScrollPane.prototype.refresh2 = function () {
            if (this._aniFlag == 1 && !this.isDragged) {
                var posX;
                var posY;
                if (this._overlapSize.x > 0)
                    posX = -Math.floor(this._xPos);
                else {
                    if (this._container.x != 0)
                        this._container.x = 0;
                    posX = 0;
                }
                if (this._overlapSize.y > 0)
                    posY = -Math.floor(this._yPos);
                else {
                    if (this._container.y != 0)
                        this._container.y = 0;
                    posY = 0;
                }
                if (posX != this._container.x || posY != this._container.y) {
                    this._tweening = 1;
                    this._tweenTime.setTo(0, 0);
                    this._tweenDuration.setTo(ScrollPane.TWEEN_TIME_GO, ScrollPane.TWEEN_TIME_GO);
                    this._tweenStart.setTo(this._container.x, this._container.y);
                    this._tweenChange.setTo(posX - this._tweenStart.x, posY - this._tweenStart.y);
                    egret.startTick(this.tweenUpdate, this);
                }
                else if (this._tweening != 0)
                    this.killTween();
            }
            else {
                if (this._tweening != 0)
                    this.killTween();
                this._container.x = Math.floor(-this._xPos);
                this._container.y = Math.floor(-this._yPos);
                this.loopCheckingCurrent();
            }
            if (this._pageMode)
                this.updatePageController();
        };
        ScrollPane.prototype.syncScrollBar = function (end) {
            if (end === void 0) { end = false; }
            if (this._vtScrollBar != null) {
                this._vtScrollBar.scrollPerc = this._overlapSize.y == 0 ? 0 : fairygui.ToolSet.clamp(-this._container.y, 0, this._overlapSize.y) / this._overlapSize.y;
                if (this._scrollBarDisplayAuto)
                    this.showScrollBar(!end);
            }
            if (this._hzScrollBar != null) {
                this._hzScrollBar.scrollPerc = this._overlapSize.x == 0 ? 0 : fairygui.ToolSet.clamp(-this._container.x, 0, this._overlapSize.x) / this._overlapSize.x;
                if (this._scrollBarDisplayAuto)
                    this.showScrollBar(!end);
            }
            if (end)
                this._maskContainer.touchChildren = true;
        };
        ScrollPane.prototype.__touchBegin = function (evt) {
            if (!this._touchEffect)
                return;
            if (this._tweening != 0) {
                this.killTween();
                this.isDragged = true;
            }
            else
                this.isDragged = false;
            var pt = this._owner.globalToLocal(evt.stageX, evt.stageY, ScrollPane.sHelperPoint);
            this._containerPos.setTo(this._container.x, this._container.y);
            this._beginTouchPos.setTo(pt.x, pt.y);
            this._lastTouchPos.setTo(pt.x, pt.y);
            this._lastTouchGlobalPos.setTo(evt.stageX, evt.stageY);
            this._isHoldAreaDone = false;
            this._velocity.setTo(0, 0);
            this._velocityScale = 1;
            this._lastMoveTime = egret.getTimer() / 1000;
            this._owner.displayObject.stage.addEventListener(egret.TouchEvent.TOUCH_MOVE, this.__touchMove, this);
            this._owner.displayObject.stage.addEventListener(egret.TouchEvent.TOUCH_END, this.__touchEnd, this);
            this._owner.displayObject.stage.addEventListener(egret.TouchEvent.TOUCH_TAP, this.__touchTap, this);
        };
        ScrollPane.prototype.__touchMove = function (evt) {
            if (this._owner.displayObject.stage == null)
                return;
            if (!this._touchEffect)
                return;
            if (ScrollPane.draggingPane != null && ScrollPane.draggingPane != this || fairygui.GObject.draggingObject != null) //已经有其他拖动
                return;
            var pt = this._owner.globalToLocal(evt.stageX, evt.stageY, ScrollPane.sHelperPoint);
            var sensitivity = fairygui.UIConfig.touchScrollSensitivity;
            var diff, diff2;
            var sv, sh, st;
            if (this._scrollType == fairygui.ScrollType.Vertical) {
                if (!this._isHoldAreaDone) {
                    //表示正在监测垂直方向的手势
                    ScrollPane._gestureFlag |= 1;
                    diff = Math.abs(this._beginTouchPos.y - pt.y);
                    if (diff < sensitivity)
                        return;
                    if ((ScrollPane._gestureFlag & 2) != 0) //已经有水平方向的手势在监测，那么我们用严格的方式检查是不是按垂直方向移动，避免冲突
                     {
                        diff2 = Math.abs(this._beginTouchPos.x - pt.x);
                        if (diff < diff2) //不通过则不允许滚动了
                            return;
                    }
                }
                sv = true;
            }
            else if (this._scrollType == fairygui.ScrollType.Horizontal) {
                if (!this._isHoldAreaDone) {
                    ScrollPane._gestureFlag |= 2;
                    diff = Math.abs(this._beginTouchPos.x - pt.x);
                    if (diff < sensitivity)
                        return;
                    if ((ScrollPane._gestureFlag & 1) != 0) {
                        diff2 = Math.abs(this._beginTouchPos.y - pt.y);
                        if (diff < diff2)
                            return;
                    }
                }
                sh = true;
            }
            else {
                ScrollPane._gestureFlag = 3;
                if (!this._isHoldAreaDone) {
                    diff = Math.abs(this._beginTouchPos.y - pt.y);
                    if (diff < sensitivity) {
                        diff = Math.abs(this._beginTouchPos.x - pt.x);
                        if (diff < sensitivity)
                            return;
                    }
                }
                sv = sh = true;
            }
            var newPosX = Math.floor(this._containerPos.x + pt.x - this._beginTouchPos.x);
            var newPosY = Math.floor(this._containerPos.y + pt.y - this._beginTouchPos.y);
            if (sv) {
                if (newPosY > 0) {
                    if (!this._bouncebackEffect)
                        this._container.y = 0;
                    else if (this._header != null && this._header.maxHeight != 0)
                        this._container.y = Math.floor(Math.min(newPosY * 0.5, this._header.maxHeight));
                    else
                        this._container.y = Math.floor(Math.min(newPosY * 0.5, this._viewSize.y * ScrollPane.PULL_RATIO));
                }
                else if (newPosY < -this._overlapSize.y) {
                    if (!this._bouncebackEffect)
                        this._container.y = -this._overlapSize.y;
                    else if (this._footer != null && this._footer.maxHeight > 0)
                        this._container.y = Math.floor(Math.max((newPosY + this._overlapSize.y) * 0.5, -this._footer.maxHeight) - this._overlapSize.y);
                    else
                        this._container.y = Math.floor(Math.max((newPosY + this._overlapSize.y) * 0.5, -this._viewSize.y * ScrollPane.PULL_RATIO) - this._overlapSize.y);
                }
                else
                    this._container.y = newPosY;
            }
            if (sh) {
                if (newPosX > 0) {
                    if (!this._bouncebackEffect)
                        this._container.x = 0;
                    else if (this._header != null && this._header.maxWidth != 0)
                        this._container.x = Math.floor(Math.min(newPosX * 0.5, this._header.maxWidth));
                    else
                        this._container.x = Math.floor(Math.min(newPosX * 0.5, this._viewSize.x * ScrollPane.PULL_RATIO));
                }
                else if (newPosX < 0 - this._overlapSize.x) {
                    if (!this._bouncebackEffect)
                        this._container.x = -this._overlapSize.x;
                    else if (this._footer != null && this._footer.maxWidth > 0)
                        this._container.x = Math.floor(Math.max((newPosX + this._overlapSize.x) * 0.5, -this._footer.maxWidth) - this._overlapSize.x);
                    else
                        this._container.x = Math.floor(Math.max((newPosX + this._overlapSize.x) * 0.5, -this._viewSize.x * ScrollPane.PULL_RATIO) - this._overlapSize.x);
                }
                else
                    this._container.x = newPosX;
            }
            //更新速度
            var now = egret.getTimer() / 1000;
            var deltaTime = Math.max(now - this._lastMoveTime, 1 / 60);
            var deltaPositionX = pt.x - this._lastTouchPos.x;
            var deltaPositionY = pt.y - this._lastTouchPos.y;
            if (!sh)
                deltaPositionX = 0;
            if (!sv)
                deltaPositionY = 0;
            if (deltaTime != 0) {
                var frameRate = this._owner.displayObject.stage.frameRate;
                var elapsed = deltaTime * frameRate - 1;
                if (elapsed > 1) //速度衰减
                 {
                    var factor = Math.pow(0.833, elapsed);
                    this._velocity.x = this._velocity.x * factor;
                    this._velocity.y = this._velocity.y * factor;
                }
                this._velocity.x = fairygui.ToolSet.lerp(this._velocity.x, deltaPositionX * 60 / frameRate / deltaTime, deltaTime * 10);
                this._velocity.y = fairygui.ToolSet.lerp(this._velocity.y, deltaPositionY * 60 / frameRate / deltaTime, deltaTime * 10);
            }
            /*速度计算使用的是本地位移，但在后续的惯性滚动判断中需要用到屏幕位移，所以这里要记录一个位移的比例。
            */
            var deltaGlobalPositionX = this._lastTouchGlobalPos.x - evt.stageX;
            var deltaGlobalPositionY = this._lastTouchGlobalPos.y - evt.stageY;
            if (deltaPositionX != 0)
                this._velocityScale = Math.abs(deltaGlobalPositionX / deltaPositionX);
            else if (deltaPositionY != 0)
                this._velocityScale = Math.abs(deltaGlobalPositionY / deltaPositionY);
            this._lastTouchPos.setTo(pt.x, pt.y);
            this._lastTouchGlobalPos.setTo(evt.stageX, evt.stageY);
            this._lastMoveTime = now;
            //同步更新pos值
            if (this._overlapSize.x > 0)
                this._xPos = fairygui.ToolSet.clamp(-this._container.x, 0, this._overlapSize.x);
            if (this._overlapSize.y > 0)
                this._yPos = fairygui.ToolSet.clamp(-this._container.y, 0, this._overlapSize.y);
            //循环滚动特别检查
            if (this._loop != 0) {
                newPosX = this._container.x;
                newPosY = this._container.y;
                if (this.loopCheckingCurrent()) {
                    this._containerPos.x += this._container.x - newPosX;
                    this._containerPos.y += this._container.y - newPosY;
                }
            }
            ScrollPane.draggingPane = this;
            this._isHoldAreaDone = true;
            this.isDragged = true;
            this._maskContainer.touchChildren = false;
            this.syncScrollBar();
            this.checkRefreshBar();
            if (this._pageMode)
                this.updatePageController();
            this.dispatchEventWith(ScrollPane.SCROLL, false);
        };
        ScrollPane.prototype.__touchEnd = function (evt) {
            evt.currentTarget.removeEventListener(egret.TouchEvent.TOUCH_MOVE, this.__touchMove, this);
            evt.currentTarget.removeEventListener(egret.TouchEvent.TOUCH_END, this.__touchEnd, this);
            evt.currentTarget.removeEventListener(egret.TouchEvent.TOUCH_TAP, this.__touchTap, this);
            if (ScrollPane.draggingPane == this)
                ScrollPane.draggingPane = null;
            ScrollPane._gestureFlag = 0;
            if (!this.isDragged || !this._touchEffect || this._owner.displayObject.stage == null) {
                this.isDragged = false;
                this._maskContainer.touchChildren = true;
                return;
            }
            // touch事件不一定都是以tap结束,拖拽的结束动作是touchEnd,这时需要正确处理isDragged标记位.否则在播放滚动动画时会因为处于拖拽状态而使滚动动画失效
            this.isDragged = false;
            this._maskContainer.touchChildren = true;
            this._tweenStart.setTo(this._container.x, this._container.y);
            ScrollPane.sEndPos.setTo(this._tweenStart.x, this._tweenStart.y);
            var flag = false;
            if (this._container.x > 0) {
                ScrollPane.sEndPos.x = 0;
                flag = true;
            }
            else if (this._container.x < -this._overlapSize.x) {
                ScrollPane.sEndPos.x = -this._overlapSize.x;
                flag = true;
            }
            if (this._container.y > 0) {
                ScrollPane.sEndPos.y = 0;
                flag = true;
            }
            else if (this._container.y < -this._overlapSize.y) {
                ScrollPane.sEndPos.y = -this._overlapSize.y;
                flag = true;
            }
            if (flag) {
                this._tweenChange.setTo(ScrollPane.sEndPos.x - this._tweenStart.x, ScrollPane.sEndPos.y - this._tweenStart.y);
                if (this._tweenChange.x < -fairygui.UIConfig.touchDragSensitivity || this._tweenChange.y < -fairygui.UIConfig.touchDragSensitivity) {
                    this._refreshEventDispatching = true;
                    this.dispatchEventWith(ScrollPane.PULL_DOWN_RELEASE);
                    this._refreshEventDispatching = false;
                }
                else if (this._tweenChange.x > fairygui.UIConfig.touchDragSensitivity || this._tweenChange.y > fairygui.UIConfig.touchDragSensitivity) {
                    this._refreshEventDispatching = true;
                    this.dispatchEventWith(ScrollPane.PULL_UP_RELEASE);
                    this._refreshEventDispatching = false;
                }
                if (this._headerLockedSize > 0 && ScrollPane.sEndPos[this._refreshBarAxis] == 0) {
                    ScrollPane.sEndPos[this._refreshBarAxis] = this._headerLockedSize;
                    this._tweenChange.x = ScrollPane.sEndPos.x - this._tweenStart.x;
                    this._tweenChange.y = ScrollPane.sEndPos.y - this._tweenStart.y;
                }
                else if (this._footerLockedSize > 0 && ScrollPane.sEndPos[this._refreshBarAxis] == -this._overlapSize[this._refreshBarAxis]) {
                    var max = this._overlapSize[this._refreshBarAxis];
                    if (max == 0)
                        max = Math.max(this._contentSize[this._refreshBarAxis] + this._footerLockedSize - this._viewSize[this._refreshBarAxis], 0);
                    else
                        max += this._footerLockedSize;
                    ScrollPane.sEndPos[this._refreshBarAxis] = -max;
                    this._tweenChange.x = ScrollPane.sEndPos.x - this._tweenStart.x;
                    this._tweenChange.y = ScrollPane.sEndPos.y - this._tweenStart.y;
                }
                this._tweenDuration.setTo(ScrollPane.TWEEN_TIME_DEFAULT, ScrollPane.TWEEN_TIME_DEFAULT);
            }
            else {
                //更新速度
                if (!this._inertiaDisabled) {
                    var frameRate = this._owner.displayObject.stage.frameRate;
                    var elapsed = (egret.getTimer() / 1000 - this._lastMoveTime) * frameRate - 1;
                    if (elapsed > 1) {
                        var factor = Math.pow(0.833, elapsed);
                        this._velocity.x = this._velocity.x * factor;
                        this._velocity.y = this._velocity.y * factor;
                    }
                    //根据速度计算目标位置和需要时间
                    this.updateTargetAndDuration(this._tweenStart, ScrollPane.sEndPos);
                }
                else
                    this._tweenDuration.setTo(ScrollPane.TWEEN_TIME_DEFAULT, ScrollPane.TWEEN_TIME_DEFAULT);
                ScrollPane.sOldChange.setTo(ScrollPane.sEndPos.x - this._tweenStart.x, ScrollPane.sEndPos.y - this._tweenStart.y);
                //调整目标位置
                this.loopCheckingTarget(ScrollPane.sEndPos);
                if (this._pageMode || this._snapToItem)
                    this.alignPosition(ScrollPane.sEndPos, true);
                this._tweenChange.x = ScrollPane.sEndPos.x - this._tweenStart.x;
                this._tweenChange.y = ScrollPane.sEndPos.y - this._tweenStart.y;
                if (this._tweenChange.x == 0 && this._tweenChange.y == 0) {
                    if (this._scrollBarDisplayAuto)
                        this.showScrollBar(false);
                    return;
                }
                //如果目标位置已调整，随之调整需要时间
                if (this._pageMode || this._snapToItem) {
                    this.fixDuration("x", ScrollPane.sOldChange.x);
                    this.fixDuration("y", ScrollPane.sOldChange.y);
                }
            }
            this._tweening = 2;
            this._tweenTime.setTo(0, 0);
            egret.startTick(this.tweenUpdate, this);
        };
        ScrollPane.prototype.__touchTap = function (evt) {
            this.isDragged = false;
        };
        ScrollPane.prototype.__rollOver = function (evt) {
            this.showScrollBar(true);
        };
        ScrollPane.prototype.__rollOut = function (evt) {
            this.showScrollBar(false);
        };
        ScrollPane.prototype.showScrollBar = function (val) {
            if (val) {
                this.__showScrollBar(true);
                fairygui.GTimers.inst.remove(this.__showScrollBar, this);
            }
            else
                fairygui.GTimers.inst.add(500, 1, this.__showScrollBar, this, val);
        };
        ScrollPane.prototype.__showScrollBar = function (val) {
            this._scrollBarVisible = val && this._viewSize.x > 0 && this._viewSize.y > 0;
            if (this._vtScrollBar)
                this._vtScrollBar.displayObject.visible = this._scrollBarVisible && !this._vScrollNone;
            if (this._hzScrollBar)
                this._hzScrollBar.displayObject.visible = this._scrollBarVisible && !this._hScrollNone;
        };
        ScrollPane.prototype.getLoopPartSize = function (division, axis) {
            return (this._contentSize[axis] + (axis == "x" ? this._owner.columnGap : this._owner.lineGap)) / division;
        };
        ScrollPane.prototype.loopCheckingCurrent = function () {
            var changed = false;
            if (this._loop == 1 && this._overlapSize.x > 0) {
                if (this._xPos < 0.001) {
                    this._xPos += this.getLoopPartSize(2, "x");
                    changed = true;
                }
                else if (this._xPos >= this._overlapSize.x) {
                    this._xPos -= this.getLoopPartSize(2, "x");
                    changed = true;
                }
            }
            else if (this._loop == 2 && this._overlapSize.y > 0) {
                if (this._yPos < 0.001) {
                    this._yPos += this.getLoopPartSize(2, "y");
                    changed = true;
                }
                else if (this._yPos >= this._overlapSize.y) {
                    this._yPos -= this.getLoopPartSize(2, "y");
                    changed = true;
                }
            }
            if (changed) {
                this._container.x = Math.floor(-this._xPos);
                this._container.y = Math.floor(-this._yPos);
            }
            return changed;
        };
        ScrollPane.prototype.loopCheckingTarget = function (endPos) {
            if (this._loop == 1)
                this.loopCheckingTarget2(endPos, "x");
            if (this._loop == 2)
                this.loopCheckingTarget2(endPos, "y");
        };
        ScrollPane.prototype.loopCheckingTarget2 = function (endPos, axis) {
            var halfSize;
            var tmp;
            if (endPos[axis] > 0) {
                halfSize = this.getLoopPartSize(2, axis);
                tmp = this._tweenStart[axis] - halfSize;
                if (tmp <= 0 && tmp >= -this._overlapSize[axis]) {
                    endPos[axis] -= halfSize;
                    this._tweenStart[axis] = tmp;
                }
            }
            else if (endPos[axis] < -this._overlapSize[axis]) {
                halfSize = this.getLoopPartSize(2, axis);
                tmp = this._tweenStart[axis] + halfSize;
                if (tmp <= 0 && tmp >= -this._overlapSize[axis]) {
                    endPos[axis] += halfSize;
                    this._tweenStart[axis] = tmp;
                }
            }
        };
        ScrollPane.prototype.loopCheckingNewPos = function (value, axis) {
            if (this._overlapSize[axis] == 0)
                return value;
            var pos = axis == "x" ? this._xPos : this._yPos;
            var changed = false;
            var v;
            if (value < 0.001) {
                value += this.getLoopPartSize(2, axis);
                if (value > pos) {
                    v = this.getLoopPartSize(6, axis);
                    v = Math.ceil((value - pos) / v) * v;
                    pos = fairygui.ToolSet.clamp(pos + v, 0, this._overlapSize[axis]);
                    changed = true;
                }
            }
            else if (value >= this._overlapSize[axis]) {
                value -= this.getLoopPartSize(2, axis);
                if (value < pos) {
                    v = this.getLoopPartSize(6, axis);
                    v = Math.ceil((pos - value) / v) * v;
                    pos = fairygui.ToolSet.clamp(pos - v, 0, this._overlapSize[axis]);
                    changed = true;
                }
            }
            if (changed) {
                if (axis == "x")
                    this._container.x = -Math.floor(pos);
                else
                    this._container.y = -Math.floor(pos);
            }
            return value;
        };
        ScrollPane.prototype.alignPosition = function (pos, inertialScrolling) {
            if (this._pageMode) {
                pos.x = this.alignByPage(pos.x, "x", inertialScrolling);
                pos.y = this.alignByPage(pos.y, "y", inertialScrolling);
            }
            else if (this._snapToItem) {
                var pt = this._owner.getSnappingPosition(-pos.x, -pos.y, ScrollPane.sHelperPoint);
                if (pos.x < 0 && pos.x > -this._overlapSize.x)
                    pos.x = -pt.x;
                if (pos.y < 0 && pos.y > -this._overlapSize.y)
                    pos.y = -pt.y;
            }
        };
        ScrollPane.prototype.alignByPage = function (pos, axis, inertialScrolling) {
            var page;
            if (pos > 0)
                page = 0;
            else if (pos < -this._overlapSize[axis])
                page = Math.ceil(this._contentSize[axis] / this._pageSize[axis]) - 1;
            else {
                page = Math.floor(-pos / this._pageSize[axis]);
                var change = inertialScrolling ? (pos - this._containerPos[axis]) : (pos - this._container[axis]);
                var testPageSize = Math.min(this._pageSize[axis], this._contentSize[axis] - (page + 1) * this._pageSize[axis]);
                var delta = -pos - page * this._pageSize[axis];
                //页面吸附策略
                if (Math.abs(change) > this._pageSize[axis]) //如果滚动距离超过1页,则需要超过页面的一半，才能到更下一页
                 {
                    if (delta > testPageSize * 0.5)
                        page++;
                }
                else //否则只需要页面的1/3，当然，需要考虑到左移和右移的情况
                 {
                    if (delta > testPageSize * (change < 0 ? 0.3 : 0.7))
                        page++;
                }
                //重新计算终点
                pos = -page * this._pageSize[axis];
                if (pos < -this._overlapSize[axis]) //最后一页未必有pageSize那么大
                    pos = -this._overlapSize[axis];
            }
            //惯性滚动模式下，会增加判断尽量不要滚动超过一页
            if (inertialScrolling) {
                var oldPos = this._tweenStart[axis];
                var oldPage;
                if (oldPos > 0)
                    oldPage = 0;
                else if (oldPos < -this._overlapSize[axis])
                    oldPage = Math.ceil(this._contentSize[axis] / this._pageSize[axis]) - 1;
                else
                    oldPage = Math.floor(-oldPos / this._pageSize[axis]);
                var startPage = Math.floor(-this._containerPos[axis] / this._pageSize[axis]);
                if (Math.abs(page - startPage) > 1 && Math.abs(oldPage - startPage) <= 1) {
                    if (page > startPage)
                        page = startPage + 1;
                    else
                        page = startPage - 1;
                    pos = -page * this._pageSize[axis];
                }
            }
            return pos;
        };
        ScrollPane.prototype.updateTargetAndDuration = function (orignPos, resultPos) {
            resultPos.x = this.updateTargetAndDuration2(orignPos.x, "x");
            resultPos.y = this.updateTargetAndDuration2(orignPos.y, "y");
        };
        ScrollPane.prototype.updateTargetAndDuration2 = function (pos, axis) {
            var v = this._velocity[axis];
            var duration = 0;
            if (pos > 0)
                pos = 0;
            else if (pos < -this._overlapSize[axis])
                pos = -this._overlapSize[axis];
            else {
                //以屏幕像素为基准
                var isMobile = egret.Capabilities.isMobile;
                var v2 = Math.abs(v) * this._velocityScale;
                //在移动设备上，需要对不同分辨率做一个适配，我们的速度判断以1136分辨率为基准
                if (isMobile)
                    v2 *= 1136 / Math.max(this._owner.displayObject.stage.stageWidth, this._owner.displayObject.stage.stageHeight);
                //这里有一些阈值的处理，因为在低速内，不希望产生较大的滚动（甚至不滚动）
                var ratio = 0;
                if (this._pageMode || !isMobile) {
                    if (v2 > 500)
                        ratio = Math.pow((v2 - 500) / 500, 2);
                }
                else {
                    if (v2 > 1000)
                        ratio = Math.pow((v2 - 1000) / 1000, 2);
                }
                if (ratio != 0) {
                    if (ratio > 1)
                        ratio = 1;
                    v2 *= ratio;
                    v *= ratio;
                    this._velocity[axis] = v;
                    //算法：v*（this._decelerationRate的n次幂）= 60，即在n帧后速度降为60（假设每秒60帧）。
                    duration = Math.log(60 / v2) / Math.log(this._decelerationRate) / 60;
                    //计算距离要使用本地速度
                    //理论公式貌似滚动的距离不够，改为经验公式
                    //var change:number = (v/ 60 - 1) / (1 - this._decelerationRate);
                    var change = Math.floor(v * duration * 0.4);
                    pos += change;
                }
            }
            if (duration < ScrollPane.TWEEN_TIME_DEFAULT)
                duration = ScrollPane.TWEEN_TIME_DEFAULT;
            this._tweenDuration[axis] = duration;
            return pos;
        };
        ScrollPane.prototype.fixDuration = function (axis, oldChange) {
            if (this._tweenChange[axis] == 0 || Math.abs(this._tweenChange[axis]) >= Math.abs(oldChange))
                return;
            var newDuration = Math.abs(this._tweenChange[axis] / oldChange) * this._tweenDuration[axis];
            if (newDuration < ScrollPane.TWEEN_TIME_DEFAULT)
                newDuration = ScrollPane.TWEEN_TIME_DEFAULT;
            this._tweenDuration[axis] = newDuration;
        };
        ScrollPane.prototype.killTween = function () {
            if (this._tweening == 1) //取消类型为1的tween需立刻设置到终点
             {
                this._container.x = this._tweenStart.x + this._tweenChange.x;
                this._container.y = this._tweenStart.y + this._tweenChange.y;
                this.dispatchEventWith(ScrollPane.SCROLL);
            }
            this._tweening = 0;
            egret.stopTick(this.tweenUpdate, this);
            this.dispatchEventWith(ScrollPane.SCROLL_END);
        };
        ScrollPane.prototype.checkRefreshBar = function () {
            if (this._header == null && this._footer == null)
                return;
            var pos = this._container[this._refreshBarAxis];
            if (this._header != null) {
                if (pos > 0) {
                    if (this._header.displayObject.parent == null)
                        this._maskContainer.addChildAt(this._header.displayObject, 0);
                    var pt = ScrollPane.sHelperPoint;
                    pt.setTo(this._header.width, this._header.height);
                    pt[this._refreshBarAxis] = pos;
                    this._header.setSize(pt.x, pt.y);
                }
                else {
                    if (this._header.displayObject.parent != null)
                        this._maskContainer.removeChild(this._header.displayObject);
                }
            }
            if (this._footer != null) {
                var max = this._overlapSize[this._refreshBarAxis];
                if (pos < -max || max == 0 && this._footerLockedSize > 0) {
                    if (this._footer.displayObject.parent == null)
                        this._maskContainer.addChildAt(this._footer.displayObject, 0);
                    pt = ScrollPane.sHelperPoint;
                    pt.setTo(this._footer.x, this._footer.y);
                    if (max > 0)
                        pt[this._refreshBarAxis] = pos + this._contentSize[this._refreshBarAxis];
                    else
                        pt[this._refreshBarAxis] = Math.max(Math.min(pos + this._viewSize[this._refreshBarAxis], this._viewSize[this._refreshBarAxis] - this._footerLockedSize), this._viewSize[this._refreshBarAxis] - this._contentSize[this._refreshBarAxis]);
                    this._footer.setXY(pt.x, pt.y);
                    pt.setTo(this._footer.width, this._footer.height);
                    if (max > 0)
                        pt[this._refreshBarAxis] = -max - pos;
                    else
                        pt[this._refreshBarAxis] = this._viewSize[this._refreshBarAxis] - this._footer[this._refreshBarAxis];
                    this._footer.setSize(pt.x, pt.y);
                }
                else {
                    if (this._footer.displayObject.parent != null)
                        this._maskContainer.removeChild(this._footer.displayObject);
                }
            }
        };
        ScrollPane.prototype.tweenUpdate = function (timestamp) {
            var nx = this.runTween("x");
            var ny = this.runTween("y");
            this._container.x = nx;
            this._container.y = ny;
            if (this._tweening == 2) {
                if (this._overlapSize.x > 0)
                    this._xPos = fairygui.ToolSet.clamp(-nx, 0, this._overlapSize.x);
                if (this._overlapSize.y > 0)
                    this._yPos = fairygui.ToolSet.clamp(-ny, 0, this._overlapSize.y);
                if (this._pageMode)
                    this.updatePageController();
            }
            if (this._tweenChange.x == 0 && this._tweenChange.y == 0) {
                this._tweening = 0;
                egret.stopTick(this.tweenUpdate, this);
                this.loopCheckingCurrent();
                this.syncScrollBar(true);
                this.checkRefreshBar();
                this.dispatchEventWith(ScrollPane.SCROLL);
                this.dispatchEventWith(ScrollPane.SCROLL_END);
            }
            else {
                this.syncScrollBar(false);
                this.checkRefreshBar();
                this.dispatchEventWith(ScrollPane.SCROLL);
            }
            return true;
        };
        ScrollPane.prototype.runTween = function (axis) {
            var newValue;
            if (this._tweenChange[axis] != 0) {
                this._tweenTime[axis] += fairygui.GTimers.deltaTime / 1000;
                if (this._tweenTime[axis] >= this._tweenDuration[axis]) {
                    newValue = this._tweenStart[axis] + this._tweenChange[axis];
                    this._tweenChange[axis] = 0;
                }
                else {
                    var ratio = ScrollPane.easeFunc(this._tweenTime[axis], this._tweenDuration[axis]);
                    newValue = this._tweenStart[axis] + Math.floor(this._tweenChange[axis] * ratio);
                }
                var threshold1 = 0;
                var threshold2 = -this._overlapSize[axis];
                if (this._headerLockedSize > 0 && this._refreshBarAxis == axis)
                    threshold1 = this._headerLockedSize;
                if (this._footerLockedSize > 0 && this._refreshBarAxis == axis) {
                    var max = this._overlapSize[this._refreshBarAxis];
                    if (max == 0)
                        max = Math.max(this._contentSize[this._refreshBarAxis] + this._footerLockedSize - this._viewSize[this._refreshBarAxis], 0);
                    else
                        max += this._footerLockedSize;
                    threshold2 = -max;
                }
                if (this._tweening == 2 && this._bouncebackEffect) {
                    if (newValue > 20 + threshold1 && this._tweenChange[axis] > 0
                        || newValue > threshold1 && this._tweenChange[axis] == 0) //开始回弹
                     {
                        this._tweenTime[axis] = 0;
                        this._tweenDuration[axis] = ScrollPane.TWEEN_TIME_DEFAULT;
                        this._tweenChange[axis] = -newValue + threshold1;
                        this._tweenStart[axis] = newValue;
                    }
                    else if (newValue < threshold2 - 20 && this._tweenChange[axis] < 0
                        || newValue < threshold2 && this._tweenChange[axis] == 0) //开始回弹
                     {
                        this._tweenTime[axis] = 0;
                        this._tweenDuration[axis] = ScrollPane.TWEEN_TIME_DEFAULT;
                        this._tweenChange[axis] = threshold2 - newValue;
                        this._tweenStart[axis] = newValue;
                    }
                }
                else {
                    if (newValue > threshold1) {
                        newValue = threshold1;
                        this._tweenChange[axis] = 0;
                    }
                    else if (newValue < threshold2) {
                        newValue = threshold2;
                        this._tweenChange[axis] = 0;
                    }
                }
            }
            else
                newValue = this._container[axis];
            return newValue;
        };
        ScrollPane.easeFunc = function (t, d) {
            return (t = t / d - 1) * t * t + 1; //cubicOut
        };
        ScrollPane._gestureFlag = 0;
        ScrollPane.SCROLL = "__scroll";
        ScrollPane.SCROLL_END = "__scrollEnd";
        ScrollPane.PULL_DOWN_RELEASE = "pullDownRelease";
        ScrollPane.PULL_UP_RELEASE = "pullUpRelease";
        ScrollPane.TWEEN_TIME_GO = 0.5; //调用SetPos(ani)时使用的缓动时间
        ScrollPane.TWEEN_TIME_DEFAULT = 0.3; //惯性滚动的最小缓动时间
        ScrollPane.PULL_RATIO = 0.5; //下拉过顶或者上拉过底时允许超过的距离占显示区域的比例
        ScrollPane.sHelperPoint = new cc.Vec2();
        ScrollPane.sHelperRect = new cc.Rect();
        ScrollPane.sEndPos = new cc.Vec2();
        ScrollPane.sOldChange = new cc.Vec2();
        return ScrollPane;
    }(fairygui.UIEventDispatcher));
    fairygui.ScrollPane = ScrollPane;
})(fairygui || (fairygui = {}));
var fairygui;
(function (fairygui) {
    var UIConfig = /** @class */ (function () {
        function UIConfig() {
        }
        //Default font name
        UIConfig.defaultFont = "SimSun";
        //When a modal window is in front, the background becomes dark.
        UIConfig.modalLayerColor = new cc.Color(0, 0, 0, 0.4);
        UIConfig.modalLayerAlpha = 0.2;
        UIConfig.buttonSoundVolumeScale = 1;
        //Scrolling step in pixels
        UIConfig.defaultScrollStep = 25;
        //Deceleration ratio of scrollpane when its in touch dragging.
        UIConfig.defaultScrollDecelerationRate = 0.967;
        //Default scrollbar display mode. Recommened visible for Desktop and Auto for mobile.
        UIConfig.defaultScrollBarDisplay = fairygui.ScrollBarDisplayType.Visible;
        //Allow dragging the content to scroll. Recommeded true for mobile.
        UIConfig.defaultScrollTouchEffect = true;
        //The "rebound" effect in the scolling container. Recommeded true for mobile.
        UIConfig.defaultScrollBounceEffect = true;
        //Max items displayed in combobox without scrolling.
        UIConfig.defaultComboBoxVisibleItemCount = 10;
        // Pixel offsets of finger to trigger scrolling.
        UIConfig.touchScrollSensitivity = 20;
        // Pixel offsets of finger to trigger dragging.
        UIConfig.touchDragSensitivity = 10;
        // Pixel offsets of mouse pointer to trigger dragging.
        UIConfig.clickDragSensitivity = 2;
        // When click the window, brings to front automatically.
        UIConfig.bringWindowToFrontOnClick = true;
        UIConfig.frameTimeForAsyncUIConstruction = 2;
        return UIConfig;
    }());
    fairygui.UIConfig = UIConfig;
})(fairygui || (fairygui = {}));
var fairygui;
(function (fairygui) {
    var UIObjectFactory = /** @class */ (function () {
        function UIObjectFactory() {
        }
        UIObjectFactory.setPackageItemExtension = function (url, type) {
            if (url == null)
                throw "Invaild url: " + url;
            var pi = fairygui.UIPackage.getItemByURL(url);
            if (pi != null)
                pi.extensionType = type;
            UIObjectFactory.packageItemExtensions[url] = type;
        };
        UIObjectFactory.setLoaderExtension = function (type) {
            UIObjectFactory.loaderType = type;
        };
        UIObjectFactory.$resolvePackageItemExtension = function (pi) {
            pi.extensionType = UIObjectFactory.packageItemExtensions["ui://" + pi.owner.id + pi.id];
            if (!pi.extensionType)
                pi.extensionType = UIObjectFactory.packageItemExtensions["ui://" + pi.owner.name + "/" + pi.name];
        };
        UIObjectFactory.newObject = function (pi) {
            switch (pi.type) {
                case fairygui.PackageItemType.Image:
                    return new fairygui.GImage();
                case fairygui.PackageItemType.MovieClip:
                    return new GMovieClip();
                case fairygui.PackageItemType.Component:
                    {
                        var cls = pi.extensionType;
                        if (cls)
                            return new cls();
                        var xml = pi.owner.getItemAsset(pi);
                        var extention = xml.attributes.extention;
                        if (extention != null) {
                            switch (extention) {
                                case "Button":
                                    return new fairygui.GButton();
                                case "Label":
                                    return new GLabel();
                                case "ProgressBar":
                                    return new GProgressBar();
                                case "Slider":
                                    return new GSlider();
                                case "ScrollBar":
                                    return new fairygui.GScrollBar();
                                case "ComboBox":
                                    return new GComboBox();
                                default:
                                    return new fairygui.GComponent();
                            }
                        }
                        else
                            return new fairygui.GComponent();
                    }
            }
            return null;
        };
        UIObjectFactory.newObject2 = function (type) {
            switch (type) {
                case "image":
                    return new fairygui.GImage();
                case "movieclip":
                    // return new GMovieClip();
                    return new fairygui.GObject();
                case "component":
                    return new fairygui.GComponent();
                case "text":
                    // return new GTextField();
                    return new fairygui.GObject();
                case "richtext":
                    // return new GRichTextField();
                    return new fairygui.GObject();
                case "inputtext":
                    // return new GTextInput();
                    return new fairygui.GObject();
                case "group":
                    // return new GGroup();
                    return new fairygui.GObject();
                case "list":
                    return new GList();
                case "graph":
                    return new fairygui.GGraph();
                case "loader":
                    if (UIObjectFactory.loaderType != null)
                        return new UIObjectFactory.loaderType();
                    else
                        return new fairygui.GLoader();
            }
            return null;
        };
        UIObjectFactory.packageItemExtensions = {};
        return UIObjectFactory;
    }());
    fairygui.UIObjectFactory = UIObjectFactory;
})(fairygui || (fairygui = {}));
var egret;
(function (egret) {
    // wrapper for non-node envs
    ;
    (function (sax) {
        sax.parser = function (strict, opt) { return new SAXParser(strict, opt); };
        sax.SAXParser = SAXParser;
        sax.SAXStream = SAXStream;
        sax.createStream = createStream;
        // When we pass the MAX_BUFFER_LENGTH position, start checking for buffer overruns.
        // When we check, schedule the next check for MAX_BUFFER_LENGTH - (max(buffer lengths)),
        // since that's the earliest that a buffer overrun could occur.  This way, checks are
        // as rare as required, but as often as necessary to ensure never crossing this bound.
        // Furthermore, buffers are only tested at most once per write(), so passing a very
        // large string into write() might have undesirable effects, but this is manageable by
        // the caller, so it is assumed to be safe.  Thus, a call to write() may, in the extreme
        // edge case, result in creating at most one complete copy of the string passed in.
        // Set to Infinity to have unlimited buffers.
        sax.MAX_BUFFER_LENGTH = 64 * 1024;
        var buffers = [
            "comment", "sgmlDecl", "textNode", "tagName", "doctype",
            "procInstName", "procInstBody", "entity", "attribName",
            "attribValue", "cdata", "script"
        ];
        sax.EVENTS = // for discoverability.
            ["text",
                "processinginstruction",
                "sgmldeclaration",
                "doctype",
                "comment",
                "attribute",
                "opentag",
                "closetag",
                "opencdata",
                "cdata",
                "closecdata",
                "error",
                "end",
                "ready",
                "script",
                "opennamespace",
                "closenamespace"
            ];
        function SAXParser(strict, opt) {
            if (!(this instanceof SAXParser))
                return new SAXParser(strict, opt);
            var parser = this;
            clearBuffers(parser);
            parser.q = parser.c = "";
            parser.bufferCheckPosition = sax.MAX_BUFFER_LENGTH;
            parser.opt = opt || {};
            parser.opt.lowercase = parser.opt.lowercase || parser.opt.lowercasetags;
            parser.looseCase = parser.opt.lowercase ? "toLowerCase" : "toUpperCase";
            parser.tags = [];
            parser.closed = parser.closedRoot = parser.sawRoot = false;
            parser.tag = parser.error = null;
            parser.strict = !!strict;
            parser.noscript = !!(strict || parser.opt.noscript);
            parser.state = S.BEGIN;
            parser.ENTITIES = Object.create(sax.ENTITIES);
            parser.attribList = [];
            // namespaces form a prototype chain.
            // it always points at the current tag,
            // which protos to its parent tag.
            if (parser.opt.xmlns)
                parser.ns = Object.create(rootNS);
            // mostly just for error reporting
            parser.trackPosition = parser.opt.position !== false;
            if (parser.trackPosition) {
                parser.position = parser.line = parser.column = 0;
            }
            emit(parser, "onready");
        }
        if (!Object.create)
            Object.create = function (o) {
                function f() { this.__proto__ = o; }
                f.prototype = o;
                return new f;
            };
        if (!Object.getPrototypeOf)
            Object.getPrototypeOf = function (o) {
                return o.__proto__;
            };
        if (!Object.keys)
            Object.keys = function (o) {
                var a = [];
                for (var i in o)
                    if (o.hasOwnProperty(i))
                        a.push(i);
                return a;
            };
        function checkBufferLength(parser) {
            var maxAllowed = Math.max(sax.MAX_BUFFER_LENGTH, 10), maxActual = 0;
            for (var i = 0, l = buffers.length; i < l; i++) {
                var len = parser[buffers[i]].length;
                if (len > maxAllowed) {
                    // Text/cdata nodes can get big, and since they're buffered,
                    // we can get here under normal conditions.
                    // Avoid issues by emitting the text node now,
                    // so at least it won't get any bigger.
                    switch (buffers[i]) {
                        case "textNode":
                            closeText(parser);
                            break;
                        case "cdata":
                            emitNode(parser, "oncdata", parser.cdata);
                            parser.cdata = "";
                            break;
                        case "script":
                            emitNode(parser, "onscript", parser.script);
                            parser.script = "";
                            break;
                        default:
                            error(parser, "Max buffer length exceeded: " + buffers[i]);
                    }
                }
                maxActual = Math.max(maxActual, len);
            }
            // schedule the next check for the earliest possible buffer overrun.
            parser.bufferCheckPosition = (sax.MAX_BUFFER_LENGTH - maxActual)
                + parser.position;
        }
        function clearBuffers(parser) {
            for (var i = 0, l = buffers.length; i < l; i++) {
                parser[buffers[i]] = "";
            }
        }
        function flushBuffers(parser) {
            closeText(parser);
            if (parser.cdata !== "") {
                emitNode(parser, "oncdata", parser.cdata);
                parser.cdata = "";
            }
            if (parser.script !== "") {
                emitNode(parser, "onscript", parser.script);
                parser.script = "";
            }
        }
        SAXParser.prototype =
            { end: function () { end(this); },
                write: write,
                resume: function () { this.error = null; return this; },
                close: function () { return this.write(null); },
                flush: function () { flushBuffers(this); }
            };
        var Stream = function () { };
        var streamWraps = sax.EVENTS.filter(function (ev) {
            return ev !== "error" && ev !== "end";
        });
        function createStream(strict, opt) {
            return new SAXStream(strict, opt);
        }
        function SAXStream(strict, opt) {
            if (!(this instanceof SAXStream))
                return new SAXStream(strict, opt);
            Stream.apply(this);
            this._parser = new SAXParser(strict, opt);
            this.writable = true;
            this.readable = true;
            var me = this;
            this._parser.onend = function () {
                me.emit("end");
            };
            this._parser.onerror = function (er) {
                me.emit("error", er);
                // if didn't throw, then means error was handled.
                // go ahead and clear error, so we can write again.
                me._parser.error = null;
            };
            this._decoder = null;
            streamWraps.forEach(function (ev) {
                Object.defineProperty(me, "on" + ev, {
                    get: function () { return me._parser["on" + ev]; },
                    set: function (h) {
                        if (!h) {
                            me.removeAllListeners(ev);
                            return me._parser["on" + ev] = h;
                        }
                        me.on(ev, h);
                    },
                    enumerable: true,
                    configurable: false
                });
            });
        }
        SAXStream.prototype = Object.create(Stream.prototype, { constructor: { value: SAXStream } });
        SAXStream.prototype.write = function (data) {
            //   if (typeof Buffer === 'function' &&
            //       typeof Buffer.isBuffer === 'function' &&
            //       Buffer.isBuffer(data)) {
            //     if (!this._decoder) {
            //       var SD = require('string_decoder').StringDecoder
            //       this._decoder = new SD('utf8')
            //     }
            //     data = this._decoder.write(data);
            //   }
            this._parser.write(data.toString());
            this.emit("data", data);
            return true;
        };
        SAXStream.prototype.end = function (chunk) {
            if (chunk && chunk.length)
                this.write(chunk);
            this._parser.end();
            return true;
        };
        SAXStream.prototype.on = function (ev, handler) {
            var me = this;
            if (!me._parser["on" + ev] && streamWraps.indexOf(ev) !== -1) {
                me._parser["on" + ev] = function () {
                    var args = arguments.length === 1 ? [arguments[0]]
                        : Array.apply(null, arguments);
                    args.splice(0, 0, ev);
                    me.emit.apply(me, args);
                };
            }
            return Stream.prototype.on.call(me, ev, handler);
        };
        // character classes and tokens
        var whitespace = "\r\n\t "
        // this really needs to be replaced with character classes.
        // XML allows all manner of ridiculous numbers and digits.
        , number = "0124356789", letter = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
        // (Letter | "_" | ":")
        , quote = "'\"", entity = number + letter + "#", attribEnd = whitespace + ">", CDATA = "[CDATA[", DOCTYPE = "DOCTYPE", XML_NAMESPACE = "http://www.w3.org/XML/1998/namespace", XMLNS_NAMESPACE = "http://www.w3.org/2000/xmlns/", rootNS = { xml: XML_NAMESPACE, xmlns: XMLNS_NAMESPACE };
        // turn all the string character sets into character class objects.
        whitespace = charClass(whitespace);
        number = charClass(number);
        letter = charClass(letter);
        // http://www.w3.org/TR/REC-xml/#NT-NameStartChar
        // This implementation works on strings, a single character at a time
        // as such, it cannot ever support astral-plane characters (10000-EFFFF)
        // without a significant breaking change to either this  parser, or the
        // JavaScript language.  Implementation of an emoji-capable xml parser
        // is left as an exercise for the reader.
        var nameStart = /[:_A-Za-z\u00C0-\u00D6\u00D8-\u00F6\u00F8-\u02FF\u0370-\u037D\u037F-\u1FFF\u200C-\u200D\u2070-\u218F\u2C00-\u2FEF\u3001-\uD7FF\uF900-\uFDCF\uFDF0-\uFFFD]/;
        var nameBody = /[:_A-Za-z\u00C0-\u00D6\u00D8-\u00F6\u00F8-\u02FF\u0370-\u037D\u037F-\u1FFF\u200C-\u200D\u2070-\u218F\u2C00-\u2FEF\u3001-\uD7FF\uF900-\uFDCF\uFDF0-\uFFFD\u00B7\u0300-\u036F\u203F-\u2040\.\d-]/;
        quote = charClass(quote);
        entity = charClass(entity);
        attribEnd = charClass(attribEnd);
        function charClass(str) {
            return str.split("").reduce(function (s, c) {
                s[c] = true;
                return s;
            }, {});
        }
        function isRegExp(c) {
            return Object.prototype.toString.call(c) === '[object RegExp]';
        }
        function is(charclass, c) {
            return isRegExp(charclass) ? !!c.match(charclass) : charclass[c];
        }
        function not(charclass, c) {
            return !is(charclass, c);
        }
        var S = 0;
        sax.STATE =
            { BEGIN: S++,
                TEXT: S++ // general stuff
                ,
                TEXT_ENTITY: S++ // &amp and such.
                ,
                OPEN_WAKA: S++ // <
                ,
                SGML_DECL: S++ // <!BLARG
                ,
                SGML_DECL_QUOTED: S++ // <!BLARG foo "bar
                ,
                DOCTYPE: S++ // <!DOCTYPE
                ,
                DOCTYPE_QUOTED: S++ // <!DOCTYPE "//blah
                ,
                DOCTYPE_DTD: S++ // <!DOCTYPE "//blah" [ ...
                ,
                DOCTYPE_DTD_QUOTED: S++ // <!DOCTYPE "//blah" [ "foo
                ,
                COMMENT_STARTING: S++ // <!-
                ,
                COMMENT: S++ // <!--
                ,
                COMMENT_ENDING: S++ // <!-- blah -
                ,
                COMMENT_ENDED: S++ // <!-- blah --
                ,
                CDATA: S++ // <![CDATA[ something
                ,
                CDATA_ENDING: S++ // ]
                ,
                CDATA_ENDING_2: S++ // ]]
                ,
                PROC_INST: S++ // <?hi
                ,
                PROC_INST_BODY: S++ // <?hi there
                ,
                PROC_INST_ENDING: S++ // <?hi "there" ?
                ,
                OPEN_TAG: S++ // <strong
                ,
                OPEN_TAG_SLASH: S++ // <strong /
                ,
                ATTRIB: S++ // <a
                ,
                ATTRIB_NAME: S++ // <a foo
                ,
                ATTRIB_NAME_SAW_WHITE: S++ // <a foo _
                ,
                ATTRIB_VALUE: S++ // <a foo=
                ,
                ATTRIB_VALUE_QUOTED: S++ // <a foo="bar
                ,
                ATTRIB_VALUE_CLOSED: S++ // <a foo="bar"
                ,
                ATTRIB_VALUE_UNQUOTED: S++ // <a foo=bar
                ,
                ATTRIB_VALUE_ENTITY_Q: S++ // <foo bar="&quot;"
                ,
                ATTRIB_VALUE_ENTITY_U: S++ // <foo bar=&quot;
                ,
                CLOSE_TAG: S++ // </a
                ,
                CLOSE_TAG_SAW_WHITE: S++ // </a   >
                ,
                SCRIPT: S++ // <script> ...
                ,
                SCRIPT_ENDING: S++ // <script> ... <
            };
        sax.ENTITIES =
            { "amp": "&",
                "gt": ">",
                "lt": "<",
                "quot": "\"",
                "apos": "'",
                "AElig": 198,
                "Aacute": 193,
                "Acirc": 194,
                "Agrave": 192,
                "Aring": 197,
                "Atilde": 195,
                "Auml": 196,
                "Ccedil": 199,
                "ETH": 208,
                "Eacute": 201,
                "Ecirc": 202,
                "Egrave": 200,
                "Euml": 203,
                "Iacute": 205,
                "Icirc": 206,
                "Igrave": 204,
                "Iuml": 207,
                "Ntilde": 209,
                "Oacute": 211,
                "Ocirc": 212,
                "Ograve": 210,
                "Oslash": 216,
                "Otilde": 213,
                "Ouml": 214,
                "THORN": 222,
                "Uacute": 218,
                "Ucirc": 219,
                "Ugrave": 217,
                "Uuml": 220,
                "Yacute": 221,
                "aacute": 225,
                "acirc": 226,
                "aelig": 230,
                "agrave": 224,
                "aring": 229,
                "atilde": 227,
                "auml": 228,
                "ccedil": 231,
                "eacute": 233,
                "ecirc": 234,
                "egrave": 232,
                "eth": 240,
                "euml": 235,
                "iacute": 237,
                "icirc": 238,
                "igrave": 236,
                "iuml": 239,
                "ntilde": 241,
                "oacute": 243,
                "ocirc": 244,
                "ograve": 242,
                "oslash": 248,
                "otilde": 245,
                "ouml": 246,
                "szlig": 223,
                "thorn": 254,
                "uacute": 250,
                "ucirc": 251,
                "ugrave": 249,
                "uuml": 252,
                "yacute": 253,
                "yuml": 255,
                "copy": 169,
                "reg": 174,
                "nbsp": 160,
                "iexcl": 161,
                "cent": 162,
                "pound": 163,
                "curren": 164,
                "yen": 165,
                "brvbar": 166,
                "sect": 167,
                "uml": 168,
                "ordf": 170,
                "laquo": 171,
                "not": 172,
                "shy": 173,
                "macr": 175,
                "deg": 176,
                "plusmn": 177,
                "sup1": 185,
                "sup2": 178,
                "sup3": 179,
                "acute": 180,
                "micro": 181,
                "para": 182,
                "middot": 183,
                "cedil": 184,
                "ordm": 186,
                "raquo": 187,
                "frac14": 188,
                "frac12": 189,
                "frac34": 190,
                "iquest": 191,
                "times": 215,
                "divide": 247,
                "OElig": 338,
                "oelig": 339,
                "Scaron": 352,
                "scaron": 353,
                "Yuml": 376,
                "fnof": 402,
                "circ": 710,
                "tilde": 732,
                "Alpha": 913,
                "Beta": 914,
                "Gamma": 915,
                "Delta": 916,
                "Epsilon": 917,
                "Zeta": 918,
                "Eta": 919,
                "Theta": 920,
                "Iota": 921,
                "Kappa": 922,
                "Lambda": 923,
                "Mu": 924,
                "Nu": 925,
                "Xi": 926,
                "Omicron": 927,
                "Pi": 928,
                "Rho": 929,
                "Sigma": 931,
                "Tau": 932,
                "Upsilon": 933,
                "Phi": 934,
                "Chi": 935,
                "Psi": 936,
                "Omega": 937,
                "alpha": 945,
                "beta": 946,
                "gamma": 947,
                "delta": 948,
                "epsilon": 949,
                "zeta": 950,
                "eta": 951,
                "theta": 952,
                "iota": 953,
                "kappa": 954,
                "lambda": 955,
                "mu": 956,
                "nu": 957,
                "xi": 958,
                "omicron": 959,
                "pi": 960,
                "rho": 961,
                "sigmaf": 962,
                "sigma": 963,
                "tau": 964,
                "upsilon": 965,
                "phi": 966,
                "chi": 967,
                "psi": 968,
                "omega": 969,
                "thetasym": 977,
                "upsih": 978,
                "piv": 982,
                "ensp": 8194,
                "emsp": 8195,
                "thinsp": 8201,
                "zwnj": 8204,
                "zwj": 8205,
                "lrm": 8206,
                "rlm": 8207,
                "ndash": 8211,
                "mdash": 8212,
                "lsquo": 8216,
                "rsquo": 8217,
                "sbquo": 8218,
                "ldquo": 8220,
                "rdquo": 8221,
                "bdquo": 8222,
                "dagger": 8224,
                "Dagger": 8225,
                "bull": 8226,
                "hellip": 8230,
                "permil": 8240,
                "prime": 8242,
                "Prime": 8243,
                "lsaquo": 8249,
                "rsaquo": 8250,
                "oline": 8254,
                "frasl": 8260,
                "euro": 8364,
                "image": 8465,
                "weierp": 8472,
                "real": 8476,
                "trade": 8482,
                "alefsym": 8501,
                "larr": 8592,
                "uarr": 8593,
                "rarr": 8594,
                "darr": 8595,
                "harr": 8596,
                "crarr": 8629,
                "lArr": 8656,
                "uArr": 8657,
                "rArr": 8658,
                "dArr": 8659,
                "hArr": 8660,
                "forall": 8704,
                "part": 8706,
                "exist": 8707,
                "empty": 8709,
                "nabla": 8711,
                "isin": 8712,
                "notin": 8713,
                "ni": 8715,
                "prod": 8719,
                "sum": 8721,
                "minus": 8722,
                "lowast": 8727,
                "radic": 8730,
                "prop": 8733,
                "infin": 8734,
                "ang": 8736,
                "and": 8743,
                "or": 8744,
                "cap": 8745,
                "cup": 8746,
                "int": 8747,
                "there4": 8756,
                "sim": 8764,
                "cong": 8773,
                "asymp": 8776,
                "ne": 8800,
                "equiv": 8801,
                "le": 8804,
                "ge": 8805,
                "sub": 8834,
                "sup": 8835,
                "nsub": 8836,
                "sube": 8838,
                "supe": 8839,
                "oplus": 8853,
                "otimes": 8855,
                "perp": 8869,
                "sdot": 8901,
                "lceil": 8968,
                "rceil": 8969,
                "lfloor": 8970,
                "rfloor": 8971,
                "lang": 9001,
                "rang": 9002,
                "loz": 9674,
                "spades": 9824,
                "clubs": 9827,
                "hearts": 9829,
                "diams": 9830
            };
        Object.keys(sax.ENTITIES).forEach(function (key) {
            var e = sax.ENTITIES[key];
            var s = typeof e === 'number' ? String.fromCharCode(e) : e;
            sax.ENTITIES[key] = s;
        });
        for (var S in sax.STATE)
            sax.STATE[sax.STATE[S]] = S;
        // shorthand
        S = sax.STATE;
        function emit(parser, event, data) {
            parser[event] && parser[event](data);
        }
        function emitNode(parser, nodeType, data) {
            if (parser.textNode)
                closeText(parser);
            emit(parser, nodeType, data);
        }
        function closeText(parser) {
            parser.textNode = textopts(parser.opt, parser.textNode);
            if (parser.textNode)
                emit(parser, "ontext", parser.textNode);
            parser.textNode = "";
        }
        function textopts(opt, text) {
            if (opt.trim)
                text = text.trim();
            if (opt.normalize)
                text = text.replace(/\s+/g, " ");
            return text;
        }
        function error(parser, er) {
            closeText(parser);
            if (parser.trackPosition) {
                er += "\nLine: " + parser.line +
                    "\nColumn: " + parser.column +
                    "\nChar: " + parser.c;
            }
            er = new Error(er);
            parser.error = er;
            emit(parser, "onerror", er);
            return parser;
        }
        function end(parser) {
            if (!parser.closedRoot)
                strictFail(parser, "Unclosed root tag");
            if ((parser.state !== S.BEGIN) && (parser.state !== S.TEXT))
                error(parser, "Unexpected end");
            closeText(parser);
            parser.c = "";
            parser.closed = true;
            emit(parser, "onend");
            SAXParser.call(parser, parser.strict, parser.opt);
            return parser;
        }
        function strictFail(parser, message) {
            if (typeof parser !== 'object' || !(parser instanceof SAXParser))
                throw new Error('bad call to strictFail');
            if (parser.strict)
                error(parser, message);
        }
        function newTag(parser) {
            if (!parser.strict)
                parser.tagName = parser.tagName[parser.looseCase]();
            var parent = parser.tags[parser.tags.length - 1] || parser, tag = parser.tag = { name: parser.tagName, attributes: {} };
            // will be overridden if tag contails an xmlns="foo" or xmlns:foo="bar"
            if (parser.opt.xmlns)
                tag.ns = parent.ns;
            parser.attribList.length = 0;
        }
        function qname(name, attribute) {
            var i = name.indexOf(":"), qualName = i < 0 ? ["", name] : name.split(":"), prefix = qualName[0], local = qualName[1];
            // <x "xmlns"="http://foo">
            if (attribute && name === "xmlns") {
                prefix = "xmlns";
                local = "";
            }
            return { prefix: prefix, local: local };
        }
        function attrib(parser) {
            if (!parser.strict)
                parser.attribName = parser.attribName[parser.looseCase]();
            if (parser.attribList.indexOf(parser.attribName) !== -1 ||
                parser.tag.attributes.hasOwnProperty(parser.attribName)) {
                return parser.attribName = parser.attribValue = "";
            }
            if (parser.opt.xmlns) {
                var qn = qname(parser.attribName, true), prefix = qn.prefix, local = qn.local;
                if (prefix === "xmlns") {
                    // namespace binding attribute; push the binding into scope
                    if (local === "xml" && parser.attribValue !== XML_NAMESPACE) {
                        strictFail(parser, "xml: prefix must be bound to " + XML_NAMESPACE + "\n"
                            + "Actual: " + parser.attribValue);
                    }
                    else if (local === "xmlns" && parser.attribValue !== XMLNS_NAMESPACE) {
                        strictFail(parser, "xmlns: prefix must be bound to " + XMLNS_NAMESPACE + "\n"
                            + "Actual: " + parser.attribValue);
                    }
                    else {
                        var tag = parser.tag, parent = parser.tags[parser.tags.length - 1] || parser;
                        if (tag.ns === parent.ns) {
                            tag.ns = Object.create(parent.ns);
                        }
                        tag.ns[local] = parser.attribValue;
                    }
                }
                // defer onattribute events until all attributes have been seen
                // so any new bindings can take effect; preserve attribute order
                // so deferred events can be emitted in document order
                parser.attribList.push([parser.attribName, parser.attribValue]);
            }
            else {
                // in non-xmlns mode, we can emit the event right away
                parser.tag.attributes[parser.attribName] = parser.attribValue;
                emitNode(parser, "onattribute", { name: parser.attribName,
                    value: parser.attribValue });
            }
            parser.attribName = parser.attribValue = "";
        }
        function openTag(parser, selfClosing) {
            if (parser.opt.xmlns) {
                // emit namespace binding events
                var tag = parser.tag;
                // add namespace info to tag
                var qn = qname(parser.tagName);
                tag.prefix = qn.prefix;
                tag.local = qn.local;
                tag.uri = tag.ns[qn.prefix] || "";
                if (tag.prefix && !tag.uri) {
                    strictFail(parser, "Unbound namespace prefix: "
                        + JSON.stringify(parser.tagName));
                    tag.uri = qn.prefix;
                }
                var parent = parser.tags[parser.tags.length - 1] || parser;
                if (tag.ns && parent.ns !== tag.ns) {
                    Object.keys(tag.ns).forEach(function (p) {
                        emitNode(parser, "onopennamespace", { prefix: p, uri: tag.ns[p] });
                    });
                }
                // handle deferred onattribute events
                // Note: do not apply default ns to attributes:
                //   http://www.w3.org/TR/REC-xml-names/#defaulting
                for (var i = 0, l = parser.attribList.length; i < l; i++) {
                    var nv = parser.attribList[i];
                    var name = nv[0], value = nv[1], qualName = qname(name, true), prefix = qualName.prefix, local = qualName.local, uri = prefix == "" ? "" : (tag.ns[prefix] || ""), a = { name: name,
                        value: value,
                        prefix: prefix,
                        local: local,
                        uri: uri
                    };
                    // if there's any attributes with an undefined namespace,
                    // then fail on them now.
                    if (prefix && prefix != "xmlns" && !uri) {
                        strictFail(parser, "Unbound namespace prefix: "
                            + JSON.stringify(prefix));
                        a.uri = prefix;
                    }
                    parser.tag.attributes[name] = a;
                    emitNode(parser, "onattribute", a);
                }
                parser.attribList.length = 0;
            }
            parser.tag.isSelfClosing = !!selfClosing;
            // process the tag
            parser.sawRoot = true;
            parser.tags.push(parser.tag);
            emitNode(parser, "onopentag", parser.tag);
            if (!selfClosing) {
                // special case for <script> in non-strict mode.
                if (!parser.noscript && parser.tagName.toLowerCase() === "script") {
                    parser.state = S.SCRIPT;
                }
                else {
                    parser.state = S.TEXT;
                }
                parser.tag = null;
                parser.tagName = "";
            }
            parser.attribName = parser.attribValue = "";
            parser.attribList.length = 0;
        }
        function closeTag(parser) {
            if (!parser.tagName) {
                strictFail(parser, "Weird empty close tag.");
                parser.textNode += "</>";
                parser.state = S.TEXT;
                return;
            }
            if (parser.script) {
                if (parser.tagName !== "script") {
                    parser.script += "</" + parser.tagName + ">";
                    parser.tagName = "";
                    parser.state = S.SCRIPT;
                    return;
                }
                emitNode(parser, "onscript", parser.script);
                parser.script = "";
            }
            // first make sure that the closing tag actually exists.
            // <a><b></c></b></a> will close everything, otherwise.
            var t = parser.tags.length;
            var tagName = parser.tagName;
            if (!parser.strict)
                tagName = tagName[parser.looseCase]();
            var closeTo = tagName;
            while (t--) {
                var close = parser.tags[t];
                if (close.name !== closeTo) {
                    // fail the first time in strict mode
                    strictFail(parser, "Unexpected close tag");
                }
                else
                    break;
            }
            // didn't find it.  we already failed for strict, so just abort.
            if (t < 0) {
                strictFail(parser, "Unmatched closing tag: " + parser.tagName);
                parser.textNode += "</" + parser.tagName + ">";
                parser.state = S.TEXT;
                return;
            }
            parser.tagName = tagName;
            var s = parser.tags.length;
            while (s-- > t) {
                var tag = parser.tag = parser.tags.pop();
                parser.tagName = parser.tag.name;
                emitNode(parser, "onclosetag", parser.tagName);
                var x = {};
                for (var i in tag.ns)
                    x[i] = tag.ns[i];
                var parent = parser.tags[parser.tags.length - 1] || parser;
                if (parser.opt.xmlns && tag.ns !== parent.ns) {
                    // remove namespace bindings introduced by tag
                    Object.keys(tag.ns).forEach(function (p) {
                        var n = tag.ns[p];
                        emitNode(parser, "onclosenamespace", { prefix: p, uri: n });
                    });
                }
            }
            if (t === 0)
                parser.closedRoot = true;
            parser.tagName = parser.attribValue = parser.attribName = "";
            parser.attribList.length = 0;
            parser.state = S.TEXT;
        }
        function parseEntity(parser) {
            var entity = parser.entity, entityLC = entity.toLowerCase(), num, numStr = "";
            if (parser.ENTITIES[entity])
                return parser.ENTITIES[entity];
            if (parser.ENTITIES[entityLC])
                return parser.ENTITIES[entityLC];
            entity = entityLC;
            if (entity.charAt(0) === "#") {
                if (entity.charAt(1) === "x") {
                    entity = entity.slice(2);
                    num = parseInt(entity, 16);
                    numStr = num.toString(16);
                }
                else {
                    entity = entity.slice(1);
                    num = parseInt(entity, 10);
                    numStr = num.toString(10);
                }
            }
            entity = entity.replace(/^0+/, "");
            if (numStr.toLowerCase() !== entity) {
                strictFail(parser, "Invalid character entity");
                return "&" + parser.entity + ";";
            }
            return String.fromCharCode(num);
        }
        function write(chunk) {
            var parser = this;
            if (this.error)
                throw this.error;
            if (parser.closed)
                return error(parser, "Cannot write after close. Assign an onready handler.");
            if (chunk === null)
                return end(parser);
            var i = 0, c = "";
            while (parser.c = c = chunk.charAt(i++)) {
                if (parser.trackPosition) {
                    parser.position++;
                    if (c === "\n") {
                        parser.line++;
                        parser.column = 0;
                    }
                    else
                        parser.column++;
                }
                switch (parser.state) {
                    case S.BEGIN:
                        if (c === "<") {
                            parser.state = S.OPEN_WAKA;
                            parser.startTagPosition = parser.position;
                        }
                        else if (not(whitespace, c)) {
                            // have to process this as a text node.
                            // weird, but happens.
                            strictFail(parser, "Non-whitespace before first tag.");
                            parser.textNode = c;
                            parser.state = S.TEXT;
                        }
                        continue;
                    case S.TEXT:
                        if (parser.sawRoot && !parser.closedRoot) {
                            var starti = i - 1;
                            while (c && c !== "<" && c !== "&") {
                                c = chunk.charAt(i++);
                                if (c && parser.trackPosition) {
                                    parser.position++;
                                    if (c === "\n") {
                                        parser.line++;
                                        parser.column = 0;
                                    }
                                    else
                                        parser.column++;
                                }
                            }
                            parser.textNode += chunk.substring(starti, i - 1);
                        }
                        if (c === "<") {
                            parser.state = S.OPEN_WAKA;
                            parser.startTagPosition = parser.position;
                        }
                        else {
                            if (not(whitespace, c) && (!parser.sawRoot || parser.closedRoot))
                                strictFail(parser, "Text data outside of root node.");
                            if (c === "&")
                                parser.state = S.TEXT_ENTITY;
                            else
                                parser.textNode += c;
                        }
                        continue;
                    case S.SCRIPT:
                        // only non-strict
                        if (c === "<") {
                            parser.state = S.SCRIPT_ENDING;
                        }
                        else
                            parser.script += c;
                        continue;
                    case S.SCRIPT_ENDING:
                        if (c === "/") {
                            parser.state = S.CLOSE_TAG;
                        }
                        else {
                            parser.script += "<" + c;
                            parser.state = S.SCRIPT;
                        }
                        continue;
                    case S.OPEN_WAKA:
                        // either a /, ?, !, or text is coming next.
                        if (c === "!") {
                            parser.state = S.SGML_DECL;
                            parser.sgmlDecl = "";
                        }
                        else if (is(whitespace, c)) {
                            // wait for it...
                        }
                        else if (is(nameStart, c)) {
                            parser.state = S.OPEN_TAG;
                            parser.tagName = c;
                        }
                        else if (c === "/") {
                            parser.state = S.CLOSE_TAG;
                            parser.tagName = "";
                        }
                        else if (c === "?") {
                            parser.state = S.PROC_INST;
                            parser.procInstName = parser.procInstBody = "";
                        }
                        else {
                            strictFail(parser, "Unencoded <");
                            // if there was some whitespace, then add that in.
                            if (parser.startTagPosition + 1 < parser.position) {
                                var pad = parser.position - parser.startTagPosition;
                                c = new Array(pad).join(" ") + c;
                            }
                            parser.textNode += "<" + c;
                            parser.state = S.TEXT;
                        }
                        continue;
                    case S.SGML_DECL:
                        if ((parser.sgmlDecl + c).toUpperCase() === CDATA) {
                            emitNode(parser, "onopencdata");
                            parser.state = S.CDATA;
                            parser.sgmlDecl = "";
                            parser.cdata = "";
                        }
                        else if (parser.sgmlDecl + c === "--") {
                            parser.state = S.COMMENT;
                            parser.comment = "";
                            parser.sgmlDecl = "";
                        }
                        else if ((parser.sgmlDecl + c).toUpperCase() === DOCTYPE) {
                            parser.state = S.DOCTYPE;
                            if (parser.doctype || parser.sawRoot)
                                strictFail(parser, "Inappropriately located doctype declaration");
                            parser.doctype = "";
                            parser.sgmlDecl = "";
                        }
                        else if (c === ">") {
                            emitNode(parser, "onsgmldeclaration", parser.sgmlDecl);
                            parser.sgmlDecl = "";
                            parser.state = S.TEXT;
                        }
                        else if (is(quote, c)) {
                            parser.state = S.SGML_DECL_QUOTED;
                            parser.sgmlDecl += c;
                        }
                        else
                            parser.sgmlDecl += c;
                        continue;
                    case S.SGML_DECL_QUOTED:
                        if (c === parser.q) {
                            parser.state = S.SGML_DECL;
                            parser.q = "";
                        }
                        parser.sgmlDecl += c;
                        continue;
                    case S.DOCTYPE:
                        if (c === ">") {
                            parser.state = S.TEXT;
                            emitNode(parser, "ondoctype", parser.doctype);
                            parser.doctype = true; // just remember that we saw it.
                        }
                        else {
                            parser.doctype += c;
                            if (c === "[")
                                parser.state = S.DOCTYPE_DTD;
                            else if (is(quote, c)) {
                                parser.state = S.DOCTYPE_QUOTED;
                                parser.q = c;
                            }
                        }
                        continue;
                    case S.DOCTYPE_QUOTED:
                        parser.doctype += c;
                        if (c === parser.q) {
                            parser.q = "";
                            parser.state = S.DOCTYPE;
                        }
                        continue;
                    case S.DOCTYPE_DTD:
                        parser.doctype += c;
                        if (c === "]")
                            parser.state = S.DOCTYPE;
                        else if (is(quote, c)) {
                            parser.state = S.DOCTYPE_DTD_QUOTED;
                            parser.q = c;
                        }
                        continue;
                    case S.DOCTYPE_DTD_QUOTED:
                        parser.doctype += c;
                        if (c === parser.q) {
                            parser.state = S.DOCTYPE_DTD;
                            parser.q = "";
                        }
                        continue;
                    case S.COMMENT:
                        if (c === "-")
                            parser.state = S.COMMENT_ENDING;
                        else
                            parser.comment += c;
                        continue;
                    case S.COMMENT_ENDING:
                        if (c === "-") {
                            parser.state = S.COMMENT_ENDED;
                            parser.comment = textopts(parser.opt, parser.comment);
                            if (parser.comment)
                                emitNode(parser, "oncomment", parser.comment);
                            parser.comment = "";
                        }
                        else {
                            parser.comment += "-" + c;
                            parser.state = S.COMMENT;
                        }
                        continue;
                    case S.COMMENT_ENDED:
                        if (c !== ">") {
                            strictFail(parser, "Malformed comment");
                            // allow <!-- blah -- bloo --> in non-strict mode,
                            // which is a comment of " blah -- bloo "
                            parser.comment += "--" + c;
                            parser.state = S.COMMENT;
                        }
                        else
                            parser.state = S.TEXT;
                        continue;
                    case S.CDATA:
                        if (c === "]")
                            parser.state = S.CDATA_ENDING;
                        else
                            parser.cdata += c;
                        continue;
                    case S.CDATA_ENDING:
                        if (c === "]")
                            parser.state = S.CDATA_ENDING_2;
                        else {
                            parser.cdata += "]" + c;
                            parser.state = S.CDATA;
                        }
                        continue;
                    case S.CDATA_ENDING_2:
                        if (c === ">") {
                            if (parser.cdata)
                                emitNode(parser, "oncdata", parser.cdata);
                            emitNode(parser, "onclosecdata");
                            parser.cdata = "";
                            parser.state = S.TEXT;
                        }
                        else if (c === "]") {
                            parser.cdata += "]";
                        }
                        else {
                            parser.cdata += "]]" + c;
                            parser.state = S.CDATA;
                        }
                        continue;
                    case S.PROC_INST:
                        if (c === "?")
                            parser.state = S.PROC_INST_ENDING;
                        else if (is(whitespace, c))
                            parser.state = S.PROC_INST_BODY;
                        else
                            parser.procInstName += c;
                        continue;
                    case S.PROC_INST_BODY:
                        if (!parser.procInstBody && is(whitespace, c))
                            continue;
                        else if (c === "?")
                            parser.state = S.PROC_INST_ENDING;
                        else
                            parser.procInstBody += c;
                        continue;
                    case S.PROC_INST_ENDING:
                        if (c === ">") {
                            emitNode(parser, "onprocessinginstruction", {
                                name: parser.procInstName,
                                body: parser.procInstBody
                            });
                            parser.procInstName = parser.procInstBody = "";
                            parser.state = S.TEXT;
                        }
                        else {
                            parser.procInstBody += "?" + c;
                            parser.state = S.PROC_INST_BODY;
                        }
                        continue;
                    case S.OPEN_TAG:
                        if (is(nameBody, c))
                            parser.tagName += c;
                        else {
                            newTag(parser);
                            if (c === ">")
                                openTag(parser);
                            else if (c === "/")
                                parser.state = S.OPEN_TAG_SLASH;
                            else {
                                if (not(whitespace, c))
                                    strictFail(parser, "Invalid character in tag name");
                                parser.state = S.ATTRIB;
                            }
                        }
                        continue;
                    case S.OPEN_TAG_SLASH:
                        if (c === ">") {
                            openTag(parser, true);
                            closeTag(parser);
                        }
                        else {
                            strictFail(parser, "Forward-slash in opening tag not followed by >");
                            parser.state = S.ATTRIB;
                        }
                        continue;
                    case S.ATTRIB:
                        // haven't read the attribute name yet.
                        if (is(whitespace, c))
                            continue;
                        else if (c === ">")
                            openTag(parser);
                        else if (c === "/")
                            parser.state = S.OPEN_TAG_SLASH;
                        else if (is(nameStart, c)) {
                            parser.attribName = c;
                            parser.attribValue = "";
                            parser.state = S.ATTRIB_NAME;
                        }
                        else
                            strictFail(parser, "Invalid attribute name");
                        continue;
                    case S.ATTRIB_NAME:
                        if (c === "=")
                            parser.state = S.ATTRIB_VALUE;
                        else if (c === ">") {
                            strictFail(parser, "Attribute without value");
                            parser.attribValue = parser.attribName;
                            attrib(parser);
                            openTag(parser);
                        }
                        else if (is(whitespace, c))
                            parser.state = S.ATTRIB_NAME_SAW_WHITE;
                        else if (is(nameBody, c))
                            parser.attribName += c;
                        else
                            strictFail(parser, "Invalid attribute name");
                        continue;
                    case S.ATTRIB_NAME_SAW_WHITE:
                        if (c === "=")
                            parser.state = S.ATTRIB_VALUE;
                        else if (is(whitespace, c))
                            continue;
                        else {
                            strictFail(parser, "Attribute without value");
                            parser.tag.attributes[parser.attribName] = "";
                            parser.attribValue = "";
                            emitNode(parser, "onattribute", { name: parser.attribName, value: "" });
                            parser.attribName = "";
                            if (c === ">")
                                openTag(parser);
                            else if (is(nameStart, c)) {
                                parser.attribName = c;
                                parser.state = S.ATTRIB_NAME;
                            }
                            else {
                                strictFail(parser, "Invalid attribute name");
                                parser.state = S.ATTRIB;
                            }
                        }
                        continue;
                    case S.ATTRIB_VALUE:
                        if (is(whitespace, c))
                            continue;
                        else if (is(quote, c)) {
                            parser.q = c;
                            parser.state = S.ATTRIB_VALUE_QUOTED;
                        }
                        else {
                            strictFail(parser, "Unquoted attribute value");
                            parser.state = S.ATTRIB_VALUE_UNQUOTED;
                            parser.attribValue = c;
                        }
                        continue;
                    case S.ATTRIB_VALUE_QUOTED:
                        if (c !== parser.q) {
                            if (c === "&")
                                parser.state = S.ATTRIB_VALUE_ENTITY_Q;
                            else
                                parser.attribValue += c;
                            continue;
                        }
                        attrib(parser);
                        parser.q = "";
                        parser.state = S.ATTRIB_VALUE_CLOSED;
                        continue;
                    case S.ATTRIB_VALUE_CLOSED:
                        if (is(whitespace, c)) {
                            parser.state = S.ATTRIB;
                        }
                        else if (c === ">")
                            openTag(parser);
                        else if (c === "/")
                            parser.state = S.OPEN_TAG_SLASH;
                        else if (is(nameStart, c)) {
                            strictFail(parser, "No whitespace between attributes");
                            parser.attribName = c;
                            parser.attribValue = "";
                            parser.state = S.ATTRIB_NAME;
                        }
                        else
                            strictFail(parser, "Invalid attribute name");
                        continue;
                    case S.ATTRIB_VALUE_UNQUOTED:
                        if (not(attribEnd, c)) {
                            if (c === "&")
                                parser.state = S.ATTRIB_VALUE_ENTITY_U;
                            else
                                parser.attribValue += c;
                            continue;
                        }
                        attrib(parser);
                        if (c === ">")
                            openTag(parser);
                        else
                            parser.state = S.ATTRIB;
                        continue;
                    case S.CLOSE_TAG:
                        if (!parser.tagName) {
                            if (is(whitespace, c))
                                continue;
                            else if (not(nameStart, c)) {
                                if (parser.script) {
                                    parser.script += "</" + c;
                                    parser.state = S.SCRIPT;
                                }
                                else {
                                    strictFail(parser, "Invalid tagname in closing tag.");
                                }
                            }
                            else
                                parser.tagName = c;
                        }
                        else if (c === ">")
                            closeTag(parser);
                        else if (is(nameBody, c))
                            parser.tagName += c;
                        else if (parser.script) {
                            parser.script += "</" + parser.tagName;
                            parser.tagName = "";
                            parser.state = S.SCRIPT;
                        }
                        else {
                            if (not(whitespace, c))
                                strictFail(parser, "Invalid tagname in closing tag");
                            parser.state = S.CLOSE_TAG_SAW_WHITE;
                        }
                        continue;
                    case S.CLOSE_TAG_SAW_WHITE:
                        if (is(whitespace, c))
                            continue;
                        if (c === ">")
                            closeTag(parser);
                        else
                            strictFail(parser, "Invalid characters in closing tag");
                        continue;
                    case S.TEXT_ENTITY:
                    case S.ATTRIB_VALUE_ENTITY_Q:
                    case S.ATTRIB_VALUE_ENTITY_U:
                        switch (parser.state) {
                            case S.TEXT_ENTITY:
                                var returnState = S.TEXT, buffer = "textNode";
                                break;
                            case S.ATTRIB_VALUE_ENTITY_Q:
                                var returnState = S.ATTRIB_VALUE_QUOTED, buffer = "attribValue";
                                break;
                            case S.ATTRIB_VALUE_ENTITY_U:
                                var returnState = S.ATTRIB_VALUE_UNQUOTED, buffer = "attribValue";
                                break;
                        }
                        if (c === ";") {
                            parser[buffer] += parseEntity(parser);
                            parser.entity = "";
                            parser.state = returnState;
                        }
                        else if (is(entity, c))
                            parser.entity += c;
                        else {
                            strictFail(parser, "Invalid character entity");
                            parser[buffer] += "&" + parser.entity + c;
                            parser.entity = "";
                            parser.state = returnState;
                        }
                        continue;
                    default:
                        throw new Error(parser, "Unknown state: " + parser.state);
                }
            } // while
            // cdata blocks can get very big under normal conditions. emit and move on.
            // if (parser.state === S.CDATA && parser.cdata) {
            //   emitNode(parser, "oncdata", parser.cdata)
            //   parser.cdata = ""
            // }
            if (parser.position >= parser.bufferCheckPosition)
                checkBufferLength(parser);
            return parser;
        }
    })(typeof exports === "undefined" ? sax = {} : exports);
    var saxparser = sax.parser(true);
    function g_parse(xmlString) {
        var object = null;
        var namespaces = {};
        var hasError = false;
        saxparser.onerror = function (err) {
            hasError = true;
        };
        saxparser.onopentag = function (node) {
            var attribs = node.attributes;
            // delete node["attributes"];
            if (!node["children"]) {
                node["children"] = [];
            }
            for (var key in attribs) {
                index = key.indexOf("xmlns:");
                if (index == 0) {
                    var prefix = key.substring(6);
                    var uri = attribs[key];
                    namespaces[prefix] = uri;
                    delete attribs[key];
                }
                else {
                    node["$" + key] = attribs[key];
                }
            }
            node.text = "";
            node.toString = toString;
            var name = node.name;
            var index = name.indexOf(":");
            if (index == -1) {
                node.namespace = "";
                node.prefix = "";
                node.localName = name;
            }
            else {
                var prefix = name.substring(0, index);
                node.prefix = prefix;
                node.namespace = namespaces[prefix];
                node.localName = name.substring(index + 1);
            }
            if (object) {
                var children = object.children;
                if (!children) {
                    children = object.children = [];
                    if (object.text) {
                        object.text = "";
                    }
                }
                children.push(node);
                node.parent = object;
                object = node;
            }
            else {
                object = node;
            }
        };
        saxparser.onclosetag = function (node) {
            if (object.parent)
                object = object.parent;
        };
        saxparser.oncdata = function (cdata) {
            if (object && !object.children)
                object.text = cdata;
        };
        saxparser.ontext = function (text) {
            if (object && !object.text && !object.children)
                object.text = text;
        };
        saxparser.write(xmlString).close();
        if (hasError) {
            return null;
        }
        else {
            return object;
        }
    }
    ;
    function toString() {
        return this.text;
    }
    ;
    var XML = /** @class */ (function () {
        function XML() {
        }
        XML.parse = function (str) {
            return g_parse(str);
        };
        return XML;
    }());
    egret.XML = XML;
})(egret || (egret = {}));
//////////////////////////////////////////////////////////////////////////////////////
//
//  Copyright (c) 2014-present, Egret Technology.
//  All rights reserved.
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the Egret nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY EGRET AND CONTRIBUTORS "AS IS" AND ANY EXPRESS
//  OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
//  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
//  IN NO EVENT SHALL EGRET AND CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
//  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
//  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;LOSS OF USE, DATA,
//  OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//////////////////////////////////////////////////////////////////////////////////////
var egret;
(function (egret) {
    /**
     * The Endian class contains values that denote the byte order used to represent multibyte numbers.
     * The byte order is either bigEndian (most significant byte first) or littleEndian (least significant byte first).
     * @version Egret 2.4
     * @platform Web,Native
     * @language en_US
     */
    /**
     * Endian 类中包含一些值，它们表示用于表示多字节数字的字节顺序。
     * 字节顺序为 bigEndian（最高有效字节位于最前）或 littleEndian（最低有效字节位于最前）。
     * @version Egret 2.4
     * @platform Web,Native
     * @language zh_CN
     */
    var Endian = /** @class */ (function () {
        function Endian() {
        }
        /**
         * Indicates the least significant byte of the multibyte number appears first in the sequence of bytes.
         * The hexadecimal number 0x12345678 has 4 bytes (2 hexadecimal digits per byte). The most significant byte is 0x12. The least significant byte is 0x78. (For the equivalent decimal number, 305419896, the most significant digit is 3, and the least significant digit is 6).
         * @version Egret 2.4
         * @platform Web,Native
         * @language en_US
         */
        /**
         * 表示多字节数字的最低有效字节位于字节序列的最前面。
         * 十六进制数字 0x12345678 包含 4 个字节（每个字节包含 2 个十六进制数字）。最高有效字节为 0x12。最低有效字节为 0x78。（对于等效的十进制数字 305419896，最高有效数字是 3，最低有效数字是 6）。
         * @version Egret 2.4
         * @platform Web,Native
         * @language zh_CN
         */
        Endian.LITTLE_ENDIAN = "littleEndian";
        /**
         * Indicates the most significant byte of the multibyte number appears first in the sequence of bytes.
         * The hexadecimal number 0x12345678 has 4 bytes (2 hexadecimal digits per byte).  The most significant byte is 0x12. The least significant byte is 0x78. (For the equivalent decimal number, 305419896, the most significant digit is 3, and the least significant digit is 6).
         * @version Egret 2.4
         * @platform Web,Native
         * @language en_US
         */
        /**
         * 表示多字节数字的最高有效字节位于字节序列的最前面。
         * 十六进制数字 0x12345678 包含 4 个字节（每个字节包含 2 个十六进制数字）。最高有效字节为 0x12。最低有效字节为 0x78。（对于等效的十进制数字 305419896，最高有效数字是 3，最低有效数字是 6）。
         * @version Egret 2.4
         * @platform Web,Native
         * @language zh_CN
         */
        Endian.BIG_ENDIAN = "bigEndian";
        return Endian;
    }());
    egret.Endian = Endian;
    /**
     * The ByteArray class provides methods and attributes for optimized reading and writing as well as dealing with binary data.
     * Note: The ByteArray class is applied to the advanced developers who need to access data at the byte layer.
     * @version Egret 2.4
     * @platform Web,Native
     * @includeExample egret/utils/ByteArray.ts
     * @language en_US
     */
    /**
     * ByteArray 类提供用于优化读取、写入以及处理二进制数据的方法和属性。
     * 注意：ByteArray 类适用于需要在字节层访问数据的高级开发人员。
     * @version Egret 2.4
     * @platform Web,Native
     * @includeExample egret/utils/ByteArray.ts
     * @language zh_CN
     */
    var ByteArray = /** @class */ (function () {
        /**
         * @version Egret 2.4
         * @platform Web,Native
         */
        function ByteArray(buffer, bufferExtSize) {
            if (bufferExtSize === void 0) { bufferExtSize = 0; }
            /**
             * @private
             */
            this.bufferExtSize = 0; //Buffer expansion size
            /**
             * @private
             */
            this.EOF_byte = -1;
            /**
             * @private
             */
            this.EOF_code_point = -1;
            if (bufferExtSize < 0) {
                bufferExtSize = 0;
            }
            this.bufferExtSize = bufferExtSize;
            var bytes, wpos = 0;
            if (buffer) { //有数据，则可写字节数从字节尾开始
                var uint8 = void 0;
                if (buffer instanceof Uint8Array) {
                    uint8 = buffer;
                    wpos = buffer.length;
                }
                else {
                    wpos = buffer.byteLength;
                    uint8 = new Uint8Array(buffer);
                }
                if (bufferExtSize == 0) {
                    bytes = new Uint8Array(wpos);
                }
                else {
                    var multi = (wpos / bufferExtSize | 0) + 1;
                    bytes = new Uint8Array(multi * bufferExtSize);
                }
                bytes.set(uint8);
            }
            else {
                bytes = new Uint8Array(bufferExtSize);
            }
            this.write_position = wpos;
            this._position = 0;
            this._bytes = bytes;
            this.data = new DataView(bytes.buffer);
            this.endian = Endian.BIG_ENDIAN;
        }
        Object.defineProperty(ByteArray.prototype, "endian", {
            /**
             * Changes or reads the byte order; egret.EndianConst.BIG_ENDIAN or egret.EndianConst.LITTLE_EndianConst.
             * @default egret.EndianConst.BIG_ENDIAN
             * @version Egret 2.4
             * @platform Web,Native
             * @language en_US
             */
            /**
             * 更改或读取数据的字节顺序；egret.EndianConst.BIG_ENDIAN 或 egret.EndianConst.LITTLE_ENDIAN。
             * @default egret.EndianConst.BIG_ENDIAN
             * @version Egret 2.4
             * @platform Web,Native
             * @language zh_CN
             */
            get: function () {
                return this.$endian == 0 /* LITTLE_ENDIAN */ ? Endian.LITTLE_ENDIAN : Endian.BIG_ENDIAN;
            },
            set: function (value) {
                this.$endian = value == Endian.LITTLE_ENDIAN ? 0 /* LITTLE_ENDIAN */ : 1 /* BIG_ENDIAN */;
            },
            enumerable: true,
            configurable: true
        });
        /**
         * @deprecated
         * @version Egret 2.4
         * @platform Web,Native
         */
        ByteArray.prototype.setArrayBuffer = function (buffer) {
        };
        Object.defineProperty(ByteArray.prototype, "readAvailable", {
            /**
             * 可读的剩余字节数
             *
             * @returns
             *
             * @memberOf ByteArray
             */
            get: function () {
                return this.write_position - this._position;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ByteArray.prototype, "buffer", {
            get: function () {
                return this.data.buffer.slice(0, this.write_position);
            },
            /**
             * @private
             */
            set: function (value) {
                var wpos = value.byteLength;
                var uint8 = new Uint8Array(value);
                var bufferExtSize = this.bufferExtSize;
                var bytes;
                if (bufferExtSize == 0) {
                    bytes = new Uint8Array(wpos);
                }
                else {
                    var multi = (wpos / bufferExtSize | 0) + 1;
                    bytes = new Uint8Array(multi * bufferExtSize);
                }
                bytes.set(uint8);
                this.write_position = wpos;
                this._bytes = bytes;
                this.data = new DataView(bytes.buffer);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ByteArray.prototype, "rawBuffer", {
            get: function () {
                return this.data.buffer;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ByteArray.prototype, "bytes", {
            get: function () {
                return this._bytes;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ByteArray.prototype, "dataView", {
            /**
             * @private
             * @version Egret 2.4
             * @platform Web,Native
             */
            get: function () {
                return this.data;
            },
            /**
             * @private
             */
            set: function (value) {
                this.buffer = value.buffer;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ByteArray.prototype, "bufferOffset", {
            /**
             * @private
             */
            get: function () {
                return this.data.byteOffset;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ByteArray.prototype, "position", {
            /**
             * The current position of the file pointer (in bytes) to move or return to the ByteArray object. The next time you start reading reading method call in this position, or will start writing in this position next time call a write method.
             * @version Egret 2.4
             * @platform Web,Native
             * @language en_US
             */
            /**
             * 将文件指针的当前位置（以字节为单位）移动或返回到 ByteArray 对象中。下一次调用读取方法时将在此位置开始读取，或者下一次调用写入方法时将在此位置开始写入。
             * @version Egret 2.4
             * @platform Web,Native
             * @language zh_CN
             */
            get: function () {
                return this._position;
            },
            set: function (value) {
                this._position = value;
                if (value > this.write_position) {
                    this.write_position = value;
                }
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ByteArray.prototype, "length", {
            /**
             * The length of the ByteArray object (in bytes).
                      * If the length is set to be larger than the current length, the right-side zero padding byte array.
                      * If the length is set smaller than the current length, the byte array is truncated.
             * @version Egret 2.4
             * @platform Web,Native
             * @language en_US
             */
            /**
             * ByteArray 对象的长度（以字节为单位）。
             * 如果将长度设置为大于当前长度的值，则用零填充字节数组的右侧。
             * 如果将长度设置为小于当前长度的值，将会截断该字节数组。
             * @version Egret 2.4
             * @platform Web,Native
             * @language zh_CN
             */
            get: function () {
                return this.write_position;
            },
            set: function (value) {
                this.write_position = value;
                if (this.data.byteLength > value) {
                    this._position = value;
                }
                this._validateBuffer(value);
            },
            enumerable: true,
            configurable: true
        });
        ByteArray.prototype._validateBuffer = function (value) {
            if (this.data.byteLength < value) {
                var be = this.bufferExtSize;
                var tmp = void 0;
                if (be == 0) {
                    tmp = new Uint8Array(value);
                }
                else {
                    var nLen = ((value / be >> 0) + 1) * be;
                    tmp = new Uint8Array(nLen);
                }
                tmp.set(this._bytes);
                this._bytes = tmp;
                this.data = new DataView(tmp.buffer);
            }
        };
        Object.defineProperty(ByteArray.prototype, "bytesAvailable", {
            /**
             * The number of bytes that can be read from the current position of the byte array to the end of the array data.
             * When you access a ByteArray object, the bytesAvailable property in conjunction with the read methods each use to make sure you are reading valid data.
             * @version Egret 2.4
             * @platform Web,Native
             * @language en_US
             */
            /**
             * 可从字节数组的当前位置到数组末尾读取的数据的字节数。
             * 每次访问 ByteArray 对象时，将 bytesAvailable 属性与读取方法结合使用，以确保读取有效的数据。
             * @version Egret 2.4
             * @platform Web,Native
             * @language zh_CN
             */
            get: function () {
                return this.data.byteLength - this._position;
            },
            enumerable: true,
            configurable: true
        });
        /**
         * Clears the contents of the byte array and resets the length and position properties to 0.
         * @version Egret 2.4
         * @platform Web,Native
         * @language en_US
         */
        /**
         * 清除字节数组的内容，并将 length 和 position 属性重置为 0。
         * @version Egret 2.4
         * @platform Web,Native
         * @language zh_CN
         */
        ByteArray.prototype.clear = function () {
            var buffer = new ArrayBuffer(this.bufferExtSize);
            this.data = new DataView(buffer);
            this._bytes = new Uint8Array(buffer);
            this._position = 0;
            this.write_position = 0;
        };
        /**
         * Read a Boolean value from the byte stream. Read a simple byte. If the byte is non-zero, it returns true; otherwise, it returns false.
         * @return If the byte is non-zero, it returns true; otherwise, it returns false.
         * @version Egret 2.4
         * @platform Web,Native
         * @language en_US
         */
        /**
         * 从字节流中读取布尔值。读取单个字节，如果字节非零，则返回 true，否则返回 false
         * @return 如果字节不为零，则返回 true，否则返回 false
         * @version Egret 2.4
         * @platform Web,Native
         * @language zh_CN
         */
        ByteArray.prototype.readBoolean = function () {
            if (this.validate(1 /* SIZE_OF_BOOLEAN */))
                return !!this._bytes[this.position++];
        };
        /**
         * Read signed bytes from the byte stream.
         * @return An integer ranging from -128 to 127
         * @version Egret 2.4
         * @platform Web,Native
         * @language en_US
         */
        /**
         * 从字节流中读取带符号的字节
         * @return 介于 -128 和 127 之间的整数
         * @version Egret 2.4
         * @platform Web,Native
         * @language zh_CN
         */
        ByteArray.prototype.readByte = function () {
            if (this.validate(1 /* SIZE_OF_INT8 */))
                return this.data.getInt8(this.position++);
        };
        /**
         * Read data byte number specified by the length parameter from the byte stream. Starting from the position specified by offset, read bytes into the ByteArray object specified by the bytes parameter, and write bytes into the target ByteArray
         * @param bytes ByteArray object that data is read into
         * @param offset Offset (position) in bytes. Read data should be written from this position
         * @param length Byte number to be read Default value 0 indicates reading all available data
         * @version Egret 2.4
         * @platform Web,Native
         * @language en_US
         */
        /**
         * 从字节流中读取 length 参数指定的数据字节数。从 offset 指定的位置开始，将字节读入 bytes 参数指定的 ByteArray 对象中，并将字节写入目标 ByteArray 中
         * @param bytes 要将数据读入的 ByteArray 对象
         * @param offset bytes 中的偏移（位置），应从该位置写入读取的数据
         * @param length 要读取的字节数。默认值 0 导致读取所有可用的数据
         * @version Egret 2.4
         * @platform Web,Native
         * @language zh_CN
         */
        ByteArray.prototype.readBytes = function (bytes, offset, length) {
            if (offset === void 0) { offset = 0; }
            if (length === void 0) { length = 0; }
            if (!bytes) { //由于bytes不返回，所以new新的无意义
                return;
            }
            var pos = this._position;
            var available = this.write_position - pos;
            if (available < 0) {
                console.log(1025);
                return;
            }
            if (length == 0) {
                length = available;
            }
            else if (length > available) {
                console.log(1025);
                return;
            }
            var position = bytes._position;
            bytes._position = 0;
            bytes.validateBuffer(offset + length);
            bytes._position = position;
            bytes._bytes.set(this._bytes.subarray(pos, pos + length), offset);
            this.position += length;
        };
        /**
         * Read an IEEE 754 double-precision (64 bit) floating point number from the byte stream
         * @return Double-precision (64 bit) floating point number
         * @version Egret 2.4
         * @platform Web,Native
         * @language en_US
         */
        /**
         * 从字节流中读取一个 IEEE 754 双精度（64 位）浮点数
         * @return 双精度（64 位）浮点数
         * @version Egret 2.4
         * @platform Web,Native
         * @language zh_CN
         */
        ByteArray.prototype.readDouble = function () {
            if (this.validate(8 /* SIZE_OF_FLOAT64 */)) {
                var value = this.data.getFloat64(this._position, this.$endian == 0 /* LITTLE_ENDIAN */);
                this.position += 8 /* SIZE_OF_FLOAT64 */;
                return value;
            }
        };
        /**
         * Read an IEEE 754 single-precision (32 bit) floating point number from the byte stream
         * @return Single-precision (32 bit) floating point number
         * @version Egret 2.4
         * @platform Web,Native
         * @language en_US
         */
        /**
         * 从字节流中读取一个 IEEE 754 单精度（32 位）浮点数
         * @return 单精度（32 位）浮点数
         * @version Egret 2.4
         * @platform Web,Native
         * @language zh_CN
         */
        ByteArray.prototype.readFloat = function () {
            if (this.validate(4 /* SIZE_OF_FLOAT32 */)) {
                var value = this.data.getFloat32(this._position, this.$endian == 0 /* LITTLE_ENDIAN */);
                this.position += 4 /* SIZE_OF_FLOAT32 */;
                return value;
            }
        };
        /**
         * Read a 32-bit signed integer from the byte stream.
         * @return A 32-bit signed integer ranging from -2147483648 to 2147483647
         * @version Egret 2.4
         * @platform Web,Native
         * @language en_US
         */
        /**
         * 从字节流中读取一个带符号的 32 位整数
         * @return 介于 -2147483648 和 2147483647 之间的 32 位带符号整数
         * @version Egret 2.4
         * @platform Web,Native
         * @language zh_CN
         */
        ByteArray.prototype.readInt = function () {
            if (this.validate(4 /* SIZE_OF_INT32 */)) {
                var value = this.data.getInt32(this._position, this.$endian == 0 /* LITTLE_ENDIAN */);
                this.position += 4 /* SIZE_OF_INT32 */;
                return value;
            }
        };
        /**
         * Read a 16-bit signed integer from the byte stream.
         * @return A 16-bit signed integer ranging from -32768 to 32767
         * @version Egret 2.4
         * @platform Web,Native
         * @language en_US
         */
        /**
         * 从字节流中读取一个带符号的 16 位整数
         * @return 介于 -32768 和 32767 之间的 16 位带符号整数
         * @version Egret 2.4
         * @platform Web,Native
         * @language zh_CN
         */
        ByteArray.prototype.readShort = function () {
            if (this.validate(2 /* SIZE_OF_INT16 */)) {
                var value = this.data.getInt16(this._position, this.$endian == 0 /* LITTLE_ENDIAN */);
                this.position += 2 /* SIZE_OF_INT16 */;
                return value;
            }
        };
        /**
         * Read unsigned bytes from the byte stream.
         * @return A 32-bit unsigned integer ranging from 0 to 255
         * @version Egret 2.4
         * @platform Web,Native
         * @language en_US
         */
        /**
         * 从字节流中读取无符号的字节
         * @return 介于 0 和 255 之间的 32 位无符号整数
         * @version Egret 2.4
         * @platform Web,Native
         * @language zh_CN
         */
        ByteArray.prototype.readUnsignedByte = function () {
            if (this.validate(1 /* SIZE_OF_UINT8 */))
                return this._bytes[this.position++];
        };
        /**
         * Read a 32-bit unsigned integer from the byte stream.
         * @return A 32-bit unsigned integer ranging from 0 to 4294967295
         * @version Egret 2.4
         * @platform Web,Native
         * @language en_US
         */
        /**
         * 从字节流中读取一个无符号的 32 位整数
         * @return 介于 0 和 4294967295 之间的 32 位无符号整数
         * @version Egret 2.4
         * @platform Web,Native
         * @language zh_CN
         */
        ByteArray.prototype.readUnsignedInt = function () {
            if (this.validate(4 /* SIZE_OF_UINT32 */)) {
                var value = this.data.getUint32(this._position, this.$endian == 0 /* LITTLE_ENDIAN */);
                this.position += 4 /* SIZE_OF_UINT32 */;
                return value;
            }
        };
        /**
         * Read a 16-bit unsigned integer from the byte stream.
         * @return A 16-bit unsigned integer ranging from 0 to 65535
         * @version Egret 2.4
         * @platform Web,Native
         * @language en_US
         */
        /**
         * 从字节流中读取一个无符号的 16 位整数
         * @return 介于 0 和 65535 之间的 16 位无符号整数
         * @version Egret 2.4
         * @platform Web,Native
         * @language zh_CN
         */
        ByteArray.prototype.readUnsignedShort = function () {
            if (this.validate(2 /* SIZE_OF_UINT16 */)) {
                var value = this.data.getUint16(this._position, this.$endian == 0 /* LITTLE_ENDIAN */);
                this.position += 2 /* SIZE_OF_UINT16 */;
                return value;
            }
        };
        /**
         * Read a UTF-8 character string from the byte stream Assume that the prefix of the character string is a short unsigned integer (use byte to express length)
         * @return UTF-8 character string
         * @version Egret 2.4
         * @platform Web,Native
         * @language en_US
         */
        /**
         * 从字节流中读取一个 UTF-8 字符串。假定字符串的前缀是无符号的短整型（以字节表示长度）
         * @return UTF-8 编码的字符串
         * @version Egret 2.4
         * @platform Web,Native
         * @language zh_CN
         */
        ByteArray.prototype.readUTF = function () {
            var length = this.readUnsignedShort();
            if (length > 0) {
                return this.readUTFBytes(length);
            }
            else {
                return "";
            }
        };
        /**
         * Read a UTF-8 byte sequence specified by the length parameter from the byte stream, and then return a character string
         * @param Specify a short unsigned integer of the UTF-8 byte length
         * @return A character string consists of UTF-8 bytes of the specified length
         * @version Egret 2.4
         * @platform Web,Native
         * @language en_US
         */
        /**
         * 从字节流中读取一个由 length 参数指定的 UTF-8 字节序列，并返回一个字符串
         * @param length 指明 UTF-8 字节长度的无符号短整型数
         * @return 由指定长度的 UTF-8 字节组成的字符串
         * @version Egret 2.4
         * @platform Web,Native
         * @language zh_CN
         */
        ByteArray.prototype.readUTFBytes = function (length) {
            if (!this.validate(length)) {
                return;
            }
            var data = this.data;
            var bytes = new Uint8Array(data.buffer, data.byteOffset + this._position, length);
            this.position += length;
            return this.decodeUTF8(bytes);
        };
        /**
         * Write a Boolean value. A single byte is written according to the value parameter. If the value is true, write 1; if the value is false, write 0.
         * @param value A Boolean value determining which byte is written. If the value is true, write 1; if the value is false, write 0.
         * @version Egret 2.4
         * @platform Web,Native
         * @language en_US
         */
        /**
         * 写入布尔值。根据 value 参数写入单个字节。如果为 true，则写入 1，如果为 false，则写入 0
         * @param value 确定写入哪个字节的布尔值。如果该参数为 true，则该方法写入 1；如果该参数为 false，则该方法写入 0
         * @version Egret 2.4
         * @platform Web,Native
         * @language zh_CN
         */
        ByteArray.prototype.writeBoolean = function (value) {
            this.validateBuffer(1 /* SIZE_OF_BOOLEAN */);
            this._bytes[this.position++] = +value;
        };
        /**
         * Write a byte into the byte stream
         * The low 8 bits of the parameter are used. The high 24 bits are ignored.
         * @param value A 32-bit integer. The low 8 bits will be written into the byte stream
         * @version Egret 2.4
         * @platform Web,Native
         * @language en_US
         */
        /**
         * 在字节流中写入一个字节
         * 使用参数的低 8 位。忽略高 24 位
         * @param value 一个 32 位整数。低 8 位将被写入字节流
         * @version Egret 2.4
         * @platform Web,Native
         * @language zh_CN
         */
        ByteArray.prototype.writeByte = function (value) {
            this.validateBuffer(1 /* SIZE_OF_INT8 */);
            this._bytes[this.position++] = value & 0xff;
        };
        /**
         * Write the byte sequence that includes length bytes in the specified byte array, bytes, (starting at the byte specified by offset, using a zero-based index), into the byte stream
         * If the length parameter is omitted, the default length value 0 is used and the entire buffer starting at offset is written. If the offset parameter is also omitted, the entire buffer is written
         * If the offset or length parameter is out of range, they are clamped to the beginning and end of the bytes array.
         * @param bytes ByteArray Object
         * @param offset A zero-based index specifying the position into the array to begin writing
         * @param length An unsigned integer specifying how far into the buffer to write
         * @version Egret 2.4
         * @platform Web,Native
         * @language en_US
         */
        /**
         * 将指定字节数组 bytes（起始偏移量为 offset，从零开始的索引）中包含 length 个字节的字节序列写入字节流
         * 如果省略 length 参数，则使用默认长度 0；该方法将从 offset 开始写入整个缓冲区。如果还省略了 offset 参数，则写入整个缓冲区
         * 如果 offset 或 length 超出范围，它们将被锁定到 bytes 数组的开头和结尾
         * @param bytes ByteArray 对象
         * @param offset 从 0 开始的索引，表示在数组中开始写入的位置
         * @param length 一个无符号整数，表示在缓冲区中的写入范围
         * @version Egret 2.4
         * @platform Web,Native
         * @language zh_CN
         */
        ByteArray.prototype.writeBytes = function (bytes, offset, length) {
            if (offset === void 0) { offset = 0; }
            if (length === void 0) { length = 0; }
            var writeLength;
            if (offset < 0) {
                return;
            }
            if (length < 0) {
                return;
            }
            else if (length == 0) {
                writeLength = bytes.length - offset;
            }
            else {
                writeLength = Math.min(bytes.length - offset, length);
            }
            if (writeLength > 0) {
                this.validateBuffer(writeLength);
                this._bytes.set(bytes._bytes.subarray(offset, offset + writeLength), this._position);
                this.position = this._position + writeLength;
            }
        };
        /**
         * Write an IEEE 754 double-precision (64 bit) floating point number into the byte stream
         * @param value Double-precision (64 bit) floating point number
         * @version Egret 2.4
         * @platform Web,Native
         * @language en_US
         */
        /**
         * 在字节流中写入一个 IEEE 754 双精度（64 位）浮点数
         * @param value 双精度（64 位）浮点数
         * @version Egret 2.4
         * @platform Web,Native
         * @language zh_CN
         */
        ByteArray.prototype.writeDouble = function (value) {
            this.validateBuffer(8 /* SIZE_OF_FLOAT64 */);
            this.data.setFloat64(this._position, value, this.$endian == 0 /* LITTLE_ENDIAN */);
            this.position += 8 /* SIZE_OF_FLOAT64 */;
        };
        /**
         * Write an IEEE 754 single-precision (32 bit) floating point number into the byte stream
         * @param value Single-precision (32 bit) floating point number
         * @version Egret 2.4
         * @platform Web,Native
         * @language en_US
         */
        /**
         * 在字节流中写入一个 IEEE 754 单精度（32 位）浮点数
         * @param value 单精度（32 位）浮点数
         * @version Egret 2.4
         * @platform Web,Native
         * @language zh_CN
         */
        ByteArray.prototype.writeFloat = function (value) {
            this.validateBuffer(4 /* SIZE_OF_FLOAT32 */);
            this.data.setFloat32(this._position, value, this.$endian == 0 /* LITTLE_ENDIAN */);
            this.position += 4 /* SIZE_OF_FLOAT32 */;
        };
        /**
         * Write a 32-bit signed integer into the byte stream
         * @param value An integer to be written into the byte stream
         * @version Egret 2.4
         * @platform Web,Native
         * @language en_US
         */
        /**
         * 在字节流中写入一个带符号的 32 位整数
         * @param value 要写入字节流的整数
         * @version Egret 2.4
         * @platform Web,Native
         * @language zh_CN
         */
        ByteArray.prototype.writeInt = function (value) {
            this.validateBuffer(4 /* SIZE_OF_INT32 */);
            this.data.setInt32(this._position, value, this.$endian == 0 /* LITTLE_ENDIAN */);
            this.position += 4 /* SIZE_OF_INT32 */;
        };
        /**
         * Write a 16-bit integer into the byte stream. The low 16 bits of the parameter are used. The high 16 bits are ignored.
         * @param value A 32-bit integer. Its low 16 bits will be written into the byte stream
         * @version Egret 2.4
         * @platform Web,Native
         * @language en_US
         */
        /**
         * 在字节流中写入一个 16 位整数。使用参数的低 16 位。忽略高 16 位
         * @param value 32 位整数，该整数的低 16 位将被写入字节流
         * @version Egret 2.4
         * @platform Web,Native
         * @language zh_CN
         */
        ByteArray.prototype.writeShort = function (value) {
            this.validateBuffer(2 /* SIZE_OF_INT16 */);
            this.data.setInt16(this._position, value, this.$endian == 0 /* LITTLE_ENDIAN */);
            this.position += 2 /* SIZE_OF_INT16 */;
        };
        /**
         * Write a 32-bit unsigned integer into the byte stream
         * @param value An unsigned integer to be written into the byte stream
         * @version Egret 2.4
         * @platform Web,Native
         * @language en_US
         */
        /**
         * 在字节流中写入一个无符号的 32 位整数
         * @param value 要写入字节流的无符号整数
         * @version Egret 2.4
         * @platform Web,Native
         * @language zh_CN
         */
        ByteArray.prototype.writeUnsignedInt = function (value) {
            this.validateBuffer(4 /* SIZE_OF_UINT32 */);
            this.data.setUint32(this._position, value, this.$endian == 0 /* LITTLE_ENDIAN */);
            this.position += 4 /* SIZE_OF_UINT32 */;
        };
        /**
         * Write a 16-bit unsigned integer into the byte stream
         * @param value An unsigned integer to be written into the byte stream
         * @version Egret 2.5
         * @platform Web,Native
         * @language en_US
         */
        /**
         * 在字节流中写入一个无符号的 16 位整数
         * @param value 要写入字节流的无符号整数
         * @version Egret 2.5
         * @platform Web,Native
         * @language zh_CN
         */
        ByteArray.prototype.writeUnsignedShort = function (value) {
            this.validateBuffer(2 /* SIZE_OF_UINT16 */);
            this.data.setUint16(this._position, value, this.$endian == 0 /* LITTLE_ENDIAN */);
            this.position += 2 /* SIZE_OF_UINT16 */;
        };
        /**
         * Write a UTF-8 string into the byte stream. The length of the UTF-8 string in bytes is written first, as a 16-bit integer, followed by the bytes representing the characters of the string
         * @param value Character string value to be written
         * @version Egret 2.4
         * @platform Web,Native
         * @language en_US
         */
        /**
         * 将 UTF-8 字符串写入字节流。先写入以字节表示的 UTF-8 字符串长度（作为 16 位整数），然后写入表示字符串字符的字节
         * @param value 要写入的字符串值
         * @version Egret 2.4
         * @platform Web,Native
         * @language zh_CN
         */
        ByteArray.prototype.writeUTF = function (value) {
            var utf8bytes = this.encodeUTF8(value);
            var length = utf8bytes.length;
            this.validateBuffer(2 /* SIZE_OF_UINT16 */ + length);
            this.data.setUint16(this._position, length, this.$endian == 0 /* LITTLE_ENDIAN */);
            this.position += 2 /* SIZE_OF_UINT16 */;
            this._writeUint8Array(utf8bytes, false);
        };
        /**
         * Write a UTF-8 string into the byte stream. Similar to the writeUTF() method, but the writeUTFBytes() method does not prefix the string with a 16-bit length word
         * @param value Character string value to be written
         * @version Egret 2.4
         * @platform Web,Native
         * @language en_US
         */
        /**
         * 将 UTF-8 字符串写入字节流。类似于 writeUTF() 方法，但 writeUTFBytes() 不使用 16 位长度的词为字符串添加前缀
         * @param value 要写入的字符串值
         * @version Egret 2.4
         * @platform Web,Native
         * @language zh_CN
         */
        ByteArray.prototype.writeUTFBytes = function (value) {
            this._writeUint8Array(this.encodeUTF8(value));
        };
        /**
         *
         * @returns
         * @version Egret 2.4
         * @platform Web,Native
         */
        ByteArray.prototype.toString = function () {
            return "[ByteArray] length:" + this.length + ", bytesAvailable:" + this.bytesAvailable;
        };
        /**
         * @private
         * 将 Uint8Array 写入字节流
         * @param bytes 要写入的Uint8Array
         * @param validateBuffer
         */
        ByteArray.prototype._writeUint8Array = function (bytes, validateBuffer) {
            if (validateBuffer === void 0) { validateBuffer = true; }
            var pos = this._position;
            var npos = pos + bytes.length;
            if (validateBuffer) {
                this.validateBuffer(npos);
            }
            this.bytes.set(bytes, pos);
            this.position = npos;
        };
        /**
         * @param len
         * @returns
         * @version Egret 2.4
         * @platform Web,Native
         * @private
         */
        ByteArray.prototype.validate = function (len) {
            var bl = this._bytes.length;
            if (bl > 0 && this._position + len <= bl) {
                return true;
            }
            else {
                console.log(1025);
            }
        };
        /**********************/
        /*  PRIVATE METHODS   */
        /**********************/
        /**
         * @private
         * @param len
         * @param needReplace
         */
        ByteArray.prototype.validateBuffer = function (len) {
            this.write_position = len > this.write_position ? len : this.write_position;
            len += this._position;
            this._validateBuffer(len);
        };
        /**
         * @private
         * UTF-8 Encoding/Decoding
         */
        ByteArray.prototype.encodeUTF8 = function (str) {
            var pos = 0;
            var codePoints = this.stringToCodePoints(str);
            var outputBytes = [];
            while (codePoints.length > pos) {
                var code_point = codePoints[pos++];
                if (this.inRange(code_point, 0xD800, 0xDFFF)) {
                    this.encoderError(code_point);
                }
                else if (this.inRange(code_point, 0x0000, 0x007f)) {
                    outputBytes.push(code_point);
                }
                else {
                    var count = void 0, offset = void 0;
                    if (this.inRange(code_point, 0x0080, 0x07FF)) {
                        count = 1;
                        offset = 0xC0;
                    }
                    else if (this.inRange(code_point, 0x0800, 0xFFFF)) {
                        count = 2;
                        offset = 0xE0;
                    }
                    else if (this.inRange(code_point, 0x10000, 0x10FFFF)) {
                        count = 3;
                        offset = 0xF0;
                    }
                    outputBytes.push(this.div(code_point, Math.pow(64, count)) + offset);
                    while (count > 0) {
                        var temp = this.div(code_point, Math.pow(64, count - 1));
                        outputBytes.push(0x80 + (temp % 64));
                        count -= 1;
                    }
                }
            }
            return new Uint8Array(outputBytes);
        };
        /**
         * @private
         *
         * @param data
         * @returns
         */
        ByteArray.prototype.decodeUTF8 = function (data) {
            var fatal = false;
            var pos = 0;
            var result = "";
            var code_point;
            var utf8_code_point = 0;
            var utf8_bytes_needed = 0;
            var utf8_bytes_seen = 0;
            var utf8_lower_boundary = 0;
            while (data.length > pos) {
                var _byte = data[pos++];
                if (_byte == this.EOF_byte) {
                    if (utf8_bytes_needed != 0) {
                        code_point = this.decoderError(fatal);
                    }
                    else {
                        code_point = this.EOF_code_point;
                    }
                }
                else {
                    if (utf8_bytes_needed == 0) {
                        if (this.inRange(_byte, 0x00, 0x7F)) {
                            code_point = _byte;
                        }
                        else {
                            if (this.inRange(_byte, 0xC2, 0xDF)) {
                                utf8_bytes_needed = 1;
                                utf8_lower_boundary = 0x80;
                                utf8_code_point = _byte - 0xC0;
                            }
                            else if (this.inRange(_byte, 0xE0, 0xEF)) {
                                utf8_bytes_needed = 2;
                                utf8_lower_boundary = 0x800;
                                utf8_code_point = _byte - 0xE0;
                            }
                            else if (this.inRange(_byte, 0xF0, 0xF4)) {
                                utf8_bytes_needed = 3;
                                utf8_lower_boundary = 0x10000;
                                utf8_code_point = _byte - 0xF0;
                            }
                            else {
                                this.decoderError(fatal);
                            }
                            utf8_code_point = utf8_code_point * Math.pow(64, utf8_bytes_needed);
                            code_point = null;
                        }
                    }
                    else if (!this.inRange(_byte, 0x80, 0xBF)) {
                        utf8_code_point = 0;
                        utf8_bytes_needed = 0;
                        utf8_bytes_seen = 0;
                        utf8_lower_boundary = 0;
                        pos--;
                        code_point = this.decoderError(fatal, _byte);
                    }
                    else {
                        utf8_bytes_seen += 1;
                        utf8_code_point = utf8_code_point + (_byte - 0x80) * Math.pow(64, utf8_bytes_needed - utf8_bytes_seen);
                        if (utf8_bytes_seen !== utf8_bytes_needed) {
                            code_point = null;
                        }
                        else {
                            var cp = utf8_code_point;
                            var lower_boundary = utf8_lower_boundary;
                            utf8_code_point = 0;
                            utf8_bytes_needed = 0;
                            utf8_bytes_seen = 0;
                            utf8_lower_boundary = 0;
                            if (this.inRange(cp, lower_boundary, 0x10FFFF) && !this.inRange(cp, 0xD800, 0xDFFF)) {
                                code_point = cp;
                            }
                            else {
                                code_point = this.decoderError(fatal, _byte);
                            }
                        }
                    }
                }
                //Decode string
                if (code_point !== null && code_point !== this.EOF_code_point) {
                    if (code_point <= 0xFFFF) {
                        if (code_point > 0)
                            result += String.fromCharCode(code_point);
                    }
                    else {
                        code_point -= 0x10000;
                        result += String.fromCharCode(0xD800 + ((code_point >> 10) & 0x3ff));
                        result += String.fromCharCode(0xDC00 + (code_point & 0x3ff));
                    }
                }
            }
            return result;
        };
        /**
         * @private
         *
         * @param code_point
         */
        ByteArray.prototype.encoderError = function (code_point) {
            console.log(1026, code_point);
        };
        /**
         * @private
         *
         * @param fatal
         * @param opt_code_point
         * @returns
         */
        ByteArray.prototype.decoderError = function (fatal, opt_code_point) {
            if (fatal) {
                console.log(1027);
            }
            return opt_code_point || 0xFFFD;
        };
        /**
         * @private
         *
         * @param a
         * @param min
         * @param max
         */
        ByteArray.prototype.inRange = function (a, min, max) {
            return min <= a && a <= max;
        };
        /**
         * @private
         *
         * @param n
         * @param d
         */
        ByteArray.prototype.div = function (n, d) {
            return Math.floor(n / d);
        };
        /**
         * @private
         *
         * @param string
         */
        ByteArray.prototype.stringToCodePoints = function (string) {
            /** @type {Array.<number>} */
            var cps = [];
            // Based on http://www.w3.org/TR/WebIDL/#idl-DOMString
            var i = 0, n = string.length;
            while (i < string.length) {
                var c = string.charCodeAt(i);
                if (!this.inRange(c, 0xD800, 0xDFFF)) {
                    cps.push(c);
                }
                else if (this.inRange(c, 0xDC00, 0xDFFF)) {
                    cps.push(0xFFFD);
                }
                else { // (inRange(c, 0xD800, 0xDBFF))
                    if (i == n - 1) {
                        cps.push(0xFFFD);
                    }
                    else {
                        var d = string.charCodeAt(i + 1);
                        if (this.inRange(d, 0xDC00, 0xDFFF)) {
                            var a = c & 0x3FF;
                            var b = d & 0x3FF;
                            i += 1;
                            cps.push(0x10000 + (a << 10) + b);
                        }
                        else {
                            cps.push(0xFFFD);
                        }
                    }
                }
                i += 1;
            }
            return cps;
        };
        return ByteArray;
    }());
    egret.ByteArray = ByteArray;
})(egret || (egret = {}));
var fairygui;
(function (fairygui) {
    var UIPackage = /** @class */ (function () {
        function UIPackage() {
            this._items = new Array();
            this._sprites = {};
        }
        UIPackage.addLoadedRes = function (key, asset) {
            UIPackage._loadedRes[key] = asset;
        };
        UIPackage.getRes = function (key) {
            return UIPackage._loadedRes[key];
        };
        UIPackage.getById = function (id) {
            return UIPackage._packageInstById[id];
        };
        UIPackage.getByName = function (name) {
            return UIPackage._packageInstByName[name];
        };
        UIPackage.addPackage = function (resKey, descData) {
            if (descData === void 0) { descData = null; }
            var pkg = new UIPackage();
            pkg.create(resKey, descData);
            UIPackage._packageInstById[pkg.id] = pkg;
            UIPackage._packageInstByName[pkg.name] = pkg;
            pkg.customId = resKey;
            return pkg;
        };
        UIPackage.removePackage = function (packageId) {
            var pkg = UIPackage._packageInstById[packageId];
            pkg.dispose();
            delete UIPackage._packageInstById[pkg.id];
            if (pkg._customId != null)
                delete UIPackage._packageInstById[pkg._customId];
            delete UIPackage._packageInstByName[pkg.name];
        };
        UIPackage.createObject = function (pkgName, resName, userClass) {
            if (userClass === void 0) { userClass = null; }
            var pkg = UIPackage.getByName(pkgName);
            if (pkg)
                return pkg.createObject(resName, userClass);
            else
                return null;
        };
        UIPackage.createObjectFromURL = function (url, userClass) {
            if (userClass === void 0) { userClass = null; }
            var pi = UIPackage.getItemByURL(url);
            if (pi)
                return pi.owner.internalCreateObject(pi, userClass);
            else
                return null;
        };
        UIPackage.getItemURL = function (pkgName, resName) {
            var pkg = UIPackage.getByName(pkgName);
            if (!pkg)
                return null;
            var pi = pkg._itemsByName[resName];
            if (!pi)
                return null;
            return "ui://" + pkg.id + pi.id;
        };
        UIPackage.getItemByURL = function (url) {
            var pos1 = url.indexOf("//");
            if (pos1 == -1)
                return null;
            var pos2 = url.indexOf("/", pos1 + 2);
            if (pos2 == -1) {
                if (url.length > 13) {
                    var pkgId = url.substr(5, 8);
                    var pkg = UIPackage.getById(pkgId);
                    if (pkg != null) {
                        var srcId = url.substr(13);
                        return pkg.getItemById(srcId);
                    }
                }
            }
            else {
                var pkgName = url.substr(pos1 + 2, pos2 - pos1 - 2);
                pkg = UIPackage.getByName(pkgName);
                if (pkg != null) {
                    var srcName = url.substr(pos2 + 1);
                    return pkg.getItemByName(srcName);
                }
            }
            return null;
        };
        UIPackage.normalizeURL = function (url) {
            if (url == null)
                return null;
            var pos1 = url.indexOf("//");
            if (pos1 == -1)
                return null;
            var pos2 = url.indexOf("/", pos1 + 2);
            if (pos2 == -1)
                return url;
            var pkgName = url.substr(pos1 + 2, pos2 - pos1 - 2);
            var srcName = url.substr(pos2 + 1);
            return UIPackage.getItemURL(pkgName, srcName);
        };
        UIPackage.getBitmapFontByURL = function (url) {
            return UIPackage._bitmapFonts[url];
        };
        UIPackage.setStringsSource = function (source) {
            UIPackage._stringsSource = {};
            var xml = egret.XML.parse(source);
            var nodes = xml.children;
            var length1 = nodes.length;
            for (var i1 = 0; i1 < length1; i1++) {
                var cxml = nodes[i1];
                if (cxml.name == "string") {
                    var key = cxml.attributes.name;
                    var text = cxml.children.length > 0 ? cxml.children[0].text : "";
                    var i = key.indexOf("-");
                    if (i == -1)
                        continue;
                    var key2 = key.substr(0, i);
                    var key3 = key.substr(i + 1);
                    var col = UIPackage._stringsSource[key2];
                    if (!col) {
                        col = {};
                        UIPackage._stringsSource[key2] = col;
                    }
                    col[key3] = text;
                }
            }
        };
        UIPackage.prototype.create = function (resKey, descData) {
            this._resKey = resKey;
            this.loadPackage(descData);
        };
        UIPackage.prototype.loadPackage = function (descData) {
            var str;
            var arr;
            var buf;
            if (descData)
                buf = descData;
            else {
                buf = UIPackage.getRes(this._resKey);
                if (!buf)
                    buf = UIPackage.getRes(this._resKey + "_fui");
                if (!buf)
                    throw "Resource '" + this._resKey + "' not found, please check default.res.json!";
            }
            this.decompressPackage(buf);
            str = this.getDesc("sprites.bytes");
            arr = str.split(UIPackage.sep1);
            var cnt = arr.length;
            for (var i = 1; i < cnt; i++) {
                str = arr[i];
                if (!str)
                    continue;
                var arr2 = str.split(UIPackage.sep2);
                var sprite = new AtlasSprite();
                var itemId = arr2[0];
                var binIndex = parseInt(arr2[1]);
                if (binIndex >= 0)
                    sprite.atlas = "atlas" + binIndex;
                else {
                    var pos = itemId.indexOf("_");
                    if (pos == -1)
                        sprite.atlas = "atlas_" + itemId;
                    else
                        sprite.atlas = "atlas_" + itemId.substr(0, pos);
                }
                sprite.rect.x = parseInt(arr2[2]);
                sprite.rect.y = parseInt(arr2[3]);
                sprite.rect.width = parseInt(arr2[4]);
                sprite.rect.height = parseInt(arr2[5]);
                sprite.rotated = arr2[6] == "1";
                this._sprites[itemId] = sprite;
            }
            str = this.getDesc("package.xml");
            var xml = egret.XML.parse(str);
            this._id = xml.attributes.id;
            this._name = xml.attributes.name;
            var resources = xml.children[0].children;
            this._itemsById = {};
            this._itemsByName = {};
            var pi;
            var cxml;
            var length1 = resources.length;
            for (var i1 = 0; i1 < length1; i1++) {
                cxml = resources[i1];
                pi = new fairygui.PackageItem();
                pi.owner = this;
                pi.type = fairygui.parsePackageItemType(cxml.name);
                pi.id = cxml.attributes.id;
                pi.name = cxml.attributes.name;
                pi.file = cxml.attributes.file;
                str = cxml.attributes.size;
                if (str) {
                    arr = str.split(UIPackage.sep0);
                    pi.width = parseInt(arr[0]);
                    pi.height = parseInt(arr[1]);
                }
                switch (pi.type) {
                    case fairygui.PackageItemType.Image: {
                        str = cxml.attributes.scale;
                        if (str == "9grid") {
                            pi.scale9Grid = new cc.Rect();
                            str = cxml.attributes.scale9grid;
                            if (str) {
                                arr = str.split(UIPackage.sep0);
                                pi.scale9Grid.x = parseInt(arr[0]);
                                pi.scale9Grid.y = parseInt(arr[1]);
                                pi.scale9Grid.width = parseInt(arr[2]);
                                pi.scale9Grid.height = parseInt(arr[3]);
                                str = cxml.attributes.gridTile;
                                if (str)
                                    pi.tileGridIndice = parseInt(str);
                            }
                        }
                        else if (str == "tile") {
                            pi.scaleByTile = true;
                        }
                        str = cxml.attributes.smoothing;
                        pi.smoothing = str != "false";
                        break;
                    }
                    case fairygui.PackageItemType.MovieClip:
                        str = cxml.attributes.smoothing;
                        pi.smoothing = str != "false";
                        break;
                    case fairygui.PackageItemType.Component:
                        fairygui.UIObjectFactory.$resolvePackageItemExtension(pi);
                        break;
                }
                this._items.push(pi);
                this._itemsById[pi.id] = pi;
                if (pi.name != null)
                    this._itemsByName[pi.name] = pi;
            }
            cnt = this._items.length;
            for (i = 0; i < cnt; i++) {
                pi = this._items[i];
                if (pi.type == fairygui.PackageItemType.Font) {
                    this.loadFont(pi);
                    UIPackage._bitmapFonts[pi.bitmapFont.id] = pi.bitmapFont;
                }
            }
        };
        UIPackage.prototype.decompressPackage = function (buf) {
            this._resData = {};
            var mark = new Uint8Array(buf.slice(0, 2));
            if (mark[0] == 0x50 && mark[1] == 0x4b) {
                this.decodeUncompressed(buf);
                return;
            }
            // var inflater: Zlib.RawInflate = new Zlib.RawInflate(buf);
            // var data: Uint8Array = inflater.decompress();
            // var tmp: egret.ByteArray = new egret.ByteArray();
            // var source: string = tmp["decodeUTF8"](data); //ByteArray.decodeUTF8 is private @_@
            // var curr: number = 0;
            // var fn: string;
            // var size: number;
            // while (true) {
            //     var pos: number = source.indexOf("|", curr);
            //     if (pos == -1)
            //         break;
            //     fn = source.substring(curr, pos);
            //     curr = pos + 1;
            //     pos = source.indexOf("|", curr);
            //     size = parseInt(source.substring(curr, pos));
            //     curr = pos + 1;
            //     this._resData[fn] = source.substr(curr, size);
            //     curr += size;
            // }
        };
        UIPackage.prototype.decodeUncompressed = function (buf) {
            var ba = new egret.ByteArray(buf);
            ba.endian = egret.Endian.LITTLE_ENDIAN;
            var pos = ba.length - 22;
            ba.position = pos + 10;
            var entryCount = ba.readUnsignedShort();
            ba.position = pos + 16;
            pos = ba.readInt();
            for (var i = 0; i < entryCount; i++) {
                ba.position = pos + 28;
                var len = ba.readUnsignedShort();
                var len2 = ba.readUnsignedShort() + ba.readUnsignedShort();
                ba.position = pos + 46;
                var entryName = ba.readUTFBytes(len);
                if (entryName[entryName.length - 1] != '/' && entryName[entryName.length - 1] != '\\') //not directory
                 {
                    ba.position = pos + 20;
                    var size = ba.readInt();
                    ba.position = pos + 42;
                    var offset = ba.readInt() + 30 + len;
                    if (size > 0) {
                        ba.position = offset;
                        this._resData[entryName] = ba.readUTFBytes(size);
                    }
                }
                pos += 46 + len + len2;
            }
        };
        UIPackage.prototype.dispose = function () {
            // var cnt: number = this._items.length;
            // for (var i: number = 0; i < cnt; i++) {
            //     var pi: PackageItem = this._items[i];
            //     var texture: egret.Texture = pi.texture;
            //     if (texture != null)
            //         texture.dispose();
            //     else if (pi.frames != null) {
            //         var frameCount: number = pi.frames.length;
            //         for (var j: number = 0; j < frameCount; j++) {
            //             texture = pi.frames[j].texture;
            //             if (texture != null)
            //                 texture.dispose();
            //         }
            //     }
            //     else if (pi.bitmapFont != null) {
            //         delete UIPackage._bitmapFonts[pi.bitmapFont.id];
            //     }
            // }
        };
        Object.defineProperty(UIPackage.prototype, "id", {
            get: function () {
                return this._id;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(UIPackage.prototype, "name", {
            get: function () {
                return this._name;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(UIPackage.prototype, "customId", {
            get: function () {
                return this._customId;
            },
            set: function (value) {
                if (this._customId != null)
                    delete UIPackage._packageInstById[this._customId];
                this._customId = value;
                if (this._customId != null)
                    UIPackage._packageInstById[this._customId] = this;
            },
            enumerable: true,
            configurable: true
        });
        UIPackage.prototype.createObject = function (resName, userClass) {
            if (userClass === void 0) { userClass = null; }
            var pi = this._itemsByName[resName];
            if (pi)
                return this.internalCreateObject(pi, userClass);
            else
                return null;
        };
        UIPackage.prototype.internalCreateObject = function (item, userClass) {
            if (userClass === void 0) { userClass = null; }
            var g;
            if (item.type == fairygui.PackageItemType.Component) {
                if (userClass != null)
                    g = new userClass();
                else
                    g = fairygui.UIObjectFactory.newObject(item);
            }
            else
                g = fairygui.UIObjectFactory.newObject(item);
            if (g == null)
                return null;
            UIPackage._constructing++;
            g.packageItem = item;
            g.constructFromResource();
            UIPackage._constructing--;
            return g;
        };
        UIPackage.prototype.getItemById = function (itemId) {
            return this._itemsById[itemId];
        };
        UIPackage.prototype.getItemByName = function (resName) {
            return this._itemsByName[resName];
        };
        UIPackage.prototype.getItemAssetByName = function (resName) {
            var pi = this._itemsByName[resName];
            if (pi == null) {
                throw "Resource not found -" + resName;
            }
            return this.getItemAsset(pi);
        };
        UIPackage.prototype.getItemAsset = function (item) {
            switch (item.type) {
                case fairygui.PackageItemType.Image:
                    if (!item.decoded) {
                        item.decoded = true;
                        var sprite = this._sprites[item.id];
                        if (sprite != null)
                            item.texture = this.createSpriteTexture(sprite);
                    }
                    return item.texture;
                case fairygui.PackageItemType.Atlas:
                    if (!item.decoded) {
                        item.decoded = true;
                        var fileName = (item.file != null && item.file.length > 0) ? item.file : (item.id + ".png");
                        item.texture = UIPackage.getRes(this._resKey + "@" + fairygui.ToolSet.getFileName(fileName));
                        if (!item.texture)
                            item.texture = UIPackage.getRes(this._resKey + "@" + fileName.replace("\.", "_"));
                    }
                    return item.texture;
                case fairygui.PackageItemType.Sound:
                    if (!item.decoded) {
                        item.decoded = true;
                        var fileName = (item.file != null && item.file.length > 0) ? item.file : (item.id + ".mp3");
                        item.sound = UIPackage.getRes(this._resKey + "@" + fairygui.ToolSet.getFileName(fileName));
                        if (!item.sound)
                            item.sound = UIPackage.getRes(this._resKey + "@" + fileName.replace("\.", "_"));
                    }
                    return item.sound;
                case fairygui.PackageItemType.Font:
                    if (!item.decoded) {
                        item.decoded = true;
                        this.loadFont(item);
                    }
                    return item.bitmapFont;
                case fairygui.PackageItemType.MovieClip:
                    if (!item.decoded) {
                        item.decoded = true;
                        this.loadMovieClip(item);
                    }
                    return item.frames;
                case fairygui.PackageItemType.Component:
                    if (!item.decoded) {
                        item.decoded = true;
                        var str = this.getDesc(item.id + ".xml");
                        var xml = egret.XML.parse(str);
                        item.componentData = xml;
                        this.loadComponentChildren(item);
                        this.translateComponent(item);
                    }
                    return item.componentData;
                default:
                    return UIPackage.getRes(this._resKey + "@" + item.id);
            }
        };
        UIPackage.prototype.loadComponentChildren = function (item) {
            var listNode = fairygui.ToolSet.findChildNode(item.componentData, "displayList");
            if (listNode != null) {
                var col = listNode.children;
                var dcnt = col.length;
                item.displayList = new Array(dcnt);
                var di;
                for (var i = 0; i < dcnt; i++) {
                    var cxml = col[i];
                    var tagName = cxml.name;
                    var src = cxml.attributes.src;
                    if (src) {
                        var pkgId = cxml.attributes.pkg;
                        var pkg;
                        if (pkgId && pkgId != item.owner.id)
                            pkg = UIPackage.getById(pkgId);
                        else
                            pkg = item.owner;
                        var pi = pkg != null ? pkg.getItemById(src) : null;
                        if (pi != null)
                            di = new fairygui.DisplayListItem(pi, null);
                        else
                            di = new fairygui.DisplayListItem(null, tagName);
                    }
                    else {
                        if (tagName == "text" && cxml.attributes.input == "true")
                            di = new fairygui.DisplayListItem(null, "inputtext");
                        else
                            di = new fairygui.DisplayListItem(null, tagName);
                    }
                    di.desc = cxml;
                    item.displayList[i] = di;
                }
            }
            else
                item.displayList = new Array();
        };
        UIPackage.prototype.getDesc = function (fn) {
            return this._resData[fn];
        };
        UIPackage.prototype.translateComponent = function (item) {
            if (UIPackage._stringsSource == null)
                return;
            var strings = UIPackage._stringsSource[this.id + item.id];
            if (strings == null)
                return;
            var length1 = item.displayList.length;
            var length2;
            var value;
            var cxml, dxml, exml;
            var ename;
            var elementId;
            var items;
            var i1, i2, j;
            var str;
            for (i1 = 0; i1 < length1; i1++) {
                cxml = item.displayList[i1].desc;
                ename = cxml.name;
                elementId = cxml.attributes.id;
                str = cxml.attributes.tooltips;
                if (str) {
                    value = strings[elementId + "-tips"];
                    if (value != undefined)
                        cxml.attributes.tooltips = value;
                }
                dxml = fairygui.ToolSet.findChildNode(cxml, "gearText");
                if (dxml) {
                    value = strings[elementId + "-texts"];
                    if (value != undefined)
                        dxml.attributes.values = value;
                    value = strings[elementId + "-texts_def"];
                    if (value != undefined)
                        dxml.attributes.default = value;
                }
                if (ename == "text" || ename == "richtext") {
                    value = strings[elementId];
                    if (value != undefined)
                        cxml.attributes.text = value;
                    value = strings[elementId + "-prompt"];
                    if (value != undefined)
                        cxml.attributes.prompt = value;
                }
                else if (ename == "list") {
                    items = cxml.children;
                    length2 = items.length;
                    j = 0;
                    for (i2 = 0; i2 < length2; i2++) {
                        exml = items[i2];
                        if (exml.name != "item")
                            continue;
                        value = strings[elementId + "-" + j];
                        if (value != undefined)
                            exml.attributes.title = value;
                        j++;
                    }
                }
                else if (ename == "component") {
                    dxml = fairygui.ToolSet.findChildNode(cxml, "Button");
                    if (dxml) {
                        value = strings[elementId];
                        if (value != undefined)
                            dxml.attributes.title = value;
                        value = strings[elementId + "-0"];
                        if (value != undefined)
                            dxml.attributes.selectedTitle = value;
                        continue;
                    }
                    dxml = fairygui.ToolSet.findChildNode(cxml, "Label");
                    if (dxml) {
                        value = strings[elementId];
                        if (value != undefined)
                            dxml.attributes.title = value;
                        value = strings[elementId + "-prompt"];
                        if (value != undefined)
                            dxml.attributes.prompt = value;
                        continue;
                    }
                    dxml = fairygui.ToolSet.findChildNode(cxml, "ComboBox");
                    if (dxml) {
                        value = strings[elementId];
                        if (value != undefined)
                            dxml.attributes.title = value;
                        items = dxml.children;
                        length2 = items.length;
                        j = 0;
                        for (i2 = 0; i2 < length2; i2++) {
                            exml = items[i2];
                            if (exml.name != "item")
                                continue;
                            value = strings[elementId + "-" + j];
                            if (value != undefined)
                                exml.attributes.title = value;
                            j++;
                        }
                        continue;
                    }
                }
            }
        };
        UIPackage.prototype.createSpriteTexture = function (sprite) {
            var atlasItem = this._itemsById[sprite.atlas];
            if (atlasItem != null) {
                var atlasTexture = this.getItemAsset(atlasItem);
                if (atlasTexture == null)
                    return null;
                else
                    return this.createSubTexture(atlasTexture, sprite.rect);
            }
            else
                return null;
        };
        // private createSubTexture(atlasTexture: egret.Texture, uvRect: cc.Rect): egret.Texture {
        //     var texture: egret.Texture = new egret.Texture();
        //     if (atlasTexture["_bitmapData"]) {
        //         texture["_bitmapData"] = atlasTexture["_bitmapData"];
        //         texture.$initData(atlasTexture["_bitmapX"] + uvRect.x, atlasTexture["_bitmapY"] + uvRect.y,
        //             uvRect.width, uvRect.height, 0, 0, uvRect.width, uvRect.height,
        //             atlasTexture["_sourceWidth"], atlasTexture["_sourceHeight"]);
        //     }
        //     else {
        //         texture.bitmapData = atlasTexture.bitmapData;
        //         texture.$initData(atlasTexture["$bitmapX"] + uvRect.x, atlasTexture["$bitmapY"] + uvRect.y,
        //             uvRect.width, uvRect.height, 0, 0, uvRect.width, uvRect.height,
        //             atlasTexture["$sourceWidth"], atlasTexture["$sourceHeight"]);
        //     }
        //     return texture;
        // }
        UIPackage.prototype.createSubTexture = function (atlasTexture, uvRect) {
            var sp = new cc.SpriteFrame();
            sp.setTexture(atlasTexture, uvRect);
            return sp;
        };
        UIPackage.prototype.loadMovieClip = function (item) {
            var xml = egret.XML.parse(this.getDesc(item.id + ".xml"));
            var str;
            var arr;
            str = xml.attributes.interval;
            if (str != null)
                item.interval = parseInt(str);
            str = xml.attributes.swing;
            if (str != null)
                item.swing = str == "true";
            str = xml.attributes.repeatDelay;
            if (str != null)
                item.repeatDelay = parseInt(str);
            var frameCount = parseInt(xml.attributes.frameCount);
            item.frames = new Array(frameCount);
            var frameNodes = xml.children[0].children;
            for (var i = 0; i < frameCount; i++) {
                var frame = new Frame();
                var frameNode = frameNodes[i];
                str = frameNode.attributes.rect;
                arr = str.split(UIPackage.sep0);
                frame.rect = new cc.Rect(parseInt(arr[0]), parseInt(arr[1]), parseInt(arr[2]), parseInt(arr[3]));
                str = frameNode.attributes.addDelay;
                if (str)
                    frame.addDelay = parseInt(str);
                item.frames[i] = frame;
                if (frame.rect.width == 0)
                    continue;
                str = frameNode.attributes.sprite;
                if (str)
                    str = item.id + "_" + str;
                else
                    str = item.id + "_" + i;
                var sprite = this._sprites[str];
                if (sprite != null) {
                    frame.texture = this.createSpriteTexture(sprite);
                }
            }
        };
        UIPackage.prototype.loadFont = function (item) {
            var font = new fairygui.BitmapFont();
            font.id = "ui://" + this.id + item.id;
            var str = this.getDesc(item.id + ".fnt");
            var lines = str.split(UIPackage.sep1);
            var lineCount = lines.length;
            var i = 0;
            var kv = {};
            var ttf = false;
            var size = 0;
            var xadvance = 0;
            var resizable = false;
            var atlasOffsetX = 0, atlasOffsetY = 0;
            var charImg;
            var mainTexture;
            var lineHeight = 0;
            for (i = 0; i < lineCount; i++) {
                str = lines[i];
                if (str.length == 0)
                    continue;
                str = fairygui.ToolSet.trim(str);
                var arr = str.split(UIPackage.sep2);
                for (var j = 1; j < arr.length; j++) {
                    var arr2 = arr[j].split(UIPackage.sep3);
                    kv[arr2[0]] = arr2[1];
                }
                str = arr[0];
                if (str == "char") {
                    var bg = new fairygui.BMGlyph();
                    bg.x = isNaN(kv.x) ? 0 : parseInt(kv.x);
                    bg.y = isNaN(kv.y) ? 0 : parseInt(kv.y);
                    bg.offsetX = isNaN(kv.xoffset) ? 0 : parseInt(kv.xoffset);
                    bg.offsetY = isNaN(kv.yoffset) ? 0 : parseInt(kv.yoffset);
                    bg.width = isNaN(kv.width) ? 0 : parseInt(kv.width);
                    bg.height = isNaN(kv.height) ? 0 : parseInt(kv.height);
                    bg.advance = isNaN(kv.xadvance) ? 0 : parseInt(kv.xadvance);
                    if (kv.chnl != undefined) {
                        bg.channel = parseInt(kv.chnl);
                        if (bg.channel == 15)
                            bg.channel = 4;
                        else if (bg.channel == 1)
                            bg.channel = 3;
                        else if (bg.channel == 2)
                            bg.channel = 2;
                        else
                            bg.channel = 1;
                    }
                    if (!ttf) {
                        if (kv.img) {
                            charImg = this._itemsById[kv.img];
                            if (charImg != null) {
                                charImg.load();
                                bg.width = charImg.width;
                                bg.height = charImg.height;
                                bg.texture = charImg.texture;
                            }
                        }
                    }
                    else if (mainTexture != null) {
                        bg.texture = this.createSubTexture(mainTexture, new cc.Rect(bg.x + atlasOffsetX, bg.y + atlasOffsetY, bg.width, bg.height));
                    }
                    if (ttf)
                        bg.lineHeight = lineHeight;
                    else {
                        if (bg.advance == 0) {
                            if (xadvance == 0)
                                bg.advance = bg.offsetX + bg.width;
                            else
                                bg.advance = xadvance;
                        }
                        bg.lineHeight = bg.offsetY < 0 ? bg.height : (bg.offsetY + bg.height);
                        if (size > 0 && bg.lineHeight < size)
                            bg.lineHeight = size;
                    }
                    font.glyphs[String.fromCharCode(kv.id)] = bg;
                }
                else if (str == "info") {
                    ttf = kv.face != null;
                    if (!isNaN(kv.size))
                        size = parseInt(kv.size);
                    resizable = kv.resizable == "true";
                    if (ttf) {
                        var sprite = this._sprites[item.id];
                        if (sprite != null) {
                            atlasOffsetX = sprite.rect.x;
                            atlasOffsetY = sprite.rect.y;
                            var atlasItem = this._itemsById[sprite.atlas];
                            if (atlasItem != null)
                                mainTexture = this.getItemAsset(atlasItem);
                        }
                    }
                }
                else if (str == "common") {
                    if (!isNaN(kv.lineHeight))
                        lineHeight = parseInt(kv.lineHeight);
                    if (size == 0)
                        size = lineHeight;
                    else if (lineHeight == 0)
                        lineHeight = size;
                    if (!isNaN(kv.xadvance))
                        xadvance = parseInt(kv.xadvance);
                }
            }
            if (size == 0 && bg)
                size = bg.height;
            font.ttf = ttf;
            font.size = size;
            font.resizable = resizable;
            item.bitmapFont = font;
        };
        //internal
        UIPackage._constructing = 0;
        UIPackage._packageInstById = {};
        UIPackage._packageInstByName = {};
        UIPackage._bitmapFonts = {};
        UIPackage._stringsSource = null;
        UIPackage._loadedRes = {};
        UIPackage.sep0 = ",";
        UIPackage.sep1 = "\n";
        UIPackage.sep2 = " ";
        UIPackage.sep3 = "=";
        return UIPackage;
    }());
    fairygui.UIPackage = UIPackage;
    var AtlasSprite = /** @class */ (function () {
        function AtlasSprite() {
            this.rect = new cc.Rect();
        }
        return AtlasSprite;
    }());
})(fairygui || (fairygui = {}));
var fairygui;
(function (fairygui) {
    var Window = /** @class */ (function (_super) {
        __extends(Window, _super);
        function Window() {
            var _this = _super.call(this) || this;
            _this._requestingCmd = 0;
            _this.focusable = true;
            _this._uiSources = new Array();
            _this.bringToFontOnClick = fairygui.UIConfig.bringWindowToFrontOnClick;
            _this.displayObject.addEventListener(egret.Event.ADDED_TO_STAGE, _this.__onShown, _this);
            _this.displayObject.addEventListener(egret.Event.REMOVED_FROM_STAGE, _this.__onHidden, _this);
            _this.displayObject.addEventListener(egret.TouchEvent.TOUCH_BEGIN, _this.__mouseDown, _this, true);
            return _this;
        }
        Window.prototype.addUISource = function (source) {
            this._uiSources.push(source);
        };
        Object.defineProperty(Window.prototype, "contentPane", {
            get: function () {
                return this._contentPane;
            },
            set: function (val) {
                if (this._contentPane != val) {
                    if (this._contentPane != null)
                        this.removeChild(this._contentPane);
                    this._contentPane = val;
                    if (this._contentPane != null) {
                        this.addChild(this._contentPane);
                        this.setSize(this._contentPane.width, this._contentPane.height);
                        this._contentPane.addRelation(this, fairygui.RelationType.Size);
                        this._frame = (this._contentPane.getChild("frame"));
                        if (this._frame != null) {
                            this.closeButton = this._frame.getChild("closeButton");
                            this.dragArea = this._frame.getChild("dragArea");
                            this.contentArea = this._frame.getChild("contentArea");
                        }
                    }
                }
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Window.prototype, "frame", {
            get: function () {
                return this._frame;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Window.prototype, "closeButton", {
            get: function () {
                return this._closeButton;
            },
            set: function (value) {
                if (this._closeButton != null)
                    this._closeButton.removeClickListener(this.closeEventHandler, this);
                this._closeButton = value;
                if (this._closeButton != null)
                    this._closeButton.addClickListener(this.closeEventHandler, this);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Window.prototype, "dragArea", {
            get: function () {
                return this._dragArea;
            },
            set: function (value) {
                if (this._dragArea != value) {
                    if (this._dragArea != null) {
                        this._dragArea.draggable = false;
                        this._dragArea.removeEventListener(DragEvent.DRAG_START, this.__dragStart, this);
                    }
                    this._dragArea = value;
                    if (this._dragArea != null) {
                        if ((this._dragArea instanceof fairygui.GGraph) && (this._dragArea).displayObject == null)
                            this._dragArea.asGraph.drawRect(0, 0, 0, 0, 0);
                        this._dragArea.draggable = true;
                        this._dragArea.addEventListener(DragEvent.DRAG_START, this.__dragStart, this);
                    }
                }
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Window.prototype, "contentArea", {
            get: function () {
                return this._contentArea;
            },
            set: function (value) {
                this._contentArea = value;
            },
            enumerable: true,
            configurable: true
        });
        Window.prototype.show = function () {
            fairygui.GRoot.inst.showWindow(this);
        };
        Window.prototype.showOn = function (root) {
            root.showWindow(this);
        };
        Window.prototype.hide = function () {
            if (this.isShowing)
                this.doHideAnimation();
        };
        Window.prototype.hideImmediately = function () {
            var r = (this.parent instanceof fairygui.GRoot) ? (this.parent) : null;
            if (!r)
                r = fairygui.GRoot.inst;
            r.hideWindowImmediately(this);
        };
        Window.prototype.centerOn = function (r, restraint) {
            if (restraint === void 0) { restraint = false; }
            this.setXY(Math.round((r.width - this.width) / 2), Math.round((r.height - this.height) / 2));
            if (restraint) {
                this.addRelation(r, fairygui.RelationType.Center_Center);
                this.addRelation(r, fairygui.RelationType.Middle_Middle);
            }
        };
        Window.prototype.toggleStatus = function () {
            if (this.isTop)
                this.hide();
            else
                this.show();
        };
        Object.defineProperty(Window.prototype, "isShowing", {
            get: function () {
                return this.parent != null;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Window.prototype, "isTop", {
            get: function () {
                return this.parent != null && this.parent.getChildIndex(this) == this.parent.numChildren - 1;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Window.prototype, "modal", {
            get: function () {
                return this._modal;
            },
            set: function (val) {
                this._modal = val;
            },
            enumerable: true,
            configurable: true
        });
        Window.prototype.bringToFront = function () {
            this.root.bringToFront(this);
        };
        Window.prototype.showModalWait = function (requestingCmd) {
            if (requestingCmd === void 0) { requestingCmd = 0; }
            if (requestingCmd != 0)
                this._requestingCmd = requestingCmd;
            if (fairygui.UIConfig.windowModalWaiting) {
                if (!this._modalWaitPane)
                    this._modalWaitPane = fairygui.UIPackage.createObjectFromURL(fairygui.UIConfig.windowModalWaiting);
                this.layoutModalWaitPane();
                this.addChild(this._modalWaitPane);
            }
        };
        Window.prototype.layoutModalWaitPane = function () {
            if (this._contentArea != null) {
                var pt = this._frame.localToGlobal();
                pt = this.globalToLocal(pt.x, pt.y, pt);
                this._modalWaitPane.setXY(pt.x + this._contentArea.x, pt.y + this._contentArea.y);
                this._modalWaitPane.setSize(this._contentArea.width, this._contentArea.height);
            }
            else
                this._modalWaitPane.setSize(this.width, this.height);
        };
        Window.prototype.closeModalWait = function (requestingCmd) {
            if (requestingCmd === void 0) { requestingCmd = 0; }
            if (requestingCmd != 0) {
                if (this._requestingCmd != requestingCmd)
                    return false;
            }
            this._requestingCmd = 0;
            if (this._modalWaitPane && this._modalWaitPane.parent != null)
                this.removeChild(this._modalWaitPane);
            return true;
        };
        Object.defineProperty(Window.prototype, "modalWaiting", {
            get: function () {
                return this._modalWaitPane && this._modalWaitPane.parent != null;
            },
            enumerable: true,
            configurable: true
        });
        Window.prototype.init = function () {
            if (this._inited || this._loading)
                return;
            if (this._uiSources.length > 0) {
                this._loading = false;
                var cnt = this._uiSources.length;
                for (var i = 0; i < cnt; i++) {
                    var lib = this._uiSources[i];
                    if (!lib.loaded) {
                        lib.load(this.__uiLoadComplete, this);
                        this._loading = true;
                    }
                }
                if (!this._loading)
                    this._init();
            }
            else
                this._init();
        };
        Window.prototype.onInit = function () {
        };
        Window.prototype.onShown = function () {
        };
        Window.prototype.onHide = function () {
        };
        Window.prototype.doShowAnimation = function () {
            this.onShown();
        };
        Window.prototype.doHideAnimation = function () {
            this.hideImmediately();
        };
        Window.prototype.__uiLoadComplete = function () {
            var cnt = this._uiSources.length;
            for (var i = 0; i < cnt; i++) {
                var lib = this._uiSources[i];
                if (!lib.loaded)
                    return;
            }
            this._loading = false;
            this._init();
        };
        Window.prototype._init = function () {
            this._inited = true;
            this.onInit();
            if (this.isShowing)
                this.doShowAnimation();
        };
        Window.prototype.dispose = function () {
            this.displayObject.removeEventListener(egret.Event.ADDED_TO_STAGE, this.__onShown, this);
            this.displayObject.removeEventListener(egret.Event.REMOVED_FROM_STAGE, this.__onHidden, this);
            if (this.parent != null)
                this.hideImmediately();
            _super.prototype.dispose.call(this);
        };
        Window.prototype.closeEventHandler = function (evt) {
            this.hide();
        };
        Window.prototype.__onShown = function (evt) {
            if (!this._inited)
                this.init();
            else
                this.doShowAnimation();
        };
        Window.prototype.__onHidden = function (evt) {
            this.closeModalWait();
            this.onHide();
        };
        Window.prototype.__mouseDown = function (evt) {
            if (this.isShowing && this.bringToFontOnClick)
                this.bringToFront();
        };
        Window.prototype.__dragStart = function (evt) {
            evt.preventDefault();
            this.startDrag(evt.touchPointID);
        };
        return Window;
    }(fairygui.GComponent));
    fairygui.Window = Window;
})(fairygui || (fairygui = {}));

if(window){
    window.fairygui = fairygui
}
